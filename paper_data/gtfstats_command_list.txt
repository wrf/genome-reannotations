gtfstats.py -i symbB.v1.2.augustus.reformat.gff3 -s symbB.v1.0.genome.fa -w
gtfstats.py -i Symbiodinium_kawagutii.mRNA.gff -s Symbiodinium_kawagutii.assembly.935Mb.fa.gz -w -c
gtfstats.py -i sphaeroforma_arctica_jp610_1_transcripts.clean.gtf -s sphaeroforma_arctica_jp610_1_supercontigs.fasta.gz -w -g
gtfstats.py -i T_thermophila_June2014.gff3 -s T_thermophila_June2014_assembly.fasta -w
gtfstats.py -i Volvox_carteri.FilteredModels2.gff.gz -s Volvox_carteri_v2.main_genome.scaffolds.fa.gz -w -g
gtfstats.py -i Emihu1_best_genes.gff.gz -s Emihu1_scaffolds.fasta -w
gtfstats.py -i capsaspora_owczarzaki_atcc_30864_2_transcripts.gtf -s capsaspora_owczarzaki_atcc_30864_2_supercontigs.fasta -w -g
gtfstats.py -i Monbr1_augustus_v1.gff -c -g -s Monbr1_scaffolds.fasta -w
gtfstats.py -i salpingoeca_rosetta_1_transcripts.gtf -s salpingoeca_rosetta_1_supercontigs.fasta -w -g
gtfstats.py -i twilhelma_stringtie_split.gtf -s twilhelma_scaffolds_v1.fasta -w
gtfstats.py -i Aqu2.1_Genes.gff3 -s ../jgi_annotation/Amphimedon_queenslandica.Aqu1.23.dna.genome.fa -w 
gtfstats.py -i sycon.cds.gff3 -s SCIL_WGA_130802.fasta -w
gtfstats.py -i gmap/pasa_lite.valid_alignments.gtf -s MlScaffold09.nt -w
gtfstats.py -i Trichoplax_scaffolds_JGI_AUGUSTUS_maxtrack2.gff3 -G -c -s Triad1_genomic_scaffolds.fasta -w
gtfstats.py -i PlacoH13_tophat2_stringtie.gtf -s PlacoH13_final_contigs.fasta -w
gtfstats.py -i nveGenes.vienna130208.nemVec1.gtf -s Nemve1.fasta -w -g
gtfstats.py -i aiptasia_genome.gff3 -s aiptasia_genome.scaffolds.fa -w
gtfstats.py -i GCF_000222465.1_Adig_1.1_genomic.gff.gz -s GCF_000222465.1_Adig_1.1_genomic.fna.gz -w
gtfstats.py -i hydra_stringtie.gtf -s Hm105_Dovetail_Assembly_1.0.fasta -w
gtfstats.py -i FilteredModelsv1.0.gff -s Capitella_spI_scaffolds.fasta -w -g
gtfstats.py -i Helobdella_robusta_FilteredModels3.gff.gz -s Helobdella_robusta.fasta.gz -w -g
gtfstats.py -i lingula_genome_v1.0_masked.gff -s lingula_genome_v1.0_masked.fa.gz -w -c
gtfstats.py -i pfu_transcripts_gmap.gff3 -s pfu_genome1.0.fasta -w -m -g
gtfstats.py -i Crassostrea_gigas.GCA_000297895.1.30.gff3 -s Crassostrea_gigas.GCA_000297895.1.30.dna.toplevel.fa.gz -w
gtfstats.py -i Lotgi1_GeneModels_FilteredModels1.gff -s Lotgi1_assembly_scaffolds.fasta -w -g
gtfstats.py -i obi-rnaseq-stringtie.gtf -s Obimaculoides_280.fa -w
gtfstats.py -i Caenorhabditis_elegans.WBcel235.31.gff3 -s GCF_000002985.6_WBcel235_genomic.fna.gz -w
gtfstats.py -i Strigamia_maritima.Smar1.31.gff3.gz -s Strigamia_maritima.Smar1.31.dna.toplevel.fa.gz -w
gtfstats.py -i Daphnia_pulex.GCA_000187875.1.30.gff3 -s Daphnia_pulex.GCA_000187875.1.30.dna.genome.fa -w
gtfstats.py -i Ixodes_scapularis.IscaW1.31.gff3.gz -s Ixodes_scapularis.IscaW1.31.dna.toplevel.fa.gz -w
gtfstats.py -i GCF_000002335.2_Tcas_3.0_genomic.gff -s GCF_000002335.2_Tcas_3.0_genomic.fna.gz -w
gtfstats.py -i GCA_000355655.1_DendPond_male_1.0_genomic.gff -s GCA_000355655.1_DendPond_male_1.0_genomic.fna.gz -w -p
gtfstats.py -i GCF_000002195.4_Amel_4.5_genomic.gff -s GCF_000002195.4_Amel_4.5_genomic.fna.gz -w
gtfstats.py -i Drosophila_melanogaster.BDGP6.30.nocomment.gff3 -s Drosophila_melanogaster.BDGP6.30.dna.genome.fa -w
gtfstats.py -i GCF_000517525.1_Limulus_polyphemus-2.1.2_genomic.gff.gz -s GCF_000517525.1_Limulus_polyphemus-2.1.2_genomic.fna.gz -w
gtfstats.py -i Strongylocentrotus_purpuratus.GCA_000002235.2.30.gff3 -s Strongylocentrotus_purpuratus.GCA_000002235.2.30.dna.genome.fa -w
gtfstats.py -i pfl.genes.gff3 -s 1426034866.unique_0.7_1e-10.fasta -w -c
gtfstats.py -i SkowalevskiiJGIv3.0.longestTrs.gff3 -s Saccoglossus_kowalevskii_v3.fasta -w -c -u
gtfstats.py -i bflo-rnaseq_stringtie.gtf -s Branchiostoma_floridae_v2.0.assembly.fasta -w
gtfstats.py -i Oikopleura_annot_v1.0.gff -s Odioica_reference_v3.0.fa -w -c -u
gtfstats.py -i Cintestinalis_FilteredModels1.gff -s ciona050324.unmasked.fasta -w -g
gtfstats.py -i botznik-chr-all.gff -s botznik-ctg.fa -w -G -c
gtfstats.py -i pmar_16lib_rna_stringtie.gtf -s GCA_000148955.1_Petromyzon_marinus-7.0_genomic.fna -w
gtfstats.py -i GCF_000165045.1_Callorhinchus_milii-6.1.3_genomic.gff -s GCF_000165045.1_Callorhinchus_milii-6.1.3_genomic.fna.gz -w
gtfstats.py -i GCF_000002035.5_GRCz10_genomic.gff.gz -s GCF_000002035.5_GRCz10_genomic.fna.gz -w
gtfstats.py -i ref_FUGU5_top_level.gff3 -s GCF_000180615.1_FUGU5_genomic.fna.gz -w
gtfstats.py -i GCF_000225785.1_LatCha1_genomic.gff -s GCF_000225785.1_LatCha1_genomic.fna.gz -w
gtfstats.py -i GCF_000004195.2_Xtropicalis_v7_genomic.gff.gz -s GCF_000004195.2_Xtropicalis_v7_genomic.fna.gz -w
gtfstats.py -i GCF_000090745.1_AnoCar2.0_genomic.gff.gz -s GCF_000090745.1_AnoCar2.0_genomic.fna.gz -w
gtfstats.py -i ref_ASM28112v3_top_level.gff3 -s allMis1.fa.gz -w
gtfstats.py -i GCF_000344595.1_CheMyd_1.0_genomic.gff.gz -s GCF_000344595.1_CheMyd_1.0_genomic.fna.gz -w
gtfstats.py -i GCF_000241765.3_Chrysemys_picta_bellii-3.0.3_genomic.gff.gz -s GCF_000241765.3_Chrysemys_picta_bellii-3.0.3_genomic.fna.gz -w
gtfstats.py -i GCF_000230535.1_PelSin_1.0_genomic.gff.gz -s GCF_000230535.1_PelSin_1.0_genomic.fna.gz -w -d
gtfstats.py -i GCF_000186305.1_Python_molurus_bivittatus-5.0.2_genomic.gff.gz -s GCF_000186305.1_Python_molurus_bivittatus-5.0.2_genomic.fna.gz -w
gtfstats.py -i GCF_000233375.1_ICSASG_v2_genomic.gff.gz -s GCF_000233375.1_ICSASG_v2_genomic.fna.gz -w
gtfstats.py -i GCF_000151625.1_ASM15162v1_genomic.gff -s GCF_000151625.1_ASM15162v1_genomic.fna.gz -w
gtfstats.py -i GCF_000698965.1_ASM69896v1_genomic.gff -s GCF_000698965.1_ASM69896v1_genomic.fna.gz -w
gtfstats.py -i ref_Gallus_gallus-5.0_top_level.gff3
gtfstats.py -i GCF_000151805.1_Taeniopygia_guttata-3.2.4_genomic.gff -s GCF_000151805.1_Taeniopygia_guttata-3.2.4_genomic.fna.gz -w
gtfstats.py -i GCF_000355885.1_BGI_duck_1.0_genomic.gff.gz -s GCF_000355885.1_BGI_duck_1.0_genomic.fna.gz -w
gtfstats.py -i GCF_000699145.1_ASM69914v1_genomic.gff -s GCF_000699145.1_ASM69914v1_genomic.fna.gz -w
gtfstats.py -i GCF_000238935.1_Melopsittacus_undulatus_6.3_genomic.gff.gz -s GCF_000238935.1_Melopsittacus_undulatus_6.3_genomic.fna.gz -w
gtfstats.py -i GCF_000002275.2_Ornithorhynchus_anatinus_5.0.1_genomic.gff.gz -s GCF_000002275.2_Ornithorhynchus_anatinus_5.0.1_genomic.fna.gz -w
gtfstats.py -i GCF_000002295.2_MonDom5_genomic.gff.gz -s GCF_000002295.2_MonDom5_genomic.fna.gz -w
gtfstats.py -i GCF_000002285.3_CanFam3.1_genomic.gff.gz  -s GCF_000002285.3_CanFam3.1_genomic.fna.gz -w
gtfstats.py -i ref_GRCm38.p3_top_level.gff3.gz -s mm_ref_GRCm38.p3_all_chr.fa.gz -w
gtfstats.py -i GCF_000001515.6_Pan_troglodytes-2.1.4_genomic.gff.gz -s GCF_000001515.6_Pan_troglodytes-2.1.4_genomic.fna.gz -w
gtfstats.py -i ref_GRCh38.p2_scaffolds.gff3 -s hg38.fa -w
