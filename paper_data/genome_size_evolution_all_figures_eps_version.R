# genome size and exon-intron-genic comparison

file1 = "~/git/genome-reannotations/metazoan_genome_size_comparison.tab"

dat1 = read.table(file1, header=TRUE, sep="\t", row.names=1)

total = rev(dat1[["Total"]])
Exon = rev(dat1[["Exon"]])
genic = rev(dat1[["Genic"]])
Intronic_gaps = rev(dat1[["Intron.gaps"]])
Gene_gaps = rev(dat1[["Intergenic.gaps"]])

Intron = genic - Exon - Intronic_gaps
Intergenic = total - genic - Gene_gaps

calcstats = cbind(Exon, Intron, Intronic_gaps, Intergenic, Gene_gaps)

labels = rev(gsub(" ","\n", rownames(dat1)))

statsandlabs = data.frame(calcstats, row.names=labels)

setEPS()

colvec = c("#00cc22", "#DA6077", "#997788", "#64b8f0", "#778899")
postscript(file="~/git/genome-reannotations/metazoan_genome_size_comparison.eps", width=11, height=36)
par(mar=c(4.2,10,3,2))
legendlabs = c("Exon","Intron","Intronic gaps","Intergenic","Intergenic gaps")
barplot(t(statsandlabs), horiz=TRUE, col=colvec, xlab="Size (Mb)", cex.axis=1.4, cex.lab=1.4, las=1, cex.names=1.25, axes=FALSE, xlim=c(0,2500), legend=TRUE, args.legend=list(cex=1.4, legend=legendlabs))
axislabs = c(0,100,250,500,750,1000,1500,2000,2500)
axis(1, at=axislabs, labels=axislabs, cex.axis=1.4)
axis(3, at=axislabs, labels=axislabs, cex.axis=1.4)
dev.off()

postscript(file="~/git/genome-reannotations/metazoan_genome_size_comparison_nonbilaterians.eps", width=10, height=8)
par(mar=c(4.2,9,3,2))
barplot(t(statsandlabs[50:63,]), horiz=TRUE, col=colvec, xlab="Size (Mb)", cex.axis=1.4, cex.lab=1.4, las=1, cex.names=1.1, axes=FALSE, xlim=c(0,900), legend=TRUE, args.legend=list(cex=1.4, legend=legendlabs) )
axislabs = c(0,100,200,300,400,500,600,750,900)
axis(1, at=axislabs, labels=axislabs, cex.axis=1.4)
axis(3, at=axislabs, labels=axislabs, cex.axis=1.4)
dev.off()

postscript(file="~/git/genome-reannotations/metazoan_genome_size_comparison_protos.eps", width=10, height=8)
par(mar=c(4.2,8,3,2))
barplot(t(statsandlabs[33:49,]), horiz=TRUE, col=colvec, xlab="Size (Mb)", cex.axis=1.4, cex.lab=1.4, las=1, cex.names=0.9, axes=FALSE, xlim=c(0,2500), legend=TRUE, args.legend=list(cex=1.4, legend=legendlabs) )
axislabs = c(0,100,250,500,750,1000,1500,2000,2500)
axis(1, at=axislabs, labels=axislabs, cex.axis=1.4)
axis(3, at=axislabs, labels=axislabs, cex.axis=1.4)
dev.off()

postscript(file="~/git/genome-reannotations/metazoan_genome_size_comparison_deuteros.eps", width=10, height=8)
par(mar=c(4.2,9,3,2))
barplot(t(statsandlabs[19:32,]), horiz=TRUE, col=colvec, xlab="Size (Mb)", cex.axis=1.4, cex.lab=1.4, las=1, cex.names=1.1, axes=FALSE, xlim=c(0,3000), legend=TRUE, args.legend=list(cex=1.4, legend=legendlabs) )
axislabs = c(0,250,500,1000,1500,2000,2500,3000)
axis(1, at=axislabs, labels=axislabs, cex.axis=1.4)
axis(3, at=axislabs, labels=axislabs, cex.axis=1.4)
dev.off()

bestgenomes = c(33,36,47,58,65,66,68)


# # # # # # #
# Intron Intergenic Plot for all species
# # # # # # #

postscript(file="~/git/genome-reannotations/metazoan_intron_intergenic_plot.eps", width=8, height=8)
xm = 2100
par(mar=c(4.5,4.5,2,2))
intronswg = dat1[["Genic"]] - dat1[["Exon"]]
intergenicwg = dat1[["Total"]] - dat1[["Genic"]]
# 16 circle for single cell euks
# 1 open circle for non-bilats
# 15 square for lophotrochozoans - capitella to lottia
# 15 square for ecdysozoans - limulus to beetles
# 17 triangle for deuterostomes - urchin to lamprey
# 22 open square black for verts - shark to birds
# 22 open square green for reptiles - turtles etc
# 6 open triangle for birds
# 5 diamond for mammals
cladelabs = c("Protists", "Non-bilaterians", "Lophotrochozoans", "Ecdysozoans", "Deuterostomes", "Vertebrates", "Non-avian reptiles", "Birds", "Mammals")
cladereps = c(9, 10, 7, 10, 7, 7, 6, 6, 6)
cladesymbols = c(19,21,15,15,17,22,22,6,5)
cladecols = c("#00cd60", "#0808ba", "#2cbabf", "#d8a722", "#c276ff", "#453600", "#599400", "#6da7d5", "#c8005d")
plot(intronswg, intergenicwg, xlim=c(0,xm), ylim=c(0,xm), xlab="Introns (Mb)", ylab="Intergenic (Mb)", frame.plot=FALSE, pch=rep(cladesymbols, cladereps), cex=1.55, col=rep(cladecols, cladereps), lwd=2.8, cex.axis=1.2, cex.lab=1.2)
rect(-10,-10,500,500,lty=3)
abline(0,1, lty=2) # 1:1 reference line
twoletternames = sapply(strsplit(row.names(dat1),' '),function(w){paste(substr(w,1,1),collapse='')})
threeletternames = gsub("([\\w])\\w+\\s([\\w][\\w]\\d?).*","\\1\\2",row.names(dat1),perl=TRUE)
smallcut = 480
bigonlynames = threeletternames[dat1[["Total"]]>smallcut]
bigonlyinge = intergenicwg[dat1[["Total"]]>smallcut]
bigonlyintr = intronswg[dat1[["Total"]]>smallcut]
text(bigonlyintr+xm/100, bigonlyinge, bigonlynames, pos=4)
legend(1280,800,legend=cladelabs,pch=cladesymbols, col=cladecols, pt.lwd=2, cex=1.2)
dev.off()


# # # # # # #
# Log version Intron Intergenic Plot
# # # # # # #
postscript(file="~/git/genome-reannotations/fig9_metazoan_intron_intergenic_log_plot.eps", width=8, height=8)
par(mar=c(4.5,4.5,2,2))
plot(log10(intronswg),log10(intergenicwg), xlim=c(0,4), ylim=c(0,4), xlab="Introns (log Mb)", ylab="Intergenic (log Mb)", frame.plot=FALSE, pch=rep(cladesymbols, cladereps), cex=1.55, col=rep(cladecols, cladereps), lwd=2.8, cex.axis=1.2, cex.lab=1.2)
logiilm = lm(log10(intergenicwg) ~ log10(intronswg))
abline(0,1, lty=3) # 1:1 reference line
abline(logiilm, lty=4, lwd=2)
text(2.5,3.75, bquote( "R"^2 == .(rs), list(rs=round(c(summary(logiilm)$r.squared),digits=4))), col="black", cex=1.2)

logiilm.best = lm(log10(intergenicwg[bestgenomes]) ~ log10(intronswg[bestgenomes]))
abline(logiilm.best, lty=2, lwd=2, col="#a02323")
text(3.75,2.5, bquote( "R"^2 == .(rs), list(rs=round(c(summary(logiilm.best)$r.squared),digits=4))), col="#a02323", cex=1.2)
ismodel = rep(0,length(intronswg))
ismodel[bestgenomes] <- 1
logiilm.multi = lm(log10(intergenicwg) ~ log10(intronswg) + ismodel)
xnamespacing = c(0.05,0.05,0.05,0.05,0.05,0.1,0.05)
ynamespacing = c(0,0,0,-0.1,-0.1,-0.05,0)
weirdprotistindex = c(1,2,8,15,17,32)
text(log10(intronswg[weirdprotistindex])+0.04, log10(intergenicwg[weirdprotistindex]), threeletternames[weirdprotistindex], pos=4)
text(log10(intronswg[bestgenomes])+xnamespacing, log10(intergenicwg[bestgenomes])+ynamespacing, threeletternames[bestgenomes], pos=4)
points(log10(intronswg[bestgenomes]), log10(intergenicwg[bestgenomes]), pch=8, cex=1.2, col="yellow")

legend(2.7,1.8,legend=cladelabs,pch=cladesymbols, col=cladecols, pt.lwd=2, cex=1.2)

par(fig = c(grconvertX(c(0,1.9), from="user", to="ndc"), grconvertY(c(2.5,4), from="user", to="ndc")), mar = c(2,3,0,1), new = TRUE)
modelintintratio = abs(1-(log10(intronswg)/log10(intergenicwg) ) )
modelnonmodeldata = list(modelintintratio[-bestgenomes],modelintintratio[bestgenomes])
boxplot(modelnonmodeldata,ylim=c(0,0.6), frame.plot=TRUE, col=c("#999999", "#e05353"), names=c("Non-model","Model") )
title(ylab="Difference from 1:1 ratio",line=2)
points(rep(1,length(modelintintratio[-bestgenomes])),modelintintratio[-bestgenomes], col="#00000044")
points(rep(2,length(bestgenomes)),modelintintratio[bestgenomes], col="#00000044")
modelnonmodeltest = t.test(modelintintratio[-bestgenomes],modelintintratio[bestgenomes], mu=0)
text(2,0.48, "***", cex=1.2)
nmtpv = paste("p==",sub("e","%*%10^",format(modelnonmodeltest$p.value,scientific=TRUE, digits=2) ),sep="")
text(2,0.4, parse(text=nmtpv), cex=1.2 )
dev.off()

# # # # # # #
# Intron Intergenic Ratio Boxplots
# # # # # # #

inverts = 10:40 # 10:40
vertebrates = 44:68 # 41:68
deuterostomes = 37:68 # includes hemichordates and echinos
protostomes = 20:36
nonbilats = 10:19
bilats = 20:68
vertebratetest = t.test(modelintintratio[inverts],modelintintratio[vertebrates], mu=0)
deuterotest =  t.test(modelintintratio[deuterostomes],modelintintratio[protostomes], mu=0)
vertnonbilattext = t.test(modelintintratio[nonbilats],modelintintratio[vertebrates])
bilateriantest = t.test(modelintintratio[bilats],modelintintratio[nonbilats], mu=0)

postscript(file="~/git/genome-reannotations/fig10_metazoan_intron_intergenic_boxplot_tests.eps", width=8, height=8)
par(mfrow=c(2,2))

par(mar=c(3,3,1,1))
boxplot(list(modelintintratio[vertebrates],modelintintratio[inverts]),ylim=c(0,0.4), frame.plot=TRUE, col=c("#c8005d", "#97e861"), names=c("Vertebrates","Invertebrates"), cex.axis=1.2, cex.lab=1.1 )
points(rep(1,length(vertebrates)),modelintintratio[vertebrates], col="#00000044")
points(rep(2,length(inverts)),modelintintratio[inverts], col="#00000044")
text(1,0.38, "**", cex=1.2)
verttpv = paste("p==",sub("e","%*%10^",format(vertebratetest$p.value,scientific=TRUE, digits=3) ),sep="")
text(1,0.35, parse(text=verttpv), cex=1.2 )

par(mar=c(3,4,1,1))
boxplot(list(modelintintratio[deuterostomes],modelintintratio[protostomes]),ylim=c(0,0.4), frame.plot=TRUE, col=c("#c276ff", "#d8a722"), names=c("Deuterostomes","Protostomes"), cex.axis=1.2, cex.lab=1.1  )
points(rep(1,length(deuterostomes)),modelintintratio[deuterostomes], col="#00000044")
points(rep(2,length(protostomes)),modelintintratio[protostomes], col="#00000044")
text(1,0.38, "*", cex=1.2)
deuttpv = paste("p==",sub("e","%*%10^",format(deuterotest$p.value,scientific=TRUE, digits=3) ),sep="")
text(1,0.35, parse(text=deuttpv), cex=1.2 )
title(ylab="Difference from 1:1 ratio",line=3, cex.lab=1.2)

par(mar=c(3,3,1,1))
boxplot(list(modelintintratio[vertebrates],modelintintratio[nonbilats]),ylim=c(0,0.4), frame.plot=TRUE, col=c("#c8005d", "#4375f6"), names=c("Vertebrates","Non-bilaterians"), cex.axis=1.2, cex.lab=1.1  )
points(rep(1,length(vertebrates)),modelintintratio[vertebrates], col="#00000044")
points(rep(2,length(nonbilats)),modelintintratio[nonbilats], col="#00000044")
vnbtpv = paste("p==",sub("e","%*%10^",format(vertnonbilattext$p.value,scientific=TRUE, digits=3) ),sep="")
text(1,0.35, parse(text=vnbtpv), cex=1.2 )

par(mar=c(3,4,1,1))
boxplot(list(modelintintratio[bilats],modelintintratio[nonbilats]),ylim=c(0,0.4), frame.plot=TRUE, col=c("#2cbabf", "#4375f6"), names=c("Bilaterians","Non-bilaterians"), cex.axis=1.2, cex.lab=1.1  )
points(rep(1,length(bilats)),modelintintratio[bilats], col="#00000044")
points(rep(2,length(nonbilats)),modelintintratio[nonbilats], col="#00000044")
bitpv = paste("p==",sub("e","%*%10^",format(bilateriantest$p.value,scientific=TRUE, digits=3) ),sep="")
text(1,0.35, parse(text=bitpv), cex=1.2 )
title(ylab="Difference from 1:1 ratio",line=3, cex.lab=1.2)
dev.off()


# # # # # # #
# Reannotation Intron Intergenic Plot
# # # # # # #

file2 = "~/git/genome-reannotations/metazoan_reannotation_comparison.tab"
dat2 = read.table(file2, header=TRUE, sep="\t", row.names=1)
introns2 = dat2[["Genic"]] - dat2[["Exon"]]
intergenic2 = dat2[["Total"]] - dat2[["Genic"]]
postscript(file="~/git/genome-reannotations/fig5_metazoan_intron_intergenic_reannotation.eps", width=8, height=8)
xm=2000
par(mar=c(4.5,4.5,2,2))
cladesymbols2 = c(19,21,21,21,21,21,17,15)
plot(log10(introns2),log10(intergenic2), xlim=c(1,3.5), ylim=c(1,3.5), xlab="Introns (log Mb)", ylab="Intergenic (log Mb)", frame.plot=FALSE, pch=rep(cladesymbols2, rep(c(2),length(cladesymbols2))), cex=1.55, col=c("#c8005d", "#00cd60"), lwd=2.8, cex.axis=1.2, cex.lab=1.2)
abline(0,1, lty=3) # 1:1 reference line
text(log10(introns2)+0.05, log10(intergenic2) , row.names(dat2), pos=4)

reds = seq(1,length(introns2),2)
greens = seq(2,length(introns2),2)
redlm = lm(log10(intergenic2[reds]) ~ log10(introns2[reds]) )
greenlm = lm(log10(intergenic2[greens]) ~ log10(introns2[greens]) )

rgonlylm = lm(log10(intergenic2) ~ log10(introns2))
isgreen = rep(c(0,1),length(introns2)/2)
greeninteraction = isgreen*introns2
rglm = lm(log10(intergenic2) ~ log10(introns2) + isgreen  )
isgreenlm = lm(log10(intergenic2) ~ log10(introns2) + isgreen + greeninteraction )
par(fig = c(grconvertX(c(1,2.1), from="user", to="ndc"), grconvertY(c(2.5,3.5), from="user", to="ndc")), mar = c(2,3,0,1), new = TRUE)
intintratio = abs(1-log10(introns2)/log10(intergenic2))   # DIFFERENCE TO 1:1 RATIO
#intintratio = abs( (log10(intergenic2) - log10(introns2))/sqrt(2) )  # DISTANCE FROM LINE
redgreendata = data.frame(intintratio[reds],intintratio[greens])
boxplot(redgreendata,ylim=c(0,0.6), frame.plot=TRUE, col=c("#c8005d", "#00cd60"), names=c("Original","Redo") )
points(rep(1,length(reds)),intintratio[reds])
points(rep(2,length(greens)),intintratio[greens])
title(ylab="Difference from 1:1 ratio",line=2)
redgreenttest = t.test(intintratio[reds],intintratio[greens],paired=TRUE, mu=0)
text(2,0.46, "*", cex=1.2)
text(2,0.4, paste("p=",round(redgreenttest$p.value,4),sep=""), cex=1.2)
dev.off()

# # # # # # #
# Reannotation bar plot
# # # # # # #

postscript(file="~/git/genome-reannotations/fig4_metazoan_reannotation_barplot.eps", width=10, height=8)
par(mar=c(4.2,9,3,2))
dat2 = read.table(file2, header=TRUE, sep="\t", row.names=1)
dat2 = dat2[3:14,] # ignore last two, which should be octopus
reannot.introns = rev(dat2[["Genic"]] - dat2[["Exon"]] - dat2[["Intron.gaps"]])
reannot.intergenic = rev(dat2[["Total"]] - dat2[["Genic"]] - dat2[["Intergenic.gaps"]])
reannot.calcstats = cbind(rev(dat2[["Exon"]]), reannot.introns, rev(dat2[["Intron.gaps"]]), reannot.intergenic, rev(dat2[["Intergenic.gaps"]]) )
labels = rev(gsub(" ","\n", rownames(dat1)))

reannot.statslabs = data.frame(reannot.calcstats, row.names=rev(rownames(dat2)) )
barplot(t(reannot.statslabs), horiz=TRUE, col=colvec, xlab="Size (Mb)", cex.axis=1.4, cex.lab=1.4, las=1, cex.names=1.1, axes=FALSE, xlim=c(0,900), legend=TRUE, args.legend=list(cex=1.4, legend=legendlabs) )
axislabs = c(0,100,200,300,400,500,600,750,900)
axis(1, at=axislabs, labels=axislabs, cex.axis=1.4)
axis(3, at=axislabs, labels=axislabs, cex.axis=1.4)
dev.off()


# # # # # # #
# Total size against all features
# # # # # # #

postscript(file="~/git/genome-reannotations/fig6_metazoan_total_size_vs_feature_plot.eps", width=8, height=7)
par(mar=c(4.4,4.5,2,2))
plot(dat1[["Total"]],dat1[["Exon"]],xlim=c(0,3550),ylim=c(0,2500),pch=rep(cladesymbols, cladereps), col="#10cb22", xlab="Total size (Mb)", ylab="Feature amount (Mb)", frame.plot=FALSE, cex=1.4, cex.axis=1.2, cex.lab=1.4, lwd=2.4)
ecor = cor(dat1[["Total"]],dat1[["Exon"]])
ecor2 = ecor^2
et = lm(dat1[["Exon"]] ~ dat1[["Total"]])
abline(et, lty=3, lwd=2, col="#146d20")
icor = cor(dat1[["Total"]],intronswg)
icor2 = icor^2
it = lm(intronswg ~ dat1[["Total"]])
abline(it, lty=2, col="#ac4556")
gcor = cor(dat1[["Total"]],intergenicwg)
gcor2 = gcor^2
gt = lm(intergenicwg ~ dat1[["Total"]])
abline(gt, lty=1, col="#1d5d85")
points(dat1[["Total"]], intronswg, pch=rep(cladesymbols, cladereps), col="#ee6077", cex=1.4, lwd=2.4)
points(dat1[["Total"]], intergenicwg, pch=rep(cladesymbols, cladereps), col="#66d0ff", cex=1.4, lwd=2.4)
legend(1320,2500,legend=c("Exon", "Intron", "Intergenic"), pch=16, col=c("#10ab22","#ee6077","#88d0ff"), cex=1.4, pt.cex=1.6 )
legend(20,2500,legend=c("Protists", "Non-bilaterians", "Protostomes", "Deuterostomes", "Vertebrates", "Birds", "Mammals"), pch=c(19,21,15,17,22,6,5), col=c("#000000"), cex=1.4, pt.cex=1.4, pt.lwd=2.4 )
text(3250,500, bquote( "R"^2 == .(rs), list(rs=round(c(summary(et)$r.squared),digits=4))), col="#146d20", cex=1.2)
text(3000,300, bquote( y == .(coef(et)[1]) + .(coef(et)[2]) * x ), col="#146d20", cex=1.2 )
text(3250,1000, bquote( "R"^2 == .(rs), list(rs=round(c(summary(it)$r.squared),digits=4))), col="#ac4556", cex=1.2)
text(3000,800, bquote( y == .(coef(it)[1]) + .(coef(it)[2]) * x ), col="#ac4556", cex=1.2 )
text(3250,2500, bquote( "R"^2 == .(rs), list(rs=round(c(summary(gt)$r.squared),digits=4))), col="#1d5d85", cex=1.2)
text(3000,2300, bquote( y == .(coef(gt)[1]) + .(coef(gt)[2]) * x ), col="#1d5d85", cex=1.2 )
dev.off()


# # # # # # #
# Total size against average exon length and intron length
# # # # # # #

postscript(file="~/git/genome-reannotations/fig7_metazoan_total_size_vs_intron_length.eps", width=8, height=7)
par(mar=c(4.4,4.5,2,2))
plot(dat1[["Total"]],dat1[["Avg.exon.length"]],xlim=c(0,3550),ylim=c(0,10000),pch=rep(cladesymbols, cladereps), col="#10cb22", xlab="Total size (Mb)", ylab="Average length (bp)", frame.plot=FALSE, cex=1.4, cex.axis=1.2, cex.lab=1.4, lwd=2.4)
points(dat1[["Total"]], dat1[["Avg.intron.length"]], pch=rep(cladesymbols, cladereps), col="#ee6077", cex=1.4, lwd=2.4)
avgecor = cor(dat1[["Total"]],dat1[["Avg.exon.length"]])
avgecor2 = avgecor^2
avget = lm(dat1[["Avg.exon.length"]] ~ dat1[["Total"]])
abline(avget, lty=3, lwd=2, col="#146d20")
text(3000,1800, bquote( "R"^2 == .(rs), list(rs=round(c(summary(avget)$r.squared),digits=4))), col="#146d20", cex=1.2)
text(2800,1200, bquote( y == .(coef(avget)[1]) + .(coef(avget)[2]) * x ), col="#146d20", cex=1.2 )
avgicor = cor(dat1[["Total"]],dat1[["Avg.intron.length"]])
avgicor2 = avgicor^2
avgit = lm(dat1[["Avg.intron.length"]] ~ dat1[["Total"]])
abline(avgit, lty=2, col="#ac4556")
text(3000,9300, bquote( "R"^2 == .(rs), list(rs=round(c(summary(avgit)$r.squared),digits=4))), col="#ac4556", cex=1.2)
text(2800,8700, bquote( y == .(coef(avgit)[1]) + .(coef(avgit)[2]) * x ), col="#ac4556", cex=1.2 )
legend(1400,9500,legend=c("Exon", "Intron"), pch=16, col=c("#10ab22","#ee6077"), cex=1.4, pt.cex=1.6 )
legend(100,9500,legend=c("Protists", "Non-bilaterians", "Protostomes", "Deuterostomes", "Vertebrates", "Birds", "Mammals"), pch=c(19,21,15,17,22,6,5), col=c("#000000"), cex=1.4, pt.cex=1.4, pt.lwd=2.4 )
dev.off()


# # # # # # #
# Genic fraction
# # # # # # #

postscript(file="~/git/genome-reannotations/fig11_metazoan_total_size_vs_genic_plot.eps", width=8, height=7)
par(mar=c(4.4,4.5,2,1.5))
# gaps are already included since genic is measured by gff intervals
genicfraction = dat1[["Genic"]]/dat1[["Total"]]
plot(dat1[["Total"]],genicfraction, type='p', xlim=c(0,3700), ylim=c(0,1), frame.plot=FALSE, axes=FALSE, xlab="Total genome size (Mb)", ylab="Genic fraction", pch=rep(cladesymbols, cladereps), cex=1.55, col=rep(cladecols, cladereps), lwd=2.8, cex.axis=1.2, cex.lab=1.2)
axis(1, at=seq(0,3500,500), labels=seq(0,3500,500) )
axis(2, at=seq(0,1,0.1), labels=seq(0,1,0.1) )
abline(0.5,0, lty=2)
points(dat1[["Total"]][bestgenomes],genicfraction[bestgenomes], pch=8, cex=1.2, col="yellow")
text(dat1[["Total"]][bestgenomes]+30, genicfraction[bestgenomes], threeletternames[bestgenomes], pos=4)
legend(2350,1,legend=cladelabs,pch=cladesymbols, col=cladecols, pt.lwd=2, cex=1.2)
sortedgsizes = sort(dat1[["Total"]], index.return=TRUE)
genomevals = sortedgsizes$x
ghypermodel = lm(genicfraction[sortedgsizes$ix] ~ I(1/sortedgsizes$x))
gfraction.model = predict(ghypermodel,list(genomevals))
lines(genomevals, gfraction.model, col="#b618d2", lty=1, lwd=1.5)
text(700,0.85, bquote( "hyp R"^2 == .(rs), list(rs=round(c(summary(ghypermodel)$r.squared),digits=4))), col="#b618d2", cex=1.2)
text(750,0.78, pos=4, bquote( hat(y)[i] == .(coef(ghypermodel)[1]) + .(coef(ghypermodel)[2]) / x[i] ), col="#b618d2", cex=1.2 )
gexpmodel = lm(log(genicfraction[sortedgsizes$ix]) ~ genomevals)
gexpfract.model = exp(predict(gexpmodel,list(genomevals)))
lines(genomevals, gexpfract.model, col="#1234bb", lty=1, lwd=1.5)
text(3000,0.3, bquote( "exp R"^2 == .(rs), list(rs=round(c(summary(gexpmodel)$r.squared),digits=4))), col="#1234bb", cex=1.2)
glinmodel = lm(genicfraction[sortedgsizes$ix] ~ genomevals)
glinfract.model = predict(glinmodel,list(genomevals))
lines(genomevals, glinfract.model, col="#c88423", lty=1, lwd=1.5)
text(3000,0.24, bquote( "linear R"^2 == .(rs), list(rs=round(c(summary(glinmodel)$r.squared),digits=4))), col="#c88423", cex=1.2)
glinmodel500 = lm(genicfraction[sortedgsizes$ix][genomevals>500] ~ genomevals[genomevals>500])
glinfract500.model = predict(glinmodel500,list(genomevals[genomevals>500]))
lines(genomevals[genomevals>500], glinfract500.model, col="#84c823", lty=1, lwd=1.5)
text(3000,0.18, bquote( ">500 R"^2 == .(rs), list(rs=round(c(summary(glinmodel500)$r.squared),digits=4))), col="#84c823", cex=1.2)
glinmodel499 = lm(genicfraction[sortedgsizes$ix][genomevals<500] ~ genomevals[genomevals<500])
glinfract499.model = predict(glinmodel499,list(genomevals[genomevals<500]))
besttotals = dat1[["Total"]][bestgenomes]
modelhyperlm = lm(genicfraction[bestgenomes] ~ I(1/besttotals) )
mhlm.predict = predict(modelhyperlm, data.frame(besttotals=genomevals) )
lines(genomevals, mhlm.predict, col="#a02323", lty=1, lwd=1.5)
text(700,0.99, bquote( "model hyp R"^2 == .(rs), list(rs=round(c(summary(modelhyperlm)$r.squared),digits=4))), col="#a02323", cex=1.2)
text(750,0.92, pos=4, bquote( hat(y)[i] == .(coef(modelhyperlm)[1]) + .(coef(modelhyperlm)[2]) / x[i] ), col="#a02323", cex=1.2 )
dev.off()


# # # # # # #
# Exonic fraction
# # # # # # #

postscript(file="~/git/genome-reannotations/fig8_metazoan_total_size_vs_exonic_plot.eps", width=8, height=7)
par(mar=c(4.4,4.5,2,1.5))
exonfraction = dat1[["Exon"]]/dat1[["Total"]]
plot(dat1[["Total"]],exonfraction, type='p', xlim=c(0,3800), ylim=c(0,1), frame.plot=FALSE, axes=FALSE, xlab="Total genome size (Mb)", ylab="Exonic fraction", pch=rep(cladesymbols, cladereps), cex=1.55, col=rep(cladecols, cladereps), lwd=2.8, cex.axis=1.2, cex.lab=1.2)
axis(1, at=seq(0,3500,500), labels=seq(0,3500,500) )
axis(2, at=seq(0,1,0.1), labels=seq(0,1,0.1) )
points(dat1[["Total"]][bestgenomes],exonfraction[bestgenomes], pch=8, cex=1.2, col="yellow")
xexonoffsets = c(30,30,0,-100,0,-10,-10)
yexonoffsets = c(-0.01,0,0.03,0.03,0.05,0.04,0.04)
text(dat1[["Total"]][bestgenomes]+xexonoffsets, exonfraction[bestgenomes]+yexonoffsets, threeletternames[bestgenomes], pos=4)
legend(2000,1,legend=cladelabs,pch=cladesymbols, col=cladecols, pt.lwd=2, cex=1.2)
sortedgsizes = sort(dat1[["Total"]], index.return=TRUE)
genomevals = sortedgsizes$x
exhypermodel = lm(exonfraction[sortedgsizes$ix] ~ I(1/sortedgsizes$x))
efraction.model = predict(exhypermodel,list(genomevals))
lines(genomevals, efraction.model, col="#a02323", lty=1, lwd=1.5)
text(1000,0.45, bquote( R^2 == .(rs), list(rs=round(c(summary(exhypermodel)$r.squared),digits=4))), col="#a02323", cex=1.2)
text(2500,0.45, "Modeled as:", cex=1.2)
# with residuals gives the original points
text(2500,0.35, expression( hat(y)[i] == hat(beta)[0] + hat(beta)[1] / x[i] ) , cex=1.2 )
text(2500,0.25, bquote( hat(y)[i] == .(coef(exhypermodel)[1]) + .(coef(exhypermodel)[2]) / x[i] ), col="#a02323", cex=1.2 )
dev.off()

