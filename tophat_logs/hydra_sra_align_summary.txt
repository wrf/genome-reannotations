Left reads:
          Input     : 128241885
           Mapped   :  99315082 (77.4% of input)
            of these:   4937854 ( 5.0%) have multiple alignments (86530 have >20)
Right reads:
          Input     : 128241885
           Mapped   : 105262096 (82.1% of input)
            of these:   5251789 ( 5.0%) have multiple alignments (86613 have >20)
79.8% overall read mapping rate.

Aligned pairs:  90772499
     of these:   4422257 ( 4.9%) have multiple alignments
                  629894 ( 0.7%) are discordant alignments
70.3% concordant pair alignment rate.
