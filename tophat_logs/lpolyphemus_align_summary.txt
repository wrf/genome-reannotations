Left reads:
          Input     :  59840743
           Mapped   :  49644816 (83.0% of input)
            of these:   3512497 ( 7.1%) have multiple alignments (50379 have >20)
Right reads:
          Input     :  59840743
           Mapped   :  51782256 (86.5% of input)
            of these:   4353618 ( 8.4%) have multiple alignments (50409 have >20)
84.7% overall read mapping rate.

Aligned pairs:  44361975
     of these:   3261350 ( 7.4%) have multiple alignments
                 1720372 ( 3.9%) are discordant alignments
71.3% concordant pair alignment rate.
