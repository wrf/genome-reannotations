#!/usr/bin/env python

'''
./rename_pbac_scaffolds.py AVPN01.1.fsa_nt.gz > AVPN01.1.scaffolds.fa

   download scaffolds from:
   https://www.ncbi.nlm.nih.gov/Traces/wgs/AVPN01

'''

import sys
import gzip

# >gi|636657779|gb|AVPN01000001.1| Pleurobrachia bachei scaffold3.1, whole genome shotgun sequence

if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	for line in gzip.open(sys.argv[1],'r'):
		if line[0]==">":
			accession, description = line.split(" ", 1)
			seqid = accession.split("|")[3]
			scaffold_id = description.split(" ")[2].replace(",","")
			newid = ">{} Pbachei_{}\n".format(seqid,scaffold_id)
			sys.stdout.write(newid)
		else:
			sys.stdout.write(line)
