# Pleurobrachia bachei genome tracks #
Using *Pleurobrachia bachei* [genome (probably v1)](https://www.ncbi.nlm.nih.gov/Traces/wgs/AVPN01) from Friday Harbor, by [Moroz 2014](https://doi.org/10.1038/nature13400).

![pbac_rnaseq_mapping_screenshot.png](https://bitbucket.org/wrf/genome-reannotations/raw/791b1ca1018ca9a1aa4f18f7af7bf2b0f99d7f28/jbrowse-tracks/pleurobrachia/pbac_rnaseq_mapping_screenshot.png)

### rename scaffolds ###
The GenBank default name is difficult to use and parse. Rename them, and then generate the fasta index.

`./rename_pbac_scaffolds.py AVPN01.1.fsa_nt.gz > AVPN01.1.scaffolds.fa`

`~/samtools-1.9/samtools faidx AVPN01.1.scaffolds.fa`

### repeat track ###

`~/git/genomeGTFtools/repeat2gtf.py AVPN01.1.scaffolds.fa -l > AVPN01.1.gaps.gff`

```
Counted 17150 repeats of 19278202 total bases, average 1124.09
Longest repeat was 5294 bases on AVPN01003012.1
```

### generating annotation file from transcripts ###
No annotation files (like a GFF) appeared to be [provided with the genome](https://neurobase.rc.ufl.edu/pleurobrachia/download), and there was no systematic naming between the scaffolds, the transcripts, and protein sets (as was done for Mnemiopsis). For instance, there are 18950 transcripts, but 19524 proteins.

For the proteins, the headers are:

```
grep ">" pbachei_02_filtered_protein_Models.fa | head
>sb|3460263|
>sb|3460264|
>sb|3460265|
>sb|3460266|
>sb|3460267|
>sb|3460268|
>sb|3460269|
>sb|3460270|
>sb|3460271|
>sb|3460272|
>sb|3460273|
```

The transcripts are slightly more sensibly named, as it includes a scaffold and a gene number:

```
grep ">" pbachei_03_filtered_gene_models_transcripts.fa | head
>scaffold3_1_size219608_gene_2Barcelona
>scaffold3_1_size219608_gene_4Barcelona
>scaffold3_1_size219608_gene_7Barcelona
>scaffold3_1_size219608_gene_8Barcelona
>scaffold3_1_size219608_gene_9Barcelona
>scaffold3_1_size219608_gene_12Barcelona
>scaffold3_1_size219608_gene_13Barcelona
>scaffold3_1_size219608_gene_14Barcelona
>scaffold3_1_size219608_gene_15Barcelona
>scaffold3_1_size219608_gene_16Barcelona
```

A rough annotation file was generated from the transcripts using [minimap2](https://github.com/lh3/minimap2), and [samtools](https://github.com/samtools/samtools):

`~/minimap2-2.17_x64-linux/minimap2 -a -x splice -t 4 --secondary=no AVPN01.1.scaffolds.fa pbachei_03_filtered_gene_models_transcripts.fa | ~/samtools-1.9/samtools sort - -o pbachei_03_filtered_gene_models_transcripts.bam`

Next, the `.bam` file was converted to a GTF for the transcripts with [pinfish](https://github.com/nanoporetech/pinfish):

`~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M pbachei_03_filtered_gene_models_transcripts.bam > pbachei_03_filtered_gene_models_transcripts.pinfish.gtf`

Of these, 18924 transcripts map for a total of 19087 loci, meaning some of them map twice or are split across scaffolds. Then the GTF was converted to a proper GFF with `ID=` attributes with [stringtie_gtf_to_gff3.py](https://github.com/wrf/genomeGTFtools/blob/master/misc/stringtie_gtf_to_gff3.py):

`~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py pbachei_03_filtered_gene_models_transcripts.pinfish.gtf > pbachei_03_filtered_gene_models_transcripts.pinfish.gff`

This GFF can be downloaded [here](https://bitbucket.org/wrf/genome-reannotations/downloads/pbachei_03_filtered_gene_models_transcripts.pinfish.gff.gz).

### generating proteins from the transcripts ###
Proteins are generated with [prottrans.py](https://bitbucket.org/wrf/sequences/src/master/prottrans.py) with `-n` for no renaming, and `-r` to only check forward strand.

`prottrans.py -n -r pbachei_03_filtered_gene_models_transcripts.fa > pbachei_03_filtered_gene_models_transcripts.prot.fa`

This yielded 18947 proteins, meaning 3 are missing due to multiframe ties.

### blast against ML2 ###
Align the transcripts against ML2 proteins, and generate the GFF:

`~/diamond-latest/diamond blastx -q pbachei_03_filtered_gene_models_transcripts.fa -d ~/genomes/mnemiopsis_leidyi/ML2.2.aa -o pbachei_03_filtered_gene_models_transcripts.vs_ml2_blastx.tab`

`~/git/genomeGTFtools/blast2genomegff.py -b pbachei_03_filtered_gene_models_transcripts.vs_ml2_blastx.tab -d ~/genomes/mnemiopsis_leidyi/ML2.2.aa -g pbachei_03_filtered_gene_models_transcripts.pinfish.gff -p blastx -P -M 10 > pbachei_03_filtered_gene_models_transcripts.vs_ml2_blastx.gff`

### blast against SwissProt ###
Align the transcripts against SwissProt proteins, and generate the GFF:

`~/diamond-latest/diamond blastx -q pbachei_03_filtered_gene_models_transcripts.fa -d ~/db/model_organism_uniprot.w_cnido.fasta -o pbachei_03_filtered_gene_models_transcripts.vs_models_blastx.tab`

`~/git/genomeGTFtools/blast2genomegff.py -b pbachei_03_filtered_gene_models_transcripts.vs_models_blastx.tab -d ~/db/model_organism_uniprot.w_cnido.fasta -g pbachei_03_filtered_gene_models_transcripts.pinfish.gff -p blastx -P -M 10 -S --add-description --add-accession > pbachei_03_filtered_gene_models_transcripts.vs_models_blastx.gff`

### mapping transcript libraries ###
Six 454 or IonTorrent libraries were downloaded [here](https://neurobase.rc.ufl.edu/pleurobrachia/download). The throughput was low (less than 1Gb) and transcripts are extremely fragmented, average lengths ranging from 151bp (Mouth library) to only 245bp (Comb rows).

```
for FILE in P-bachei_a* 
do sizecutter.py -p -n $FILE > $FILE.fasta
~/minimap2-2.17_x64-linux/minimap2 -a -x splice -t 4 --secondary=no AVPN01.1.scaffolds.fa $FILE.fasta | ~/samtools-1.9/samtools sort - -o $FILE.bam
~/samtools-1.9/samtools index $FILE.bam
done
```

The average of all combined is slightly higher (254bp), but compared to the predicted genomic transcripts, most transcripts are too fragmented and do not even span a single gene. The minimap/pinfish transcripts are 1116bp on average, substantially longer than an average 454/IonTorrent transcript.

### mapping RNAseq reads ###

The only large RNAseq set is [SRX045964](https://www.ncbi.nlm.nih.gov/sra/SRX045964).

`~/sratoolkit.2.10.3-ubuntu64/bin/fastq-dump --gzip --split-files --defline-seq '@$sn[_$rn]/$ri' SRR116669 SRR116670`

`sizecutter.py -f AVPN01.1.scaffolds.fa | sed s/" "/"\t"/ > AVPN01.1.scaffolds.sizes`

`hisat2-build AVPN01.1.scaffolds.fa AVPN01.1.scaffolds.fa`

`{ hisat2 -q -x AVPN01.1.scaffolds.fa -1 SRR116669_1.fastq.gz,SRR116670_1.fastq.gz -2 SRR116669_2.fastq.gz,SRR116670_2.fastq.gz -p 6 --dta 2>&3 | ~/samtools-1.8/samtools sort - -o pbac_SRX045964_hisat2.bam ; } 3> pbac_SRX045964_hisat.log`

Mapping rate is low, only 24%. It is not clear if indiviudals were pooled, as high heterozygosity (from other individuals) may interfere with mapping. In any case, the bigwig is then generated with [bedtools](https://github.com/arq5x/bedtools2), [bedSort](http://hgdownload.soe.ucsc.edu/admin/exe/), and [bedGraphToBigWig](http://hgdownload.soe.ucsc.edu/admin/exe/).

`~/bedtools2/bin/bedtools genomecov -ibam pbac_SRX045964_hisat2.bam -bg -split > pbac_SRX045964_hisat2.bg`

`~/ucsc/bedSort pbac_SRX045964_hisat2.bg pbac_SRX045964_hisat2.bg`

`~/ucsc/bedGraphToBigWig pbac_SRX045964_hisat2.bg AVPN01.1.scaffolds.sizes pbac_SRX045964_hisat2.bw`

### augustus gene prediction ###
Created [AUGUSTUS gene models](http://bioinf.uni-greifswald.de/webaugustus/trainingtutorial) using the scaffolds and the [transcript models](https://neurobase.rc.ufl.edu/pleurobrachia/download). Training parameters are [here](https://bitbucket.org/wrf/genome-reannotations/downloads/Pleurobrachia_bachei_parameters.tar.gz).

Two datasets are created. The `hints-pred` has [36545 predicted genes](https://bitbucket.org/wrf/genome-reannotations/downloads/Pbachei_augustus.hints_pred.no_comments.gff.gz), clearly an overestimate. Given that they use the transcript models as the hints, many of these preserve the same erroneous gene structure as the transcripts. Nonetheless, there are some new genes in unannotated regions.

`~/git/genomeGTFtools/misc/augustus_to_gff3.py augustus.gff > augustus.hints_pred.no_comments.gff`

By comparison, the `ab-initio` dataset has [32683 genes](https://bitbucket.org/wrf/genome-reannotations/downloads/Pbachei_augustus.ab_initio.no_comments.gff.gz). These appear broadly to be more fragmented than the other set, but as the transcript models seem to contain a lot of both fragmented genes and fused genes, some of the fused ones are possible fixed.

`~/git/genomeGTFtools/misc/augustus_to_gff3.py augustus.gff > augustus.ab_initio.no_comments.gff`

`~/git/genomeGTFtools/blast2genomegff.py -b pbachei_03_filtered_gene_models_transcripts.vs_Hcal1_blastx.tab -d ~/genomes/hormiphora_californensis/hormiphora/annotation/Hcv1a1d20200325_release/Hcv1a1d20200325_model_proteins.pep -g pbachei_03_filtered_gene_models_transcripts.pinfish.gff -p blastx -P -M 10 > pbachei_03_filtered_gene_models_transcripts.vs_Hcal1_blastx.gff`

`~/diamond-latest/diamond blastp -q augustus.aa -d ~/genomes/hormiphora_californensis/hormiphora/annotation/Hcv1av85_release/Hcv1av85_model_proteins.pep -o augustus_ab_initio.vs_hcal.tab`

`~/git/genomeGTFtools/blast2genomegff.py -b augustus_ab_initio.vs_hcal.tab -d ~/genomes/hormiphora_californensis/hormiphora/annotation/Hcv1av85_release/Hcv1av85_model_proteins.pep -g augustus.ab_initio.no_comments.gff -p blastp -P -M 10 -G -F "." > augustus_ab_initio.vs_hcal.gff`

### mapping Hormiphora gene predictions ###
Due to the relatedness of *Pleurobrachia* and *Hormiphora*, the gene models from Hcal-v1 were used to try and predict the homologs. The *Hormiphora* models contain 14574 genes with 20075 transcripts.

`~/minimap2-2.17_x64-linux/minimap2 -a -x splice --secondary=no AVPN01.1.scaffolds.fa Hcal_transcripts.fasta | ~/samtools-1.9/samtools sort - -o Hcal_transcripts.vs_pbac_scaffolds.bam`

Broadly, this works extremely well, but indicates about 8000 secondary mappings, very likely due to uncollapsed haplotypes.

```
$ ~/samtools-1.9/samtools flagstat Hcal_transcripts.vs_pbac_scaffolds.bam
28271 + 0 in total (QC-passed reads + QC-failed reads)
0 + 0 secondary
8196 + 0 supplementary
0 + 0 duplicates
27941 + 0 mapped (98.83% : N/A)
```

`~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M Hcal_transcripts.vs_pbac_scaffolds.bam > Hcal_transcripts.vs_pbac_scaffolds.pinfish.gtf`

Though the numbers stay the same, the name is changed to Pbac, so that the predicted proteins can be distinguished in protein alignments from the Hcal proteins.

`~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py Hcal_transcripts.vs_pbac_scaffolds.pinfish.gtf | sed s/Hcv1/Pbac/g > Hcal_transcripts.vs_pbac_scaffolds.pinfish.gff`

This file can be [downloaded here](https://bitbucket.org/wrf/genome-reannotations/downloads/Hcal_transcripts.vs_pbac_scaffolds.pinfish.gff.gz).

`~/gffread-0.11.7.Linux_x86_64/gffread -g AVPN01.1.scaffolds.fa Hcal_transcripts.vs_pbac_scaffolds.pinfish.gff -w Hcal_transcripts.vs_pbac_scaffolds.pinfish.fasta`

`prottrans.py -n -r Hcal_transcripts.vs_pbac_scaffolds.pinfish.fasta | sed s/Hcv1/Pbac/g > Hcal_transcripts.vs_pbac_scaffolds.prots.fasta`

```
>Hcv1.av93.c5.g684.i1
MLAGRATRSALRLLGQNAKQAKQTGNLAGVVTKGIATSALKAAGAGASEISSILEQRILG
HTSSAELEETGRVLSIGDGIARVYGLKNIQAEEMVEFSSGLKGMALNLEPDNVGVVVFGN
DKEIKEGDVVKRTGAIVDVPVGEGLLGRVVDALGNPIDGMGPLDSSERKRVGLKAPGIIP
RESVREPMLTGIKAVDSLVPIGRGQRELIIGDRQTGKTAIAIDAIINQKQFNDGTDDKKK
LYCLYVAIGQKRSTVAQLVKRLTDSGAMKYTTIVSATASDAAPLQYLAPYSACSMGEYFR
DTGKHALIIYDDLSKQAVAYRQMSLLLRRPPGREAYPGDVFYLHSRLLERAAKMSDAYGG
GSLTALPVIETQAGDVSAYIPTNVISITDGQIFLESELFYKGIRPAINVGLSVSRVGAAA
QTKGMKQVAGKMKLELAQYREVAAFAQFGSDLDTATQSLLSRGVRLTELLKQGQYVPMDI
DEQVAVIYCGVRGHLDRIDPSKITDFEKAFVAHIRSNHPDIVGDIKATGQLTEANEAKLK
DVVINFVSTFV
>Pbac.av93.c5.g684.i1
MLAGRATRSALRLLGQNAKQAKQTGNLAGVVTKGIATSALKAAGAGASEISSILEQRILG
HTSSAELEETGRVLSIGDGIARVYGLKNIQAEEMVEFSSGLKGMALNLEPDNVGVVVFGN
DKEIKEGDVVKRTGAIVDVPVGEGLLGRVVDALGNPIDGMGPLDSSERKRVGLKAPGIIP
RESVREPMLTGIKAVDSLVPIGRGQRELIIGDRQTGKTAIAIDAIINQKQFNDGTDDKKK
LYCLYVAIGQKRSTVAQLVKRLTDSGAMKYTTIVSATASDAAPLQYLAPYSACSMGEYFR
DTGKHALIIYDDLSKQAVAYRQMSLLLRRPPGRLRMGPLQKMEVRADNLAAAKMSDAYGG
GSLTALPVIETQAGDVSAYIPTNVISITDGQIFLESELFYKGIRPAINVGLSVSRVGAAA
QTKGMKQVAGKMKLELAQYREVAAFAQFGSDLDTATQSLLSRGVRLTELLKQGQYVPMDI
DEQVAVIYCGVRGHLDRIDPSKITDFEKAFVAHIRSNHPDIVGDIKATGQLTEANEAKLK
DVVINFVSTFV
```


### filtering by correctly collapsed scaffolds ###
A large number of scaffolds appeared to have half average coverage of other scaffolds, suggesting these were unmerged haplotypes in the final assembly. Coverage cutoff was set to 175x, i.e. scaffolds with lower average coverage were excluded. This yielded 5310 scaffolds, accounting for 59.4Mb. Thus, 96.6Mb is likely unmerged haplotypes in the final assembly. If these were to be correctly merged (i.e. half of the bases merged), the predicted final genome size would be 107.8Mb.

Genes mapping to those scaffolds were extracted with [getgtfscaffolds.py](https://bitbucket.org/wrf/sequences/src/master/getgtfscaffolds.py):

`getgtfscaffolds.py -g pbachei_03_filtered_gene_models_transcripts.pinfish.gff -s pleuro_correctly_collapsed_scaffolds_list.txt > pbachei_03_filtered_gene_models_transcripts_collapsed_only.gff`

The IDs of transcripts that were on those scaffolds were extracted from the GFF with `cut`:

`cut -f 9 pbachei_03_filtered_gene_models_transcripts_collapsed_only.gff | cut -d ";" -f 1 | cut -d "=" -f 2 | sort | uniq > pbachei_03_filtered_gene_models_transcripts_collapsed_only.names`

`getAinB.py -v pbachei_03_filtered_gene_models_transcripts_collapsed_only.names pbachei_03_filtered_gene_models_transcripts.prot.fa > pbachei_03_filtered_gene_models_transcripts_collapsed_only.prot.fa`

This yielded 7013 proteins. One protein was excluded `scaffold1136_1_size28075_gene_10007Barcelona`, though this transcript was short (300bp) and was multiframe in the translation step, hence removed. This suggests that the other 11937 transcripts should have an allele of the other haplotype. For instance, if half of these were excluded, this would give 12981 total proteins instead.

Thus, for both size of the genome, and number of proteins, the reported numbers are likely an overestimate by about a third.


### absence of photoproteins ###
The non-photoprotein is a predicted calcium binding protein of unknown function, but homologous to ctenophore and cnidarian photoproteins ([Francis 2015](https://doi.org/10.1371/journal.pone.0128742)). This protein was not found in the transcripts or protein models, but a partial protein (`sb|9953503`) was found in the transcriptome datasets. No gene is predicted at that locus on scaffold00203 (AVPN01018200.1), though the best tblastn hit to the scaffold directly corresponds to this protein. The two remaining hits are to two scaffolds (AVPN01002599.1 and AVPN01012753.1) where the hit would run off the end of the scaffold. These are likely part of the other unmerged haplotype.

```
Hcal_nonPP	AVPN01018200.1	59.559	136	21	1	35	136	4887	4480	2.76e-43	155
Hcal_nonPP	AVPN01018200.1	92.308	39	3	0	155	193	4092	3976	3.33e-17	80.9
Hcal_nonPP	AVPN01018200.1	61.111	36	14	0	120	155	4394	4287	2.41e-06	48.9
Hcal_nonPP	AVPN01002599.1	92.308	39	3	0	155	193	86	202	5.09e-17	80.1
Hcal_nonPP	AVPN01012753.1	78.571	42	9	0	35	76	1019	1144	1.05e-15	75.9
```

Based on the mapping of two neighboring transcripts, assuming canonical splicing, the full protein can be reconstructed (this was left partial in [Francis 2015](https://doi.org/10.1371/journal.pone.0128742)). Line breaks indicate the splice sites between the transcripts.

```
>Pbac_9913490_9953503_9853218
AGATCAGTAACTTCGTCACTTGGACTAAATTTGGTAGCGTACTGAAGATGATATTGATAG
GCTGTTTTTCTTTGTCACACCGAGGTGTCACAAAGCAGAACTACCGCTGTTTTGATTTGA
AGATCACCAATCATCTGACCATATTTAGTAATCTAACAAATTGTTAGTACACTACCATAT
ATGGTATATGAAGTGCTGTCGGGGAGGAAGTAAAGGGGACAATGCCTTGTACAACAACTA
CTATTGGAGAAGGAAAATGGGAGAGTACTTTAAATGTGAAGATAGTAGCAG
GGACGGATTCCTAACAATTGAAGAGATGATAAACAATACGAGACCGTTGAAAGTTCAC
TGTGATGCCCCTGAATACAAGATGGAAGCACTGGCAAGGGCGTACAAAGAATTCTGGGGC
CAAGTAGGATTGAAGCCTGGAAGGAAAGTGGGTAAAACCAAGTTCCTCAAGGGGCTCAGT
AAGCTTGGAAGAGAAGAGTTGGATCGAGAGGCAGCGGGGGTACCAACCCTTCACAGTCGA
GTATCCCATGCCCTGTTTGATATCATTGATGCGAACAATAACAACCGACTTCAGAAGAAA
GAAATTTCACAATGGATGGCTGCTTCGGGACTAAATCCGGAAAATGCAGATAAAATGTTC
CAGGAGGCGGATGTTAGTGGTAAAGGGTACATCAG
TAGGGAT
GAACTAATAGAGGCAGAGTTTTGTTCTTTCTTCCATCCGGAGAAATGTATGGCGCAACTG
GGCGTCAAAGTGGTTTAACTGGAACTGATATCAAGATTTATTAAAAACTTTCCCGCCAAC
CAACCAACCAATCACTGCATGCAACTTGTGATCCTTAGATTCGACAAAATGTGACTTAGT
TATCGTTTGTGGCAAGACACAAGTCATGAACCCAATCCGACCCAATATGATGACACGTAA
ATGATTTGTACTTTTTAGTGAATCAAAATCTATGATAATGAGAGATTTTTGGATTGAATA
TCAGTTATCATGAATGAGATAGACTATCTTTAACAAATACAGTCGGACTCCGATTTAGTG
ACATCCTCGGGGGAAAGGGTTTTGGTCACTAAATCGGGGTCCGACTGTATATTATCAATT
TGATTGTAAAATTCTTTTAGATATTCTATCGAACTAAAATAAATTCGTAACCTTATTGTT
TTAACCAATCATCAACAGTATTGGTTTGACAAGTAAAGATAAAGACTGTGTCGTAATGTC
AACTATTTAAC
```

Thus, the complete protein would be as shown below. Many motifs are shared with the [Hormiphora sequence](https://www.ncbi.nlm.nih.gov/protein/AIW06415.1) found in the transcriptome. The N-terminal motif is most different, but both motifs are found in the transcriptome of the [Pukia falcata](https://neurobase.rc.ufl.edu/pleurobrachia/download), suggesting that there is conserved splice variation of an alternate 5' exon. Upstream of this locus in Pbachei is a large gap, and then the scaffold ends, so it cannot be determined if the exon is there in this assembly.

```
>Pbac_9913490_9953503_9853218_1
MKCC-RGGSKGDNALYNNYYWRRKMGEYFKCEDSSRDGFLTIEEMINNTRPLKVHCDAPEYKMEALARAYKEFWGQVGLKPGRKVGKTKFLKGLSKLGREELDREAAGVPTLHSRVSHALFDIIDANNNNRLQKKEISQWMAASGLNPENADKMFQEADVSGKGYISRDELIEAEFCSFFHPEKCMAQLGVKVV
>Hcal_nonPP
MGLLRRGRRRRP-SLLKDPYWRAQMVRLFHDVDKDKNGYLTIEEMIANTRPLKEHCNAPEFKMEALAAAYTEFWGQVGLRRGKKVKKSQFLKGLSRLGREELNREAAGVPTLHSKVSHALFDIIDANNNNRLQKREIAQWMAASGLNPDDAEKMFQEADTKGKGYISREELIEAEFCSFFHPEKCMAQLGVKVV
>Pukia_falcata_comp26544_c0_seq3
MSCC--GGNNHD-SLYKNVFWREKMIQYFKNEDSDRDGYLTIEEMIKNTRPLKTHCDAPEYKMEALASAYREFWGQVGLKPGKKVGKQKFLKGLSKLGKEELDREAAGIPTLHSRVSHALFDIIDANNNNRLQKTEIAQWMAASGLNPDNAEKMFEEADRSGKGYISRDELIEAEFCSFFHPEKCMVQLGVKVV
>Pukia_falcata_comp26544_c0_seq2
MGLRRRGRVSLSRQLMMNPYWRKEMSILFSEVDRNRDGYLTIEEMIKNTRPLKTHCDAPEYKMEALASAYREFWGQVGLKPGKKVGKQKFLKGLSKLGKEELDREAAGIPTLHSRVSHALFDIIDANNNNRLQKTEIAQWMAASGLNPDNAEKMFEEADRSGKGYISRDELIEAEFCSFFHPEKCMVQLGVKVV
```

This locus spans around 2kb in Pbachei, while the comparable protein spans almost 8kb in Mleidyi. As this scaffold is very short, it is hard to identify any other genes.

`tblastn -query mlei_photoprots.fa -db AVPN01.1.scaffolds.fa -outfmt 6 -evalue 0.1`

As for the photoproteins themselves, none are found in the transcripts or proteins, and only weak hits are found directly on the scaffolds (very likely due to an EF-hand type motif) but there is no expression at that site (in an intron of another gene), suggesting a repeat or ancient inactivated pseudogene. This supports the results of [Haddock 1995](https://doi.org/10.2307/1542153) that pleurobrachiidae are non-luminous.

```
Mlei_PP9__ML215421a	AVPN01000156.1	38.095	63	36	2	133	193	61324	61139	0.006	39.3
Mlei_PP8__ML38411a	AVPN01000156.1	38.095	63	36	2	133	193	61324	61139	0.006	39.3
Mlei_PP10__ML215422b	AVPN01000156.1	38.095	63	36	2	133	193	61324	61139	0.010	38.5
```

### non-fluorescent protein ###
The non-fluorescent protein is an ortholog of cnidarian GFPs, but lacks residues involved in chromophore formation. As most ctenophores are not fluorescent anyway, the function is unknown ([Francis 2016](https://doi.org/10.1186/s12862-016-0738-5)).

```
Hormiphora_californensis_AOI27780.1	scaffold4143_1_size8087_gene_22219Barcelona	39.3	239	57	3	1	239	1	151	2.2e-36	149.1
```

`grep scaffold4143_1_size8087_gene_22219Barcelona pbachei_03_filtered_gene_models_transcripts.pinfish.gff`

```
AVPN01004015.1	pinfish	transcript	346	1905	.	+	.	ID=scaffold4143_1_size8087_gene_22219Barcelona;Name=scaffold4143_1_size8087_gene_22219Barcelona
AVPN01004015.1	pinfish	exon	346	447	.	+	.	Parent=scaffold4143_1_size8087_gene_22219Barcelona
AVPN01004015.1	pinfish	exon	507	535	.	+	.	Parent=scaffold4143_1_size8087_gene_22219Barcelona
AVPN01004015.1	pinfish	exon	1263	1395	.	+	.	Parent=scaffold4143_1_size8087_gene_22219Barcelona
AVPN01004015.1	pinfish	exon	1598	1691	.	+	.	Parent=scaffold4143_1_size8087_gene_22219Barcelona
AVPN01004015.1	pinfish	exon	1808	1905	.	+	.	Parent=scaffold4143_1_size8087_gene_22219Barcelona
```

However, an internal exon appears to be missing. The same gene in Mlei (ML181711a) has 6 exons. Using the [nonFP protein](https://www.ncbi.nlm.nih.gov/popset?LinkName=protein_popset&from_uid=1061431261) set, the proteins are aligned with [mafft](https://mafft.cbrc.jp/alignment/software/), then the protein profile (hmm) is generated:

```
mafft cteno_nonfps.fa > cteno_nonfps.aln
~/augustus-3.3.1/scripts/msa2prfl.pl --qij=/home/dummy/augustus-3.3.1/config/profile/blosum62.qij cteno_nonfps.aln > cteno_nonfps.prfl
```

The final protein is predicted from the scaffold with [augustus](https://github.com/Gaius-Augustus/Augustus), using *Mnemiopsis* for the [species parameters](https://bitbucket.org/wrf/genome-reannotations/downloads/mleidyi_parameters.tar.gz):

`~/augustus-3.3.1/bin/augustus --proteinprofile=cteno_nonfps.prfl --predictionStart=200 --predictionEnd=2500 --protein=on --cds=on --strand=forward --gff3=on --genemodel=exactlyone --species=mnemiopsis_leidyi AVPN01004015.fa`

```
AVPN01004015.1	AUGUSTUS	gene	346	1905	0.63	+	.	ID=g1
AVPN01004015.1	AUGUSTUS	transcript	346	1905	0.63	+	.	ID=g1.t1;Parent=g1
AVPN01004015.1	AUGUSTUS	start_codon	346	348	.	+	0	Parent=g1.t1
AVPN01004015.1	AUGUSTUS	intron	448	669	1	+	.	Parent=g1.t1
AVPN01004015.1	AUGUSTUS	intron	818	949	1	+	.	Parent=g1.t1
AVPN01004015.1	AUGUSTUS	intron	1086	1262	1	+	.	Parent=g1.t1
AVPN01004015.1	AUGUSTUS	intron	1396	1597	0.85	+	.	Parent=g1.t1
AVPN01004015.1	AUGUSTUS	intron	1692	1807	0.63	+	.	Parent=g1.t1
AVPN01004015.1	AUGUSTUS	CDS	346	447	1	+	0	ID=g1.t1.cds;Parent=g1.t1
AVPN01004015.1	AUGUSTUS	CDS	670	817	1	+	0	ID=g1.t1.cds;Parent=g1.t1
AVPN01004015.1	AUGUSTUS	CDS	950	1085	1	+	2	ID=g1.t1.cds;Parent=g1.t1
AVPN01004015.1	AUGUSTUS	CDS	1263	1395	0.99	+	1	ID=g1.t1.cds;Parent=g1.t1
AVPN01004015.1	AUGUSTUS	CDS	1598	1691	0.63	+	0	ID=g1.t1.cds;Parent=g1.t1
AVPN01004015.1	AUGUSTUS	CDS	1808	1905	0.84	+	2	ID=g1.t1.cds;Parent=g1.t1
```

The alignment is shown below, in good agreement with the [Hormiphora protein](https://www.ncbi.nlm.nih.gov/protein/1061431261):

```
>Hormiphora_californensis_AOI27780.1
MAEQRLERADSIFTGTTKFKVVAELTFEEDLVIQVNGEGTASPREGVQTMELSCAKYAGP--INLNIIGTIIQTNLRAFTGYTGSNLYDFFKTSFPGSPKTEIEAMFVDGAIVSGSCTLSFVKSTLMCRFKLAIAGVSEDSPARSPNLKETLTCFEVLDEGAKKGEVMTSVDLTWEVGTTGGRYSCRMDTRAGSGDHGAPNFTPPRHFIGHHFKVLEHTPDNRHFAQRLKSRATSINYFKNQ
>scaffold4143_1_size8087_gene_22219Barcelona_0
MAENRLERADSFFTGTTRFKLVAEISYDDELCVQNRNRATQS-------------------------------------------------------------------------------------AKFKLAVCSVPESSPARSLKLGEALTCFEVVDEGAGKDGVLTTTDLSWPM--VNERYSCRMDTRTASFE---GNYLPSRHFIGLQFKVLEHSGDNHHFAQRLKSRATTINYYKN-
>Pbachei_nonFP
MAENRLERADSFFTGTTRFKLVAEISYDDELCVQVNGEGTGSPREGVQTMELSCPNSPGPVAINLGIIGTIIQTNLKPFINYMGSQVYDFYKTNFPGSPSTDIEAIFIDGMTISGSSTLSFVKNILMCRFKLAVCSVPESSPARSLKLGEALTCFEVVDEGAGKDGVLTTTDLSWPM--VNERYSCRMDTRTASFE---GNYLPSRHFIGLQFKVLEHSGDNHHFAQRLKSRATTINYYKN-
```

### bioluminescence proteins ###
The FYY proteins were not found by [Francis 2015](https://doi.org/10.1371/journal.pone.0128742). The top hits of *Dryodora glandiformis* [FYY protein](https://www.ncbi.nlm.nih.gov/protein/AIW06431.1) are better matches of Hcal 2OGFe groups 1 and 2 enzymes. No other hits were found in the transcript models.

```
Hcal_2OGFe1	scaffold960_1_size31897_gene_8854Barcelona	67.9	324	98	3	35	356	152	471	3.4e-129	458.0
Hcal_2OGFe2	scaffold322_1_size62744_gene_3399Barcelona	75.8	335	80	1	12	345	5	339	2.9e-154	541.2
Hcal_2OGFe2	scaffold960_1_size31897_gene_8854Barcelona	31.2	340	202	9	12	341	155	472	3.7e-40	162.2
Dgla_FYY1	scaffold960_1_size31897_gene_8854Barcelona	41.8	325	171	5	35	356	157	466	4.2e-71	265.0
Dgla_FYY1	scaffold322_1_size62744_gene_3399Barcelona	32.9	322	193	11	38	347	10	320	6.6e-48	188.0
```

