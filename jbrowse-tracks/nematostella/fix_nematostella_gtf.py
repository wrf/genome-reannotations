#!/usr/bin/env python

'''
fix_nematostella_gtf.py nveGenes.vienna130208.nemVec1.gtf > nveGenes.vienna130208.nemVec1.gff
'''

import sys
import re
import time
from collections import defaultdict,Counter

#scaffold_1	nveGenes	exon	1045101	1045564	.	+	.	gene_id "NVE93"; transcript_id "NVE93"; exon_number "1"; exon_id "NVE93.1";
#scaffold_1	nveGenes	5UTR	1045101	1045269	.	+	.	gene_id "NVE93"; transcript_id "NVE93"; exon_number "1"; exon_id "NVE93.1";
#scaffold_1	nveGenes	CDS	1045270	1045564	.	+	0	gene_id "NVE93"; transcript_id "NVE93"; exon_number "1"; exon_id "NVE93.1";


if len(sys.argv) < 2:
	print >> sys.stderr, __doc__
else:
	ID_order = defaultdict(dict) # key is scaffold, then ID, then start position
	feature_dict = defaultdict(list)
	linecounter = 0
	print >> sys.stderr, "# Reading features from {}".format(sys.argv[1]), time.asctime()
	for line in open(sys.argv[1],'r'):
		line = line.strip()
		if line and line[0]!="#":
			linecounter += 1
			lsplits = line.split("\t")
			scaffold = lsplits[0]
			program = lsplits[1]
			feature = lsplits[2]
			if feature=="5UTR":
				lsplits[2] = "five_prime_UTR"
			elif feature=="3UTR":
				lsplits[2] = "three_prime_UTR"
			start = int(lsplits[3])
			attributes = lsplits[8]
			if attributes.find("gene_id")>=0: # gff3 format but no ID
				geneid = re.search('gene_id "([\w.|-]+)"', attributes).group(1)
			lsplits[8] = "Parent={}".format( geneid )
			if geneid not in ID_order[scaffold]:
				ID_order[scaffold][geneid] = start
			else:
				if ID_order[scaffold].get(geneid) > start:
					ID_order[scaffold][geneid] = start
			feature_dict[geneid].append(lsplits)
	print >> sys.stderr, "# Counted {} lines for {} features".format(linecounter, len(feature_dict))
	print >> sys.stderr, "# Generating parent features", time.asctime()
	for scaffold in sorted(ID_order.keys()):
		for geneid, pos in sorted(ID_order[scaffold].items(), key=lambda x: x[1]):
			allstarts = map(int,[ls[3] for ls in feature_dict[geneid]])
			allends = map(int,[ls[4] for ls in feature_dict[geneid]])
			parentstart = min(allstarts)
			parentend = max(allends)
			try:
				topscore = max(map(int,[ls[5] for ls in feature_dict[geneid]]))
			except ValueError:
				topscore = 0
			mainstrand = Counter([ls[6] for ls in feature_dict[geneid]]).most_common(1)[0][0]
			outline = "{0}\t{1}\tmRNA\t{2}\t{3}\t{4}\t{5}\t.\tID={6};Name={6}".format( scaffold, program, parentstart, parentend, topscore, mainstrand, geneid)
			print >> sys.stdout, outline
			for outsplits in feature_dict.get(geneid):
				outline = "\t".join(outsplits)
				print >> sys.stdout, outline
