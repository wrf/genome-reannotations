# Nematostella processing steps
From [Zimmerman 2021 Sea anemone genomes reveal ancestral metazoan chromosomal macrosynteny](https://doi.org/10.1101/2020.10.30.359448)

# for the 2021 version #
### Process chromosomes for display
New [version of the assembly](https://simrbase.stowers.org/starletseaanemone) contains 30 sequences totalling 252Mb, with N50 of 16Mb, indicating a good assembly. The fasta index for JBrowse is generated with [samtools](https://github.com/samtools/samtools).

`~/samtools-1.9/samtools faidx Nvec200.fasta`

### minimap of old scaffolds and gene models ###
The 2007 version had 10804 scaffolds. Nearly every scaffold mapped at least once, with 45k supplementary mappings, possibly due to repeats.

`~/minimap2-2.17_x64-linux/minimap2 -a -x asm10 -t 4 --secondary=no Nvec200.fasta Nemve1.fasta | ~/samtools-1.9/samtools sort - -o Nemve1_scaffolds_vs_Nvec200.bam`

```
~/samtools-1.9/samtools flagstat Nemve1_scaffolds_vs_Nvec200.bam
56676 + 0 in total (QC-passed reads + QC-failed reads)
0 + 0 secondary
45872 + 0 supplementary
0 + 0 duplicates
55800 + 0 mapped (98.45% : N/A)
```

`~/samtools-1.9/samtools index Nemve1_scaffolds_vs_Nvec200.bam`

Mapping the 27273 gene models from v1:

`~/minimap2-2.17_x64-linux/minimap2 -a -x splice -t 4 --secondary=no Nvec200.fasta Nemve1.FilteredModels1_for_jbrowse.fasta | ~/samtools-1.9/samtools sort - -o Nemve1_filtered_models_vs_Nvec200.bam`

```
~/samtools-1.9/samtools flagstat Nemve1_filtered_models_vs_Nvec200.bam
27990 + 0 in total (QC-passed reads + QC-failed reads)
0 + 0 secondary
717 + 0 supplementary
0 + 0 duplicates
27097 + 0 mapped (96.81% : N/A)
```

`~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M -s Nemve1_filtered_models_vs_Nvec200.bam > Nemve1_filtered_models_vs_Nvec200.gtf`

`~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py Nemve1_filtered_models_vs_Nvec200.gtf > Nemve1_filtered_models_vs_Nvec200.gff`

Mapping the 25729 "Vienna" set models:

`~/minimap2-2.17_x64-linux/minimap2 -a -x splice -t 4 --secondary=no Nvec200.fasta nveGenes.vienna130208.fasta | ~/samtools-1.9/samtools sort - -o nveGenes.vienna130208_vs_Nvec200.bam`

```
~/samtools-1.9/samtools flagstat nveGenes.vienna130208_vs_Nvec200.bam
27050 + 0 in total (QC-passed reads + QC-failed reads)
0 + 0 secondary
1321 + 0 supplementary
0 + 0 duplicates
26917 + 0 mapped (99.51% : N/A)
```

`~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M -s nveGenes.vienna130208_vs_Nvec200.bam > nveGenes.vienna130208_vs_Nvec200.gtf`

`~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py nveGenes.vienna130208_vs_Nvec200.gtf > nveGenes.vienna130208_vs_Nvec200.gff`

### link and generate tracks ###

```
cd ~/jbrowse_data/nematostella/
ln -s /mnt/data/genomes/nematostella_vectensis_CNID/Nvec200.fasta
ln -s /mnt/data/genomes/nematostella_vectensis_CNID/Nvec200.fasta.fai

/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta Nvec200.fasta --out ./

ln -s /mnt/data/genomes/nematostella_vectensis_CNID/nveGenes.vienna130208_vs_Nvec200.gff
ln -s /mnt/data/genomes/nematostella_vectensis_CNID/Nemve1_filtered_models_vs_Nvec200.gff 
ln -s /mnt/data/genomes/nematostella_vectensis_CNID/NVEC200.20200813.gff

/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff NVEC200.20200813.gff --trackType CanvasFeatures  --trackLabel gene-models-v2 --out ./
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff nveGenes.vienna130208_vs_Nvec200.gff --trackType CanvasFeatures  --trackLabel gene-models-Vienna --out ./
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff Nemve1_filtered_models_vs_Nvec200.gff --trackType CanvasFeatures  --trackLabel gene-models-v1 --out ./

ln -s /mnt/data/genomes/nematostella_vectensis_CNID/Nemve1_scaffolds_vs_Nvec200.bam
ln -s /mnt/data/genomes/nematostella_vectensis_CNID/Nemve1_scaffolds_vs_Nvec200.bam.bai 
```

### add blastn for ultra-conserved elements ###
Using [UCEs](https://en.wikipedia.org/wiki/Ultra-conserved_element) from study by [Quattrini 2020](https://doi.org/10.1038/s41559-020-01291-1) about phylogeny and molecular clock of anthozoans. The [supplemental data](https://doi.org/10.5061/dryad.36n40) hosts the original baits as fasta format. The file `Quattrini_anthozoa_baits_V1.fas` contains 16306 sequences of 120bp each. Of these, 2640 hit at least once to the genome (some more than once, or split across two exons).

```
blastn -query Quattrini_anthozoa_baits_V1.fas -db Nvec200.fasta -outfmt 6 -max_target_seqs 1 > Quattrini_anthozoa_baits_V1.vs_Nvec200.tab
~/git/genomeGTFtools/blast2gff.py -b blastn -t UCE -b Quattrini_anthozoa_baits_V1.vs_Nvec200.tab -U > Quattrini_anthozoa_baits_V1.vs_Nvec200.gff

ln -s /mnt/data/project/genomes/nematostella_vectensis_CNID/Quattrini_anthozoa_baits_V1.vs_Nvec200.gff 
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff Quattrini_anthozoa_baits_V1.vs_Nvec200.gff --trackType CanvasFeatures --trackLabel Quattrini-UCE-blastn --out ./
```

As these baits were designed from a [combination of genomic UCEs and transcriptomes](https://doi.org/10.1111/1755-0998.12736), they mostly appear to match exons of conserved genes (2715 hits with ID `trans`), rather than putative cis-regulatory elements (1143 hits with ID `uce`). Even 997 out of 1143 of the `uce` baits appear to still match exons, due to their complete blastn matching to transcripts, rather than the genome itself. If most are still exons, then mapping to the genome could also indicate places where the gene models may be incorrect.

```
getAinB.py uce Quattrini_anthozoa_baits_V1.fas -s > Quattrini_anthozoa_baits_V1.uce_only.fasta
blastn -query Quattrini_anthozoa_baits_V1.uce_only.fasta -db NVEC200.20200813.transcripts.fasta -outfmt 6 -max_target_seqs 1 -evalue 1e-20 > Quattrini_anthozoa_baits_V1.uce_only.vs_transcripts.tab
wc Quattrini_anthozoa_baits_V1.uce_only.vs_transcripts.tab
```

Making track of UCEs in the genome:

```
sizecutter.py -g "?" -b 1 -p nvmask.fasta > nvmask.no_missing.fasta
degapper.py nvmask.no_missing.fasta
blastn -query nvmask.no_missing.fasta.nogaps -db Nvec200.fasta -outfmt 6 -max_target_seqs 1 > nvmask.no_missing.vs_Nvec200.tab
~/git/genomeGTFtools/blast2gff.py -b blastn -t UCE -b nvmask.no_missing.vs_Nvec200.tab > nvmask.no_missing.vs_Nvec200.gff

ln -s /mnt/data/project/genomes/nematostella_vectensis_CNID/nvmask.no_missing.vs_Nvec200.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff nvmask.no_missing.vs_Nvec200.gff --trackType CanvasFeatures --trackLabel Quattrini-UCE-50p --out ./
```

---
---

# for the original JGI version #
From [Putnam et al 2007 Sea Anemone Genome Reveals Ancestral Eumetazoan Gene Repertoire and Genomic Organization](https://doi.org/10.1126/science.1139158)

### Process scaffolds for display
Prepare scaffold index for the [v1 scaffolds](https://mycocosm.jgi.doe.gov/Nemve1/Nemve1.home.html)

`~/samtools-1.8/samtools faidx Nemve1.fasta`

Link and create the reference sequence:

```
ln -s /mnt/data/genomes/nematostella_vectensis/Nemve1.fasta
ln -s /mnt/data/genomes/nematostella_vectensis/Nemve1.fasta.fai
/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta Nemve1.fasta --out ./
```

### Add v1 models
The original [v1 gene models](https://mycocosm.jgi.doe.gov/Nemve1/Nemve1.home.html) were GTF format, and need to be converted to a parent-child GFF file. The `name` tag is found in basically all features, when it only needs to be in `gene` or `mRNA`, so is removed for exons or CDS features.

```
scaffold_1	JGI	exon	25970	26135	.	-	.	name "fgenesh1_pg.scaffold_1000005"; transcriptId 196074
scaffold_1	JGI	CDS	25970	26135	.	-	0	name "fgenesh1_pg.scaffold_1000005"; proteinId 196074; exonNumber 8
```

`./fix_nematostella_v1_models.py Nemve1.FilteredModels1.gff > Nemve1.FilteredModels1.for_jbrowse.gff`

```
scaffold_1	JGI	mRNA	25970	37222	0	-	.	ID=196074;Name=fgenesh1_pg.scaffold_1000005
scaffold_1	JGI	exon	25970	26135	.	-	.	Parent=196074
scaffold_1	JGI	CDS	25970	26135	.	-	0	Parent=196074
```

Then link the file and create the track in jbrowse:

```
ln -s /mnt/data/genomes/nematostella_vectensis/Nemve1.FilteredModels1.for_jbrowse.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Nemve1.FilteredModels1.for_jbrowse.gff --trackType CanvasFeatures --trackLabel v1-models
```

### Add Vienna models
The ["Vienna" set](https://ndownloader.figshare.com/files/1215191), from [Moran 2014](https://doi.org/10.1101/gr.162503.113), contains a GTF file. This is missing top level features, and includes some unconventional feature definitions (`5UTR` and `3UTR`, instead of `five_prime_UTR` and `three_prime_UTR`, respectively) and must be converted to a jbrowse-compatible GFF file.

```
scaffold_1	nveGenes	exon	1045101	1045564	.	+	.	gene_id "NVE93"; transcript_id "NVE93"; exon_number "1"; exon_id "NVE93.1";
scaffold_1	nveGenes	5UTR	1045101	1045269	.	+	.	gene_id "NVE93"; transcript_id "NVE93"; exon_number "1"; exon_id "NVE93.1";
```

`fix_nematostella_gtf.py nveGenes.vienna130208.nemVec1.gtf > nveGenes.vienna130208.nemVec1.gff`

```
scaffold_1	nveGenes	mRNA	16686	18722	0	+	.	ID=NVE1;Name=NVE1
scaffold_1	nveGenes	exon	16686	18722	.	+	.	Parent=NVE1
scaffold_1	nveGenes	five_prime_UTR	16686	16942	.	+	.	Parent=NVE1
scaffold_1	nveGenes	CDS	16943	17581	.	+	0	Parent=NVE1
scaffold_1	nveGenes	three_prime_UTR	17585	18722	.	+	.	Parent=NVE1
scaffold_1	nveGenes	start_codon	16943	16945	.	+	0	Parent=NVE1
scaffold_1	nveGenes	stop_codon	17582	17584	.	+	0	Parent=NVE1
```

Again, link and create the track:

```
ln -s /mnt/data/genomes/nematostella_vectensis/nveGenes.vienna130208.nemVec1.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff nveGenes.vienna130208.nemVec1.gff --trackType CanvasFeatures --trackLabel Vienna-models
```

### Blast versus SwissProt
Using [diamond](https://github.com/bbuchfink/diamond), and a database of 5 organisms from [SwissProt](https://www.uniprot.org/downloads) (human, yeast, *Arabidopsis*, *E. coli* and *Pseudomonas*) of 48763 sequences likely to have meaningful annotations, as well as all 549 cnidarian proteins currently on SwissProt, generate the diamond db. Requires [getAinB.py](https://bitbucket.org/wrf/sequences/src/master/getAinB.py) and [uniprotphylumtable.py](https://bitbucket.org/wrf/sequences/src/master/uniprotphylumtable.py).

```
uniprotphylumtable.py -d uniprot_sprot_invertebrates.dat.gz -c -f > uniprot_sprot_invertebrates.full_taxonomy
grep Metazoa uniprot_sprot_invertebrates.full_taxonomy | grep Cnidaria | cut -f 1 | sed s/^/_/ > cnidarian_taxa_list
getAinB.py cnidarian_taxa_list uniprot_sprot.fasta -s > cnidarian_swissprots.fasta
cat model_organism_uniprot.fasta cnidarian_swissprots.fasta > model_organism_uniprot.w_cnido.fasta
```

`~/diamond-latest/diamond makedb --in model_organism_uniprot.w_cnido.fasta --db model_organism_uniprot.w_cnido.fasta`

Then use the transcripts directly, with 108540 hits for 13934 queries, 206 are antisense:

`~/diamond-latest/diamond blastx -q nveGenes.vienna130208.fasta -d ~/db/model_organism_uniprot.w_cnido.fasta -p 4 -o nveGenes.vienna130208.blastx_v_models.tab`

`~/git/genomeGTFtools/blast2genomegff.py -p blastx -d ~/db/model_organism_uniprot.w_cnido.fasta -b nveGenes.vienna130208.blastx_v_models.tab -g nveGenes.vienna130208.nemVec1.gff -S --add-description --add-accession > nveGenes.vienna130208.blastx_v_models.gff`

`ln -s /mnt/data/project/genomes/nematostella_vectensis/nveGenes.vienna130208.blastx_v_models.gff`

`/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff nveGenes.vienna130208.blastx_v_models.gff --trackType CanvasFeatures --trackLabel blast-v-models`

The transcripts reveal that some proteins are truncated, typically by choosing a mid-protein methionine as the start, rather than the true start. For NVE146 on scaffold 1, the top hit to [MATN2_HUMAN](https://www.uniprot.org/uniprot/O00339) (Matrilin-2) begins 2 exons upstream of the predicted start codon. Six-frame translation shows that there is no M predicted on this transcript before the given CDS, suggesting that an N-terminal exon is missing, very likely belonging to the upstream transcript NVE145.

Other examples appear of split transcripts. For instance, NVE8208 and NVE8209 on scaffold 2 match to different parts of the same human protein, [ATG7_HUMAN](https://www.uniprot.org/uniprot/O95352) at positions 603-703 and 1-500, respectively. As the blast matches are in order and not overlapping, it is likely that these are the same transcript, rather than tandem duplicates.

### GMAP of transcriptomes
Downloaded [HADN01 transcript set](https://www.ncbi.nlm.nih.gov/Traces/wgs/HADN01) of 118377 transcripts, mean 1003bp, by [Babonis 2016](https://doi.org/10.1186/s12862-016-0683-3). This set combined all tissues, and was assembled from all the trimmed reads from single replicate of each tissue. First, rename the sequence headers from the formatted ones on NCBI:

`fastarenamer.py -r "|" -s 5 HADN01.1.fsa_nt | sed s/,//g > babonis2016_renamed_HADN01.1.fsa`

`~/gmap-2019-09-12/bin/gmap_build -d Nemve1.fasta Nemve1.fasta`

`~/gmap-2019-09-12/bin/gmap -d Nemve1.fasta -B 5 -t 4 -f 2 -n 1 -p 3 --max-intronlength-middle=10000 --split-large-introns babonis2016_renamed_HADN01.1.fsa > babonis2016_renamed_HADN01.gmap.gff 2> babonis2016_renamed_HADN01.gmap.log`

Again, link and make the track:

`ln -s /mnt/data/project/genomes/nematostella_vectensis/babonis2016_renamed_HADN01.gmap.gff`

`/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff babonis2016_renamed_HADN01.gmap.gff --trackType CanvasFeatures --trackLabel Trinity-HADN01`

### RNAseq expression levels of developmental series
From [Characterization of differential transcript abundance through time during Nematostella vectensis development](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA189768) by [Helm 2013](https://doi.org/10.1186/1471-2164-14-266), first download the data from SRA:

`~/sratoolkit.2.9.6-1-ubuntu64/bin/fastq-dump --gzip --defline-seq '@$sn[_$rn]/$ri' SRR765902 SRR765906 SRR765907 SRR765909 SRR765897 SRR765898 SRR765899 SRR765900 SRR765901 SRR765904 SRR765905 SRR765911`

For organization, rename all files by time point:

```
mv SRR765897.fastq.gz 00h_SRR765897.fastq.gz
mv SRR765898.fastq.gz 06h_SRR765898.fastq.gz
mv SRR765899.fastq.gz 12h_SRR765899.fastq.gz
mv SRR765900.fastq.gz 24h_SRR765900.fastq.gz
mv SRR765901.fastq.gz 5d_SRR765901.fastq.gz
mv SRR765902.fastq.gz 10d_SRR765902.fastq.gz
mv SRR765904.fastq.gz 00h_SRR765904.fastq.gz
mv SRR765905.fastq.gz 06h_SRR765905.fastq.gz
mv SRR765906.fastq.gz 12h_SRR765906.fastq.gz
mv SRR765907.fastq.gz 24h_SRR765907.fastq.gz
mv SRR765909.fastq.gz 5d_SRR765909.fastq.gz
mv SRR765911.fastq.gz 10d_SRR765911.fastq.gz
```

Create hisat2 build, then map while writing appending all STDERR to the same log file:

`~/hisat2-2.1.0/hisat2-build Nemve1.fasta Nemve1.fasta`

```
~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -U 00h_SRR765897.fastq.gz,00h_SRR765904.fastq.gz -S h00_helm2013_on_Nemve1.sam -p 4 2>> helm2013_on_Nemve1.log
~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -U 06h_SRR765898.fastq.gz,06h_SRR765905.fastq.gz -S h06_helm2013_on_Nemve1.sam -p 4 2>> helm2013_on_Nemve1.log
~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -U 12h_SRR765899.fastq.gz,12h_SRR765906.fastq.gz -S h12_helm2013_on_Nemve1.sam -p 4 2>> helm2013_on_Nemve1.log
~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -U 24h_SRR765900.fastq.gz,24h_SRR765907.fastq.gz -S h24_helm2013_on_Nemve1.sam -p 4 2>> helm2013_on_Nemve1.log
~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -U 5d_SRR765901.fastq.gz,5d_SRR765909.fastq.gz -S d5_helm2013_on_Nemve1.sam -p 4 2>> helm2013_on_Nemve1.log
~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -U 10d_SRR765902.fastq.gz,10d_SRR765911.fastq.gz -S d10_helm2013_on_Nemve1.sam -p 4 2>> helm2013_on_Nemve1.log
```

Mapping rate ranged from 56-77%. Then create sorted bams, bedgraphs, and bigwigs of each of the 6 conditions. This requires [samtools](https://github.com/samtools/samtools/releases), [bedtools2](https://github.com/arq5x/bedtools2), and the [UCSC binary utilities](http://hgdownload.soe.ucsc.edu/admin/exe/) `bedsort` and `bedGraphToBigWig`. A tab-delimited list of contigs and their lengths must also be generated.

`sizecutter.py -f Nemve1.fasta | sed s/" "/"\t"/ > Nemve1.sizes`

`for FILE in *.sam ; do BASE="${FILE%.sam}" ; ~/samtools-1.9/samtools sort $FILE -o $BASE.sorted.bam ; ~/bedtools2/bin/bedtools genomecov -ibam $BASE.sorted.bam -bg -split > $BASE.bg ; ~/ucsc/bedSort $BASE.bg $BASE.bg ; ~/ucsc/bedGraphToBigWig $BASE.bg Nemve1.sizes $BASE.cov.bw ; done`

Then link the files, and add the track objects to `trackList.json`:

```
ln -s /mnt/data/project/genomes/nematostella_vectensis/h00_helm2013_on_Nemve1.cov.bw
ln -s /mnt/data/project/genomes/nematostella_vectensis/h06_helm2013_on_Nemve1.cov.bw
ln -s /mnt/data/project/genomes/nematostella_vectensis/h12_helm2013_on_Nemve1.cov.bw
ln -s /mnt/data/project/genomes/nematostella_vectensis/h24_helm2013_on_Nemve1.cov.bw 
ln -s /mnt/data/project/genomes/nematostella_vectensis/d5_helm2013_on_Nemve1.cov.bw 
ln -s /mnt/data/project/genomes/nematostella_vectensis/d10_helm2013_on_Nemve1.cov.bw 
```

### RNAseq from different tissues
From [Do novel genes drive morphological novelty? An investigation of the nematosomes in the sea anemone Nematostella vectensis](https://www.ncbi.nlm.nih.gov/bioproject/PRJEB13676) by [Babonis 2016](https://doi.org/10.1186/s12862-016-0683-3):

`~/sratoolkit.2.9.2-ubuntu64/bin/fastq-dump --gzip --split-files --defline-seq '@$sn[_$rn]/$ri' ERR1368838 ERR1368845 ERR1368846 ERR1368847 ERR1368848 ERR1368849 ERR1368850 ERR1368851 ERR1368852`

Rename all files, by tentacles, nematosomes, or mesentery:

```
mv ERR1368838_1.fastq.gz ten_ERR1368838_1.fastq.gz
mv ERR1368838_2.fastq.gz ten_ERR1368838_2.fastq.gz
mv ERR1368845_1.fastq.gz nem_ERR1368845_1.fastq.gz
mv ERR1368845_2.fastq.gz nem_ERR1368845_2.fastq.gz
mv ERR1368846_1.fastq.gz mes_ERR1368846_1.fastq.gz
mv ERR1368846_2.fastq.gz mes_ERR1368846_2.fastq.gz
mv ERR1368847_1.fastq.gz mes_ERR1368847_1.fastq.gz
mv ERR1368847_2.fastq.gz mes_ERR1368847_2.fastq.gz
mv ERR1368848_1.fastq.gz nem_ERR1368848_1.fastq.gz
mv ERR1368848_2.fastq.gz nem_ERR1368848_2.fastq.gz
mv ERR1368849_1.fastq.gz nem_ERR1368849_1.fastq.gz
mv ERR1368849_2.fastq.gz nem_ERR1368849_2.fastq.gz
mv ERR1368850_1.fastq.gz ten_ERR1368850_1.fastq.gz
mv ERR1368850_2.fastq.gz ten_ERR1368850_2.fastq.gz
mv ERR1368851_1.fastq.gz ten_ERR1368851_1.fastq.gz
mv ERR1368851_2.fastq.gz ten_ERR1368851_2.fastq.gz
mv ERR1368852_1.fastq.gz mes_ERR1368852_1.fastq.gz
mv ERR1368852_2.fastq.gz mes_ERR1368852_2.fastq.gz
```

Run hisat2, keeping the logs, and piping the output to [samtools](https://github.com/samtools/samtools/releases):

```
{ ~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -1 mes_ERR1368846_1.fastq.gz,mes_ERR1368847_1.fastq.gz,mes_ERR1368852_1.fastq.gz -2 mes_ERR1368846_2.fastq.gz,mes_ERR1368847_2.fastq.gz,mes_ERR1368852_2.fastq.gz -p 4 --dta --max-intronlen 10000 2>&3 | ~/samtools-1.8/samtools sort - -o mesentery_v_Nemve1_hisat2.bam; } 3> mesentery_v_Nemve1_hisat2.log
{ ~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -1 ten_ERR1368838_1.fastq.gz,ten_ERR1368850_1.fastq.gz,ten_ERR1368851_1.fastq.gz -2 ten_ERR1368838_2.fastq.gz,ten_ERR1368850_2.fastq.gz,ten_ERR1368851_2.fastq.gz -p 4 --dta --max-intronlen 10000 2>&3 | ~/samtools-1.8/samtools sort - -o tentacle_v_Nemve1_hisat2.bam; } 3> tentacle_v_Nemve1_hisat2.log
{ ~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -1 nem_ERR1368845_1.fastq.gz,nem_ERR1368848_1.fastq.gz,nem_ERR1368849_1.fastq.gz -2 nem_ERR1368845_2.fastq.gz,nem_ERR1368848_2.fastq.gz,nem_ERR1368849_2.fastq.gz -p 4 --dta --max-intronlen 10000 2>&3 | ~/samtools-1.8/samtools sort - -o nematocyst_v_Nemve1_hisat2.bam; } 3> nematocyst_v_Nemve1_hisat2.log
```

Convert the sorted bams to bedgraphs, then to bigwigs:

```
~/bedtools2/bin/bedtools genomecov -ibam mesentery_v_Nemve1_hisat2.bam -bg -split > mesentery_v_Nemve1_hisat2.bg
~/bedtools2/bin/bedtools genomecov -ibam tentacle_v_Nemve1_hisat2.bam -bg -split > tentacle_v_Nemve1_hisat2.bg
~/bedtools2/bin/bedtools genomecov -ibam nematocyst_v_Nemve1_hisat2.bam -bg -split > nematocyst_v_Nemve1_hisat2.bg

~/ucsc/bedSort mesentery_v_Nemve1_hisat2.bg mesentery_v_Nemve1_hisat2.bg
~/ucsc/bedSort tentacle_v_Nemve1_hisat2.bg tentacle_v_Nemve1_hisat2.bg
~/ucsc/bedSort nematocyst_v_Nemve1_hisat2.bg nematocyst_v_Nemve1_hisat2.bg

~/ucsc/bedGraphToBigWig mesentery_v_Nemve1_hisat2.bg Nemve1.sizes mesentery_v_Nemve1_hisat2.cov.bw
~/ucsc/bedGraphToBigWig tentacle_v_Nemve1_hisat2.bg Nemve1.sizes tentacle_v_Nemve1_hisat2.cov.bw
~/ucsc/bedGraphToBigWig nematocyst_v_Nemve1_hisat2.bg Nemve1.sizes nematocyst_v_Nemve1_hisat2.cov.bw
```

Link and add the features:

```
ln -s /mnt/data/genomes/nematostella_vectensis/mesentery_v_Nemve1_hisat2.cov.bw
ln -s /mnt/data/genomes/nematostella_vectensis/tentacle_v_Nemve1_hisat2.cov.bw 
ln -s /mnt/data/genomes/nematostella_vectensis/nematocyst_v_Nemve1_hisat2.cov.bw 
```


