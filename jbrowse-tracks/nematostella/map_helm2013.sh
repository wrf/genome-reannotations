#!/bin/bash

~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -U 00h_SRR765897.fastq.gz,00h_SRR765904.fastq.gz -S h00_helm2013_on_Nemve1.sam -p 4 2>> helm2013_on_Nemve1.log
~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -U 06h_SRR765898.fastq.gz,06h_SRR765905.fastq.gz -S h06_helm2013_on_Nemve1.sam -p 4 2>> helm2013_on_Nemve1.log
~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -U 12h_SRR765899.fastq.gz,12h_SRR765906.fastq.gz -S h12_helm2013_on_Nemve1.sam -p 4 2>> helm2013_on_Nemve1.log
~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -U 24h_SRR765900.fastq.gz,24h_SRR765907.fastq.gz -S h24_helm2013_on_Nemve1.sam -p 4 2>> helm2013_on_Nemve1.log
~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -U 5d_SRR765901.fastq.gz,5d_SRR765909.fastq.gz -S d5_helm2013_on_Nemve1.sam -p 4 2>> helm2013_on_Nemve1.log
~/hisat2-2.1.0/hisat2 -q -x Nemve1.fasta -U 10d_SRR765902.fastq.gz,10d_SRR765911.fastq.gz -S d10_helm2013_on_Nemve1.sam -p 4 2>> helm2013_on_Nemve1.log


