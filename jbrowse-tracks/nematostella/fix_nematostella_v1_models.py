#!/usr/bin/env python

'''
fix_nematostella_v1_models.py Nemve1.FilteredModels1.gff > Nemve1.FilteredModels1.for_jbrowse.gff
'''

import sys
import re
import time
from collections import defaultdict,Counter

#scaffold_1	JGI	exon	25970	26135	.	-	.	name "fgenesh1_pg.scaffold_1000005"; transcriptId 196074
#scaffold_1	JGI	CDS	25970	26135	.	-	0	name "fgenesh1_pg.scaffold_1000005"; proteinId 196074; exonNumber 8
#scaffold_1	JGI	stop_codon	25970	25972	.	-	0	name "fgenesh1_pg.scaffold_1000005"
#scaffold_1	JGI	exon	29363	29540	.	-	.	name "fgenesh1_pg.scaffold_1000005"; transcriptId 196074
#scaffold_1	JGI	CDS	29363	29540	.	-	1	name "fgenesh1_pg.scaffold_1000005"; proteinId 196074; exonNumber 7

if len(sys.argv) < 2:
	print >> sys.stderr, __doc__
else:
	ID_order = defaultdict(dict) # key is scaffold, then ID, then start position
	feature_dict = defaultdict(list)
	transid_to_name = {} # key is transcriptId, value is name
	linecounter = 0
	print >> sys.stderr, "# Reading features from {}".format(sys.argv[1]), time.asctime()
	for line in open(sys.argv[1],'r'):
		line = line.strip()
		if line and line[0]!="#":
			linecounter += 1
			lsplits = line.split("\t")
			scaffold = lsplits[0]
			program = lsplits[1]
			feature = lsplits[2]

			start = int(lsplits[3])
			attributes = lsplits[8]
			genename = re.search('name "([\w.|-]+)"', attributes).group(1)
			if attributes.find("transcriptId")>=0: # gff3 format but no ID
				geneid = re.search('transcriptId ([\w.|-]+)', attributes).group(1)
				transid_to_name[geneid] = genename
			elif attributes.find("proteinId")>=0: # gff3 format but no ID
				geneid = re.search('proteinId ([\w.|-]+)', attributes).group(1)
			lsplits[8] = "Parent=t{}".format( geneid )
			if geneid not in ID_order[scaffold]:
				ID_order[scaffold][geneid] = start
			else:
				if ID_order[scaffold].get(geneid) > start:
					ID_order[scaffold][geneid] = start
			feature_dict[geneid].append(lsplits)
	print >> sys.stderr, "# Counted {} lines for {} features".format(linecounter, len(feature_dict))
	print >> sys.stderr, "# Generating parent features", time.asctime()
	for scaffold in sorted(ID_order.keys()):
		for geneid, pos in sorted(ID_order[scaffold].items(), key=lambda x: x[1]):
			allstarts = map(int,[ls[3] for ls in feature_dict[geneid]])
			allends = map(int,[ls[4] for ls in feature_dict[geneid]])
			parentstart = min(allstarts)
			parentend = max(allends)
			try:
				topscore = max(map(int,[ls[5] for ls in feature_dict[geneid]]))
			except ValueError:
				topscore = 0
			mainstrand = Counter([ls[6] for ls in feature_dict[geneid]]).most_common(1)[0][0]
			outline = "{0}\t{1}\tmRNA\t{2}\t{3}\t{4}\t{5}\t.\tID=t{6};Name={7}".format( scaffold, program, parentstart, parentend, topscore, mainstrand, geneid, transid_to_name.get(geneid,geneid) )
			print >> sys.stdout, outline
			for outsplits in feature_dict.get(geneid):
				outline = "\t".join(outsplits)
				print >> sys.stdout, outline
