# Trichoplax processing steps

### Fixing scaffolds from JGI
The [original JGI scaffolds](https://genome.jgi.doe.gov/Triad1/Triad1.home.html), for whatever reason, contain null characters in two of them. This is diagnosed with `contigcomposition.py` ([current version here](https://bitbucket.org/wrf/sequences/src/master/contigcomposition.py)), the output of the first 10 lines is shown below:

```
scaffold_1 Counter({'T': 4100653, 'A': 4086407, 'G': 2003365, 'C': 2000176, 'N': 1070103})
scaffold_2 Counter({'A': 2999995, 'T': 2994020, 'C': 1459224, 'G': 1458924, 'N': 827380})
scaffold_3 Counter({'A': 2955610, 'T': 2954822, 'C': 1445727, 'G': 1443243, 'N': 861725})
scaffold_4 Counter({'T': 2649945, 'A': 2649143, 'G': 1297172, 'C': 1295023, 'N': 273109})
scaffold_5 Counter({'T': 2499031, 'A': 2494223, 'G': 1213426, 'C': 1208731, 'N': 1157702})
scaffold_6 Counter({'T': 1932772, 'A': 1928359, 'G': 943021, 'C': 942824, 'N': 231682})
scaffold_7 Counter({'T': 1710408, 'A': 1704555, 'C': 829596, 'G': 829211, 'N': 808087})
scaffold_8 Counter({'T': 933223, 'A': 929518, 'N': 596026, 'G': 454558, 'C': 453786, '\x00': 8192})
scaffold_9 Counter({'A': 928026, 'T': 927053, 'N': 461677, 'G': 448778, 'C': 445179})
scaffold_10 Counter({'A': 768980, 'T': 768387, 'C': 374824, 'G': 370903, 'N': 189771})
```

scaffold 8 contains 8192 null (`\x00`) characters. These still count as characters in some strings for some programs, and eventually make errors.

These are removed with `sed`, and scaffolds are reset to 60 characters per line for indexing.

`cat Triad1_genomic_scaffolds.fasta | sed s/"\x00"//g | sizecutter.py -p - > Triad1_genomic_scaffolds.corrected.fa`

`mv Triad1_genomic_scaffolds.fasta Triad1_genomic_scaffolds_original_w_null.fasta`

`mv Triad1_genomic_scaffolds.corrected.fa Triad1_genomic_scaffolds.fasta`

### Process scaffolds for display

```
~/samtools-1.8/samtools faidx Triad1_genomic_scaffolds.fasta

ln -s /mnt/data/genomes/trichoplax_adhaerens/Triad1_genomic_scaffolds.fasta Triad1_genomic_scaffolds.fasta
ln -s /mnt/data/genomes/trichoplax_adhaerens/Triad1_genomic_scaffolds.fasta.fai Triad1_genomic_scaffolds.fasta.fai

/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta Triad1_genomic_scaffolds.fasta --out ./
```

The gap track can be produced with `repeat2gtf.py`, using the option `-l`. Strangely, the longest gap is 434kb, far longer than any mate pair used in the study.

`~/git/genomeGTFtools/repeat2gtf.py -l Triad1_genomic_scaffolds.fasta > Triad1_genomic_scaffolds.gaps.gff`

### Prepare original annotation
The original annotations are in a exon-CDS format, which cannot be used with jbrowse:
```
scaffold_1	JGI	exon	29779	29920	.	-	.	name "fgeneshTA2_pg.C_scaffold_1000001"; transcriptId 51254
scaffold_1	JGI	CDS	29779	29920	.	-	0	name "fgeneshTA2_pg.C_scaffold_1000001"; proteinId 51254; exonNumber 8
scaffold_1	JGI	stop_codon	29779	29781	.	-	0	name "fgeneshTA2_pg.C_scaffold_1000001"
scaffold_1	JGI	exon	30240	30323	.	-	.	name "fgeneshTA2_pg.C_scaffold_1000001"; transcriptId 51254
scaffold_1	JGI	CDS	30240	30323	.	-	1	name "fgeneshTA2_pg.C_scaffold_1000001"; proteinId 51254; exonNumber 7
```

These need to be converted to a GFF3 type format:

`./fix_trichoplax_gff.py Triad1_best_genes.gff > Triad1_best_genes.fixed.gff`

and [parent features]() need to be created:

`~/git/genomeGTFtools/make_parent_features.py Triad1_best_genes.fixed.gff > Triad1_best_genes_w_parent.gff`

Then prepare the tracks for jbrowse:

```
ln -s /mnt/data/genomes/trichoplax_adhaerens/Triad1_best_genes_w_parent.gff Triad1_best_genes_w_parent.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Triad1_best_genes_w_parent.gff --trackType CanvasFeatures --trackLabel JGI-models
```

### Prepare AUGUSTUS
Lastly, the [new AUGUSTUS models](https://bitbucket.org/wrf/genome-reannotations/downloads/Trichoplax_scaffolds_JGI_AUGUSTUS_maxtrack2.gff.gz) are included for comparison. Comment lines are removed and `transcript` features are renamed to `mRNA` with `sed`.

`reformatgff.py -C Trichoplax_scaffolds_JGI_AUGUSTUS_maxtrack2.gff | sed s/transcript/mRNA/ > Trichoplax_scaffolds_JGI_AUGUSTUS_no_comment.gff`

Again, prepare the tracks for jbrowse:

```
ln -s /mnt/data/genomes/trichoplax_adhaerens/Trichoplax_scaffolds_JGI_AUGUSTUS_no_comment.gff Trichoplax_scaffolds_JGI_AUGUSTUS_no_comment.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Trichoplax_scaffolds_JGI_AUGUSTUS_no_comment.gff --trackType CanvasFeatures --trackLabel AUGUSTUS
```

### Map RNAseq
Using RNAseq from [ERR2215729](https://www.ncbi.nlm.nih.gov/sra/ERX2270206[accn])

`~/sratoolkit.2.9.2-ubuntu64/bin/fastq-dump --split-files --gzip --defline-seq '@$sn[_$rn]/$ri' ERR2215729`

`hisat2-build Triad1_genomic_scaffolds.fasta Triad1_genomic_scaffolds.fasta`

`hisat2 -q -x Triad1_genomic_scaffolds.fasta -1 ERR2215729_1.fastq.gz -2 ERR2215729_2.fastq.gz -S ERR2215729_vs_Triad1_hisat.sam -p 6 --dta 2> ERR2215729_vs_Triad1_hisat.log`

`~/samtools-1.8/samtools view -Su ERR2215729_vs_Triad1_hisat.sam | ~/samtools-1.8/samtools sort - -o ERR2215729_vs_Triad1_hisat.sorted.bam`

`~/bedtools2/bin/bedtools genomecov -ibam ERR2215729_vs_Triad1_hisat.sorted.bam -bg -split > ERR2215729_vs_Triad1_hisat.sorted.bam.cov`

`~/ucsc/bedSort ERR2215729_vs_Triad1_hisat.sorted.bam.cov ERR2215729_vs_Triad1_hisat.sorted.bam.cov`

`~/ucsc/bedGraphToBigWig ERR2215729_vs_Triad1_hisat.sorted.bam.cov Triad1_genomic_scaffolds.sizes ERR2215729_vs_Triad1_hisat.bw`

`~/stringtie-1.3.4d.Linux_x86_64/stringtie ERR2215729_vs_Triad1_hisat.sorted.bam -l Triad1_rnaseq -p 2 -o ERR2215729_vs_Triad1.stringtie.gtf`

`~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py ERR2215729_vs_Triad1.stringtie.gtf > ERR2215729_vs_Triad1.stringtie.gff`

Lastly, prepare the tracks for jbrowse:

```
ln -s /mnt/data/genomes/trichoplax_adhaerens/ERR2215729_vs_Triad1_hisat.bw ERR2215729_vs_Triad1_hisat.bw
ln -s /mnt/data/genomes/trichoplax_adhaerens/ERR2215729_vs_Triad1.stringtie.gff ERR2215729_vs_Triad1.stringtie.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff ERR2215729_vs_Triad1.stringtie.gff --trackType CanvasFeatures --trackLabel StringTie
```

### blastp vs Human
`blastp -query Trichoplax_scaffolds_JGI_AUGUSTUS_maxtrack2.prot_no_rename.fasta -db ~/db/human_uniprot.fasta -evalue 1e-3 -max_target_seqs 5 -num_threads 4 -outfmt 6 > Trichoplax_AUGUSTUS_maxtrack2.prots.vs_human_blastp.tab`

`~/git/genomeGTFtools/blast2genomegff.py -b Trichoplax_AUGUSTUS_maxtrack2.prots.vs_human_blastp.tab -p blastp -S -g Trichoplax_scaffolds_JGI_AUGUSTUS_no_comment.gff -d ~/db/human_uniprot.fasta -x -G --gff-delimiter "." > Trichoplax_AUGUSTUS_maxtrack2.prots.vs_human_blastp.gff`

### EST mapping
14000 ESTs [downloaded from JGI](https://genome.jgi.doe.gov/Triad1/Triad1.download.html)

`hisat2 -f -x Triad1_genomic_scaffolds.fasta -U Triad1_ESTs.fasta.gz -p 4 --max-intronlen 30000 -S Triad1_ESTs.sam 2> Triad1_ESTs_hisat2.log`

`~/samtools-1.9/samtools view -Su Triad1_ESTs.sam | ~/samtools-1.9/samtools sort - -o Triad1_ESTs.sorted.bam`

`~/samtools-1.9/samtools index Triad1_ESTs.sorted.bam`

```
ln -s /mnt/data/genomes/trichoplax_adhaerens/Triad1_ESTs.sorted.bam Triad1_ESTs.sorted.bam
ln -s /mnt/data/genomes/trichoplax_adhaerens/Triad1_ESTs.sorted.bam.bai Triad1_ESTs.sorted.bam.bai
```

### Expansion of NaV channels ###
Plot was generated in April 2020, but much of this was discussed in the paper by [Romanova Oct 2020](https://doi.org/10.1016/j.bbrc.2020.08.020). They chose to show a useless circular phylogeny, and also did not include the gene structure, which shows the placozoan NaV channels as tandem duplicates.

![placozoan_naV_channels_loci.v1.png](https://bitbucket.org/wrf/genome-reannotations/raw/d1838aa58614df99c14e94fe4dcaea069658c42a/jbrowse-tracks/trichoplax/placozoan_naV_channels_loci.v1.png)

```
~/git/genomeGTFtools/extract_coordinates.py -s contig_114_length_245376 -b 1 -e 100000 -G -g ~/genomes/hoilungia-genome/tracks/PlacoH13_BRAKER1_augustus_no_comment.gff > hoilungia_ctg114_0-100k_short_annot.tab
Rscript ~/git/genomeGTFtools/draw_annotation_blocks.R hoilungia_ctg114_0-100k_short_annot.tab
~/git/genomeGTFtools/extract_coordinates.py -s scaffold_3 -b 6240000 -e 6340000 -G -g ~/genomes/trichoplax_adhaerens/Trichoplax_scaffolds_JGI_AUGUSTUS_no_comment.gff > trichoplax_scf3_6240k-6340k_short_annot.tab
Rscript ~/git/genomeGTFtools/draw_annotation_blocks.R trichoplax_scf3_6240k-6340k_short_annot.tab

~/git/genomeGTFtools/extract_coordinates.py -s scaffold_3 -b 6455000 -e 6555000 -G -g ~/genomes/trichoplax_adhaerens/Trichoplax_scaffolds_JGI_AUGUSTUS_no_comment.gff > trichoplax_scf3_6455k-6555k_short_annot.tab
Rscript ~/git/genomeGTFtools/draw_annotation_blocks.R trichoplax_scf3_6455k-6555k_short_annot.tab
~/git/genomeGTFtools/extract_coordinates.py -s contig_180_length_137249 -b 1 -e 100000 -G -g ~/genomes/hoilungia-genome/tracks/PlacoH13_BRAKER1_augustus_no_comment.gff | sed s/".t1"//g > hoilungia_ctg180_0-100k_short_annot.tab
Rscript ~/git/genomeGTFtools/draw_annotation_blocks.R hoilungia_ctg180_0-100k_short_annot.tab
```

As mentioned above, the NaV locus on the *Trichoplax* scaffold contains a 100kb long gap splitting the two clusters, longer than any of the mate pairs. It is not clear how this came to be in the assembly.

