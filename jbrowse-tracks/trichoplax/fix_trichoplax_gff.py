#!/usr/bin/env python

'''
fix_trichoplax_gff.py Triad1_best_genes.gff > Triad1_best_genes.fixed.gff
'''

import sys

if len(sys.argv) < 2:
	print >> sys.stderr, __doc__
else:
	for line in open(sys.argv[1],'r'):
		line = line.strip()
		if line and line[0]!="#":
			lsplits = line.split("\t")
			feature = lsplits[2]
			attributes = lsplits[8]
			genename = attributes.split('"')[1].split(".")[0]
			attributes = attributes.replace('"','')
			attributes = attributes.replace('name ','Name=')
			attributes = attributes.replace('transcriptId ','ID=')
			attributes = attributes.replace('proteinId ','ID={}'.format(genename))
			attributes = attributes.replace('exonNumber ','exonNumber=')
			if feature=="CDS":
				lsplits[8] = attributes.split(";",1)[1].strip()
				print >> sys.stdout, "\t".join(lsplits)
