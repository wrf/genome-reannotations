# [Mnemiopsis leidyi processing steps](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/mnemiopsis/)

![ML0857_screenshot.png](https://bitbucket.org/wrf/genome-reannotations/raw/cee485fac6cd8b6327ed21f9bc46b5cbdf26c191/jbrowse-tracks/mnemiopsis/ML0857_screenshot.png)

# [Pleurobrachia bachei processing steps](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/pleurobrachia/)

![pbac_rnaseq_mapping_screenshot.png](https://bitbucket.org/wrf/genome-reannotations/raw/333e3f5c43b6f3a905eaf70e2f6a12f00a88db15/jbrowse-tracks/pleurobrachia/pbac_rnaseq_mapping_screenshot.png)

# [Trichoplax adhaerens processing steps](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/trichoplax/)

![Triad1_screenshot.png](https://bitbucket.org/wrf/genome-reannotations/raw/cee485fac6cd8b6327ed21f9bc46b5cbdf26c191/jbrowse-tracks/trichoplax/Triad1_screenshot.png)

# [Renilla muelleri processing steps](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/renilla/)

![renilla_jbrowse_screenshot.png](https://bitbucket.org/wrf/genome-reannotations/raw/33b663495bd5dd9b840ec21a217830eb87643204/jbrowse-tracks/renilla/renilla_jbrowse_screenshot.png)

# [Rhopilema esculentum processing steps](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/rhopilema/)

![rhopilema_jbrowse_screenshot.png](https://bitbucket.org/wrf/genome-reannotations/raw/333e3f5c43b6f3a905eaf70e2f6a12f00a88db15/jbrowse-tracks/rhopilema/rhopilema_jbrowse_screenshot.png)

# [Nematostella vectensis processing steps](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/nematostella/)

![Nemve1_jbrowse_screenshot.png](https://bitbucket.org/wrf/genome-reannotations/raw/706581d3c812042da32b6e55e32a44872d6d4008/jbrowse-tracks/nematostella/Nemve1_jbrowse_screenshot.png)

# [Branchiostoma floridae processing steps](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/branchiostoma/)

![brafl2_screenshot.png](https://bitbucket.org/wrf/genome-reannotations/raw/7d6aba68fb4c70ba442c3879eda64b888d0618bf/jbrowse-tracks/branchiostoma/brafl2_screenshot.png)

# [Amphimedon queenslandica processing steps](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/amphimedon/)

![amphimedon_jbrowse_screenshot.png](https://bitbucket.org/wrf/genome-reannotations/raw/333e3f5c43b6f3a905eaf70e2f6a12f00a88db15/jbrowse-tracks/amphimedon/amphimedon_jbrowse_screenshot.png)

# [Sycon ciliatum processing steps](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/sycon/)

![sycon_jbrowse_screenshot.png](https://bitbucket.org/wrf/genome-reannotations/raw/333e3f5c43b6f3a905eaf70e2f6a12f00a88db15/jbrowse-tracks/sycon/sycon_jbrowse_screenshot.png)

# [Aphrocallistes vastus processing steps](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/aphrocallistes/)

![screenshot of Aphrocallistes browser](https://bitbucket.org/wrf/genome-reannotations/raw/e199e2096df9a2b580cd533e12c6bcd1689ffb5e/jbrowse-tracks/aphrocallistes/avas_jbrowse_screenshot.png)

# [Oopsacas minuta processing steps](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/oopsacas/)

![Oopsacas preprint browser screenshot](https://bitbucket.org/wrf/genome-reannotations/raw/759fdb3a693064ddf83d344179a89fbdb443fdeb/jbrowse-tracks/oopsacas/oopsacas_preprint_browser_screenshot.png)

# db management #

```
getAinB.py _HUMAN uniprot_sprot.fasta -s > human_uniprot.fasta
fastarenamer.py -d "GN=" -s 1 human_uniprot.fasta > human_uniprot.renamed_by_gene.fasta
getAinB.py _YEAST uniprot_sprot.fasta -s > yeast_uniprot.fasta
getAinB.py _MOUSE uniprot_sprot.fasta -s > mouse_uniprot.fasta
getAinB.py cnidarian_taxa_list uniprot_sprot.fasta -s -v > cnidarian_swissprots.fasta
cat model_organism_uniprot.fasta cnidarian_swissprots.fasta > model_organism_uniprot.w_cnido.fasta
```

To get species counts:

`grep ">" uniprot_sprot.fasta | cut -d "_" -f 2 | cut -d " " -f 1 | sort | uniq -c | sort -n > species_counts_uniprot.txt`


