# Oikopleura dioica #
Genome of the tunicate *Oikopleura dioica*, by [Denoeud 2010](https://pubmed.ncbi.nlm.nih.gov/21097902/).

## prepare scaffolds ##
Download scaffolds [from NCBI](https://www.ncbi.nlm.nih.gov/genome/368), which appear to be version 1 and contains 1260 scaffolds for a total of 70.4Mb.

```
wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/209/535/GCA_000209535.1_ASM20953v1/GCA_000209535.1_ASM20953v1_genomic.fna.gz
wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/209/535/GCA_000209535.1_ASM20953v1/GCA_000209535.1_ASM20953v1_protein.faa.gz
wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/209/535/GCA_000209535.1_ASM20953v1/GCA_000209535.1_ASM20953v1_genomic.gff.gz
```

`~/samtools-1.14/samtools faidx GCA_000209535.1_ASM20953v1_genomic.fna`

Make gap track:

```
~/git/genomeGTFtools/repeat2gtf.py -l GCA_000209535.1_ASM20953v1_genomic.fna > GCA_000209535.1_ASM20953v1_genomic.n_gaps.gff
# Counted 1260 sequences
# Counted 4683 repeats of 3938490 total bases, average 841.02
# Longest repeat was 9490 bases on FN653292.1
```

## map trinity transcripts ##
From [NCBI GKAI00000000.1](https://www.ncbi.nlm.nih.gov/nuccore/GKAI00000000.1), including 19749 sequences

```
wget https://sra-download.ncbi.nlm.nih.gov/traces/wgs03/wgs_aux/GK/AI/GKAI01/GKAI01.1.fsa_nt.gz
~/minimap2-2.23_x64-linux/minimap2
```

