# Xenia sp #
Genome of the octocoral *Xenia sp*, by [Hu 2020](https://doi.org/10.1038/s41586-020-2385-7).

## prepare scaffolds ##
The bulk of the 222Mb assembly (92%) is contained within 15 scaffolds 2Mb or longer, with another 12 scaffolds 500kb or longer that probably represent arms of chromosomes.

`~/samtools-1.9/samtools faidx xenSp1.scaffolds.fa`

Make gap track:

`~/git/genomeGTFtools/repeat2gtf.py -l xenSp1.scaffolds.fa > xenSp1.scaffolds.n_gaps.gff`

```
# Counted 388 repeats of 194000 total bases, average 500.00
# Longest repeat was 500 bases on HiC_scaffold_1
```

## blast against models ##

`~/diamond-latest/diamond blastp -q xenSp1.proteins.fa -d ~/db/model_organism_uniprot.w_cnido.fasta -o xenSp1.proteins.vs_models.tab`

`~/git/genomeGTFtools/blast2genomegff.py -b xenSp1.proteins.vs_models.tab -g xenSp1.gff3 -S --add-description --add-accession -d ~/db/model_organism_uniprot.w_cnido.fasta -P -G -F "." -x -K -p blastp > xenSp1.proteins.vs_models.gff`

## microsynteny with Renilla ##
Using the script [microsynteny.py](https://github.com/wrf/genomeGTFtools/blob/master/microsynteny.py).

## microsynteny with Dendronephthya ##
Using the data from the octocoral [Dendronephthya gigantea](http://www.jellyfishgenome.com/index.php/%27%27Dendronephthya_gigantea%27%27_genome), by [Jeon 2019](https://doi.org/10.1093/gbe/evz043)
The format of the GFF is inconvenient for downstream analyses, so the subset of features for proteins are extracted:

`./filter_dengig_gff_cds.py GCF_004324835.1_DenGig_1.0_genomic.gff > GCF_004324835.1_DenGig_1.0_genomic.cds_only.gff`	

`~/git/genomeGTFtools/microsynteny.py -q anno_1_nocomments_cds.gff -d ~/genomes/dendronephthya_gigantea/GCF_004324835.1_DenGig_1.0_genomic.cds_only.gff -b renilla_augustus_vs_dengig1.tab -c -G > renilla_augustus_vs_dengig1.microsynteny.gff`

## plotting loci ##

```
~/git/genomeGTFtools/extract_coordinates.py -s HiC_scaffold_21 -b 975000 -e 985000 -g xenSp1.gff3 > xenia_sc21_975k_rluc_locus_short_annot.tab
~/git/genomeGTFtools/extract_coordinates.py -s HiC_scaffold_21 -b 8446000 -e 8456000 -g xenSp1.gff3 > xenia_sc21_8446k_rluc_locus_short_annot.tab
~/git/genomeGTFtools/extract_coordinates.py -s HiC_scaffold_3 -b 7907000 -e 7927000 -g xenSp1.gff3 > xenia_sc3_7927k_gfp_locus_short_annot.tab
```

## making browser ##

```
cd ~/jbrowse_data/xenia
ln -s ~/genomes/xenia_sp_CNID/xenSp1.scaffolds.fa
ln -s ~/genomes/xenia_sp_CNID/xenSp1.scaffolds.fa.fai 
/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta xenSp1.gff3 --out ./
ln -s ~/genomes/xenia_sp_CNID/xenSp1.gff3 
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff xenSp1.gff3 --trackType CanvasFeatures  --trackLabel gene-models --out ./
ln -s ~/genomes/xenia_sp_CNID/xenSp1.proteins.vs_models.gff 
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff xenSp1.proteins.vs_models.gff --trackType CanvasFeatures  --trackLabel blast-v-models --out ./
```
