#!/usr/bin/env python
# clean_aque_ensembl_gff.py

'''
clean_aque_ensembl_gff.py Amphimedon_queenslandica.Aqu1.45.gff3 > Amphimedon_queenslandica.Aqu1.45.cleaned.gff
'''

import sys

if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	for line in open(sys.argv[1],'r'):
		line = line.strip()
		if line and line[0]!="#":
			lsplits = line.split("\t")
			feature = lsplits[2]
			if feature=="scaffold":
				continue
			attributes = lsplits[8]
			attributes = attributes.replace('gene:','')
			lsplits[8] = attributes
			outline = "{}\n".format( "\t".join(lsplits) )
			sys.stdout.write(outline)

