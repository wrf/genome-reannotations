# *Amphimedon queenslandica* #
Annotation for [*Amphimedon queenslandica*](http://metazoa.ensembl.org/Amphimedon_queenslandica/Info/Index), from [Srivastava 2010](https://doi.org/10.1038/nature09201). The author's website [https://amphimedon.qcloud.qcif.edu.au/](https://amphimedon.qcloud.qcif.edu.au/) no longer appears to be active.

## prepare stock data of Aqu1 assembly ##
Ensembl still hosts v1 of the assembly, with a newer gene set from [Fernandez-Valverde 2015](https://doi.org/10.1186/s12864-015-1588-z). The assembly is numbered with the smallest contigs first, which is in reverse order to the convention.

The gff format contains a few useless/annoying features, like the full `scaffold` features. Additionally, the gene IDs contain tags like `gene:` and `transcript:`, which are unnecessary since the gene ID and transcripts are already defined in the systematic numbering of the genes. Thus, these are cleaned up with the `clean_aque_ensembl_gff.py` script.

```
cd /mnt/data/project/genomes/amphimedon_queenslandica_PORI/
~/samtools-1.9/samtools faidx Amphimedon_queenslandica.Aqu1.dna.toplevel.fa
clean_aque_ensembl_gff.py Amphimedon_queenslandica.Aqu1.45.gff3 > Amphimedon_queenslandica.Aqu1.45.cleaned.gff
~/git/genomeGTFtools/repeat2gtf.py -l Amphimedon_queenslandica.Aqu1.dna.toplevel.fa > Amphimedon_queenslandica.Aqu1.dna.n_gaps.gff
cd ~/jbrowse/amphimedon/
ln -s /mnt/data/project/genomes/amphimedon_queenslandica_PORI/Amphimedon_queenslandica.Aqu1.dna.toplevel.fa
ln -s /mnt/data/project/genomes/amphimedon_queenslandica_PORI/Amphimedon_queenslandica.Aqu1.dna.toplevel.fa.fai
ln -s /mnt/data/project/genomes/amphimedon_queenslandica_PORI/Amphimedon_queenslandica.Aqu1.45.cleaned.gff
ln -s /mnt/data/project/genomes/amphimedon_queenslandica_PORI/Amphimedon_queenslandica.Aqu1.dna.n_gaps.gff
/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta Amphimedon_queenslandica.Aqu1.dna.toplevel.fa --out ./
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff Amphimedon_queenslandica.Aqu1.45.cleaned.gff --trackType CanvasFeatures  --trackLabel Aque-2.1 --out ./
/var/www/html/jbrowse/bin/generate-names.pl --tracks Aque-2.1 --out ./
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff Amphimedon_queenslandica.Aqu1.dna.n_gaps.gff --trackType CanvasFeatures --trackLabel n-gaps --out ./
```

## mapping ESTs ##
Current set of 63542 ESTs from [NCBI](https://www.ncbi.nlm.nih.gov/nuccore/?term=txid400682[Organism:noexp]), for 32Mb, average length of 517bp.

```
~/minimap2-2.23_x64-linux/minimap2 -a -x splice --secondary=no Amphimedon_queenslandica.Aqu1.dna.toplevel.fa aque_NCBI_ESTs.fasta | ~/samtools-1.14/samtools sort - -o aque_NCBI_ESTs.bam
~/samtools-1.14/samtools index aque_NCBI_ESTs.bam
```

## mapping 2015 txome data ##
Libraries are said to be [stranded](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA255066), possibly RF-direction, though it does not appear to be stated.

`{ ~/hisat2-2.2.1/hisat2 -q -x Amphimedon_queenslandica.Aqu1.dna.toplevel.fa -1 SRR1511618_1.fastq.gz,SRR1511619_1.fastq.gz,SRR1511620_1.fastq.gz,SRR1511621_1.fastq.gz -2 SRR1511618_2.fastq.gz,SRR1511619_2.fastq.gz,SRR1511620_2.fastq.gz,SRR1511621_2.fastq.gz -p 4 --dta --max-intronlen 20000 --rna-strandness RF 2>&3 | ~/samtools-1.14/samtools sort - -o aque_2015_txome_stranded_hisat2.bam; } 3> aque_2015_txome_stranded_hisat2.log`

```
170749049 reads; of these:
  170749049 (100.00%) were paired; of these:
    59574544 (34.89%) aligned concordantly 0 times
    107703418 (63.08%) aligned concordantly exactly 1 time
    3471087 (2.03%) aligned concordantly >1 times
    ----
    59574544 pairs aligned concordantly 0 times; of these:
      3747213 (6.29%) aligned discordantly 1 time
    ----
    55827331 pairs aligned 0 times concordantly or discordantly; of these:
      111654662 mates make up the pairs; of these:
        85125934 (76.24%) aligned 0 times
        24550064 (21.99%) aligned exactly 1 time
        1978664 (1.77%) aligned >1 times
75.07% overall alignment rate
```

```
$ ~/samtools-1.14/samtools flagstat aque_2015_txome_stranded_hisat2.bam
355345194 + 0 in total (QC-passed reads + QC-failed reads)
341498098 + 0 primary
13847096 + 0 secondary
0 + 0 supplementary
0 + 0 duplicates
0 + 0 primary duplicates
270219260 + 0 mapped (76.04% : N/A)
256372164 + 0 primary mapped (75.07% : N/A)
341498098 + 0 paired in sequencing
170749049 + 0 read1
170749049 + 0 read2
222349010 + 0 properly paired (65.11% : N/A)
237040372 + 0 with itself and mate mapped
19331792 + 0 singletons (5.66% : N/A)
3637928 + 0 with mate mapped to a different chr
3025487 + 0 with mate mapped to a different chr (mapQ>=5)
```

`~/stringtie-2.2.1.Linux_x86_64/stringtie -o aque_2015_txome_stranded_hisat2.stringtie.gtf --fr aque_2015_txome_stranded_hisat2.bam`

`~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py aque_2015_txome_stranded_hisat2.stringtie.gtf > aque_2015_txome_stranded_hisat2.stringtie.gff`

Clearly the transcripts are all flipped, and must be reversed, using the script [`reverse_gff_strand.py`](https://github.com/wrf/genomeGTFtools/blob/master/misc/reverse_gff_strand.py):

`~/git/genomeGTFtools/misc/reverse_gff_strand.py aque_2015_txome_stranded_hisat2.stringtie.gff > aque_2015_txome_stranded_hisat2.stringtie.rev.gff`

```
~/bedtools2/bin/bedtools genomecov -ibam aque_2015_txome_stranded_hisat2.bam -bg -split > aque_2015_txome_stranded_hisat2.bed
~/genomes/amphimedon_queenslandica_PORI$ ~/ucsc_genome_tools/bedSort aque_2015_txome_stranded_hisat2.bed aque_2015_txome_stranded_hisat2.bed
~/genomes/amphimedon_queenslandica_PORI$ ~/ucsc_genome_tools/bedGraphToBigWig aque_2015_txome_stranded_hisat2.bed Amphimedon_queenslandica.Aqu1.dna.toplevel.sizes aque_2015_txome_stranded_hisat2.bw
```

## blast against models ##

```
~/diamond-latest/diamond blastp -q Aqu2.1_Genes_proteins.fasta -d ~/db/model_organism_uniprot.w_cnido.fasta -o Aqu2.1_Genes_proteins.blastp_vs_models.tab
grep CDS Amphimedon_queenslandica.Aqu1.45.cleaned.gff | sed s/"CDS:"//g > Amphimedon_queenslandica.Aqu1.45.cds_only.gff
~/git/genomeGTFtools/blast2genomegff.py -b Aqu2.1_Genes_proteins.blastp_vs_models.tab -d ~/db/model_organism_uniprot.w_cnido.fasta -S --add-description --add-accession -P -p blastp -g Amphimedon_queenslandica.Aqu1.45.cds_only.gff -G -x > Aqu2.1_Genes_proteins.blastp_vs_models.gff
```

## silicateins and cathepsins ##
Using the [dataset](https://data.mendeley.com/datasets/xx7hgd3dg2/1) from [Aguilar-Camacho 2019](https://doi.org/10.1016/j.ympev.2018.11.015), the 8 silicatein and cathepsin proteins are extracted, and blasted against the protein set. The alignment length (181AAs) is about half the length of most of the proteins (~350AAs).

`blastp -query aqu_silicateins.trimmed.fa -db ~/est/porifera/Aqu2.1_Genes_proteins.fasta -outfmt 6 -evalue 1e-100`

This produces:

```
Silicatein_Amphimedon5	Aqu2.1.29399_001	87.500	192	9	8	1	177	138	329	6.12e-108	311
Silicatein_Amphimedon1	Aqu2.1.42494_001	93.684	190	0	7	1	178	176	365	9.31e-116	333
Silicatein_Amphimedon2	Aqu2.1.42495_001	94.211	190	0	7	1	179	62	251	1.56e-119	338
Silicatein_Amphimedon3	Aqu2.1.41046_001	93.684	190	0	7	1	178	172	361	1.71e-116	334
Silicatein_Amphimedon4	Aqu2.1.41047_001	94.211	190	0	7	1	179	140	329	6.53e-120	342
CathepsinL_Amphimedon1	Aqu2.1.41049_001	92.105	190	4	7	1	179	127	316	3.23e-114	338
CathepsinL_Amphimedon2	Aqu2.1.41049_001	92.105	190	4	7	1	179	475	664	9.75e-113	335
CathepsinL_Amphimedon3	Aqu2.1.43732_001	93.684	190	1	7	1	179	130	319	2.68e-120	343
```

Firstly, there is collinearity in the genome, where `Silicatein_Amphimedon3`, `Silicatein_Amphimedon4`, and both `CathepsinL_Amphimedon1` and `CathepsinL_Amphimedon2` are sequentially numbered, and in a row in the genome. Secondly, as is a common problem of tandem duplicates, the two cathepsins appear erroneously fused to make the gene `Aqu2.1.41049_001`, which has blast hits to two non-overlapping portions (127-316 and 475-664). In between, `Aqu2.1.41048_001`, another gene with the same exon structure, also appears to blast against cathepsins in human, and was probably mistakenly excluded from their analysis because of a likely-erroneous repeat in the middle of one of the exons. The same goes for the gene after, another capthepsin `Aqu2.1.41050_001`, though there is an additional intron added due to a 1282bp gap in one of the exons.

```
Aqu2.1.41048_001	sp|O60911|CATL2_HUMAN	47.5	221	112	1	193	409	114	334	9.9e-60	229.2
Aqu2.1.41048_001	sp|P07711|CATL1_HUMAN	49.1	218	106	2	194	407	115	331	9.3e-58	222.6
Aqu2.1.41050_001	sp|P07711|CATL1_HUMAN	43.0	377	148	9	15	381	14	333	4.6e-75	280.0
Aqu2.1.41050_001	sp|O60911|CATL2_HUMAN	40.8	377	157	7	15	381	14	334	5.6e-73	273.1
```

![contig13508_318k-334k_silicatein_locus.png](https://bitbucket.org/wrf/genome-reannotations/raw/20d66cd5a9c58a340deb42b6809ddd29c7ef847f/jbrowse-tracks/amphimedon/contig13508_318k-334k_silicatein_locus.png)

`~/git/genomeGTFtools/extract_coordinates.py -s Contig13508 -b 318000 -e 334000 -g Amphimedon_queenslandica.Aqu1.45.cleaned.gff > contig13508_318k-334k_silicatein_locus.tab`

There is another group of two silicateins on a different scaffold, of `Silicatein_Amphimedon1` and `Silicatein_Amphimedon2`, both with similar exon structure as the other genes. The gene `Aqu2.1.42495_001` appears to be trucated at the N-terminus, possibly due to selection of the wrong starting M.


