# Rhopilema esculentum #
Genome of the medusa *Rhopilema esculentum*, by [Li 2020](https://doi.org/10.1093/gigascience/giaa036).

## prepare scaffolds ##
Chromosome-level scaffolds [from NCBI](https://www.ncbi.nlm.nih.gov/assembly/GCA_013076305.1/) appear to be version 1 (HiRise v. Aug-2018) with 1681 scaffolds of 256Mb. Most of the assembly (95%) is contained in 21 large scaffolds.

`~/samtools-1.9/samtools faidx rhopilema_chr_assembly_v1.fa`

Make gap track:

`~/git/genomeGTFtools/repeat2gtf.py -l rhopilema_chr_assembly_v1.fa > rhopilema_chr_assembly_v1.n_gaps.gff`

```
# Counted 2830 repeats of 283749 total bases, average 100.26
# Longest repeat was 988 bases on SWAQ01001658.1
```

Gaps appear to be placed from the two stages of the `falcon` assembly, and are overall infrequent.

## approximate CDS from other version ##
From the minimap2 mapping of the transcript CDS nucleotides from the [gigaDB version](http://www.gigadb.org/dataset/view/id/100720),

`~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M cds_to_genome.sorted.bam > cds_to_genome.sorted.gtf`

`~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py cds_to_genome.sorted.gtf > cds_to_genome.sorted.gff`

## map trinity transcripts ##
Almost a million transcripts, based on the mapping, many appear to be either introns or DNA, meaning that possibly poly-A selection was not applied strictly enough. The paper does not detail this in the methods at all.

```
# Wrote 820410 sequences: 100.0% of total
# Total input letters is: 742870615
# Average length is: 905.49
```

```
~/minimap2-2.23_x64-linux/minimap2 -a -x splice --secondary=no rhopilema_chr_assembly_v1.fa trinity.fasta.gz | ~/samtools-1.14/samtools sort - -o trinity.vs_rhopilema_v1.bam
[M::mm_idx_gen::5.898*1.50] collected minimizers
[M::mm_idx_gen::7.879*1.88] sorted minimizers
[M::main::7.879*1.88] loaded/built the index for 1681 target sequence(s)
[M::mm_mapopt_update::8.522*1.81] mid_occ = 92
[M::mm_idx_stat] kmer size: 15; skip: 5; is_hpc: 0; #seq: 1681
[M::mm_idx_stat::8.914*1.78] distinct minimizers: 47703087 (69.47% are singletons); average occurrences: 1.834; average spacing: 2.934; total length: 256689583
[M::worker_pipeline::384.251*2.95] mapped 607342 sequences
[M::worker_pipeline::573.104*2.97] mapped 213068 sequences
[M::main] Version: 2.23-r1111
[M::main] CMD: /home/dummy/minimap2-2.23_x64-linux/minimap2 -a -x splice --secondary=no rhopilema_chr_assembly_v1.fa trinity.fasta.gz
[M::main] Real time: 573.253 sec; CPU: 1700.222 sec; Peak RSS: 6.529 GB
[bam_sort_core] merging from 1 files and 1 in-memory blocks...
```

```
~/samtools-1.14/samtools flagstat trinity.vs_rhopilema_v1.bam
848643 + 0 in total (QC-passed reads + QC-failed reads)
820410 + 0 primary
0 + 0 secondary
28233 + 0 supplementary
0 + 0 duplicates
0 + 0 primary duplicates
711795 + 0 mapped (83.87% : N/A)
683562 + 0 primary mapped (83.32% : N/A)
```

`~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M -s trinity.vs_rhopilema_v1.bam > trinity.vs_rhopilema_v1.gtf`

`~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py trinity.vs_rhopilema_v1.gtf > trinity.vs_rhopilema_v1.gff`

## blast vs models ##

`~/diamond-latest/diamond blastx -q cds.fasta -d ~/db/model_organism_uniprot.w_cnido.fasta -o cds.vs_models.tab`

`~/git/genomeGTFtools/blast2genomegff.py -b cds.vs_models.tab -g cds_to_genome.sorted.gff -S --add-description --add-accession -d ~/db/model_organism_uniprot.w_cnido.fasta -P > cds.vs_models.gff`

`/var/www/html/jbrowse/bin/generate-names.pl --tracks CDS,blast-vs-models --out ./`

## mapping EST sets ##
[SRX1692760: Rhopilema esculentum oral tissue sequencing](https://www.ncbi.nlm.nih.gov/sra/SRX1692760[accn])

`~/sratoolkit.2.11.3-ubuntu64/bin/fastq-dump --gzip SRR3360901`

```
# Counted 25192790 sequences:
# Total input letters is: 6298197500
# Average length is: 250.00
```

[SRX1692774: Rhopilema esculentum gonad transcriptome sequencing](https://www.ncbi.nlm.nih.gov/sra/SRX1692774[accn])

`~/sratoolkit.2.11.3-ubuntu64/bin/fastq-dump --gzip SRR3360928`

[SRX1692805: Rhopilema esculentum umbrella part transcriptome sequencing](https://www.ncbi.nlm.nih.gov/sra/SRX1692805[accn])

`~/sratoolkit.2.11.3-ubuntu64/bin/fastq-dump --gzip SRR3360955`

`{ ~/hisat2-2.2.1/hisat2 -q -x rhopilema_chr_assembly_v1.fa -U SRR3360901.fastq.gz,SRR3360928.fastq.gz,SRR3360955.fastq.gz -p 4 --dta --max-intronlen 50000 2>&3 | ~/samtools-1.14/samtools sort - -o rhopilema_txome_all_250bp_hisat2.bam ; } 3> rhopilema_txome_all_250bp_hisat2.log`

`~/stringtie-2.2.1.Linux_x86_64/stringtie -o rhopilema_txome_all_250bp_hisat2.stringtie.gtf rhopilema_txome_all_250bp_hisat2.bam`





