#!/usr/bin/env python

'''
./fix_monosiga_gff.py Monbr1_best_models.gff > Monbr1_best_models.gff3

or

./fix_monosiga_gff.py Monbr1_all_models.gff > Monbr1_all_models.gff3

'''

import sys

if len(sys.argv) < 2:
	sys.stderr.write( __doc__ )
else:
	for line in open(sys.argv[1],'r'):
		line = line.strip()
		if line and line[0]!="#":
			lsplits = line.split("\t")
			feature = lsplits[2]
			attributes = lsplits[8]
			genename = attributes.split('"')[1].split(".")[0]
			attributes = attributes.replace('"','')
			attributes = attributes.replace('name ','Name=')
			attributes = attributes.replace('transcriptId ','ID=')
			attributes = attributes.replace('proteinId ','ID={}'.format(genename))
			attributes = attributes.replace('exonNumber ','exonNumber=')
			if feature=="CDS":
				lsplits[8] = attributes.split(";",1)[1].strip()
				print >> sys.stdout, "\t".join(lsplits)
