# *Monosiga brevicollis* processing steps
Annotation for [Monbr1 scaffolds](https://mycocosm.jgi.doe.gov/Monbr1/Monbr1.home.html), from [King 2008 The genome of the choanoflagellate Monosiga brevicollis and the origin of metazoans](https://doi.org/10.1038/nature06617)

# prepare scaffolds
Make index and link:

```
~/samtools-1.8/samtools faidx Monbr1_scaffolds.fasta
ln -s /mnt/data/genomes/monosiga_brevicollis/Monbr1_scaffolds.fasta
ln -s /mnt/data/genomes/monosiga_brevicollis/Monbr1_scaffolds.fasta.fai
/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta Monbr1_scaffolds.fasta --out ./
```

Make [gap track](https://github.com/wrf/genomeGTFtools/blob/master/repeat2gtf.py):

```
~/git/genomeGTFtools/repeat2gtf.py -l Monbr1_scaffolds.fasta > Monbr1_scaffolds.gaps.gff
ln -s /mnt/data/genomes/monosiga_brevicollis/Monbr1_scaffolds.gaps.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Monbr1_scaffolds.gaps.gff --trackType CanvasFeatures --trackLabel N-gaps
```

# add JGI models
The annotation needs to be reformatted to work with jbrowse. First, the annotations are converted from GTF to GFF3, then parent features are created. Exon features are removed, since they are the same as the CDS for all cases. This is applied to both the "best" gene models, as well as "all" models.

```
scaffold_10	JGI	exon	1462	2706	.	-	.	name "fgenesh1_pg.scaffold_10000001"; transcriptId 8140
scaffold_10	JGI	CDS	1462	2706	.	-	0	name "fgenesh1_pg.scaffold_10000001"; proteinId 8140; exonNumber 1
scaffold_10	JGI	start_codon	2704	2706	.	-	0	name "fgenesh1_pg.scaffold_10000001"
scaffold_10	JGI	stop_codon	1462	1464	.	-	0	name "fgenesh1_pg.scaffold_10000001"
scaffold_10	JGI	exon	45049	45228	.	+	.	name "fgenesh2_pg.scaffold_10000002"; transcriptId 25352
scaffold_10	JGI	CDS	45049	45228	.	+	0	name "fgenesh2_pg.scaffold_10000002"; proteinId 25352; exonNumber 1
scaffold_10	JGI	start_codon	45049	45051	.	+	0	name "fgenesh2_pg.scaffold_10000002"
scaffold_10	JGI	exon	45320	45504	.	+	.	name "fgenesh2_pg.scaffold_10000002"; transcriptId 25352
scaffold_10	JGI	CDS	45320	45504	.	+	0	name "fgenesh2_pg.scaffold_10000002"; proteinId 25352; exonNumber 2
scaffold_10	JGI	exon	45565	45742	.	+	.	name "fgenesh2_pg.scaffold_10000002"; transcriptId 25352
scaffold_10	JGI	CDS	45565	45742	.	+	2	name "fgenesh2_pg.scaffold_10000002"; proteinId 25352; exonNumber 3
scaffold_10	JGI	stop_codon	45740	45742	.	+	0	name "fgenesh2_pg.scaffold_10000002"
```

`./fix_monosiga_gff.py Monbr1_best_models.gff > Monbr1_best_models.gff3`

```
scaffold_10	JGI	CDS	1462	2706	.	-	0	ID=fgenesh1_pg8140; exonNumber=1
scaffold_10	JGI	CDS	45049	45228	.	+	0	ID=fgenesh2_pg25352; exonNumber=1
scaffold_10	JGI	CDS	45320	45504	.	+	0	ID=fgenesh2_pg25352; exonNumber=2
scaffold_10	JGI	CDS	45565	45742	.	+	2	ID=fgenesh2_pg25352; exonNumber=3
```

`~/git/genomeGTFtools/make_parent_features.py Monbr1_best_models.gff3 > Monbr1_best_models_for_jbrowse.gff3`

```
scaffold_10	JGI	mRNA	1462	2706	0	-	.	ID=fgenesh1_pg8140;Name=fgenesh1_pg8140
scaffold_10	JGI	CDS	1462	2706	.	-	0	Parent=fgenesh1_pg8140; exonNumber=1
scaffold_10	JGI	mRNA	45049	45742	0	+	.	ID=fgenesh2_pg25352;Name=fgenesh2_pg25352
scaffold_10	JGI	CDS	45049	45228	.	+	0	Parent=fgenesh2_pg25352; exonNumber=1
scaffold_10	JGI	CDS	45320	45504	.	+	0	Parent=fgenesh2_pg25352; exonNumber=2
scaffold_10	JGI	CDS	45565	45742	.	+	2	Parent=fgenesh2_pg25352; exonNumber=3
```

`./fix_monosiga_gff.py Monbr1_all_models.gff > Monbr1_all_models.gff3`

`~/git/genomeGTFtools/make_parent_features.py Monbr1_all_models.gff3 > Monbr1_all_models_for_jbrowse.gff3`

```
ln -s /mnt/data/genomes/monosiga_brevicollis/Monbr1_best_models_for_jbrowse.gff3
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Monbr1_best_models_for_jbrowse.gff3 --trackType CanvasFeatures --trackLabel JGI-best-models
ln -s /mnt/data/genomes/monosiga_brevicollis/Monbr1_all_models_for_jbrowse.gff3
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Monbr1_all_models_for_jbrowse.gff3 --trackType CanvasFeatures --trackLabel JGI-all-models
```

# add AUGUSTUS re-annotation
Re-annotation with [AUGUSTUS](http://bioinf.uni-greifswald.de/augustus/), following training with the JGI EST set (29k ESTs), found [10864 genes](https://bitbucket.org/wrf/genome-reannotations/downloads/Monbr1_augustus_v1.nocomment.gff.gz), of [15601 transcripts/predicted proteins](https://bitbucket.org/wrf/genome-reannotations/downloads/Monbr1_augustus_v1.prots.fasta.gz). Training parameters are [here](https://bitbucket.org/wrf/genome-reannotations/downloads/mbrevicollis_parameters.tar)

Comments are removed:

`ln -s /mnt/data/genomes/monosiga_brevicollis/Monbr1_augustus_v1_no_comment.gff`

`/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Monbr1_augustus_v1_no_comment.gff --trackType CanvasFeatures --trackLabel AUGUSTUS`

# blast against swissprot
Using [diamond](https://github.com/bbuchfink/diamond), 7827 queries aligned:

`~/diamond-v2.0.13/diamond-v2.0.13 blastp -q Monbr1_augustus_v1.prot_no_rename.fasta -d ~/db/model_organism_uniprot.w_cnido.fasta -o Monbr1_augustus_v1.vs_models.tab`

Add the description and accession for display and linking with `-S --add-description --add-accession`:

`~/git/genomeGTFtools/blast2genomegff.py -b Monbr1_augustus_v1.vs_models.tab -d ~/db/model_organism_uniprot.fasta -g Monbr1_augustus_v1_no_comment.gff -p blastp --gff-delimiter "." -x -S --add-description --add-accession -G > Monbr1_augustus_v1.vs_models.gff`

`ln -s /mnt/data/genomes/monosiga_brevicollis/Monbr1_augustus_v1.vs_models.gff`

`/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Monbr1_augustus_v1.vs_models.gff --trackType CanvasFeatures --trackLabel blastp-v-models`

The mouseover will display the target, click will open a new window of the protein hit in uniprot.org:

```
       "onClick": {
            "url": "https://www.uniprot.org/uniprot/{accession}",
            "action": "newWindow",
            "label" : "function() { return this.feature.get('target'); }",
       },
```

# microsynteny with *Salpingoeca rosetta*
Some synteny blocks are detectable:

`blastp -query Monbr1_augustus_v1.prot_no_rename.fasta -db ~/genomes/salpingoeca_rosetta/Salpingoeca_rosetta.Proterospongia_sp_ATCC50818.pep.all.fa -outfmt 6 -max_target_seqs 5 -evalue 1e-3 -num_threads 4 > monbr1_augustus_vs_srosetta_blastp.tab`

`~/git/genomeGTFtools/microsynteny.py -b monbr1_augustus_vs_srosetta_blastp.tab -q Monbr1_augustus_v1_no_comment.gff -d ~/genomes/salpingoeca_rosetta/srosetta_no_comments.gff -G > monbr1_augustus_vs_srosetta_microsynteny.gff`

`ln -s /mnt/data/genomes/monosiga_brevicollis/monbr1_augustus_vs_srosetta_microsynteny.gff`

`/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff monbr1_augustus_vs_srosetta_microsynteny.gff --trackType CanvasFeatures --trackLabel Srosetta-microsynteny`



