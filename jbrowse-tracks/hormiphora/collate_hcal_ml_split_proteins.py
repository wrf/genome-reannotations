#!/usr/bin/env python

'''collate_hcal_ml_split_proteins.py
    based on output of compare_hcal_ref_proteins.py
    collect proteins and generate alignments

collate_hcal_ml_split_proteins.py -p Hcv1a1d20200309_model_proteins.pep ML2.2.aa

'''

import sys
import time
import argparse
from collections import defaultdict
from Bio import SeqIO


def get_protein_seqs(proteinfile):
	seqdict = SeqIO.to_dict(SeqIO.parse(proteinfile,"fasta"))
	return seqdict

def collect_proteins_by_matches(protdict, splitgenesfile):
	genes_to_collate = defaultdict(list)

	for line in open(splitgenesfile,'r'):
		line = line.strip()
		if line:
			lsplits = line.split("\t")
			qu_prot = lsplits[0]
			genes_to_collate[qu_prot].extend(lsplits[3].split(","))
			for sub_prot in lsplits[3].split(","):
				genes_to_collate[sub_prot].append(qu_prot)









def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-b','--blast', help="tabular blast output", required=True)
	parser.add_argument('-p','--proteins', nargs='*', help="fasta files of proteins", required=True)
	args = parser.parse_args(argv)

	# read proteins from all genomes
	seqdict = {}
	for protfile in args.proteins:
		seqdict.update(get_protein_seqs(protfile))

	# read query sequences for length
	all_querylen_dict = get_prot_lengths(args.query_proteins)
	# keep only long queries
	long_querylen_dict = get_longest_isoform(all_querylen_dict)

	# process blast hits
	parse_tabular_blast(args.blast, long_querylen_dict, sublength_dict)
	



if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
