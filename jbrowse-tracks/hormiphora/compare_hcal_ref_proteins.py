#!/usr/bin/env python

'''compare_hcal_ref_proteins.py  created 2020-03-24
    check blast hits between Hcal and ML for non-overlapping intervals
    of neighboring proteins, as sign for split proteins

compare_hcal_ref_proteins.py -b Hcv1av85_transcripts.vs_ml2.tab -q Hcv1av85_model_proteins.pep -d ML2.2.aa

~/genomes/hormiphora_californensis/compare_hcal_ref_proteins.py -b ~/genomes/hormiphora_californensis/compare_hcal_ref_proteins.test.tab -q Hcv1a1d20200325_model_proteins.pep -d ~/genomes/mnemiopsis_leidyi/ML2.2.aa

    first run diamond:
~/diamond-latest/diamond blastp -q Hcv1av85_model_proteins.pep -d ~/genomes/mnemiopsis_leidyi/ML2.2.aa -o Hcv1av85_model_proteins.vs_ml2.tab

~/genomes/hormiphora_californensis/compare_hcal_ref_proteins.py -b ~/genomes/hormiphora_californensis/hormiphora/protein_analysis/Hcv1av85_model_proteins.pep.vs_ml2.tab -q Hcv1av85_model_proteins.pep.longest_only.fa -d ~/genomes/mnemiopsis_leidyi/ML2.2.aa > candidate_problem_genes_in_ML.tab

~/diamond-latest/diamond blastp -q Hcv1av85_model_proteins.pep -d ~/genomes/pleurobrachia_bachei/pbachei_03_filtered_gene_models_transcripts.prot.fa -o Hcv1av85_model_proteins.vs_pbac.tab

~/genomes/hormiphora_californensis/compare_hcal_ref_proteins.py -b ~/genomes/hormiphora_californensis/hormiphora/protein_analysis/Hcv1av87_model_proteins.pep.vs_ml2.tab -q Hcv1av87_model_proteins.pep.longest_only.fa -d ~/genomes/mnemiopsis_leidyi/ML2.2.aa -t 5 > candidate_problem_genes_in_ML.short.tab

~/genomes/hormiphora_californensis/compare_hcal_ref_proteins.py -b ~/genomes/hormiphora_californensis/hormiphora/protein_analysis/Hcv1av85_model_proteins.vs_pbac.tab -q Hcv1av85_model_proteins.pep.longest_only.fa -d ~/genomes/pleurobrachia_bachei/pbachei_03_filtered_gene_models_transcripts.prot.fa > candidate_problem_genes_in_Pbac.tab

'''

import sys
import time
import argparse
from collections import defaultdict
from Bio import SeqIO

#Hcv1.1.c3.g728.i1	split	5	ML126210a,ML126212a,ML126213a,ML126214a,ML12627a	Midasin
#Hcv1.1.c8.g539.i2	split	4	ML327415a,ML327417a,ML327419a,ML327421a	maybe centriolin
#     Hcal has 3 antisense genes, ML has a 3, one is conserved
#Hcv1.1.c11.g318.i1	split	4	ML018021a,ML018022a,ML018025a,ML018027a	PIEZO
#Hcv1.1.c5.g977.i1	split	4	ML040020a,ML040022a,ML040024a,ML040028a	pecanex
#     contains same strand full single exon spatacsin gene in intron, and another gene
#Hcv1.1.c2.g876.i1	split	4	ML043810a,ML043813a,ML043815a,ML04387a	dynein
#Hcv1.1.c4.g151.i1	split	4	ML08421a,ML08423a,ML08425a,ML08426a	glycogen synthase
#     has conserved alt 5pr exon
#Hcv1.1.c2.g708.i4	split	4	ML07551a,ML07553a,ML07554a,ML07555a	oxysterol-binding protein
#Hcv1.1.c2.g622.i1	split	4	ML475510a,ML475512a,ML475514a,ML47558a	maybe SZT2
#     ML has 3 genes in introns, Hcal has many small and 1 giant intron w 1 sense gene

# all 3 are dyneins
#Hcv1.1.c2.g876.i2	split	2	ML011724a,ML011727a
#Hcv1.1.c2.g876.i2	split	2	ML34751a,ML34752a
#Hcv1.1.c2.g876.i2	split	2	ML046510a,ML04659a

#Hcv1.1.c1.g447.i3	split	2	ML14854a,ML14857a
#Hcv1.1.c1.g447.i3	split	2	ML011723a,ML011724a
#Hcv1.1.c1.g447.i3	split	2	ML046510a,ML04658a

# was not flagged, but should have been
#Hcv1.1.c4.g354.i1	ML263523a	59.2	483	114	6	892	1374	56	455	1.7e-151	534.6
#Hcv1.1.c4.g354.i1	ML263523a	59.2	483	114	6	892	1374	56	455	1.7e-151	534.6
#Hcv1.1.c4.g354.i1	ML12511a	78.9	279	59	0	1694	1972	1	279	3.5e-128	457.2
#Hcv1.1.c4.g354.i1	ML263524a	76.4	246	31	2	1375	1593	418	663	5.2e-100	363.6
#Hcv1.1.c4.g354.i1	ML263519a	53.0	268	111	5	35	292	13	275	1.8e-63	242.3
#Hcv1.1.c4.g354.i1	ML216344a	24.4	409	289	7	1229	1632	87	480	2.6e-22	105.5
#Hcv1.1.c4.g354.i1	ML263520a	38.9	180	95	5	312	489	131	297	3.3e-17	88.6



class BlastHit:
	def __init__(self, queryid, scaffold, querylen):
		self.queryid = queryid # gene ID
		self.scaffold = scaffold # systematic scaffold ID
		self.querylen = querylen # full length of protein
		self.bitscores = {} # dict of gene names, value is score
		self.matches_to_sub = defaultdict(list) #
		self.subject_to_ints = defaultdict(list) # dict of gene names, value is list of intervals
		self.matches_on_scaf = defaultdict(list)
		self.intervals_on_scaf = defaultdict(list)

	def score_check(self, subjectid, score):
		if subjectid in self.bitscores or len(self.bitscores)==0:
			return True
		elif score/max(self.bitscores.values()) >= 0.1:
			return True
		else:
			return False

	def add_score(self, subjectid, score):
		self.bitscores[subjectid] = self.bitscores.get(subjectid, 0) + score

	def add_hit_on_scaf(self, subjectid, scaffold, query_int):
		self.matches_on_scaf[scaffold].append(subjectid)
		self.intervals_on_scaf[scaffold].append(query_int)

	def add_hit_on_gene(self, subjectid, subject_int):
		self.subject_to_ints[subjectid].append(subject_int)

	def give_subject_scaffolds(self):
		return self.intervals_on_scaf.keys()

	def num_matches_on_scaf(self, scaffold):
		return len( set( self.matches_on_scaf.get(scaffold) ) )

	def num_intervals_on_scaf(self, scaffold):
		return len(self.intervals_on_scaf.get(scaffold))

	def num_nr_intervals_by_scaf(self, scaffold):
		nrintervallist = []
		srtrangelist = sorted(self.intervals_on_scaf.get(scaffold)) # sort list now
		interval = srtrangelist[0] # need to start with first time, which will be the same for the first bounds
		for bounds in srtrangelist:
			# since it is sorted bounds[0] should always be >= interval[0]
			if bounds[0] > interval[1]+1: # if the next interval starts past the end of the first + 1
				nrintervallist.append(interval) # add to the nr list, and continue with the next
				interval = bounds
			else:
				if bounds[1] > interval[1]: # bounds[1] <= interval[1] means do not extend
					interval = (interval[0], bounds[1]) # otherwise extend the interval
		else: # append last interval
			nrintervallist.append(interval)
		return len(nrintervallist)

def get_prot_lengths(sequences):
	'''from a fasta file, return a dictionary where protein ID is the key and length is the value'''
	seqlendict = {}
	sys.stderr.write("# Parsing proteins from {}  {}\n".format(sequences, time.asctime() ) )
	for seqrec in SeqIO.parse(sequences,'fasta'):
		if seqrec.id in seqlendict:
			sys.stderr.write("# WARNING: ID {} occurs multiple times\n".format(seqrec.id) )
		seqlendict[seqrec.id] = len(seqrec.seq)
	sys.stderr.write("# Found {} sequences  {}\n".format( len(seqlendict), time.asctime() ) )
	return seqlendict

def get_longest_isoform(seqlendict):
	'''from a full length dictionary, return another dictionary keeping only the longest isoforms'''
	longestiso_name = {}
	longestiso_len = {}
	sys.stderr.write("# Identifying long isoforms in query\n" )
	for seqid, seqlen in seqlendict.items():
		gene_id = seqid.rsplit(".",1)[0]
		if seqlen > longestiso_len.get(gene_id,0):
			longestiso_len[gene_id] = seqlen
			longestiso_name[gene_id] = seqid

	long_only_seqdict = {}
	for gene_id, longiso in longestiso_name.items():
		long_only_seqdict[longiso] = longestiso_len.get(gene_id)
	sys.stderr.write("# Kept {} longest query isoforms\n".format( len(long_only_seqdict) ) )
	return long_only_seqdict

def parse_tabular_blast(blastfile, querylendict, sublendict, bound_cut):
	'''parse blast hits from tabular blast and write each hit independently to stdout as genome gff'''
	linecounter = 0
	# count results to filter
	shortRemovals = 0
	evalueRemovals = 0
	bitsRemovals = 0

	hits_by_gene = {} # key is gene ID, value is BlastHit object

	for line in open(blastfile, 'r'):
		line = line.strip()
		if not line or line[0]=="#": # skip comment lines
			continue # also catch for empty line, which would cause IndexError
		linecounter += 1
		#qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore		
		lsplits = line.split("\t")

		# for Hcal
		qseqid = lsplits[0]
		qscaffold = qseqid.rsplit(".",2)[0]
		qlen = querylendict.get(qseqid,None)
		if qlen is None: # short isoform, skip
			continue

		# for ML2
		sseqid = lsplits[1]
		sscaffold = sseqid[0:6]

		# do all removals before any real counting
		evalue = float(lsplits[10])
		bitscore = float(lsplits[11])
		alignlength = float(lsplits[3])

		# set up bounds, trim is 0 by default, otherwise cut
		# if cutting too much, then use untrimmed bounds
		qstart = int(lsplits[6]) + bound_cut
		qend = int(lsplits[7]) - bound_cut
		query_span = qend - qstart + 1
		if query_span < 1:
			qstart = int(lsplits[6])
			qend = int(lsplits[7])
		query_bounds = (qstart, qend)

		sstart = int(lsplits[8]) + bound_cut
		send = int(lsplits[9]) - bound_cut
		subject_span = send - sstart + 1
		if subject_span < 1:
			sstart = int(lsplits[8])
			send = int(lsplits[9])
		subject_bounds = (sstart, send)

		if qseqid not in hits_by_gene:
			Hc_protein = BlastHit(qseqid, qscaffold, qlen)
			hits_by_gene[qseqid] = Hc_protein
		if Hc_protein.score_check(sseqid, bitscore):
			Hc_protein.add_score(sseqid, bitscore)
			Hc_protein.add_hit_on_scaf(sseqid, sscaffold, query_bounds)
			Hc_protein.add_hit_on_gene(sseqid, subject_bounds)



	candidate_splits = defaultdict(list) # key is Hcal ID, value is list of Mlei IDs
	candidate_tandem = defaultdict(list) # key is Hcal ID, value is list of Mlei IDs
	for protid, Hc_protein in hits_by_gene.items():
		for sscaffold in Hc_protein.give_subject_scaffolds():
			num_intervals = Hc_protein.num_intervals_on_scaf(sscaffold)
			num_nr_intervals = Hc_protein.num_nr_intervals_by_scaf(sscaffold)
			num_unique_matches = Hc_protein.num_matches_on_scaf(sscaffold)
		#	print >> sys.stderr, "{}\t{}\t{}\t{}\t{}".format(protid, sscaffold, num_intervals, num_nr_intervals, num_unique_matches)

			#
			# LOGIC FOR SELECTION OF SPLIT GENES
			#
			# should be N intervals and N genes, greater than 1
			# and should have multiple unique match IDs
			if num_intervals==num_nr_intervals and num_intervals>1 and num_unique_matches>1:
				split_genes = Hc_protein.matches_on_scaf.get(sscaffold)
				unique_splitgenes = list(set(split_genes))
				candidate_splits[protid].append(unique_splitgenes)

			# for tandem duplicates or matches of very similar genes
			# intervals are redundant, meaning Hcal interval matchces the same Mlei
			elif num_intervals > num_nr_intervals and num_intervals>1 and num_unique_matches>1:
				dup_genes = Hc_protein.matches_on_scaf.get(sscaffold)
				unique_dupgenes = list(set(dup_genes))
				candidate_tandem[protid].append(unique_dupgenes)

	# print candidate split genes in ML
	split_queries = {} # key is query id, value is true, just for counting
	split_subjects = {} # key is subject id, value is true, for counting
	for protid,splitlist in sorted(candidate_splits.items(), key=lambda x: x[1]):
		for splitgenes in splitlist:
			sorted_splitgene_str = "{}".format(",".join(sorted(splitgenes)))
			split_queries[ sorted_splitgene_str ] = True
			for splitgene in splitgenes:
				split_subjects[splitgene] = True
			outline = "{}\tsplit\t{}\t{}\n".format(protid, len(splitgenes), sorted_splitgene_str)
			sys.stdout.write( outline )
	sys.stderr.write("# found {} queries for {} candidate split genes\n".format( len(split_queries), len(split_subjects) ) )

	dup_queries = {}
	dup_subjects = {}
	for protid,tandemlist in sorted(candidate_tandem.items(), key=lambda x: x[1]):
		for dupgenes in tandemlist:
			sorted_tandemgene_str = "{}".format(",".join(sorted(dupgenes)))
			dup_queries[ protid ] = True
			for dupgene in dupgenes:
				dup_subjects[dupgene] = True
			outline = "{}\tdup\t{}\t{}\n".format(protid, len(dupgenes), sorted_tandemgene_str)
		#	sys.stdout.write( outline )
	sys.stderr.write("# {} queries may hit {} tandem duplicate genes\n".format( len(dup_queries), len(dup_subjects) ) )





def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-b','--blast', help="tabular blast output", required=True)
	parser.add_argument('-q','--query-proteins', help="fasta file of query proteins", required=True)
	parser.add_argument('-d','--db-proteins', help="fasta file of reference proteins", required=True)
	parser.add_argument('-t','--trim-bounds', type=int, metavar="N", default=0, help="trim bounds by N")
	args = parser.parse_args(argv)

	# read subject sequences for length
	sublength_dict = get_prot_lengths(args.db_proteins)

	# read query sequences for length
	all_querylen_dict = get_prot_lengths(args.query_proteins)
	# keep only long queries
	long_querylen_dict = get_longest_isoform(all_querylen_dict)

	# process blast hits
	parse_tabular_blast(args.blast, long_querylen_dict, sublength_dict, args.trim_bounds)
	

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
