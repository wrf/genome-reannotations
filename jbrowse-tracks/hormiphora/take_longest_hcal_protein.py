#!/usr/bin/env python
# last updated 2020-04-17
# take_longest_hcal_protein.py

'''
./take_longest_hcal_protein.py Hcv1a1d20200309_model_proteins.pep > Hcv1a1d20200309_model_proteins.pep.longest_only.fa

    in case of tie, lowest isoform number (like i1) always be taken
'''

import sys
from collections import defaultdict
from Bio import SeqIO

def get_longest_candidate(seqrec_dict):
	longest_isoform_len = max(seqrec_dict.keys())
	if len(seqrec_dict[longest_isoform_len])==1:
		longest_isoform = seqrec_dict[longest_isoform_len][0]
	elif len(seqrec_dict[longest_isoform_len]) > 1: # if equal, always take i1
		candidates_by_inum = {}
		for longcandidate in seqrec_dict[longest_isoform_len]:
			isoform_num = int(longcandidate.id.rsplit(".",1)[-1].replace("i",""))
			candidates_by_inum[isoform_num] = longcandidate
		lowest_inum = min(candidates_by_inum.keys())
		longest_isoform = candidates_by_inum[lowest_inum]
	return longest_isoform

if len(sys.argv) < 2:
	sys.exit( __doc__ )
else:
	lastgene = ""
	prot_count = 0
	write_count = 0
	seqrec_dict = defaultdict(list) # key is length, value is list of seqrecord objects
	for seqrec in SeqIO.parse(sys.argv[1],"fasta"):
		prot_count += 1
		seqid = seqrec.id
		geneid = seqid.rsplit(".",1)[0]
		#print >> sys.stderr, seqid, geneid, seqrec_dict
		if geneid==lastgene:
			seqlength = len(seqrec.seq)
			seqrec_dict[seqlength].append(seqrec)
		else: # moved on to a different gene
			if seqrec_dict: # if previous gene has entries, print the longest
				longest_isoform = get_longest_candidate(seqrec_dict)
				sys.stdout.write( longest_isoform.format("fasta") )
				write_count += 1
				seqrec_dict = defaultdict(list) # print old entry, reset dict, add new entry
				seqlength = len(seqrec.seq)
				seqrec_dict[seqlength].append(seqrec)
			else: # first gene
				seqlength = len(seqrec.seq)
				seqrec_dict[seqlength].append(seqrec)
		lastgene = geneid
	else: # for last gene
		longest_isoform = get_longest_candidate(seqrec_dict)
		sys.stdout.write( longest_isoform.format("fasta") )
		write_count += 1
	sys.stderr.write("# Counted {} prots, wrote {}\n".format( prot_count, write_count ) )


#
