# *Hormiphora californensis* genome tracks #
Using [Hcal scaffolds v1](https://github.com/conchoecia/hormiphora), by [Schultz et al 2021](https://doi.org/10.1093/g3journal/jkab302)

These are reordered, as by convention, with the longest first. The mitochondrial genome is added as the final scaffold, reformatted to 60bp/line and renamed as `Hcal_mt`.

```
~/git/genomeGTFtools/number_contigs_by_length.py -R Hcal_v1.fa > hcal_v1_reorder.fa
sizecutter.py -p tig00000146v7.fasta | sed s/tig00000146v7/Hcal_mt/ > hcal_mito_genome_tig00000146v7.fa
cat hcal_v1_reorder.fa hcal_mito_genome_tig00000146v7.fa > Hcal_v1.fa
```

### Generate reference seq track ###
Assembly consists of 13 very long scaffolds, likely to be the 13 chromosomes, 31 remaining pieces, and the mitochondrial genome.

```
~/samtools-1.9/samtools faidx UCSC_Hcal_v1.fa
ln -s /mnt/data/project/genomes/hormiphora_californensis/UCSC_Hcal_v1.fa
ln -s /mnt/data/project/genomes/hormiphora_californensis/UCSC_Hcal_v1.fa.fai
/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta UCSC_Hcal_v1.fa --out ./
```

Generate [N-gap track](https://github.com/wrf/genomeGTFtools/blob/master/repeat2gtf.py), though only 467 repeats of 21982 total bases, indicating a good assembly:

```
~/git/genomeGTFtools/repeat2gtf.py -l UCSC_Hcal_v1.fa > UCSC_Hcal_v1.n_gaps.gff
ln -s /mnt/data/project/genomes/hormiphora_californensis/UCSC_Hcal_v1.n_gaps.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff UCSC_Hcal_v1.n_gaps.gff --trackType CanvasFeatures --trackLabel N-gaps
```

### Mapping DNA reads, and HiC data ###
Get [scaffold sizes](https://bitbucket.org/wrf/sequences/src/master/sizecutter.py) for generation of bigwig track:

```
~/hisat2-2.1.0/hisat2-build UCSC_Hcal_v1.fa UCSC_Hcal_v1.fa
{ ~/hisat2-2.1.0/hisat2 -q -x UCSC_Hcal_v1.fa -1 GLO64_shot_f.fq.gz -2 GLO64_shot_r.fq.gz -p 4 --no-spliced-alignment 2>&3 | ~/samtools-1.8/samtools sort - -o GLO64_shot_v_UCSC_Hcal_v1_hisat2.bam; } 3> GLO64_shot_v_UCSC_Hcal_v1_hisat2.log
sizecutter.py -f UCSC_Hcal_v1.fa | sed s/" "/"\t"/ > UCSC_Hcal_v1.sizes
~/bedtools2/bin/bedtools genomecov -ibam GLO64_shot_v_UCSC_Hcal_v1_hisat2.bam -bga > GLO64_shot_v_UCSC_Hcal_v1_hisat2.bg
~/ucsc/bedSort GLO64_shot_v_UCSC_Hcal_v1_hisat2.bg GLO64_shot_v_UCSC_Hcal_v1_hisat2.bg
~/ucsc/bedGraphToBigWig GLO64_shot_v_UCSC_Hcal_v1_hisat2.bg UCSC_Hcal_v1.sizes GLO64_shot_v_UCSC_Hcal_v1_hisat2.cov.bw
```

### Mapping transcriptome assemblies ###
Prepare [GMAP](http://research-pub.gene.com/gmap/) for [2015 Trinity assembly](https://www.ncbi.nlm.nih.gov/Traces/wgs/GGLO01) from a whole animal transcriptome, consisting of 75402 transcripts, average 1010bp. I would not do it this way again, and instead use [minimap2](https://github.com/lh3/minimap2).

`~/gmap-2019-09-12/bin/gmap_build -d UCSC_Hcal_v1 UCSC_Hcal_v1.fa`

Using Trinity assembly from the same individual from D914, 114701 transcripts, average of 1035bp:

`~/gmap-2019-09-12/bin/gmap -d UCSC_Hcal_v1 -B 5 -t 4 -f 2 -n 1 -p 3 --max-intronlength-middle=10000 --split-large-introns ~/est/hormiphora/CTE_Hormiphora_californensis_D914_T1.fasta > Hormiphora_D914_Trinity.gff 2> Hormiphora_D914_Trinity_gmap.log`

In this case, 106k of 114k transcripts map. Then link, and generate the track:

```
ln -s /mnt/data/project/genomes/hormiphora_californensis/Hormiphora_D914_Trinity.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Hormiphora_D914_Trinity.gff --trackType CanvasFeatures --trackLabel Trinity-D914
```

`~/gmap-2019-09-12/bin/gmap -d UCSC_Hcal_v1 -B 5 -t 4 -f 2 -n 1 -p 3 --max-intronlength-middle=10000 --split-large-introns ~/est/hormiphora/Hormiphora_californensis_9110X13_Trinity.fasta > Hormiphora_9110X13_Trinity.gff 2> Hormiphora_9110X13_Trinity_gmap.log`

Overall low mapping rate, 9636 out of 75402 transcripts mapped, for 38232 exons. Again, I suspect high heterozygosity:

`ln -s /mnt/data/project/genomes/hormiphora_californensis/Hormiphora_9110X13_Trinity.gff Hormiphora_9110X13_Trinity.gff`

`/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Hormiphora_9110X13_Trinity.gff --trackType CanvasFeatures --trackLabel Trinity-GMAP`

Alternatively, use the [minimap](https://github.com/lh3/minimap2) mapping of the transcripts, create the index, then link and add the track information to `tracks.conf`:

`~/samtools-1.9/samtools index UCSC_Hcal_v1_B1_trinity.sorted.bam`

```
ln -s /mnt/data/project/genomes/hormiphora_californensis/UCSC_Hcal_v1_B1_trinity.sorted.bam
ln -s /mnt/data/project/genomes/hormiphora_californensis/UCSC_Hcal_v1_B1_trinity.sorted.bam.bai 
```

### AUGUSTUS gene predictions ###

```
~/git/genomeGTFtools/augustus_to_gff3.py augustus.hints.gtf > augustus.hints.gff
ln -s /mnt/data/project/genomes/hormiphora_californensis/augustus.hints.gff 
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff augustus.hints.gff --trackType CanvasFeatures  --trackLabel AUGUSTUS --out ./
```

### blast vs models ###
Making use of my own scripts at [genomeGTFtools](https://github.com/wrf/genomeGTFtools)

`~/diamond-latest/diamond blastp -q augustus.hints.aa -d ~/db/model_organism_uniprot.w_cnido.fasta -o augustus.hints.vs_models.tab`


`~/diamond-linux64/diamond blastx -q UCSC_Hcal_v1_B1_LR.stringtie.fasta -d ~/db/model_organism_uniprot.fasta -o UCSC_Hcal_v1_B1_LR.stringtie.vs_models.tab`

`~/git/genomeGTFtools/blast2genomegff.py -d ~/db/model_organism_uniprot.fasta -g UCSC_Hcal_v1_B1_LR.stringtie.gtf -P -b UCSC_Hcal_v1_B1_LR.stringtie.vs_models.tab -p blastx -M 4 -S --add-description --add-accession > UCSC_Hcal_v1_B1_LR.stringtie.vs_models.gff`

### Mapping RNAseq reads ###
Download whole animal [RNAseq reads](https://www.ncbi.nlm.nih.gov/sra/SRX1006956[accn]), 32M pairs of 100bp reads, by [Francis 2015](https://doi.org/10.1371/journal.pone.0128742)

`~/sratoolkit.2.9.6-1-ubuntu64/bin/fastq-dump --split-files --gzip --defline-seq '@$sn[_$rn]/$ri' SRR1992642`

`~/hisat2-2.1.0/hisat2 -q -x Hcal_v1.fa -1 SRR1992642_1.fastq.gz -2 SRR1992642_2.fastq.gz -S Hcal2015_SRR1992642_vs_Hcalv1.sam -p 4 2> Hcal2015_SRR1992642_vs_Hcalv1.log`

Very low mapping rate of raw reads (appx 1%), so actually bothering to trim:

```
ln -s ~/Trimmomatic-0.39/adapters/TruSeq3-PE.fa TruSeq3-PE.fa
java -jar ~/Trimmomatic-0.39/trimmomatic-0.39.jar PE SRR1992642_1.fastq.gz SRR1992642_2.fastq.gz SRR1992642_1.trimmed.fq.gz SRR1992642_1_unpaired.fq.gz SRR1992642_2.trimmed.fq.gz SRR1992642_2_unpaired.fq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10:2:keepBothReads LEADING:3 TRAILING:3 MINLEN:36
~/hisat2-2.1.0/hisat2 -q -x Hcal_v1.fa -1 SRR1992642_1.trimmed.fastq.gz -2 SRR1992642_2.trimmed.fastq.gz -S Hcal2015_SRR1992642_trimmed_vs_Hcalv1.sam -p 4 2> Hcal2015_SRR1992642_trimmed_vs_Hcalv1.log
```

This changed nothing, so likely some difference in species, that is, the "*H. californensis*" from [Francis 2015](https://doi.org/10.1371/journal.pone.0128742) may actually be [*H. palmata*](https://marinespecies.org/aphia.php?p=taxdetails&id=265184) or something else.

```
{ ~/hisat2-2.1.0/hisat2 -q -x UCSC_Hcal_v1.fa -1 CTE_Hormiphora_californensis_D914_T1_f.fastq.gz -2 CTE_Hormiphora_californensis_D914_T1_r.fastq.gz -p 4 --dta --max-intronlen 10000 2>&3 | ~/samtools-1.8/samtools sort - -o D914_T1_v_UCSC_Hcal_v1_hisat2.bam; } 3> D914_T1_v_UCSC_Hcal_v1_hisat2.log
~/bedtools2/bin/bedtools genomecov -ibam D914_T1_v_UCSC_Hcal_v1_hisat2.bam -bg -split > D914_T1_v_UCSC_Hcal_v1_hisat2.bg
~/ucsc/bedSort D914_T1_v_UCSC_Hcal_v1_hisat2.bg D914_T1_v_UCSC_Hcal_v1_hisat2.bg
~/ucsc/bedGraphToBigWig D914_T1_v_UCSC_Hcal_v1_hisat2.bg UCSC_Hcal_v1.sizes D914_T1_v_UCSC_Hcal_v1_hisat2.cov.bw
```

### Trans-spliced leader sequence ###
Many of the long RNAseq reads show a 40-44bp leader sequence at the 5-prime end, which causes a lot of problems with gene annotation. This occurs in [flatworms](https://doi.org/10.1016/S0166-6851(97)00040-6), [nematodes](https://doi.org/10.1371/journal.pgen.0020198), [chaetognaths](https://doi.org/10.1186/gb-2008-9-6-r94), sponges, [cnidarians](https://doi.org/10.1261/rna.1975210), some protists, and probably a lot of other organisms, meaning it is a general molecular mechanism, probably as a marker of processed mRNA.

The leader sequences themselves are extracted and counted [directly off the BAM file](https://bitbucket.org/wrf/sequences/src/master/get_read_skip_from_bam.py). This usually includes the reverse complements at around the same frequency.

```
~/samtools-1.9/samtools view UCSC_Hcal_v1_B1_LR.sorted.bam | get_read_skip_from_bam.py -n 39 - > UCSC_Hcal_v1_B1_LR.sorted.read_skip_39.tab
~/samtools-1.9/samtools view UCSC_Hcal_v1_B1_LR.sorted.bam | get_read_skip_from_bam.py - > UCSC_Hcal_v1_B1_LR.sorted.read_skip_44.tab
# Reading <stdin>, tracking S of 42-45bp
# Counted 2579890 lines for 2554695 reads
# 25195 had no CIGAR string
# 1208113 reads had either end matched to target leader length

cut -f 4 UCSC_Hcal_v1_B1_LR.sorted.read_skip_44.tab | sort | uniq -c | sort -n
```

![UCSC_Hcal_v1_B1_LR.sorted.read_skip.png](https://bitbucket.org/wrf/genome-reannotations/raw/dbb4e070a86c7c2dc564232e103b6f1c35941ffc/jbrowse-tracks/hormiphora/UCSC_Hcal_v1_B1_LR.sorted.read_skip.png)

Here are motifs with more than 1000 counts. Ones without a leader are probably fragmented, meaning they are not the true 5-prime start of the gene.

```
   1475 GGGAGTTTCAAACTTTTCAACACTACTTTATACAAATTAATTTG
   1607 CAAATTAATTTGTATAAAGTAGTGTTGAAAAGTTTGAAACTCCC
   1624 GGAGTTTTCAAACTTTTCAACACTACTTTAAACAAATTAATTTG
   1712 CAAATTAATTTGTTTAAAGTAGTGTTGAAAAGTTTGAAAACTCC
   2925 CAATAAATAGTTTTACGTTGTAGTTGATGAAAGTTTGAAACTCC
   3886 GGAGTTTCAAACTTTCATCAACTACAACGTAAAACTATTTATTG
   5259 *
 321762 GGGAGTTTCAAACTTTTCAACACTACTTTAAACAAATTAATTTG
 354257 CAAATTAATTTGTTTAAAGTAGTGTTGAAAAGTTTGAAACTCCC
1845104 0
```

The "consensus" sequence would then be `GGGAGTTTCAAACTTTTCAACACTACTTTAAACAAATTAATTTG` where it would join `AG`, making it almost resemble canonical splicing.

```
blastn -query leader_consensus.fasta -db MlScaffold09.nt.fa -outfmt 6 > leader_blastn.tab
blast2gff.py -t spliced_leader_RNA -b leader_blastn_scaffolds.tab -p blastn > leader_blastn_scaffolds.gff
```

### Comparison to Mnemiopsis scaffolds ###
The [v2.2 of the *Mnemiopsis* scaffolds](https://research.nhgri.nih.gov/mnemiopsis/download/download.cgi?dl=genome) contains 5100 scaffolds of 155Mb, N50 of 187kb, and the longest scaffold is 1.2Mb.

`~/diamond-linux64/diamond blastx -q UCSC_Hcal_v1_B1_LR.stringtie.fasta -d ~/genomes/mnemiopsis_leidyi/ML2.2.aa -o UCSC_Hcal_v1_B1_LR.stringtie.vs_ML2.2.tab`

`~/git/genomeGTFtools/blast2genomegff.py -d ~/genomes/mnemiopsis_leidyi/ML2.2.aa -g UCSC_Hcal_v1_B1_LR.stringtie.gtf -P -b UCSC_Hcal_v1_B1_LR.stringtie.vs_ML2.2.tab -p blastx -M 4 > UCSC_Hcal_v1_B1_LR.stringtie.vs_ML2.2.gff`

```
ln -s /mnt/data/genomes/hormiphora_californensis/UCSC_Hcal_v1_B1_LR.stringtie.vs_ML2.2.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff UCSC_Hcal_v1_B1_LR.stringtie.vs_ML2.2.gff --trackType CanvasFeatures --trackLabel StringTie-v-ML2
```

To make the dotplot, of `v93` against `MLv2.2`, sorting the [2nd genome](https://github.com/wrf/genomeGTFtools/blob/master/synteny_2d_w_2nd_genome_sorted.R):

`~/diamond-v2.0.13/diamond-v2.0.13 blastp -q Hcv1av93_model_proteins.pep -d ~/genomes/mnemiopsis_leidyi/ML2.2.aa -o Hcv1av93_model_proteins.vs_ml2.tab`

`~/git/genomeGTFtools/scaffold_synteny.py  -f ~/genomes/hormiphora_californensis/UCSC_Hcal_v1.fa -F ~/genomes/mnemiopsis_leidyi/MlScaffold09.nt.fa -q Hcv1av93.gff -d ~/genomes/mnemiopsis_leidyi/ML2.2.gff3 -b Hcv1av93_model_proteins.vs_ml2.tab -G 20 -l 111 -L 100 --ignore-gene-features --local-positions > hcv1.av93_v_ml2_2d_synteny_points.local.tab`

`Rscript ~/git/genomeGTFtools/synteny_2d_w_2nd_genome_sorted.R hcv1.av93_v_ml2_2d_synteny_points.local.tab`

![hcv1.av93_v_ml2_2d_synteny_points.local.png](https://bitbucket.org/wrf/genome-reannotations/raw/b4542f9faf15e0c6eb66a15af680c8345594d83b/jbrowse-tracks/hormiphora/hcv1.av93_v_ml2_2d_synteny_points.local.png)

### Comparison to Pleurobrachia scaffolds ###
The *Pleurobrachia bachei* genome was released in [2014](https://doi.org/10.1038/nature13400), a few months after the *Mnemiopsis leidyi* genome. Compared to the *Mnemiopsis* assembly, the *Pleurobrachia* assembly is rather poor, with 21979 scaffolds, N50 of 20kb, and the longest sequence only 320kb. The genome is obviously highly fragmented, is unclear from the methods exactly how they assembled it, and [did not include any annotation files](https://neurobase.rc.ufl.edu/Pleurobrachia/download) at all.

```
~/minimap2-2.17_x64-linux/minimap2 -a -x splice --secondary=no ~/genomes/hormiphora_californensis/UCSC_Hcal_v1.fa pbachei_03_filtered_gene_models_transcripts.not_in_horm.fa | ~/samtools-1.9/samtools sort - -o pbachei_03_filtered_gene_models_transcripts.not_in_horm.bam
~/samtools-1.9/samtools index pbachei_03_filtered_gene_models_transcripts.not_in_horm.bam
~/git/pinfish/spliced_bam2gff/spliced_bam2gff pbachei_03_filtered_gene_models_transcripts.not_in_horm.bam > pbachei_03_filtered_gene_models_transcripts.not_in_horm.gtf
```

Here the [Pleurobrachia scaffolds](https://www.ncbi.nlm.nih.gov/Traces/wgs/AVPN01) are mapped to the *Hormiphora* assembly using [lastz](https://github.com/lastz/lastz):

```
~/git/lastz-1.04.00/src/lastz Hcal_v1.fa ~/genomes/pleurobrachia_bachei/AVPN01.1.scaffolds.fa --format=blastn ‑‑action:target=multiple > pbac_vs_hcal_v1.lastz.tab

~/git/lastz-1.04.00/src/lastz Hcal_v1.fa ~/genomes/pleurobrachia_bachei/AVPN01.1.scaffolds.fa --nogapped --format=blastn ‑‑action:target=multiple > pbac_vs_hcal_v1.lastz_nogap.tab

~/git/lastz-1.04.00/src/lastz Hcal_v1.fa ~/genomes/pleurobrachia_bachei/AVPN01.1.scaffolds.fa --nogapped --format=maf ‑‑action:target=multiple > pbac_vs_hcal_v1.lastz_nogap.maf

~/gmap-2019-09-12/bin/gmap -d Hcal_v1 -B 5 -t 4 -f 2 -n 1 -E genomic --cross-species --nosplicing ~/genomes/pleurobrachia_bachei/AVPN01.1.scaffolds.fa > pbac_vs_hcal_v1.gmap.gff 2> pbac_vs_hcal_v1.gmap.log
```


