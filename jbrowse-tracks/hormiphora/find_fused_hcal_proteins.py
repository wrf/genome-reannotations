#!/usr/bin/env python
#
# find_fused_hcal_proteins.py 2020-02-26

'''
find_fused_hcal_proteins.py
'''

import sys
from collections import defaultdict

def combine_intervals(rangelist):
	'''convert list of tuples to non redundant invervals'''
	# sl = [(1,10), (1,6), (15,20), (1,10), (19,29), (30,35), (6,13), (40,48), (15,21), (42,50)]
	nrintervallist = []
	srtrangelist = sorted(rangelist) # sort list now, to only do this once
	interval = srtrangelist[0] # need to start with first time, which will be the same for the first bounds
	for bounds in srtrangelist:
		# since it is sorted bounds[0] should always be >= interval[0]
		if bounds[0] > interval[1]+1: # if the next interval starts past the end of the first + 1
			nrintervallist.append(interval) # add to the nr list, and continue with the next
			interval = bounds
		else:
			if bounds[1] > interval[1]: # bounds[1] <= interval[1] means do not extend
				interval = (interval[0], bounds[1]) # otherwise extend the interval
	else: # append last interval
		nrintervallist.append(interval)
	# should return [(1, 13), (15, 35), (40, 50)]
	return nrintervallist



if len(sys.argv) < 2:
	sys.exit( __doc__ )
else:
	hitcounter = 0
	genedict = defaultdict( lambda: defaultdict(str) )
	for blastfile in sys.argv[1:]:
		print >> sys.stderr, "# Reading {}".format(blastfile )
		for line in open(blastfile,'r'):
			line = line.strip()
			if line:
				hitcounter += 1
				lsplits = line.split("\t")
				geneid = lsplits[0]
				subjectid = lsplits[1].split('|')[-1]
				querybounds = (int(lsplits[6]), int(lsplits[7]))
				genedict[geneid][querybounds] = subjectid
	print >> sys.stderr, "# Counted {} HSPs for {} genes".format(hitcounter, len(genedict) )
	problemcounter = 0
	for gene in sorted(genedict.keys()):
	#	print >> sys.stderr, gene, genedict.get(gene).keys()
		nrbounds = combine_intervals(list(genedict.get(gene).keys()))
		if len(nrbounds)>1:
			problemcounter += 1
			print >> sys.stdout, gene, nrbounds
			for bounds, subject in genedict.get(gene).items():
				print >> sys.stdout, "  {} {}".format(bounds, subject)
	print >> sys.stderr, "# Counted {} genes with unmerged bounds".format(problemcounter)
