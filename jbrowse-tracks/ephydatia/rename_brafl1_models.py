#!/usr/bin/env python

import sys

linecounter = 0
for line in open(sys.argv[1],'r'):
	line = line.strip()
	if line and line[0]!="#":
		linecounter += 1
		lsplits = line.split("\t")
		feature = lsplits[2]
		attributes = lsplits[8]
		attrd = dict([(field.strip().split(" ")) for field in attributes.split(";") if field])
		if feature=="mRNA":
			newattrs = "ID={};PACid={}".format( attrd.get("mRNA"), attrd.get("PACid") )
			lsplits[8] = newattrs
		elif feature=="3'-UTR":
			lsplits[2] = "three_prime_UTR"
			newattrs = "ID={0}.3pUTR;Parent={0}".format( attrd.get("mRNA") )
			lsplits[8] = newattrs
		elif feature=="5'-UTR":
			lsplits[2] = "five_prime_UTR"
			newattrs = "ID={0}.5pUTR;Parent={0}".format( attrd.get("mRNA") )
			lsplits[8] = newattrs
		elif feature=="CDS":
			newattrs = "ID={0}.CDS;Parent={0}".format( attrd.get("mRNA") )
			lsplits[8] = newattrs
		print >> sys.stdout, "\t".join(lsplits)
print >> sys.stderr, "Rewrote {} lines".format(linecounter)

