#!/usr/bin/env python
# v1.0 created 2016-05-16

'''rename_ephydatia_augustus.py    last modified 2019-01-10

rename_ephydatia_augustus.py -c conversion_vector -g genes.gtf > renamed_genes.gtf

    creates conversion vector as:
oldnamecontig123    newnamecontig001

'''

import sys
import time
import argparse
from collections import defaultdict

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-c','--conversion', help="optional output file for naming conversions")
	parser.add_argument('-d','--delimiter', default='_', help="delimiter for headers")
	parser.add_argument('-f','--format', default='fasta', help="format of sequences [fasta] or fastq")
	parser.add_argument('-g','--gtf', help="gtf or gff format file", required=True)
	parser.add_argument('-l','--length', nargs='?', const="len", help="include length in fasta ID, printed as [len]")
	parser.add_argument('-n','--name', help="name for output [scaffold]", default="scaffold")
	parser.add_argument('-o','--old', action="store_true", help="keep old name as space separated description")
	parser.add_argument('-z','--omit-zero', action="store_true", help="do not add extra zeroes to number")
	args = parser.parse_args(argv)

	genes_per_scaffold = defaultdict(int)
	conversion_dict = {} # key is old name, value is new name
	print >> sys.stderr, "# Converting {}".format(args.gtf)
	for line in open(args.gtf,'r'):
		line = line.strip()
		if line:
			lsplits = line.split("\t")
			scaffold = lsplits[0] # should be like scaffold_0001
			scaffold_num = scaffold.split("_")[-1] # just the 0001
			feature = lsplits[2]
			attributes = lsplits[8]
			attrd = dict([(field.strip().split("=")) for field in attributes.split(";") if field])
			if feature=="gene":
				genes_per_scaffold[scaffold] += 1
				geneid = attrd.get("ID")
				genenum = genes_per_scaffold.get(scaffold)
				newid = "Em{0}g{1}a".format(scaffold_num, genenum)
				newattrs = "ID={0};Name={0}".format(newid)
				conversion_dict[geneid] = newid
			elif feature=="mRNA":
				geneid = conversion_dict.get(attrd.get("Parent"))
				transid = attrd.get("ID")
				transnum = transid.split(".")[-1]
				newid = "{}.{}".format( geneid, transnum )
				newattrs = "ID={0};Parent={1};Name={0}".format(newid, geneid)
				conversion_dict[transid] = newid
			else:
				transid = conversion_dict.get(attrd.get("Parent"))
				featid = attrd.get("ID")
				feattype = featid.split('.')[-1]
				newid = "{}.{}".format( transid, feattype )
				newattrs = "ID={0};Parent={1}".format(newid, transid)
				conversion_dict[featid] = newid
			lsplits[8] = newattrs
			outline = "\t".join(lsplits)
			print >> sys.stdout, outline
	print >> sys.stderr, "# Writing to conversion file {}".format(args.conversion)
	with open(args.conversion,'w') as cv:
		for k,v in conversion_dict.iteritems():
			print >> cv, "{}\t{}".format( k,v.replace(".t1","") )

	# ../../rename_ephydatia_augustus.py -c augustus_rename_vector.txt -g augustus_vs_nb.w_hints.gff > augustus_vs_nb_sysnames.gff
	# rename_select_fasta.py augustus_rename_vector.txt augustus.aa > augustus_sysnames_prots.fasta
	# ~/git/genomeGTFtools/blast2genomegff.py -b emuelleri_augustus_vs_human_blastp_e3.tab -p blastp -g hints_utr_pred/augustus_vs_nb_sysnames.gff -d ~/db/human_uniprot.fasta -x -S --gff-delimiter ".t^C> emuelleri_augustus_vs_human_blastp_e3.gff


if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
