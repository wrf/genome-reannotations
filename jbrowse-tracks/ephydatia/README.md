# Ephydatia muelleri #
Using the *Ephydatia muelleri* gene models from from [Kenny 2020](https://doi.org/10.1038/s41467-020-17397-w). Data are [here](https://bitbucket.org/EphydatiaGenome/ephydatiagenome/downloads/), see [Ephybase](https://spaces.facsci.ualberta.ca/ephybase/) for more info.

## prepare scaffolds ##

`~/git/lavaLampPlot/hits_to_coverage.py -f final_assembly.fa -b lib_001_vs_final.sorted.hits_from_bam.txt > final_assembly.coverage_gc.tab`

`Rscript ~/git/lavaLampPlot/contig_gc_coverage.R final_assembly.coverage_gc.tab`

`sizecutter.py -p -a 1000000 final_assembly.fa | sizecutter.py -f -g GC -l  - `

Contig `Scaffold_153;HRSCAF=192` has GC 34.43%, while the rest are 41%. Assuming it is bacterial, genes are predicted with [PRODIGAL](https://github.com/hyattpd/Prodigal):

`getAinB.py "Scaffold_153;HRSCAF=192" final_assembly.fa > Scaffold_153_HRSCAF_192.fasta`

`~/prodigal/prodigal.linux -a Scaffold_153_HRSCAF_192.prots.fa -d Scaffold_153_HRSCAF_192.nucls.fa -f gff -o Scaffold_153_HRSCAF_192.prodigal.gff -i Scaffold_153_HRSCAF_192.fasta`

Tried getting rRNA using [RNAmmer](http://www.cbs.dtu.dk/services/RNAmmer/), instead used [barrnap](https://github.com/tseemann/barrnap). 16S ribosomal RNA is 98% identical to [Flavobacterium sp. strain GS13 16S ribosomal RNA gene, complete sequence](https://www.ncbi.nlm.nih.gov/nucleotide/MH168565.1).

`~/git/barrnap/bin/barrnap -o Scaffold_153_HRSCAF_192.barrnap.fa Scaffold_153_HRSCAF_192.fasta > Scaffold_153_HRSCAF_192.barrnap.gff`

## genome browser, with bacteria removed, renumbered ##
With 1444 scaffolds including 39329 protein models, though some of these are short sequences of AT repeats, and should have been removed. This accounts for 362 scaffolds for appx 4Mb with GC content of 0 percent (effectively), in file `emue_v1_scaffolds.to_exclude.txt`, though these were not removed in the paper.

```
~/samtools-1.8/samtools faidx renumbered_final_nb.fa
ln -s /mnt/data/genomes/ephydatia_muelleri/ASM_HIC_394/renumbered_final_nb.fa renumbered_final_nb.fa
ln -s /mnt/data/genomes/ephydatia_muelleri/ASM_HIC_394/renumbered_final_nb.fa.fai renumbered_final_nb.fa.fai
/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta renumbered_final_nb.fa --out ./

ln -s /mnt/data/genomes/ephydatia_muelleri/augustus/augustus_vs_nb_sysnames.gff augustus_vs_nb_sysnames.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff augustus_vs_nb_sysnames.gff --trackType CanvasFeatures --trackLabel AUGUSTUS-UTR

ln -s /mnt/data/genomes/ephydatia_muelleri/trinity/Emu1-2_Trinity_f2_p3_rfnb.gff Emu1-2_Trinity_f2_p3_rfnb.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Emu1-2_Trinity_f2_p3_rfnb.gff --trackType CanvasFeatures --trackLabel trinity-p3

~/git/genomeGTFtools/rename_gtf_contigs.py -c ../ASM_HIC_394/renumbered_final_nb.vector -g juvenile_SRR1041944_i100k_vs_final.stringtie.gff > juvenile_SRR1041944_i100k_vs_rfnb.stringtie.gff
ln -s /mnt/data/genomes/ephydatia_muelleri/rnaseq/juvenile_SRR1041944_i100k_vs_rfnb.stringtie.gff juvenile_SRR1041944_i100k_vs_rfnb.stringtie.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff juvenile_SRR1041944_i100k_vs_rfnb.stringtie.gff --trackType CanvasFeatures --trackLabel stringtie-juvenile

~/git/genomeGTFtools/rename_gtf_contigs.py -c ../ASM_HIC_394/renumbered_final_nb.vector -g juvenile_SRR1041944_vs_final.sorted.bam.cov -n > juvenile_SRR1041944_vs_rfnb.sorted.bam.cov
 ~/ucsc/bedSort juvenile_SRR1041944_vs_rfnb.sorted.bam.cov juvenile_SRR1041944_vs_rfnb.sorted.bam.cov
~/ucsc/bedGraphToBigWig juvenile_SRR1041944_vs_rfnb.sorted.bam.cov ../ASM_HIC_394/renumbered_final_nb.sizes juvenile_SRR1041944_vs_rfnb.bigwig

~/git/genomeGTFtools/rename_gtf_contigs.py -c ../ASM_HIC_394/renumbered_final_nb.vector -n -g lib_002_vs_final.sorted.bam.cov > lib_002_vs_rfnb.sorted.bam.cov
~/ucsc/bedSort lib_002_vs_rfnb.sorted.bam.cov lib_002_vs_rfnb.sorted.bam.cov
```

Make gap track, using [repeat2gtf.py](https://github.com/wrf/genomeGTFtools/blob/master/repeat2gtf.py). These are basically all either 100bp or 1000bp, introduced at different stages of the scaffolding.

```
~/git/genomeGTFtools/repeat2gtf.py -l renumbered_final_nb.fa > renumbered_final_nb.N_gaps.gff

# Counted 1444 sequences
# Counted 2186 repeats of 1864700 total bases, average 853.02
# Longest repeat was 1000 bases on scaffold_0001
```

## preparing diamond vs model organisms ##

```
~/diamond-linux64/diamond blastp -d ~/db/model_organism_uniprot.fasta -q augustus_sysnames_prots.fasta -o augustus_sysnames_prots.vs_models_dmnd.tab

~/git/genomeGTFtools/blast2genomegff.py -b augustus_sysnames_prots.vs_models_dmnd.tab -p blastp -g augustus_vs_nb_sysnames.gff -d ~/db/model_organism_uniprot.fasta -S -x --gff-delimiter ".t" -M 2 > augustus_vs_nb_sysnames.vs_models.gff

ln -s /mnt/data/genomes/ephydatia_muelleri/augustus/augustus_vs_nb_sysnames.vs_models.gff augustus_vs_nb_sysnames.vs_models.gff

/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff augustus_vs_nb_sysnames.vs_models.gff --trackType CanvasFeatures --trackLabel blast-vs-SwissProt
```

## PFAM domains ##
Using the [PFAM downloads](https://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/):

`hmmscan --domE 0.1 --cpu 4 --domtblout augustus_sysnames_prots.pfam.tab ~/db/Pfam-A.hmm hints_utr_pred/augustus_sysnames_prots.fasta > /dev/null`

`~/git/genomeGTFtools/pfam2gff.py -g augustus_vs_nb_sysnames.gff.gz -e 1e-2 -i augustus_sysnames_prots.pfam.tab.gz -d "." > augustus_sysnames_prots.pfam.gff`

## mapping of related species ##
Dataset for *Spongilla lacustris* [on Zenodo](https://zenodo.org/record/5094890), file `spongilla_transcriptome_annotated_200501.fasta` contains 62180 transcripts, though only 24055 were mapped to the genome.

```
~/minimap2-2.23_x64-linux/minimap2 -a -x splice --secondary=no renumbered_final_nb.fa spongilla_transcriptome_annotated_200501.fasta | ~/samtools-1.14/samtools sort - -o spongilla_transcriptome_annotated_200501.vs_emue1.bam
~/samtools-1.14/samtools flagstat spongilla_transcriptome_annotated_200501.vs_emue1.bam
#62739 + 0 in total (QC-passed reads + QC-failed reads)
#62180 + 0 primary
#559 + 0 supplementary
#24055 + 0 mapped (38.34% : N/A)
#23496 + 0 primary mapped (37.79% : N/A)
~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M -s spongilla_transcriptome_annotated_200501.vs_emue1.bam > spongilla_transcriptome_annotated_200501.vs_emue1.gtf
~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py spongilla_transcriptome_annotated_200501.vs_emue1.gtf > spongilla_transcriptome_annotated_200501.vs_emue1.gff
```

The Spongilla transcriptome [GFF is downloadable here](https://bitbucket.org/wrf/genome-reannotations/downloads/spongilla_transcriptome_annotated_200501.vs_emue1.gff.gz).

## gene families ##
Several tandem clusters are evident, as well as a few incompletely assigned sequences, possibly they are nearly identical sequences but are distinct loci in the genome.

`blastp -query emu_silicateins.trimmed.fa -db ~/est/porifera/Ephydatia_augustus_sysnames_prots.fasta -outfmt 6 -evalue 1e-100`

```
Silicatein_Ephydatiamuelleri1	Em0017g930a	93.617	188	0	7	1	176	153	340	2.55e-114	328

Silicatein_Ephydatiamuelleri2	Em0011g357a	86.842	190	13	7	1	178	129	318	1.12e-106	308
Silicatein_Ephydatiamuelleri2	Em0011g358a	86.316	190	14	7	1	178	129	318	1.52e-106	308
Silicatein_Ephydatiamuelleri2	Em0011g353a	86.842	190	13	7	1	178	129	318	1.89e-106	308
Silicatein_Ephydatiamuelleri2	Em0011g351a	86.316	190	14	7	1	178	129	318	2.72e-106	307

Silicatein_Ephydatiamuelleri3	Em0005g1108a	93.684	190	0	7	1	178	135	324	2.89e-115	330

Silicatein_Ephydatiamuelleri4	Em0003g1438a	93.684	190	0	7	1	178	131	320	1.45e-115	331

Silicatein_Ephydatiamuelleri5	Em0003g1439a	93.617	188	0	7	1	176	129	316	2.27e-114	328

Silicatein_Ephydatiamuelleri6	Em0003g1437a	94.012	167	0	5	1	157	129	295	2.92e-101	293

Silicatein_Ephydatiamuelleri7	Em0011g357a	60.317	189	62	8	1	176	130	318	8.35e-65	202
Silicatein_Ephydatiamuelleri7	Em0011g358a	59.788	189	63	8	1	176	130	318	1.06e-64	201
Silicatein_Ephydatiamuelleri7	Em0011g353a	59.788	189	63	8	1	176	130	318	1.36e-64	201
Silicatein_Ephydatiamuelleri7	Em0011g351a	59.788	189	63	8	1	176	130	318	1.40e-64	201

Silicatein_Ephydatiamuelleri8	Em0009g684a	92.708	192	0	7	1	178	136	327	1.62e-116	333

Silicatein_Ephydatiamuelleri9	Em0017g931a	95.238	126	0	3	1	120	141	266	3.25e-77	231

Silicatein_Ephydatiamuelleri10	Em0056g25a	86.842	190	13	7	1	178	129	318	4.68e-107	309
Silicatein_Ephydatiamuelleri10	Em0003g1460a	86.842	190	13	7	1	178	129	318	4.68e-107	309

Silicatein_Ephydatiamuelleri11	Em0003g1436a	93.514	185	0	7	1	173	133	317	7.57e-111	319
```

For cathepsins, the collinearity with silicateins is not as evident:

```
CathepsinL_Amphimedon1	Em0019g324a	64.737	190	56	7	1	179	128	317	4.07e-78	236
CathepsinL_Amphimedon1	Em0020g69a	55.789	190	73	7	1	179	127	316	3.74e-62	195
CathepsinL_Amphimedon1	Em0020g64a	54.688	192	73	8	1	179	129	319	1.06e-60	191
CathepsinL_Amphimedon1	Em0017g816a	55.729	192	72	8	1	179	127	318	4.09e-57	182
CathepsinL_Amphimedon1	Em0005g1131a	51.579	190	80	7	1	179	126	314	6.36e-55	176
CathepsinL_Amphimedon1	Em0017g884a	52.083	192	78	8	1	179	125	315	1.46e-54	176
CathepsinL_Amphimedon1	Em0017g868a	51.064	188	69	7	1	179	125	298	9.69e-53	170
CathepsinL_Amphimedon1	Em0017g885a	52.356	191	77	8	1	179	125	313	3.41e-52	169

sp|P07711|CATL1_HUMAN	Em0019g324a	51.632	337	147	7	1	333	1	325	1.15e-117	343
sp|P07711|CATL1_HUMAN	Em0020g69a	52.568	331	144	8	7	333	3	324	1.49e-116	340
sp|P07711|CATL1_HUMAN	Em0017g816a	52.381	336	142	9	4	333	3	326	9.64e-110	323
sp|P07711|CATL1_HUMAN	Em0020g64a	51.682	327	145	9	12	333	9	327	9.95e-110	323
sp|P07711|CATL1_HUMAN	Em0017g884a	49.394	330	149	8	10	333	6	323	1.97e-105	311
sp|P07711|CATL1_HUMAN	Em0017g885a	50.641	312	137	7	28	333	21	321	1.29e-104	309
sp|P07711|CATL1_HUMAN	Em0017g868a	48.328	329	139	9	9	333	5	306	9.68e-100	296
sp|P07711|CATL1_HUMAN	Em0011g351a	43.636	330	174	6	9	333	4	326	9.76e-92	277
sp|P07711|CATL1_HUMAN	Em0005g1131a	49.201	313	145	8	24	331	17	320	1.18e-91	276

sp|P07711|CATL1_HUMAN	Em0003g1438a	43.750	336	173	7	4	333	3	328	1.47e-91	276
sp|P07711|CATL1_HUMAN	Em0056g25a	43.465	329	174	6	10	333	5	326	3.19e-91	275
sp|P07711|CATL1_HUMAN	Em0003g1460a	43.465	329	174	6	10	333	5	326	3.19e-91	275
```


