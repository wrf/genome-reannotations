#!/usr/bin/env python

'''
organize_srosetta_gff.py
'''

import sys

if len(sys.argv) < 2:
	print >> sys.stderr, __doc__
else:
	for line in open(sys.argv[1],'r'):
		line = line.strip()
		if line and line[0]!="#":
			lsplits = line.split("\t")
			feature = lsplits[2]
			attributes = lsplits[8]
			attributes = attributes.replace('gene:','')
			attributes = attributes.replace('transcript:','')
			lsplits[8] = attributes
			print >> sys.stdout, "\t".join(lsplits)
