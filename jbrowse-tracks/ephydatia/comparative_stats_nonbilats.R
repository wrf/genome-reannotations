# comparative stats of non-bilaterian genomes
# created by WRF 2019-10-11
# also happy birthday Ana!

statfile = "~/genomes/ephydatia_muelleri/size_overview/comparative_stats_nonbilats.tab"
stattable = read.table(statfile, header=TRUE, sep="\t", row.names=1)

scaffolds = stattable[["Scaffolds"]]
n50_mb = stattable[["N50"]]
total_mb = stattable[["Total"]]

# regex to make Gsp from Genus species
threeletternames = gsub("([\\w])\\w+\\s([\\w][\\w]\\d?).*","\\1\\2",row.names(stattable),perl=TRUE)

# name position and adjustment
# "Twi" "Aqu" "Sci" "Emu" "Mle" "Tad" "Nve" "Hma" "Bfl" "Dme"
name_adj = c(1, 1, 1, 1.15, 1, 1.15, 1, 1, 1.15)
pos_adj =  c(3, 1, 4, 1, 4, 1, 4, 2, 1)

#            purple for sponges                     green        red for other nonbilats                             blue         yellow
colorset = c("#b463b488", "#b463b488", "#b463b488", "#6eb95c88", "#dc793788", "#dc793788", "#dc793788", "#dc793788", "#5ab6c3bb", "#fec44f88")

pdf(file="~/genomes/ephydatia_muelleri/size_overview/comparative_stats_nonbilats.pdf", width=5, height=5)
par(mar=c(4.5,4.5,0.5,0.5))
plot(total_mb, scaffolds, xlim=c(80,900), ylim=c(350,15000), cex=n50_mb*2+1, log="xy", pch=16, col=colorset, xlab="Total Genome Size (Mb)", ylab="Number of scaffolds", cex.axis=1.4, cex.lab=1.4, frame.plot=FALSE, axes=FALSE)
axis(1, at=c(100,200,400,800), labels=TRUE, cex.axis=1.4)
axis(2, at=c(500,1000,2000,4000,8000), labels=c("500","1k","2k","4k","8k"), cex.axis=1.4)
text(total_mb, scaffolds*name_adj, threeletternames, pos=pos_adj)
#text(total_mb, scaffolds*name_adj, row.names(stattable), pos=pos_adj)
dev.off()



statfile = "~/genomes/ephydatia_muelleri/size_overview/comparative_stats_sponge_only.tab"
stattable = read.table(statfile, header=TRUE, sep="\t", row.names=1)

scaffolds = stattable[["Scaffolds"]]
n50_mb = stattable[["N50"]]
total_mb = stattable[["Total"]]

# regex to make Gsp from Genus species
threeletternames = gsub("([\\w])\\w+\\s([\\w][\\w]\\d?).*","\\1\\2",row.names(stattable),perl=TRUE)

# name position and adjustment
# "Twi" "Aqu" "Sci" "Emu" 
name_adj = c(1, 1, 1, 1.15, 1, 1, 1)
pos_adj =  c(4, 4, 4, 1, 4, 4, 1)

#            purple for sponges                     green        purple
colorset = c("#b463b488", "#b463b488", "#b463b488", "#6eb95c88", "#b463b488", "#b463b488", "#b463b488")

pdf(file="~/genomes/ephydatia_muelleri/size_overview/comparative_stats_sponge_only.pdf", width=7, height=7)
par(mar=c(4.5,4.5,0.5,0.5))
plot(total_mb, scaffolds, xlim=c(40,500), ylim=c(600,100000), cex=n50_mb*2+1, log="xy", pch=16, col=colorset, xlab="Total Genome Size (Mb)", ylab="Number of scaffolds", cex.axis=1.4, cex.lab=1.4, frame.plot=FALSE, axes=FALSE)
axis(1, at=c(50,100,200,400), labels=TRUE, cex.axis=1.4)
axis(2, at=c(1000,5000,10000,50000), labels=c("1k","5k","10k","50k"), cex.axis=1.4)
text(total_mb, scaffolds*name_adj, threeletternames, pos=pos_adj, cex=1.3)
#text(total_mb, scaffolds*name_adj, row.names(stattable), pos=pos_adj)
dev.off()









#