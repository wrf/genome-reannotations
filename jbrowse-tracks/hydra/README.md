# Hydra vulgaris processing steps
From [Chapman 2010 The dynamic genome of Hydra](https://doi.org/10.1038/nature08830) (not entirely clear what "dynamic" means in this context)

### Process scaffolds for display
Prepare scaffold index for the [Dovetail reassembly](https://arusha.nhgri.nih.gov/hydra/download/?dl=asl). The original annotations do not appear to be hosted by JGI anymore.

`~/samtools-1.9/samtools faidx Hm105_Dovetail_Assembly_1.0.fasta`

Link and create the reference sequence:

```
ln -s /mnt/data/genomes/hydra_magnipapillata/Hm105_Dovetail_Assembly_1.0.fasta
ln -s /mnt/data/genomes/hydra_magnipapillata/Hm105_Dovetail_Assembly_1.0.fasta.fai 
/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta Hm105_Dovetail_Assembly_1.0.fasta --out ./
```

### Add gap track ###

`~/git/genomeGTFtools/repeat2gtf.py Hm105_Dovetail_Assembly_1.0.fasta -l > Hm105_Dovetail_Assembly_1.0.gaps.gff`

```
# Counted 5525 sequences
# Counted 121961 repeats of 68301254 total bases, average 560.03
# Longest repeat was 7489 bases on Sc4wPfr_374
```

### Add StringTie models ###
Using libraries [SRR922615](https://www.ncbi.nlm.nih.gov/sra/SRX315376) (should be from [Fidler 2014](https://doi.org/10.1073/pnas.1318499111)) and [SRR1024340](https://www.ncbi.nlm.nih.gov/sra/SRX329137) (from [Juliano 2014](https://doi.org/10.1073/pnas.1320965111)).

These were mapped with [tophat2](https://genomebiology.biomedcentral.com/articles/10.1186/gb-2013-14-4-r36) (79.8% total mapping), then transcripts generated with [stringtie](https://ccb.jhu.edu/software/stringtie/index.shtml). The tophat command is below, the stringtie appears to be lost, but was probably default parameters.

`~/tophat-2.0.13.Linux_x86_64/tophat2 -o hydra_sra -p 6 --library-type fr-unstranded Hm105_Dovetail_Assembly_1.0 SRR1024340_1.fastq.gz,SRR922615_1.fastq.gz SRR1024340_2.fastq.gz,SRR922615_2.fastq.gz`

This produced 52366 transcripts with exons 236761. Then, convert the stringtie GTF to a GFF:

`~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py hydra_stringtie.gtf > hydra_stringtie.gff`

### Add AUGUSTUS models ###
From [NHGRI](https://arusha.nhgri.nih.gov/hydra/download/?dl=nt). This annotation has 33820 genes, of 36059 transcripts for 182777 exons.

It appears I generated exon features from each CDS, probably with [augustus_to_gff3.py](https://github.com/wrf/genomeGTFtools/blob/master/misc/augustus_to_gff3.py)

### Mapping transcriptomes ###
`~/minimap2-2.17_x64-linux/minimap2 -a -x splice -t 4 --secondary=no Hm105_Dovetail_Assembly_1.0.fasta Juliano_transcript.fa.gz | ~/samtools-1.9/samtools sort - > Juliano_transcript.bam`

`~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M -g Juliano_transcript.bam > Juliano_transcript.pinfish.gtf`

`~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py Juliano_transcript.pinfish.gtf | sed s/TRINITY_//g > Juliano_transcript.pinfish.gff`

`~/minimap2-2.17_x64-linux/minimap2 -a -x splice -t 4 --secondary=no Hm105_Dovetail_Assembly_1.0.fasta Petersen_transcript.fa.gz | ~/samtools-1.9/samtools sort - > Petersen_transcript.bam`

`~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M -g Petersen_transcript.bam > Petersen_transcript.pinfish.gtf`

`~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py Petersen_transcript.pinfish.gtf > Petersen_transcript.pinfish.gff`

### Microsynteny with other cnidarians ###
From the AUGUSTUS annotation, extract only the gene features for the microsynteny steps:

`grep gene hydra.augustus.gff3 > hydra.augustus.gene_only.gff`

For the other hydrozoan [Clytia hemisphaerica](http://marimba.obs-vlfr.fr/organism/Clytia/hemisphaerica). The sequences have a header like:

```
>TCONS_00000001-protein
```

while the IDs in the GFF are `ID=TCONS_00000138`. So rename the proteins and align:

`fastarenamer.py -s 0 -d "-" full_nr_align_prot.fasta > full_nr_align_prot.short_header.fasta`

`~/diamond-latest/diamond makedb --in full_nr_align_prot.short_header.fasta --db full_nr_align_prot.short_header.fasta`

` ~/diamond-latest/diamond blastp -q hydra_augustus_shortname_prots.fasta -d ~/genomes/clytia_hemisphaerica/full_nr_align_prot.short_header.fasta -o hydra_augustus_vs_clytia1.tab`

A higher `-z` (max distance to next gene) value is used since the genome is larger.

`~/git/genomeGTFtools/microsynteny.py -q hydra.augustus.gene_only.gff -d ~/genomes/clytia_hemisphaerica/merged_transcript_models.gff3 -b hydra_augustus_vs_clytia1.tab -G --blast-query-delimiter "." -z 100000 > hydra_augustus_vs_clytia1.microsynteny.gff`

Many of the matches appear to be false positives due to split genes, 1284 split genes in either genome, out of only 1181 query genes total in blocks.

On the scyphozoan [Nemopilema nomurai](https://www.ncbi.nlm.nih.gov/Traces/wgs/PEDN01), by [Kim 2019](https://doi.org/10.1186/s12915-019-0643-7):

`~/diamond-latest/diamond blastp -q hydra_augustus_shortname_prots.fasta -d ~/genomes/nemopilema_nomurai/NNO.final_gene_set.pep.fa -o hydra_augustus_vs_NNOv1.tab`

`~/git/genomeGTFtools/microsynteny.py -q hydra.augustus.gene_only.gff -d ~/genomes/nemopilema_nomurai/NNO.final_gene_set.gff3.gz -b hydra_augustus_vs_NNOv1.tab -G --blast-query-delimiter "." -z 100000 > hydra_augustus_vs_NNOv1.microsynteny.gff`

There are even fewer matches to scyphozoans. Other hydrozoans are needed for a proper comparison.

