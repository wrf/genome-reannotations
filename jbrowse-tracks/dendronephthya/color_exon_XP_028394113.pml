hide everything
bg white
show cartoon, (chain A)
set_color excolor_1, [0.65,0.81,0.89]
select exon_1, (resi 1-97) & (chain A)
color excolor_1, exon_1
set_color excolor_2, [0.12,0.47,0.71]
select exon_2, (resi 99-320) & (chain A)
color excolor_2, exon_2
set_color outphase, [0.3,0.3,0.3]
select out_of_phase, (resi 98) & (chain A)
color outphase, out_of_phase
