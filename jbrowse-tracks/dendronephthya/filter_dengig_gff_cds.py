#!/usr/bin/env python

"""
gzip -dc GCF_004324835.1_DenGig_1.0_protein.faa.gz > GCF_004324835.1_DenGig_1.0_protein.faa
renamegenbankfasta.py GCF_004324835.1_DenGig_1.0_protein.faa > dendronephthya_gigantea_prots_renamed.fa

gzip -dc GCF_004324835.1_DenGig_1.0_genomic.gff.gz > GCF_004324835.1_DenGig_1.0_genomic.gff
./filter_dengig_gff_cds.py GCF_004324835.1_DenGig_1.0_genomic.gff > GCF_004324835.1_DenGig_1.0_genomic.cds_only.gff
"""

import sys

for line in open(sys.argv[1]):
	if line[0]=="#":
		continue
	lsplits = line.split("\t")
	if lsplits[2]=="CDS":
		outline = line.replace("cds-","")
		sys.stdout.write(outline)
