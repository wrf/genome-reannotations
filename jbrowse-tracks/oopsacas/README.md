# Oopsacas minuta #
Genome of the glass sponge *Oopsacas minuta*, preprinted by [Santini 2022](https://www.biorxiv.org/content/10.1101/2022.07.26.501511v1.full).

![oopsacas_preprint_browser_screenshot.png](https://bitbucket.org/wrf/genome-reannotations/raw/e3f2904783f628efa592c27d417aead9f3497376/jbrowse-tracks/oopsacas/oopsacas_preprint_browser_screenshot.png)

## prepare scaffolds ##
Not-quite chromosome-level scaffolds from [NCBI WGS JAKMXF01](https://www.ncbi.nlm.nih.gov/Traces/wgs/JAKMXF01) have 365 scaffolds of 61Mb, with N50 of 0.6Mb. Fasta index is generated with [samtools](https://github.com/samtools/samtools).

```
gunzip JAKMXF01.1.fsa_nt.gz
mv JAKMXF01.1.fsa_nt JAKMXF01.1.fsa_nt.fasta
~/samtools-1.14/samtools faidx JAKMXF01.1.fsa_nt.fasta

sizecutter.py -Q JAKMXF01.1.fsa_nt.fasta
# Parsing sequences from JAKMXF01.1.fsa_nt.fasta:
# Counted 365 sequences:
# Total input letters is: 61460524
# Average length is: 168385.00
# Median length is: 31236
# Shortest sequence is: 1002
# Longest sequence is: 1926057
# n50 length is: 30811610 and n50 is: 676369.00
# n10 length is: 56260604 and n10 is: 1373860.00
```

Make gap track:

```
~/git/genomeGTFtools/repeat2gtf.py -l JAKMXF01.1.fsa_nt.fasta > JAKMXF01.1.fsa_nt.gaps.gff

# Parsing repeats of N and n from JAKMXF01.1.fsa_nt.fasta
# Counted 365 sequences
# Counted 334 repeats of 500755 total bases, average 1499.27
# Longest repeat was 6371 bases on JAKMXF010000177.1
```

Link and load:

```
ln -s ~/genomes/oopsacas_minuta_PORI/JAKMXF01.1.fsa_nt.fasta
ln -s ~/genomes/oopsacas_minuta_PORI/JAKMXF01.1.fsa_nt.fasta.fai 
ln -s ~/genomes/oopsacas_minuta_PORI/JAKMXF01.1.fsa_nt.gaps.gff
/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta JAKMXF01.1.fsa_nt.fasta --out ./
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff JAKMXF01.1.fsa_nt.gaps.gff --trackType CanvasFeatures --trackLabel N-gaps --out ./
```

## convert genbank format to GFF ##
Using [biocode repository](https://github.com/jorvis/biocode) script `convert_genbank_to_gff3.py`, make the GFF. As the scaffold names do not include the NCBI version, this has to be added using this brief python code:

```
~/git/biocode/gff/convert_genbank_to_gff3.py -i JAKMXF01.1.gbff -o JAKMXF01.1.gbff.gff.0 --no_fasta
python add_v_to_genbank_gff.py JAKMXF01.1.gbff.gff.0 > JAKMXF01.1.gbff.gff
```

This has the minor problem that the NCBI accessions are not included in the GFF, so the loci are given according to the user-submitted form (as they are listed in the paper), but the protein file uses the NCBI accessions where a user-submitted (probably? could also be NCBI pipeline) description is not included.

```
#!/usr/bin/env python
import sys
for line in open(sys.argv[1],'r'):
	if line[0]!="#":
		lsplits = line.split("\t")
		lsplits[0] = lsplits[0] + ".1"
		outline = "\t".join(lsplits)
		sys.stdout.write(outline)
	else:
		sys.stdout.write(line)
```

Then link and load:

```
ln -s ~/genomes/oopsacas_minuta_PORI/JAKMXF01.1.gbff.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff JAKMXF01.1.gbff.gff --trackType CanvasFeatures --trackLabel v1-models --out ./
```

## blast vs models ##
This takes some workaround, since NCBI renames the proteins in such a way that some of them lose their original submitted IDs. But in the paper they use the submitter IDs, not the NCBI ones.

```
$ grep ">" JAKMXF01.1.fsa_aa | head
>KAI6661990.1 hypothetical protein LOD99_9577 [Oopsacas minuta]
>KAI6661991.1 hypothetical protein LOD99_9578 [Oopsacas minuta]
>KAI6661992.1 CAMP-dependent protein kinase I-alpha regulatory subunit [Oopsacas minuta]
>KAI6661993.1 Death domain-associated protein 6 [Oopsacas minuta]
>KAI6661994.1 General transcription factor IIH subunit 3 [Oopsacas minuta]
>KAI6661995.1 hypothetical protein LOD99_9582 [Oopsacas minuta]
>KAI6661996.1 Sulfatase-modifying factor 1 [Oopsacas minuta]
>KAI6661997.1 Steroid reductase [Oopsacas minuta]
>KAI6661998.1 Small GTP-binding protein [Oopsacas minuta]
>KAI6661999.1 HEAT repeat-containing protein 5B isoform X2 [Oopsacas minuta]
```

Otherwise, follows the normal scheme from [genomeGTFtools](https://github.com/wrf/genomeGTFtools):

```
~/gffread-0.12.7.Linux_x86_64/gffread JAKMXF01.1.gbff.gff -g JAKMXF01.1.fsa_nt.fasta -w JAKMXF01.1.gbff.exon.fa
~/diamond-v2.0.13/diamond-v2.0.13 blastx -q JAKMXF01.1.gbff.exon.fa -d ~/db/model_organism_uniprot.w_cnido.fasta -o JAKMXF01.1.gbff.exon.vs_models.tab
~/git/genomeGTFtools/blast2genomegff.py -b JAKMXF01.1.gbff.exon.vs_models.tab -g JAKMXF01.1.gbff.gff -S --add-description --add-accession -d ~/db/model_organism_uniprot.w_cnido.fasta -P > JAKMXF01.1.gbff.exon.vs_models.gff
```

Link and make the track:

```
ln -s ~/genomes/oopsacas_minuta_PORI/JAKMXF01.1.gbff.exon.vs_models.gff
/var/www/html/jbrowse/bin/generate-names.pl --tracks v1-models,blast-vs-models --out ./
```

Then, against the [*A. vastus* genome](https://github.com/PalMuc/Aphrocallistes_vastus_genome):

`~/diamond-v2.0.13/diamond-v2.0.13 blastx -q JAKMXF01.1.gbff.exon.fa -d ~/genomes/aphrocallistes_vastus_PORI/v1.29/Avas.v1.29_annotations.prot.fr.fasta.dmnd -o JAKMXF01.1.gbff.exon.vs_Avas1.29.tab `

`~/git/genomeGTFtools/blast2genomegff.py -b JAKMXF01.1.gbff.exon.vs_Avas1.29.tab -g JAKMXF01.1.gbff.gff --percent -d ~/genomes/aphrocallistes_vastus_PORI/v1.29/Avas.v1.29_annotations.prot.fr.fasta -P > JAKMXF01.1.gbff.exon.vs_Avas1.29.gff`

Again, link and make the track:

```
ln -s /mnt/data/project/genomes/oopsacas_minuta_PORI/JAKMXF01.1.gbff.exon.vs_Avas1.29.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff JAKMXF01.1.gbff.exon.vs_Avas1.29.gff --trackType CanvasFeatures --trackLabel blast-vs-avas --out ./
```

