# *Branchiostoma floridae*
The lancelet [Branchiostoma floridae](http://www.marinespecies.org/aphia.php?p=taxdetails&id=266208)

# with current chr-level assembly v3 #
Using the [v3 chromosome-level assembly](https://www.ncbi.nlm.nih.gov/assembly/GCA_000003815.2/), by [Simakov 2020](https://doi.org/10.1038/s41559-020-1156-z). As of July 2020, the annotation is [NOT on NCBI](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA412957), though the paper was published months earlier. These analyses make use of a version from the authors, with 28182 genes, though the paper reports 28192 genes.

`~/git/genomeGTFtools/rename_gtf_contigs.py -c sc_to_cm_numbers.names -g amphioxus_7u5tJ.gtf -n > amphioxus_7u5tJ.cm_numbers.gtf`

`~/git/genome-reannotations/jbrowse-tracks/branchiostoma/amphioxus_evmgtf_to_gff3.py amphioxus_7u5tJ.cm_numbers.gtf > amphioxus_7u5tJ.cm_numbers.gff`

## blast against models ##
`~/gffread-0.12.1.Linux_x86_64/gffread amphioxus_7u5tJ.cm_numbers.gff -g GCA_000003815.2_Bfl_VNyyK_genomic.fna -w amphioxus_7u5tJ.cm_numbers.fasta`

`~/diamond-linux64/diamond blastx -q amphioxus_7u5tJ.cm_numbers.fasta -d ~/db/model_organism_uniprot.fasta -o amphioxus_7u5tJ.cm_numbers.vs_models.tab`

`~/git/genomeGTFtools/blast2genomegff.py -b amphioxus_7u5tJ.cm_numbers.vs_models.tab -g amphioxus_7u5tJ.cm_numbers.gff -S --add-description --add-accession -P -d ~/db/model_organism_uniprot.fasta -p blastx -F "." -G > amphioxus_7u5tJ.cm_numbers.vs_models.gff`

## mapping ESTs ##

`~/minimap2-2.14_x64-linux/minimap2 -a -x splice --secondary=no GCA_000003815.2_Bfl_VNyyK_genomic.fna branchiostoma_NCBI_ESTs.fasta.gz | ~/samtools-1.8/samtools sort - -o branchiostoma_NCBI_ESTs.vs_v3.bam`

`~/samtools-1.8/samtools flagstat branchiostoma_NCBI_ESTs.vs_v3.bam`

```
338242 + 0 in total (QC-passed reads + QC-failed reads)
0 + 0 secondary
3740 + 0 supplementary
0 + 0 duplicates
306018 + 0 mapped (90.47% : N/A)
```

`~/samtools-1.8/samtools index branchiostoma_NCBI_ESTs.vs_v3.bam`

Then generate the transcripts, and fasta sequences to fix a few of the incomplete ones:

`~/stringtie-2.0.4.Linux_x86_64/stringtie -L -l Bf3est -o branchiostoma_NCBI_ESTs.vs_v3.stringtie.gtf branchiostoma_NCBI_ESTs.vs_v3.bam`

`~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py branchiostoma_NCBI_ESTs.vs_v3.stringtie.gtf > branchiostoma_NCBI_ESTs.vs_v3.stringtie.gff`

`~/gffread-0.12.1.Linux_x86_64/gffread branchiostoma_NCBI_ESTs.vs_v3.stringtie.gtf -g GCA_000003815.2_Bfl_VNyyK_genomic.fna -w branchiostoma_NCBI_ESTs.vs_v3.stringtie.fasta`

`{ ~/hisat2-2.1.0/hisat2 -q -x GCA_000003815.2_Bfl_VNyyK_genomic.fna -1 SRR923751_1.fastq.gz -2 SRR923751_2.fastq.gz -p 6 --dta 2>&3 | ~/samtools-1.8/samtools sort - -o SRR923751_hisat2.bam; } 3> SRR923751_hisat2.log`

`~/stringtie-2.0.4.Linux_x86_64/stringtie -l Bf3st -o SRR923751_hisat2.vs_v3.stringtie.gtf SRR923751_hisat2.bam`

# with previous version Assembly v2 #
From the [original JGI assembly](https://genome.jgi.doe.gov/Brafl1/Brafl1.home.html), by [Putnam 2008](https://doi.org/10.1038/nature06967). This assembly v2.0 has 398 scaffolds, 521MB total, N50 2.5Mb.

```
~/samtools-1.8/samtools faidx Branchiostoma_floridae_v2.0.assembly.fasta
ln -s /mnt/data/genomes/branchiostoma_floridae/Branchiostoma_floridae_v2.0.assembly.fasta Branchiostoma_floridae_v2.0.assembly.fasta
ln -s /mnt/data/genomes/branchiostoma_floridae/Branchiostoma_floridae_v2.0.assembly.fasta.fai Branchiostoma_floridae_v2.0.assembly.fasta.fai
/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta Branchiostoma_floridae_v2.0.assembly.fasta --out ./
```

## Annotation v1 ##

```
./rename_brafl1_models.py Bfloridae_v1.0_FilteredModelsMappedToAssemblyv2.0.gff > Brafl1.on_v2.gff
ln -s /mnt/data/genomes/branchiostoma_floridae/Brafl1.on_v2.gff Brafl1.on_v2.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Brafl1.on_v2.gff --trackType CanvasFeatures --trackLabel v1
```

## 2nd gen RNA-seq mapping ##
RNAseq from [SRR923751](http://www.ncbi.nlm.nih.gov/sra/SRR923751) (63M pairs, presumably whole adult) was mapped with [tophat2](https://ccb.jhu.edu/software/tophat/index.shtml). Transcripts were predicted with [StringTie](http://www.ccb.jhu.edu/software/stringtie/), probably with default parameters. Predicted [proteins are here](https://bitbucket.org/wrf/genome-reannotations/downloads/bflo-rnaseq_stringtie.prots.fasta.gz), [GTF file here](https://bitbucket.org/wrf/genome-reannotations/downloads/bflo-rnaseq_stringtie.gtf.gz).

```
~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py bflo-rnaseq_stringtie.gtf > bflo-rnaseq_stringtie.gff
ln -s /mnt/data/genomes/branchiostoma_floridae/bflo-rnaseq_stringtie.gff bflo-rnaseq_stringtie.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff bflo-rnaseq_stringtie.gff --trackType CanvasFeatures --trackLabel stringtie
```

Format is mRNA-exon, so the tag `"subParts" : "exon",` needs to be added to `trackList.json`.

Apparently more transcriptomic datasets are available from the [EXPANDE project](https://www.ncbi.nlm.nih.gov/bioproject/PRJDB3785).

## AUGUSTUS ##
I don't remember what I used to train this, but the AUGUSTUS parameter files are [here](https://bitbucket.org/wrf/genome-reannotations/downloads/Brafl1_parameters.tar.gz), as well as the [predicted proteins](https://bitbucket.org/wrf/genome-reannotations/downloads/Brafl2_augustus.prots.fasta.gz) and [GFF annotation file](https://bitbucket.org/wrf/genome-reannotations/downloads/Brafl2_augustus.nocomment.gff.gz).

```
~/git/genomeGTFtools/augustus_renaming.py Brafl2_augustus.gff > Brafl2_augustus.nocomment.gff
ln -s /mnt/data/genomes/branchiostoma_floridae/Brafl2_augustus.nocomment.gff Brafl2_augustus.nocomment.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff Brafl2_augustus.nocomment.gff --trackType CanvasFeatures --trackLabel AUGUSTUS
```

## EST Mapping ##
334502 [ESTs from NCBI](https://www.ncbi.nlm.nih.gov/nuccore/?term=txid7739[Organism:noexp]), 182Mbp, average 546bp, mapped using hisat2, albeit with a poor mapping rate (only 28%):

`hisat2-build Branchiostoma_floridae_v2.0.assembly.fasta Branchiostoma_floridae_v2.0.assembly.fasta`

`hisat2 -f -x Branchiostoma_floridae_v2.0.assembly.fasta -U branchiostoma_NCBI_ESTs.fasta.gz -p 2 --max-intronlen 100000 -S branchiostoma_NCBI_ESTs.sam -5 10 -3 10 2> branchiostoma_NCBI_ESTs_hisat2.log`

`~/samtools-1.8/samtools sort branchiostoma_NCBI_ESTs_t10.sam -o branchiostoma_NCBI_ESTs.sorted.bam`

`~/samtools-1.8/samtools index branchiostoma_NCBI_ESTs.sorted.bam`

```
ln -s /mnt/data/genomes/branchiostoma_floridae/branchiostoma_NCBI_ESTs.sorted.bam branchiostoma_NCBI_ESTs.sorted.bam
ln -s /mnt/data/genomes/branchiostoma_floridae/branchiostoma_NCBI_ESTs.sorted.bam.bai branchiostoma_NCBI_ESTs.sorted.bam.bai
```

## blast against human reference proteins ##

`~/diamond-linux64/diamond blastp -q Brafl2_augustus.prot.no_rename.fasta -d ~/db/human_uniprot.fasta -o Brafl2_augustus.prot.vs_human.tab`

`~/git/genomeGTFtools/blast2genomegff.py -b Brafl2_augustus.prot.vs_human.tab -p blastp -g Brafl2_augustus.nocomment.gff -d ~/db/human_uniprot.fasta -S --add-accession --add-description -G -F "." -x -M 5 -P > Brafl2_augustus.prot.vs_human.gff`

