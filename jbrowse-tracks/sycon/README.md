# *Sycon ciliatum* #
Annotation for [*Sycon ciliatum*](https://bitbucket.org/wrf/genome-reannotations/downloads/sycon.genome.fa.gz), from [Fortunato 2014](https://doi.org/10.1038/nature13881). **Note: this is no longer hosted by compagen.org, which may have shut down (Nov 2021).** I will be hosting it until someone offers a better solution.

As of May 2021, the [raw genomic reads are not on SRA](https://www.ncbi.nlm.nih.gov/sra/?term=txid27933[Organism:exp]), only the developmental comparative transcriptomic dataset from [Fortunato 2014](https://doi.org/10.1038/nature13881).

# prepare scaffolds #
Making an indexed fasta using [samtools](https://github.com/samtools/samtools), then link to jbrowse directory, and make reference track:

```
~/samtools-1.8/samtools faidx sycon.genome.fa

cd ~/jbrowse_data/sycon_ciliatum/
ln -s /mnt/data/genomes/sycon_ciliatum/sycon.genome.fa sycon.genome.fa
ln -s /mnt/data/genomes/sycon_ciliatum/sycon.genome.fa.fai sycon.genome.fa.fai
/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta sycon.genome.fa --out ./
```

```
~/git/genomeGTFtools/repeat2gtf.py -l sycon.genome.fa > sycon.genome.n_gaps.gff

# Counted 7780 sequences
# Counted 116590 repeats of 79024862 total bases, average 677.80
# Longest repeat was 8796 bases on scis2600
```

# add v1 genome annotation #
Compagen did not host annotations, only the transcripts. The v1 annotation was sent to me directly from the authors, and is [shared here](https://bitbucket.org/wrf/genome-reannotations/downloads/sycon.cds.gff3.gz)

```
cd ~/jbrowse_data/sycon_ciliatum/
ln -s /mnt/data/genomes/sycon_ciliatum/sycon.cds.gff3 sycon.cds.gff3
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff sycon.cds.gff3 --trackType CanvasFeatures --trackLabel v1
```

Format is gene-mRNA-exon, so the tag `"subParts" : "exon",` needs to be added to `trackList.json`.

An alternate approach would be to use [minimap2](https://github.com/lh3/minimap2) to map the nucleotide CDS back to the genome, using the `-x splice` mode.

# blast against models #
Annotations appear to be from M to stop, so exon features should be the same as CDS features in the GFF.

```
~/diamond-v2.0.13/diamond-v2.0.13 blastp -q sycon.cds.prot.fasta -d ~/db/model_organism_uniprot.w_cnido.fasta -o sycon.cds.vs_models.tab
~/git/genomeGTFtools/blast2genomegff.py -b sycon.cds.vs_models.tab -d ~/db/model_organism_uniprot.w_cnido.fasta -g sycon.cds.gff3 -p blastp -S --add-description --add-accession > sycon.cds.vs_models.gff
cd ~/jbrowse_data/sycon_ciliatum/
ln -s /mnt/data/genomes/sycon_ciliatum_PORI/sycon.cds.vs_models.gff 
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff sycon.cds.vs_models.gff --trackType CanvasFeatures --trackLabel blast-vs-models --out ./
```

# mapping genomic DNA reads #
There are 4 paired-end libraries from the authors, I would guess 2x for contig building, and 2x for scaffolding.

```
{ ~/hisat2-2.2.1/hisat2 -q -x sycon.genome.fa -1 Adamska-gDNA-8s-370-600bp_NoIndex_L005_R1_001.fastq.gz,Adamska-gDNA-8s-600-700bp_NoIndex_L005_R1_001.fastq.gz -2 Adamska-gDNA-8s-370-600bp_NoIndex_L005_R2_001.fastq.gz,Adamska-gDNA-8s-600-700bp_NoIndex_L005_R2_001.fastq.gz -p 6 --no-spliced-alignment 2>&3 | ~/samtools-1.14/samtools sort - -o sycon_w_370_600_and_600_700_hisat2.bam; } 3> sycon_w_370_600_and_600_700_hisat2.log
[bam_sort_core] merging from 87 files and 1 in-memory blocks...
106141078 reads; of these:
  106141078 (100.00%) were paired; of these:
    69895364 (65.85%) aligned concordantly 0 times
    34717108 (32.71%) aligned concordantly exactly 1 time
    1528606 (1.44%) aligned concordantly >1 times
    ----
    69895364 pairs aligned concordantly 0 times; of these:
      2384135 (3.41%) aligned discordantly 1 time
    ----
    67511229 pairs aligned 0 times concordantly or discordantly; of these:
      135022458 mates make up the pairs; of these:
        97269566 (72.04%) aligned 0 times
        31385065 (23.24%) aligned exactly 1 time
        6367827 (4.72%) aligned >1 times
54.18% overall alignment rate
```

`~/samtools-1.14/samtools view sycon_w_370_600_and_600_700_hisat2.bam | ~/git/lavaLampPlot/sort_reads_from_bam.py -i - > sycon_w_370_600_and_600_700_hisat2.hits.txt`

`~/git/lavaLampPlot/hits_to_coverage.py -b sycon_w_370_600_and_600_700_hisat2.hits.txt -f sycon.genome.fa > sycon_w_370_600_and_600_700_hisat2.gc_coverage.tab`

`Rscript ~/git/lavaLampPlot/contig_gc_coverage.R sycon_w_370_600_and_600_700_hisat2.gc_coverage.tab 230`

![sycon_w_370_600_and_600_700_hisat2.gc_coverage.png](https://bitbucket.org/wrf/genome-reannotations/raw/dde22171ce5ead6a16a1bff7f26a64121dfda276/jbrowse-tracks/sycon/sycon_w_370_600_and_600_700_hisat2.gc_coverage.png)

`~/bedtools2/bin/bedtools genomecov -ibam sycon_w_370_600_and_600_700_hisat2.bam -bg > sycon_w_370_600_and_600_700_hisat2.bed`

`~/ucsc_genome_tools/bedSort sycon_w_370_600_and_600_700_hisat2.bed sycon_w_370_600_and_600_700_hisat2.bed`

`~/ucsc_genome_tools/bedGraphToBigWig sycon_w_370_600_and_600_700_hisat2.bed sycon.genome.sizes sycon_w_370_600_and_600_700_hisat2.bw`

# bacterial contig annotation #
## gammaproteobacterium ##
The first scaffold appears to be a nearly-complete bacterial genome (8 short gaps are present), and is not mentioned in any of their papers, [Fortunato 2012](https://doi.org/10.1186/2041-9139-3-14), [Fortunato 2014](https://doi.org/10.1038/nature13881), or [Fortunato 2016](https://doi.org/10.1186/s13227-016-0060-8). The genome is albeit highly reduced (only 1.38Mb), suggesting a role as a symbiont. The single scaffold is extracted with [getAinB.py](https://bitbucket.org/wrf/sequences/src/master/getAinB.py). Coding nucleotides, proteins, and GFF annotation are predicted with [prodigal](https://github.com/hyattpd/Prodigal). The rRNA is detected with [barrnap](https://github.com/tseemann/barrnap).

```
getAinB.py scis1 sycon.genome.fa > scis1.fa
~/prodigal/prodigal.linux -a scis1.prots.fa -d scis1.nucls.fa -f gff -o scis1.prodigal.gff -i scis1.fa
~/git/barrnap/bin/barrnap -o scis1.barrnap.fa scis1.fa > scis1.barrnap.gff

cd ~/jbrowse_data/sycon_ciliatum/
ln -s /mnt/data/genomes/sycon_ciliatum/scis1.prodigal.gff scis1.prodigal.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff scis1.prodigal.gff --trackType CanvasFeatures --trackLabel Prodigal
```

16s rRNA is 92% similar to the [endosymbiont of Bathymodiolus septemdierum str. Myojin knoll](https://www.ncbi.nlm.nih.gov/nucleotide/AP013042.1) and 90% similar to [Gammaproteobacteria Thioglobus singularis PS1](https://www.ncbi.nlm.nih.gov/nucleotide/CP006911.1). The [Bathymodiolus symbiont](https://doi.org/10.1038/ismej.2015.176) also has a small genome, of 1.46Mb. However, the genome of the [free-living Thioglobus](https://doi.org/10.1038/ismej.2012.78) is not much larger, [only 1.71Mb](https://doi.org/10.1128/genomeA.01155-15).

### Table 1: Overview of the 3 gammaproteobacterial species ###
| parameter   | *Sycon* scis1 symbiont | *Bathymodiolus* symbiont | *T. singluaris* |
| :---        | ---              | ---                      | ---             |
| Accession   | scis1            | [GCF_001547755.1](https://www.ncbi.nlm.nih.gov/assembly/GCF_001547755.1/) | [GCF_001281385.1](https://www.ncbi.nlm.nih.gov/assembly/GCF_001281385.1/) |
| Source      | sponge symbiont from Bergen, Norway (North Atlantic) | mussel symbiont from hydrothermal vent offshore of Japan | free-living in surface waters from Puget Sound, WA (Pacific) |
| Genome size | 1.38Mb           | 1.42Mb                   | 1.71Mb          |
| GC percent  | 34.763           | 38.743                   | 37.438          |
| Genes       | 1438             | 1587                     | 1682            |

The proteins are used as query for [blastKOALA](https://www.kegg.jp/blastkoala/) webserver. KEGG annotated 1016 entries out of 1438 (70.7%). From the results, clicking on Annotation data "View", and then "Download detail", one gets the file `user_ko_definition.txt`. The online blastKOALA results expire after a week, but the KEGG pathway maps can be reconstructed at anytime by uploading the `user_ko_definition.txt` file to the [KEGG Mapper tool](https://www.kegg.jp/kegg/mapper/reconstruct.html).

The file contains 6 columns, and is an extended version of the `user_ko.txt` file (if Annotation data "Download" was clicked). This is used as an input for the script [append_kegg_to_prodigal_gff.py](https://github.com/wrf/genomeGTFtools/blob/master/misc/append_kegg_to_prodigal_gff.py). If present, the KEGG pathway ID, the gene name, and the description will be added to the GFF so that these can be displayed in the genome browser.

```
~/git/genomeGTFtools/misc/append_kegg_to_prodigal_gff.py scis1.user_ko_definition.txt scis1.prodigal.gff > scis1.prodigal.w_kegg.gff

cd ~/jbrowse_data/sycon_ciliatum/
ln -s /mnt/data/genomes/sycon_ciliatum/scis1.prodigal.w_kegg.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff scis1.prodigal.w_kegg.gff --trackType CanvasFeatures --trackLabel Prodigal-KEGG
```

Based on the KEGG predicted pathway modules, the bacteria nonetheless appears to have substantial metabolic capacity. This is not so typical for small-genome symbionts. For instance, glycolysis to OxPhos appears mostly complete:

```
    Carbohydrate metabolism
        Central carbohydrate metabolism
            M00001 Glycolysis (Embden-Meyerhof pathway), glucose => pyruvate (8)  (2 blocks missing 7/9)
            M00002 Glycolysis, core module involving three-carbon compounds (6)  (complete 5/5)
            M00009 Citrate cycle (TCA cycle, Krebs cycle) (13)  (complete 8/8)
    Energy metabolism
            M00115 NAD biosynthesis, aspartate => quinolinate => NAD (5)  (complete 5/5)
        ATP synthesis
            M00144 NADH:quinone oxidoreductase, prokaryotes (14)  (complete 1/1)
            M00151 Cytochrome bc1 complex respiratory unit (3)  (complete 1/1)
            M00155 Cytochrome c oxidase, prokaryotes (3)  (complete 1/1)
            M00157 F-type ATPase, prokaryotes and chloroplasts (8)  (complete 1/1)
```

Likewise, nearly all nucleotides can be made, missing only 1 gene *pyrI*, the [regulatory subunit of aspartate carbamoyltransferase](https://www.uniprot.org/uniprot/P0A7F3). The other [catalytic subunit](https://www.uniprot.org/uniprot/P0A786) *pyrB* is found, so possibly the regulatory subunit is unnecessary for basic functioning.

```
    Nucleotide metabolism
            M00004 Pentose phosphate pathway (Pentose phosphate cycle) (5)  (2 blocks missing 5/7)
            M00007 Pentose phosphate pathway, non-oxidative phase, fructose 6P => ribose 5P (4)  (complete 4/4)
        Purine metabolism
            M00048 Inosine monophosphate biosynthesis, PRPP + glutamine => IMP (10)  (complete 8/8)
            M00049 Adenine ribonucleotide biosynthesis, IMP => ADP,ATP (5)  (complete 4/4)
            M00050 Guanine ribonucleotide biosynthesis IMP => GDP,GTP (5)  (complete 4/4)
        Pyrimidine metabolism
            M00051 Uridine monophosphate biosynthesis, glutamine (+ PRPP) => UMP (7)  (1 block missing 2/3)
            M00052 Pyrimidine ribonucleotide biosynthesis, UMP => UDP/UTP,CDP/CTP (3)  (complete 3/3)
            M00053 Pyrimidine deoxyribonuleotide biosynthesis, CDP/CTP => dCDP/dCTP,dTDP/dTTP (6)  (2 blocks missing 6/8)
```

Most amino acid biosynthetic pathways are also found, though there appears to be trouble with a few, possibly indicating some dependence on a host or another microbe:

```
    Amino acid metabolism
        Serine and threonine metabolism
            M00020 Serine biosynthesis, glycerate-3P => serine (2)  (1 block missing 2/3)
            M00018 Threonine biosynthesis, aspartate => homoserine => threonine (4)  (1 block missing 4/5)
        Cysteine and methionine metabolism
            M00021 Cysteine biosynthesis, serine => cysteine (1)  (1 block missing 1/2)
            M00609 Cysteine biosynthesis, methionine => cysteine (1)  (incomplete 1/6)
            M00017 Methionine biosynthesis, apartate => homoserine => methionine (5)  (2 blocks missing 5/7)
        Branched-chain amino acid metabolism
            M00019 Valine/isoleucine biosynthesis, pyruvate => valine / 2-oxobutanoate => isoleucine (5)  (complete 4/4)
            M00535 Isoleucine biosynthesis, pyruvate => 2-oxobutanoate (3)  (1 block missing 2/3)
            M00570 Isoleucine biosynthesis, threonine => 2-oxobutanoate => isoleucine (6)  (complete 5/5)
            M00432 Leucine biosynthesis, 2-oxoisovalerate => 2-oxoisocaproate (4)  (complete 3/3)
        Lysine metabolism
            M00016 Lysine biosynthesis, succinyl-DAP pathway, aspartate => lysine (9)  (complete 9/9)
        Arginine and proline metabolism
            M00028 Ornithine biosynthesis, glutamate => ornithine (4)  (complete 4/4)
            M00844 Arginine biosynthesis, ornithine => arginine (3)  (complete 3/3)
            M00015 Proline biosynthesis, glutamate => proline (3)  (complete 2/2)
        Histidine metabolism
            M00026 Histidine biosynthesis, PRPP => histidine (10)  (1 block missing 5/6)
        Aromatic amino acid metabolism
            M00022 Shikimate pathway, phosphoenolpyruvate + erythrose-4P => chorismate (7)  (complete 4/4)
            M00023 Tryptophan biosynthesis, chorismate => tryptophan (7)  (complete 3/3)
            M00024 Phenylalanine biosynthesis, chorismate => phenylpyruvate => phenylalanine (1)  (1 block missing 1/2)
            M00025 Tyrosine biosynthesis, chorismate => HPP => tyrosine (2)  (1 block missing 1/2)
            M00040 Tyrosine biosynthesis, chorismate => arogenate => tyrosine (1)  (2 blocks missing 1/3)
```

Prokaryotic type lipids appear complete:

```
    Lipid metabolism
        Fatty acid metabolism
            M00082 Fatty acid biosynthesis, initiation (6)  (complete 2/2)
            M00083 Fatty acid biosynthesis, elongation (6)  (complete 1/1)
        Terpenoid backbone biosynthesis
            M00096 C5 isoprenoid biosynthesis, non-mevalonate pathway (7)  (1 block missing 7/8)
```

A few other cofactor pathways also appear complete, though most are incomplete:

```
    Metabolism of cofactors and vitamins
        Cofactor and vitamin metabolism
            M00120 Coenzyme A biosynthesis, pantothenate => CoA (4)  (complete 3/3)
            M00572 Pimeloyl-ACP biosynthesis, BioC-BioH pathway, malonyl-ACP => pimeloyl-ACP (7)  (complete 6/6)
            M00881 Lipoic acid biosynthesis, plants and bacteria, octanoyl-ACP => dihydrolipoyl-E2/H (2)  (complete 2/2)
            M00880 Molybdenum cofactor biosynthesis, GTP => molybdenum cofactor (5)  (complete 3/3)
            M00121 Heme biosynthesis, plants and bacteria, glutamate => heme (10)  (complete 10/10)
```

The *Bathymodiolus* symbiont was 67.7% annotated by [BlastKOALA](https://www.kegg.jp/blastkoala/). KEGG data for *T. singluaris* were found at [KEGG GENES](https://www.genome.jp/dbget-bin/get_linkdb?-t+genes+gn:T04071) (via the LinkDB button in the [GENOME entry](https://www.kegg.jp/entry/gn:T04071). Of key interest are the sulfur metabolism and carbon fixation pathways. Select pathways from the three species are compared below.

### Table 2: Metabolic comparison of the 3 gammaproteobacterial species ###
| KEGG ID | pathway                 | *Sycon* scis1 symbiont | *Bathymodiolus* symbiont | *T. singluaris* |
| :---    | :---                    | ---              | ---                      | ---             |
|         | **Carbon metabolism** |                 |                          |                 |
| M00001  | Glycolysis, hexose      | 7/9 [HK](https://www.kegg.jp/dbget-bin/www_bget?K00844) [pfkA](https://www.kegg.jp/dbget-bin/www_bget?K00850) missing | 8/9 HK missing       | 8/9 [GPI](https://www.kegg.jp/dbget-bin/www_bget?K01810) missing |
| M00002  | Glycolysis, triose      | 5/5               | 5/5                    | 5/5              |
| M00010  | Citrate cycle, first carbon oxidation  | 3/3 | 3/3 predicted | 3/3 predicted |
| M00011  | Citrate cycle, second carbon oxidation | 5/5 [sdhD](https://www.kegg.jp/dbget-bin/www_bget?K00242) absent | 2/5 OGDH sdhABCD [mdh](https://www.kegg.jp/dbget-bin/www_bget?K00024) missing | 4/5 [OGDH](https://www.kegg.jp/dbget-bin/www_bget?K00164) missing |
|         | **Energy metabolism** |                 |                          |                 |
| M00166  | Calvin cycle Ribulose-5P - glyceraldehyde-3P |  2/4 [PRK](https://www.kegg.jp/dbget-bin/www_bget?K00855) rbcSL missing  | 4/4                   | 4/4                 |
| M00167  | Calvin cycle Glyceraldehyde-3P - ribulose-5P |  6/7 [glpX](https://www.kegg.jp/dbget-bin/www_bget?K11532) missing  | 5/7 [fbp](https://www.kegg.jp/dbget-bin/www_bget?K03841) glpX missing        | 5/7                 |
| M00596  | Dissimilatory sulfate reduction      | 0/3 missing  | 3/3 [sat](https://www.kegg.jp/dbget-bin/www_bget?K00958)/aprAB/dsrAB    | 2/3 dsrAB missing |
| M00595  | Thiosulfate oxidation by SOX complex | 1/1 [soxABXYZ](https://www.kegg.jp/entry/R10151) | 1/1 soxABXYZ    | 0/1 missing       |
| M00151  | Cytochrome bc1 complex  | 3/3         | 3/3             | 3/3                |
| M00155  | Cytochrome c oxidase    | 3/3         | 3/3             | 3/3                |
| M00157  | F-type ATPase           | 8/8 ATPF1/A/B/D/E/G/ATPF0A/B/C | 8/8             | 8/8                |
| M00144  | NADH:quinone oxidoreductase | 14/14   | 14/14           | missing            |
|         | **Pyrimidine metabolism** |                 |                          |                 |
| M00051  | Uridine monophosphate biosynthesis | 2/3 pyrI absent | 2/3 pyrI absent | 2/3 pyrI absent |
| M00052  | Pyrimidine ribonucleotide biosynthesis | 3/3 | 2/3 [ndk](https://www.kegg.jp/dbget-bin/www_bget?K00940) missing          | 3/3                 |
| M00053  | Pyrimidine deoxyribonuleotide, CDP - dCTP | 2/2  | 1/2 ndk missing       | 2/2             |
| M00938  | Pyrimidine deoxyribonuleotide, UDP - dTTP | 5/5  | 4/5 ndk missing       | 5/5             |
|         | **Amino acid metabolism** |                 |                          |                 |
| M00020  | Serine biosynthesis     | 2/3 - [serA](https://www.kegg.jp/dbget-bin/www_bget?K00058) missing  | 3/3                    | 3/3               |
| M00018  | Threonine biosynthesis  | 4/5 - [thrB](https://www.kegg.jp/dbget-bin/www_bget?K00872) missing  | 4/5 - thrB missing     | 4/5 - thrB missing     |
| M00021  | Cysteine biosynthesis, via serine | 1/2 [cysK](https://www.kegg.jp/dbget-bin/www_bget?K01738) missing | 1/2 cysK missing | 1/2 cysK missing |
| M00017  | Methionine biosynthesis | 5/7 - [metB](https://www.kegg.jp/dbget-bin/www_bget?K01739) [metC](https://www.kegg.jp/dbget-bin/www_bget?K01760) missing | 5/7 - metB metC missing | 7/7           |
| M00019  | Valine/isoleucine biosynthesis | 4/4 ilvBH/C/D/E  | 4/4                   | 4/4               |
| M00570  | Isoleucine biosynthesis  | 5/5 ilvA/BH/C/D/E      | 5/5                    | 5/5               |
| M00432  | Leucine biosynthesis    | 3/3 leuA/B/CD       | 3/3                    | 3/3               |
| M00016  | Lysine biosynthesis, succinyl-DAP pathway  | 9/9   | 9/9               | 9/9               |
| M00028  | Ornithine biosynthesis  | 4/4                 | 4/4                    | 4/4               |
| M00844  | Arginine biosynthesis   | 3/3 argA/B/C/D/J    | 3/3 argAB/B/C/D/J      | 3/3 argA/B/C/D/J  |
| M00015  | Proline biosynthesis    | 2/2 proA/B/C        | 2/2                    | 2/2               |
| M00026  | Histidine biosynthesis  | 10/10 hisA/B/C/D/E/FH/I/GZ | 10/10 putative complete | 10/10     |
| M00022  | Shikimate pathway       | 4/4                 | 4/4                    | 3/4 [aroA](https://www.kegg.jp/dbget-bin/www_bget?K00800) missing  |
| M00023  | Tryptophan biosynthesis | 3/3 trpAB/C/D/EG/F  | 3/3                    | 3/3               |
| M00024  | Phenylalanine biosynthesis  | 1/2 [pheA](https://www.kegg.jp/dbget-bin/www_bget?K14170) maybe complete | 1/2                    | 1/2               |
| M00025  | Tyrosine biosynthesis   | 1/2 maybe complete    | 1/2                    | 1/2               |
| M00040  | Tyrosine biosynthesis   | 1/3 [tyrB](https://www.kegg.jp/dbget-bin/www_bget?K00832)/[C](https://www.kegg.jp/dbget-bin/www_bget?K00220) missing  | 1/3                    | 1/3               |
| M00027  | GABA shunt               | 1/3 [SSADH](https://www.kegg.jp/dbget-bin/www_bget?K17761), [GAD](https://www.kegg.jp/dbget-bin/www_bget?K01580) [ABAT](https://www.kegg.jp/dbget-bin/www_bget?K13524) missing | 0/3               | 1/3 SSADH |
| M00118  | Glutathione biosynthesis | 2/2 gshA/B            | 1/2 [gshB](https://www.kegg.jp/dbget-bin/www_bget?K01920) missing   |  2/2               |

The process of becoming a symbiont is predicted to result in genomic streamlining, hence loss of pathways. For instance, the serine biosynthesis genes (*serA*, *serC*, *serB*, from 3-phosphoglycerate to serine) are located in order for *T. singluaris*, as `W908_03405`, `W908_03410`, `W908_03415`. However, in the *Sycon* symbiont, *serA* is missing and the other two are no longer adjacent at `scis1_121` and `scis1_1285`. The *Bathymodiolus* symbiont appears almost as an intermediate state, with *serA* and *serC* adjacent (`WP_066044333.1`, `WP_066044335.1`) but not *serB* (`WP_066043283.1`).

Though this was a single example, the principle is straightforward. During the process of "domestication" of symbionts, this suggests that separation of genes within a pathway would come first, and then subsequent loss of individual genes. That is, rearrangements that preserve function are first selected, and then loss of steps within a pathway occur. This would contrast a model where deletions would occur directly within an operon.

## alphaproteobacterium ##
From the plot above, there is clearly a second microbial species with higher GC content, which blasts to [*Sulfitobacter*](https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=60136), an [Alphaproteobacterium in the Roseobacteraceae](https://doi.org/10.1099/ijs.0.065961-0) family. This is split in around 100 contigs totalling around 4Mb, approximately the size of other *Sulfitobacter* genome assemblies, though much more fragmented.

### Table 3: Overview of the 3 alphaproteobacterial species ###
| parameter   | *Sycon* scis242 symbiont | *S. noctilucae* | *S. mediterraneus* |
| :---        | ---              | ---                      | ---             |
| Accession   | scis242          | [GCF_000622365.1](https://www.ncbi.nlm.nih.gov/assembly/GCF_000622365.1/) | [GCF_016814295.1](https://www.ncbi.nlm.nih.gov/assembly/GCF_016814295.1/) |
| Source      | sponge symbiont from Bergen, Norway (North Atlantic) | free living in biolum bloom near Geoje Island, Korea | free-living in harbor of Mallorca Island, Spain |
| n scaffolds | 94              | 14                       | 16              |
| Genome size | 4.50Mb (likely overestimate)  | 3.914Mb                  | 4.20Mb          |
| GC percent  | 57.747           | 58.321                   | 58.584          |
| Genes       | 5025 (likely overestimate)   | 3728                     | 4086            |

The *S. mediterraneus* pathways [are already on KEGG](https://www.kegg.jp/entry/T07747).
