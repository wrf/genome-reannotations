#!/usr/bin/env python

'''
./extract_ML_codes_for_bad_OGs.py CTE_Mnemiopsis_leidyi0.lkp ~/git/genome-reannotations/jbrowse-tracks/mnemiopsis/mnemiopsis_v2_genes_to_fix.tab Mnemi-0_genes_OG_cluster_fused.tab > Mnemi-0_genes_OG_cluster_fused.names
'''

import sys

#new	old
#CTE_Mnemiopsis_leidyi0|v8-00001	ML00011a
#CTE_Mnemiopsis_leidyi0|v8-00002	ML00012a
#CTE_Mnemiopsis_leidyi0|v8-00003	ML00013a
#CTE_Mnemiopsis_leidyi0|v8-00004	ML00014a

new_to_old_dict = {}

for line in open(sys.argv[1],'r'):
	line = line.strip()
	if line:
		lsplits = line.split("\t")
		new_og_id = lsplits[0]
		old_id = lsplits[1]
		if new_og_id=="new":
			continue
		new_to_old_dict[new_og_id] = old_id

#scaffold	gene	remove	alternate	manual	reason	gene	checked	notes

is_already_annotated = {}

for line in open(sys.argv[2], 'r'):
	line = line.strip()
	if line and line[0]!="#":
		lsplits = line.split("\t")
		gene_id = lsplits[1]
		is_already_annotated[gene_id] = True

#CTE_Mnemiopsis_leidyi0.faa	CTE_Mnemiopsis_leidyi0|v8-11267	2	OG_8459 OG_12962
#CTE_Mnemiopsis_leidyi0.faa	CTE_Mnemiopsis_leidyi0|v8-11271	2	OG_3815 OG_4057
#CTE_Mnemiopsis_leidyi0.faa	CTE_Mnemiopsis_leidyi0|v8-03084	2	OG_9839 OG_15395
#CTE_Mnemiopsis_leidyi0.faa	CTE_Mnemiopsis_leidyi0|v8-11319	2	OG_2784 OG_9713

collected_codes = {}

for line in open(sys.argv[3], 'r'):
	line = line.strip()
	if line:
		lsplits = line.split("\t")
		og_gene_id = lsplits[1]
		old_id = new_to_old_dict.get(og_gene_id)
		if old_id in is_already_annotated:
			continue
		collected_codes[old_id] = True

for code in sorted(collected_codes.keys()):
	outline = "{}\n".format( code )
	sys.stdout.write( outline )
