#!/usr/bin/env python
#
# make_ML_annotation_spreadsheet.py

'''
./make_ML_annotation_spreadsheet.py mnemiopsis_v2_genes_to_fix.tab ~/genomes/mnemiopsis_leidyi/ML2.2.gff3 > Mlei_2.2_annotation_sheet.tab
'''

import sys

if len(sys.argv) < 2:
	sys.exit( __doc__ )
else:
	replace_annots = {} # key is geneid, value is line
	geneid = ""
	current_scaf = ""
	last_scaf = ""
	seen_annotations = {} # key is geneid, value is true
	sys.stderr.write("# Reading {}\n".format(sys.argv[1]) )
	for line in open(sys.argv[1],'r'):
		if line.strip() and line[0]!="#":
			lsplits = line.split("\t")
			if len(lsplits) != 9:
				sys.stderr.write("WARNING: {} columns\n{}".format(len(lsplits), line))
			current_scaf = lsplits[0]
			if lsplits[1]: # if none is given, then it should append to previous annotations
				geneid = lsplits[1]
				if geneid in replace_annots:
					sys.stderr.write("WARNING: already encountered {}\n{}".format(geneid, line))
				replace_annots[geneid] = replace_annots.get(geneid,"") + line
			else:
				if current_scaf==last_scaf or last_scaf=="":
					replace_annots[geneid] = replace_annots.get(geneid,"") + line
			last_scaf = current_scaf
	sys.stderr.write("# Reading {}\n".format(sys.argv[2]) )
	for line in open(sys.argv[2],'r'):
		if line.strip() and line[0]!="#":
			lsplits = line.split("\t")
			current_scaf = lsplits[0]
			feature = lsplits[2]
			if feature=="gene":
				attributes = lsplits[8]
				attrd = dict([(field.strip().split("=")) for field in attributes.split(";") if field.count("=")])
				gene_id = attrd.get("ID")
				if gene_id in replace_annots:
					outline = replace_annots[gene_id]
				else:
					outline = "{}\t{}\t\t\t\t\t\t\t\n".format(current_scaf, gene_id)
				sys.stdout.write(outline)
