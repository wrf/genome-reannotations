#!/bin/bash

cd ~/git/genome-reannotations/jbrowse-tracks/mnemiopsis

# minimap requires 60bp padded sequences
#sizecutter.py -p ml_manually_fixed_txs.raw_format.fasta > ml_manually_fixed_txs.nucl.fasta

#~/minimap2-2.17_x64-linux/minimap2 -a -x splice -t 4 --secondary=no ~/genomes/mnemiopsis_leidyi/MlScaffold09.nt.fa ml_manually_fixed_txs.nucl.fasta | ~/samtools-1.9/samtools sort - -o ml_manually_fixed_txs.nucl.bam
#~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M ml_manually_fixed_txs.nucl.bam | ./ml_manual_mapping_exclusion.py - > ml_manually_fixed_txs.nucl.pinfish.gtf
#~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M ml_manually_fixed_txs.nucl.bam > ml_manually_fixed_txs.nucl.pinfish.gtf

#cat ml_manually_fixed_txs.direct.gtf ml_manually_fixed_txs.nucl.pinfish.gtf > ml_manually_fixed_txs.combined.gtf

# this file might require manual checking, as some tx still double map

./make_ML_annotation_spreadsheet.py mnemiopsis_v2_genes_to_fix.tab ~/genomes/mnemiopsis_leidyi/ML2.2.gff3 > Mlei_2.2_annotation_sheet.tab

echo "remaking annotations"

#
~/git/genome-reannotations/remake_genome_annotation.py -m ml_manually_fixed_txs.direct.gtf -i ~/git/genome-reannotations/jbrowse-tracks/mnemiopsis/Mlei_2.2_annotation_sheet.tab -g ~/genomes/mnemiopsis_leidyi/ML2.2.gff3 -a ~/genomes/mnemiopsis_leidyi/MLRB2.2.gff ~/genomes/mnemiopsis_leidyi/trn15-30hpf.gff3 ~/genomes/mnemiopsis_leidyi/MlScaffold09_stringtie.gff3 ~/genomes/mnemiopsis_leidyi/GFAT01.1.renamed.pinfish.gtf -s ML2.3.21 -f ML2_to_2.3.21_names.tab > ML2.3.21_annotations.gff 2> ML2.3.21_annotations.log

echo "creating transcripts"

~/gffread-0.12.7.Linux_x86_64/gffread ML2.3.21_annotations.gff -g ~/genomes/mnemiopsis_leidyi/MlScaffold09.nt.fa -w ML2.3.21_annotations.nucl.fasta

cd ~/genomes/mnemiopsis_leidyi

ln -s ~/git/genome-reannotations/jbrowse-tracks/mnemiopsis/ML2.3.21_annotations.gff ML2.3.21_annotations.gff
ln -s ~/git/genome-reannotations/jbrowse-tracks/mnemiopsis/ML2.3.21_annotations.nucl.fasta ML2.3.21_annotations.nucl.fasta

prottrans.py -r -n -v ML2.3.21_annotations.nucl.fasta > ML2.3.21_annotations.prot.fasta

~/diamond-v2.0.13/diamond-v2.0.13 makedb --in ML2.3.21_annotations.prot.fasta --db ML2.3.21_annotations.prot.fasta

~/diamond-v2.0.13/diamond-v2.0.13 blastx -q ML2.3.21_annotations.nucl.fasta -d ~/db/model_organism_uniprot.w_cnido.fasta -o ML2.3.21_annotations.vs_models.tab

~/git/genomeGTFtools/blast2genomegff.py -b ML2.3.21_annotations.vs_models.tab -g ML2.3.21_annotations.gff -S --add-description --add-accession -d ~/db/model_organism_uniprot.w_cnido.fasta > ML2.3.21_annotations.vs_models.gff

~/diamond-v2.0.13/diamond-v2.0.13 blastx -q ML2.3.21_annotations.nucl.fasta -d ~/genomes/hormiphora_californensis/Hcv1av93_release/Hcv1av93_model_proteins.pep.dmnd -o ML2.3.21_annotations.vs_Hcv1av93.tab

~/git/genomeGTFtools/blast2genomegff.py -b ML2.3.21_annotations.vs_Hcv1av93.tab -g ML2.3.21_annotations.gff -d ~/genomes/hormiphora_californensis/hormiphora/annotation/Hcv1av93_release/Hcv1av93_model_proteins.pep -P > ML2.3.21_annotations.vs_Hcv1av93.gff

echo "remaking links"

rm ML2.3_annotations.gff ML2.3_annotations.vs_models.gff
ln -s -f ML2.3.21_annotations.gff ML2.3_annotations.gff
ln -s -f ML2.3.21_annotations.vs_models.gff ML2.3_annotations.vs_models.gff

echo "reloading annotations for browser"

cd ~/project/jbrowse_data/mnemiopsis/

/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff ML2.3_annotations.gff --trackType CanvasFeatures  --trackLabel ML2.3 --out ./

/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff ML2.3_annotations.vs_models.gff --trackType CanvasFeatures  --trackLabel blast-vs-models --out ./

cd ~/git/genome-reannotations/jbrowse-tracks/mnemiopsis

echo "finished"
