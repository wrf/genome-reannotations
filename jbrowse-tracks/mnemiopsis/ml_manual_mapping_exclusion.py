#!/usr/bin/env python

# ml_manual_mapping_exclusion.py

'''
./ml_manual_mapping_exclusion.py pinfish.gtf > pinfish_filtered.gtf
'''

import sys
import argparse

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	# this handles the system arguments
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', type=argparse.FileType('rU'), default = '-', help="fasta format file")
	args = parser.parse_args(argv)

	gene_to_scaf = {"Mlei_GFAT01_c374569_g1_i1": "ML1042" ,
					"ML002624a_GFAT01_c367418_g2_i1": "ML1358", 
					"ML327421a_GFAT01_c374541_g2_i1": "ML0054",
					"ML215419a_GFAT01_c367809_g1_i1": "ML1885",
					"ML0302_GFAT01_c371350_g1_i1": "ML0424" }


	for line in args.input_file:
		if line.strip():
			if line[0]=="#":
				sys.stdout.write(line)
			else:
				lsplits = line.strip().split("\t")
				scaffold = lsplits[0]
				attributes = lsplits[8]
				attrd = dict([(field.strip().split(" ")) for field in attributes.split(";") if field])
				gene_id = attrd.get("transcript_id").replace('"','')
				if gene_id in gene_to_scaf:
					if gene_to_scaf.get(gene_id,"")==scaffold:
						continue
				sys.stdout.write(line)

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
