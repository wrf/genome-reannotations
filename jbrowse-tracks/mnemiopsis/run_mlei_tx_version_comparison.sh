#!/bin/bash

cd ~/genomes/mnemiopsis_leidyi

blastn -query Ml_StringTie_transcripts.fa -db ML2.2.nucl.fasta -outfmt 6 -evalue 1e-10 -out Ml_StringTie_transcripts.vs_ml2.tab

blastn -query Ml_Trinity_transcripts.fa -db ML2.2.nucl.fasta -outfmt 6 -evalue 1e-50 -out Ml_Trinity_transcripts.vs_ml2.tab

blastn -query GFAT01.1.renamed.fasta -db ML2.2.nucl.fasta -outfmt 6 -evalue 1e-10 -out GFAT01.1.renamed.vs_ml2.tab

# StringTie

compare_transcript_versions.py -q Ml_StringTie_transcripts.fa -s ML2.2.nucl.fasta -b Ml_StringTie_transcripts.vs_ml2.tab

# Sorting intervals for Ml_StringTie_transcripts.fa
#Total bases: 38355056
#Transcripts with blast hits: 21920 out of 30052 total (72.94%)
#Bases covered by other dataset: 24879917 (64.87%)
#Transcripts covered by multiple hits longer than 100bp (possible fusions): 1767 (5.88%)
# Sorting intervals for ML2.2.nucl.fasta
#Total bases: 28681222
#Transcripts with blast hits: 12841 out of 16548 total (77.60%)
#Bases covered by other dataset: 19646530 (68.50%)
#Transcripts covered by multiple hits longer than 100bp (possible fusions): 3437 (20.77%)


# Ryan 2013 Trinity

compare_transcript_versions.py -q Ml_Trinity_transcripts.fa -s ML2.2.nucl.fasta -b Ml_Trinity_transcripts.vs_ml2.tab

# Sorting intervals for Ml_Trinity_transcripts.fa
#Total bases: 29298205
#Transcripts with blast hits: 23643 out of 35203 total (67.16%)
#Bases covered by other dataset: 19579948 (66.83%)
#Transcripts covered by multiple hits longer than 100bp (possible fusions): 1208 (3.43%)
# Sorting intervals for ML2.2.nucl.fasta
#Total bases: 28681222
#Transcripts with blast hits: 12875 out of 16548 total (77.80%)
#Bases covered by other dataset: 18417078 (64.21%)
#Transcripts covered by multiple hits longer than 100bp (possible fusions): 5458 (32.98%)


# Babonis 2016 Trinity

compare_transcript_versions.py -q GFAT01.1.renamed.fasta -s ML2.2.nucl.fasta -b GFAT01.1.renamed.vs_ml2.tab

# Sorting intervals for GFAT01.1.renamed.fasta
#Total bases: 137638938
#Transcripts with blast hits: 50914 out of 140842 total (36.15%)
#Bases covered by other dataset: 51055681 (37.09%)
#Transcripts covered by multiple hits longer than 100bp (possible fusions): 5588 (3.97%)
# Sorting intervals for ML2.2.nucl.fasta
#Total bases: 28681222
#Transcripts with blast hits: 15902 out of 16548 total (96.10%)
#Bases covered by other dataset: 25496985 (88.90%)
#Transcripts covered by multiple hits longer than 100bp (possible fusions): 4973 (30.05%)

Set1	Set2	Tx	Total_bp	Tx_w_hits	ML_w_hits	Covered_by_ML2	ML2_covered_bp	Cand_Splits	Cand_Fusions
StringTie	ML2	30052	38355056	21920	12841	24879917	19646530	3437	1767
Trinity	ML2	35203	29298205	23643	12875	19579948	18417078	5458	1208
GFAT01	ML2	140842	137638938	50914	15902	51055681	25496985	4973	5588

