# Mnemiopsis leidyi genome tracks #
Using [Mnemiopsis leidyi genome v2.2](https://research.nhgri.nih.gov/mnemiopsis/), by [Ryan et al 2013](https://doi.org/10.1126/science.1242592)

Embryonic datasets from [PRJNA258375](https://www.ncbi.nlm.nih.gov/bioproject/258375), by [Levin et al 2016](https://doi.org/10.1038/nature16994)

Tentacles vs comb rows datasets from [PRJEB28334](https://www.ncbi.nlm.nih.gov/bioproject/498033), by [Babonis et al 2018](https://doi.org/10.1093/molbev/msy171)

Transcriptome from [NCBI WGS GFAT01](https://www.ncbi.nlm.nih.gov/Traces/wgs/GFAT01), of 140842 contigs, labelled as MBL embryology course 1-9hrs post-fertilization, appears to be unpublished.

Illumina transcriptome downloaded from [neurobase](https://neurobase.rc.ufl.edu/pleurobrachia/download), of 197324 contigs avg 1279bp, possibly from [SRX250329](https://www.ncbi.nlm.nih.gov/sra/SRX250329]) with no sample information, by [Moroz 2014](https://doi.org/10.1038/nature13400)

```
#!/bin/bash sh

# REPEAT TRACK

~/git/genomeGTFtools/repeat2gtf.py MlScaffold09.nt -l > MlScaffold09.gaps.gff


# MAPPING GENOMIC RNASEQ

~/sratoolkit.2.8.2-1-ubuntu64/bin/fastq-dump --split-files --gzip --defline-seq '@$sn[_$rn]/$ri' SRR1971491

hisat2-build MlScaffold09.nt MlScaffold09.nt

hisat2 -q -x ../MlScaffold09.nt -1 SRR1971491_1.fastq.gz -2 SRR1971491_2.fastq.gz -S Ml_rna_SRR1971491_vs_v2scaffolds.sam -p 6 2> Ml_rna_SRR1971491_vs_v2scaffolds.log

sizecutter.py -f MlScaffold09.nt | sed s/" "/"\t"/ > MlScaffold09.sizes

~/samtools-1.8/samtools view -Su Ml_rna_SRR1971491_vs_v2scaffolds.sam | ~/samtools-1.8/samtools sort - -o Ml_rna_SRR1971491_vs_v2scaffolds.sorted.bam

~/samtools-1.8/samtools view -b Ml_rna_SRR1971491_vs_v2scaffolds.sorted.bam | ~/bedtools2/bin/bedtools genomecov -ibam stdin -bg -split > Ml_rna_SRR1971491_vs_v2scaffolds.sorted.bam.cov

~/ucsc/bedGraphToBigWig Ml_rna_SRR1971491_vs_v2scaffolds.sorted.bam.cov ../MlScaffold09.sizes Ml_rna_SRR1971491_vs_v2scaffolds.bw

~/samtools-1.8/samtools index Ml_rna_SRR1971491_vs_v2scaffolds.sorted.bam


~/stringtie-1.3.4d.Linux_x86_64/stringtie Ml_rna_SRR1971491_vs_v2scaffolds.sorted.bam -l Ml2_rnaseq -p 2 -o Ml_rna_SRR1971491_vs_v2scaffolds.stringtie.gtf

~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py Ml_rna_SRR1971491_vs_v2scaffolds.stringtie.gtf > Ml_rna_SRR1971491_vs_v2scaffolds.stringtie.gff


# MAPPING LEVIN 2016 DATASET

hisat2 -q -x ../MlScaffold09.nt -U h00_SRR1552869.fastq.gz,h00_SRR1552889.fastq.gz -S h00_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h01_SRR1552870.fastq.gz,h01_SRR1552890.fastq.gz -S h01_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h02_SRR1552871.fastq.gz,h02_SRR1552891.fastq.gz -S h02_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h03_SRR1552872.fastq.gz,h03_SRR1552892.fastq.gz -S h03_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h04_SRR1552873.fastq.gz,h04_SRR1552893.fastq.gz -S h04_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h04.5_SRR1552874.fastq.gz,h04.5_SRR1552894.fastq.gz -S h045_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h05_SRR1552875.fastq.gz,h05_SRR1552895.fastq.gz -S h05_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h05.5_SRR1552876.fastq.gz,h05.5_SRR1552896.fastq.gz -S h055_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h06_SRR1552877.fastq.gz,h06_SRR1552897.fastq.gz -S h06_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h06.5_SRR1552878.fastq.gz,h06.5_SRR1552898.fastq.gz -S h065_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h07_SRR1552879.fastq.gz,h07_SRR1552899.fastq.gz -S h07_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h07.5_SRR1552880.fastq.gz,h07.5_SRR1552900.fastq.gz -S h075_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h08_SRR1552881.fastq.gz,h08_SRR1552901.fastq.gz -S h08_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h08.5_SRR1552882.fastq.gz,h08.5_SRR1552902.fastq.gz -S h085_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h09_SRR1552883.fastq.gz,h09_SRR1552903.fastq.gz -S h09_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h10_SRR1552884.fastq.gz,h10_SRR1552904.fastq.gz -S h10_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h11_SRR1552885.fastq.gz,h11_SRR1552905.fastq.gz -S h11_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h12_SRR1552886.fastq.gz,h12_SRR1552906.fastq.gz -S h12_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h13_SRR1552887.fastq.gz,h13_SRR1552907.fastq.gz -S h13_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log
hisat2 -q -x ../MlScaffold09.nt -U h20_SRR1552888.fastq.gz,h20_SRR1552908.fastq.gz -S h20_vs_v2scaffolds.sam -p 6 2>> levin_set_vs_v2scaffolds.log

# produced around 40-50% alignment rate

for FILE in *.sam ; do BASE="${FILE%.sam}" ; ~/samtools-1.8/samtools view -Su $FILE | ~/samtools-1.8/samtools sort - -o $BASE.sorted.bam ; ~/samtools-1.8/samtools view -b $BASE.sorted.bam | ~/bedtools2/bin/bedtools genomecov -ibam stdin -bg -split > $BASE.sorted.bam.cov ; ~/ucsc/bedGraphToBigWig $BASE.sorted.bam.cov ../MlScaffold09.sizes $BASE.sorted.bam.cov.bw ; done

# MAPPING BABONIS 2018 DATASET
# TESTING FOR STRANDED LIBRARY
hisat2 -q -x ../MlScaffold09.nt -1 tentacle_ERR2752243_1.fastq.gz,tentacle_ERR2752250_1.fastq.gz,tentacle_ERR2752251_1.fastq.gz -2 tentacle_ERR2752243_2.fastq.gz,tentacle_ERR2752250_2.fastq.gz,tentacle_ERR2752251_2.fastq.gz --rna-strandness RF -S tentacle_vs_v2_stranded.sam -p 6 --dta 2> tentacle_vs_v2_stranded.log

~/samtools-1.8/samtools view -Su tentacle_vs_v2_stranded.sam | ~/samtools-1.8/samtools sort - -o tentacle_vs_v2_stranded.sorted.bam

~/stringtie-1.3.4d.Linux_x86_64/stringtie tentacle_vs_v2_stranded.sorted.bam -v -o tentacle_vs_v2_stranded.stringtie.gtf -l tentacle_stranded -p 2

hisat2 -q -x ../MlScaffold09.nt -1 comb_ERR2752244_1.fastq.gz,comb_ERR2752245_1.fastq.gz,comb_ERR2752246_1.fastq.gz -2 comb_ERR2752244_2.fastq.gz,comb_ERR2752245_2.fastq.gz,comb_ERR2752246_2.fastq.gz --rna-strandness RF -S comb_vs_v2_stranded.sam -p 6 --dta 2> comb_vs_v2_stranded.log

~/samtools-1.8/samtools view -Su comb_vs_v2_stranded.sam | ~/samtools-1.8/samtools sort - -o comb_vs_v2_stranded.sorted.bam

~/stringtie-1.3.4d.Linux_x86_64/stringtie comb_vs_v2_stranded.sorted.bam -o comb_vs_v2_stranded.stringtie.gtf -l comb_stranded -p 2

# APPEARS NOT TO BE STRANDED
# RERUN WITHOUT STRAND for tentacle bulb
hisat2 -q -x ../MlScaffold09.nt -1 tentacle_ERR2752243_1.fastq.gz,tentacle_ERR2752250_1.fastq.gz,tentacle_ERR2752251_1.fastq.gz -2 tentacle_ERR2752243_2.fastq.gz,tentacle_ERR2752250_2.fastq.gz,tentacle_ERR2752251_2.fastq.gz -S tentacle_vs_v2_unstranded.sam -p 6 --dta 2> tentacle_vs_v2_unstranded.log

~/samtools-1.8/samtools view -Su tentacle_vs_v2_unstranded.sam | ~/samtools-1.8/samtools sort - -o tentacle_vs_v2_unstranded.sorted.bam

~/stringtie-1.3.4d.Linux_x86_64/stringtie tentacle_vs_v2_unstranded.sorted.bam -o tentacle_vs_v2_unstranded.stringtie.gtf -l tentacle_unstranded -p 2

~/samtools-1.8/samtools view -b tentacle_vs_v2_unstranded.sorted.bam | ~/bedtools2/bin/bedtools genomecov -ibam stdin -bg -split > tentacle_vs_v2_unstranded.sorted.bam.cov

~/ucsc/bedGraphToBigWig tentacle_vs_v2_unstranded.sorted.bam.cov ../MlScaffold09.sizes tentacle_vs_v2_unstranded.sorted.bam.cov.bw

# HISAT for comb rows

hisat2 -q -x ../MlScaffold09.nt -1 comb_ERR2752244_1.fastq.gz,comb_ERR2752245_1.fastq.gz,comb_ERR2752246_1.fastq.gz -2 comb_ERR2752244_2.fastq.gz,comb_ERR2752245_2.fastq.gz,comb_ERR2752246_2.fastq.gz -S comb_vs_v2_unstranded.sam -p 6 --dta 2> comb_vs_v2_unstranded.log

~/samtools-1.8/samtools view -Su comb_vs_v2_unstranded.sam | ~/samtools-1.8/samtools sort - -o comb_vs_v2_unstranded.sorted.bam

~/stringtie-1.3.4d.Linux_x86_64/stringtie comb_vs_v2_unstranded.sorted.bam -o comb_vs_v2_unstranded.stringtie.gtf -l comb_unstranded -p 2

~/samtools-1.8/samtools view -b comb_vs_v2_unstranded.sorted.bam | ~/bedtools2/bin/bedtools genomecov -ibam stdin -bg -split > comb_vs_v2_unstranded.sorted.bam.cov

~/ucsc/bedGraphToBigWig comb_vs_v2_unstranded.sorted.bam.cov ../MlScaffold09.sizes comb_vs_v2_unstranded.sorted.bam.cov.bw

# minimap2 of WGS transcriptome

gzip -dc GFAT01.1.fsa_nt.gz | fastarenamer.py -d ' ' -s 4 -p Mlei_GFAT01 - > GFAT01.1.renamed.fasta

prottrans.py -a 50 GFAT01.1.renamed.fasta | commonseq.py -1 - -e > GFAT01.1.renamed.prot.fasta

~/minimap2-2.17_x64-linux/minimap2 -a -x splice -t 4 --secondary=no MlScaffold09.nt.fa GFAT01.1.renamed.fasta | ~/samtools-1.9/samtools sort - -o GFAT01.1.renamed.bam

~/samtools-1.9/samtools flagstat GFAT01.1.renamed.bam

#149763 + 0 in total (QC-passed reads + QC-failed reads)
#0 + 0 secondary
#8921 + 0 supplementary
#131139 + 0 mapped (87.56% : N/A)

~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M GFAT01.1.renamed.bam > GFAT01.1.renamed.pinfish.gtf

~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py GFAT01.1.renamed.pinfish.gtf > GFAT01.1.renamed.pinfish.gff

# minimap2 of Moroz set transcriptome

~/minimap2-2.14_x64-linux/minimap2 -a -x splice -t 4 --secondary=no MlScaffold09.nt Mnemiopsis_leidyi_Illumina_RNA-seq | ~/samtools-1.8/samtools sort - -o Mnemiopsis_leidyi_Illumina_RNA-seq.bam

~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M Mnemiopsis_leidyi_Illumina_RNA-seq.bam > Mnemiopsis_leidyi_Illumina_RNA-seq.pinfish.gtf

~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py Mnemiopsis_leidyi_Illumina_RNA-seq.pinfish.gtf > Mnemiopsis_leidyi_Illumina_RNA-seq.pinfish.gff

~/samtools-1.9/samtools flagstat Mnemiopsis_leidyi_Illumina_RNA-seq.bam

#213573 + 0 in total
#16249 + 0 supplementary
#209735 + 0 mapped (98.20% : N/A)

```

### remake of annotations ###
Using the script [remake_genome_annotation.py](https://bitbucket.org/wrf/genome-reannotations/src/master/remake_genome_annotation.py):

`~/git/genome-reannotations/remake_genome_annotation.py -m ml_manually_fixed_txs.direct.gtf -i ~/git/genome-reannotations/jbrowse-tracks/mnemiopsis/Mlei_2.2_annotation_sheet.tab -g ~/genomes/mnemiopsis_leidyi/ML2.2.gff3 -a ~/genomes/mnemiopsis_leidyi/MLRB2.2.gff ~/genomes/mnemiopsis_leidyi/trn15-30hpf.gff3 ~/genomes/mnemiopsis_leidyi/MlScaffold09_stringtie.gff3 ~/genomes/mnemiopsis_leidyi/GFAT01.1.renamed.pinfish.gtf -s ML2.3.21 -f ML2_to_2.3.21_names.tab > ML2.3.21_annotations.gff`

Error log as of the 21th iteration of manual fixes:

```
# Getting manual annotations from -m
# Reading GFF file ml_manually_fixed_txs.direct.gtf
# File contained:
#exon	4600
#mRNA	185
# Collected exons for 185 transcripts
# 0 errors detected in ml_manually_fixed_txs.direct.gtf
# Getting main gene annotation from -g
# Reading GFF file /home/wrf/genomes/mnemiopsis_leidyi/ML2.2.gff3
# File contained:
#exon	91489
#CDS	91469
#gene	16548
#mRNA	16548
#three_prime_UTR	5243
#five_prime_UTR	4990
#contig	1748
# Collected exons for 16548 transcripts
# 0 errors detected in /home/wrf/genomes/mnemiopsis_leidyi/ML2.2.gff3
# Getting alternate annotation from -a
# Reading GFF file /home/wrf/genomes/mnemiopsis_leidyi/MLRB2.2.gff
# File contained:
#exon	423660
#CDS	423660
#gene	60006
#mRNA	60006
#three_prime_UTR	10157
#five_prime_UTR	9460
#contig	2580
# Collected exons for 60006 transcripts
ERROR: transcript MLRB000210 with bounds 13029,17454 extends beyond features 13029,13210
ERROR: transcript MLRB00161 with bounds 1,13415 extends beyond features 3525,13415
ERROR: transcript MLRB00181 with bounds 1,19415 extends beyond features 2458,19415
ERROR: transcript MLRB00191 with bounds 1,17652 extends beyond features 594,17652
ERROR: transcript MLRB0021112 with bounds 322652,328053 extends beyond features 322652,326598
ERROR: transcript MLRB003352 with bounds 171065,177727 extends beyond features 171065,174110
ERROR: transcript MLRB006355 with bounds 121765,143479 extends beyond features 121765,141826
ERROR: transcript MLRB006613 with bounds 15478,16542 extends beyond features 15478,16521
ERROR: transcript MLRB00714 with bounds 4866,56132 extends beyond features 4866,50136
ERROR: transcript MLRB007438 with bounds 114657,115282 extends beyond features 114657,114905
ERROR: transcript MLRB00881 with bounds 1,45890 extends beyond features 9358,45890
ERROR: transcript MLRB00951 with bounds 1,34331 extends beyond features 2265,34331
ERROR: transcript MLRB00971 with bounds 1,5006 extends beyond features 1182,5006
ERROR: transcript MLRB009871 with bounds 165221,167572 extends beyond features 165221,167220
ERROR: transcript MLRB01001 with bounds 1,14412 extends beyond features 2741,14412
ERROR: transcript MLRB0101131 with bounds 370972,375781 extends beyond features 370972,375387
ERROR: transcript MLRB01021 with bounds 1,17594 extends beyond features 829,17594
ERROR: transcript MLRB010214 with bounds 17614,23982 extends beyond features 17614,22993
ERROR: transcript MLRB01141 with bounds 1,12401 extends beyond features 3421,12401
ERROR: transcript MLRB012746 with bounds 132720,135824 extends beyond features 135401,135824
ERROR: transcript MLRB01311 with bounds 1,34769 extends beyond features 40,34769
ERROR: transcript MLRB014061 with bounds 147480,155180 extends beyond features 147480,154442
ERROR: transcript MLRB01411 with bounds 1,3937 extends beyond features 986,3937
ERROR: transcript MLRB01531 with bounds 1,8357 extends beyond features 865,8357
ERROR: transcript MLRB016620 with bounds 34280,34845 extends beyond features 34280,34718
ERROR: transcript MLRB01741 with bounds 1,16949 extends beyond features 1174,16949
ERROR: transcript MLRB01761 with bounds 1,2074 extends beyond features 1889,2074
ERROR: transcript MLRB0180170 with bounds 572515,599162 extends beyond features 572515,597310
ERROR: transcript MLRB018172 with bounds 196174,205716 extends beyond features 196174,197795
ERROR: transcript MLRB01861 with bounds 1,20808 extends beyond features 1062,20808
ERROR: transcript MLRB018712 with bounds 36596,38108 extends beyond features 36596,36821
ERROR: transcript MLRB01931 with bounds 1,5027 extends beyond features 2651,5027
ERROR: transcript MLRB019469 with bounds 158647,168574 extends beyond features 158647,166700
ERROR: transcript MLRB01954 with bounds 11857,12574 extends beyond features 11857,12547
ERROR: transcript MLRB021190 with bounds 228308,229243 extends beyond features 228308,228823
ERROR: transcript MLRB021934 with bounds 86046,103877 extends beyond features 86046,102494
ERROR: transcript MLRB02431 with bounds 1,3578 extends beyond features 2167,3578
ERROR: transcript MLRB02511 with bounds 1,4174 extends beyond features 180,4174
ERROR: transcript MLRB02521 with bounds 1,36812 extends beyond features 276,36812
ERROR: transcript MLRB025420 with bounds 45504,75223 extends beyond features 45504,74968
ERROR: transcript MLRB02601 with bounds 1,15776 extends beyond features 1761,15776
ERROR: transcript MLRB02615 with bounds 22673,60988 extends beyond features 22673,60886
ERROR: transcript MLRB026716 with bounds 34476,58517 extends beyond features 34476,55410
ERROR: transcript MLRB02691 with bounds 1,25174 extends beyond features 7985,25174
ERROR: transcript MLRB02741 with bounds 1,23249 extends beyond features 2817,23249
ERROR: transcript MLRB027537 with bounds 60956,65451 extends beyond features 60956,63059
ERROR: transcript MLRB02761 with bounds 1,14257 extends beyond features 3590,14257
ERROR: transcript MLRB027739 with bounds 101707,108304 extends beyond features 101707,108139
ERROR: transcript MLRB02871 with bounds 1,21942 extends beyond features 5925,21942
ERROR: transcript MLRB02893 with bounds 5311,9019 extends beyond features 5311,6657
ERROR: transcript MLRB029612 with bounds 16578,17097 extends beyond features 16578,16855
ERROR: transcript MLRB02988 with bounds 16255,29021 extends beyond features 16255,28945
ERROR: transcript MLRB02991 with bounds 1,5751 extends beyond features 2837,5751
ERROR: transcript MLRB030973 with bounds 233981,248355 extends beyond features 233981,245701
ERROR: transcript MLRB032561 with bounds 195845,225942 extends beyond features 195845,222428
ERROR: transcript MLRB033683 with bounds 212914,215009 extends beyond features 212914,213150
ERROR: transcript MLRB0343137 with bounds 368811,371909 extends beyond features 368811,371453
ERROR: transcript MLRB0346109 with bounds 396333,396593 extends beyond features 396358,396593
ERROR: transcript MLRB035013 with bounds 17555,139214 extends beyond features 17555,138715
ERROR: transcript MLRB03531 with bounds 1,37448 extends beyond features 166,37448
ERROR: transcript MLRB03541 with bounds 1,13617 extends beyond features 1040,13617
ERROR: transcript MLRB03563 with bounds 2265,14921 extends beyond features 2265,13986
ERROR: transcript MLRB03591 with bounds 1,430 extends beyond features 134,430
ERROR: transcript MLRB03611 with bounds 1,61205 extends beyond features 231,61205
ERROR: transcript MLRB036317 with bounds 33056,37934 extends beyond features 33056,37188
ERROR: transcript MLRB03661 with bounds 1,3608 extends beyond features 459,3608
ERROR: transcript MLRB036633 with bounds 69454,90463 extends beyond features 69454,82311
ERROR: transcript MLRB03671 with bounds 1,48129 extends beyond features 397,48129
ERROR: transcript MLRB03681 with bounds 1,44868 extends beyond features 6702,44868
ERROR: transcript MLRB03701 with bounds 1,4758 extends beyond features 1949,4758
ERROR: transcript MLRB037830 with bounds 88196,118744 extends beyond features 88196,118720
ERROR: transcript MLRB03832 with bounds 1,3776 extends beyond features 302,3776
ERROR: transcript MLRB038313 with bounds 35528,69851 extends beyond features 35528,68315
ERROR: transcript MLRB03881 with bounds 1,68040 extends beyond features 457,68040
ERROR: transcript MLRB03931 with bounds 1,711 extends beyond features 21,711
ERROR: transcript MLRB03995 with bounds 9976,21300 extends beyond features 9976,21208
ERROR: transcript MLRB04151 with bounds 1,2843 extends beyond features 1293,2843
ERROR: transcript MLRB04221 with bounds 1,4239 extends beyond features 1,3485
ERROR: transcript MLRB04251 with bounds 1,4181 extends beyond features 131,4181
ERROR: transcript MLRB042779 with bounds 239904,240703 extends beyond features 240207,240703
ERROR: transcript MLRB04389 with bounds 15120,113076 extends beyond features 15120,113026
ERROR: transcript MLRB04391 with bounds 1,5750 extends beyond features 125,5750
ERROR: transcript MLRB04538 with bounds 11593,23574 extends beyond features 11593,22883
ERROR: transcript MLRB04698 with bounds 7425,12279 extends beyond features 7425,12009
ERROR: transcript MLRB04701 with bounds 1,13004 extends beyond features 1546,13004
ERROR: transcript MLRB047211 with bounds 19470,22629 extends beyond features 19470,22533
ERROR: transcript MLRB04821 with bounds 1,48888 extends beyond features 997,48888
ERROR: transcript MLRB048412 with bounds 31730,47810 extends beyond features 31730,47361
ERROR: transcript MLRB04861 with bounds 1,6510 extends beyond features 4088,6510
ERROR: transcript MLRB048648 with bounds 119610,217522 extends beyond features 119610,217005
ERROR: transcript MLRB04961 with bounds 1,4690 extends beyond features 1338,4690
ERROR: transcript MLRB05091 with bounds 1,621 extends beyond features 188,621
ERROR: transcript MLRB05151 with bounds 1,375 extends beyond features 52,375
ERROR: transcript MLRB05171 with bounds 1,962 extends beyond features 11,962
ERROR: transcript MLRB05172 with bounds 1,35033 extends beyond features 39,35033
ERROR: transcript MLRB051767 with bounds 182094,182518 extends beyond features 182094,182272
ERROR: transcript MLRB05211 with bounds 1,16371 extends beyond features 4306,16371
ERROR: transcript MLRB05217 with bounds 22096,23783 extends beyond features 22096,22503
ERROR: transcript MLRB05422 with bounds 2085,15898 extends beyond features 2085,15639
ERROR: transcript MLRB05452 with bounds 1234,1489 extends beyond features 1234,1424
ERROR: transcript MLRB05501 with bounds 1,36896 extends beyond features 284,36896
ERROR: transcript MLRB05651 with bounds 1,2819 extends beyond features 50,2819
ERROR: transcript MLRB056575 with bounds 178656,186076 extends beyond features 178656,183186
ERROR: transcript MLRB05801 with bounds 1,8062 extends beyond features 684,8062
ERROR: transcript MLRB05802 with bounds 1,36677 extends beyond features 1534,36677
ERROR: transcript MLRB059043 with bounds 111246,133179 extends beyond features 111246,132078
ERROR: transcript MLRB05941 with bounds 1,4280 extends beyond features 957,4280
ERROR: transcript MLRB05944 with bounds 1197,7045 extends beyond features 1197,6834
ERROR: transcript MLRB05958 with bounds 7360,30498 extends beyond features 7360,29906
ERROR: transcript MLRB060333 with bounds 77157,94501 extends beyond features 77157,91335
ERROR: transcript MLRB06041 with bounds 1,25384 extends beyond features 688,25384
ERROR: transcript MLRB06071 with bounds 1,26491 extends beyond features 165,26491
ERROR: transcript MLRB060717 with bounds 35476,36857 extends beyond features 35476,36728
ERROR: transcript MLRB06081 with bounds 1,360 extends beyond features 199,360
ERROR: transcript MLRB061614 with bounds 27508,33416 extends beyond features 27508,31286
ERROR: transcript MLRB061619 with bounds 30051,33416 extends beyond features 30051,32778
ERROR: transcript MLRB06191 with bounds 1,14622 extends beyond features 254,14622
ERROR: transcript MLRB06212 with bounds 1347,3396 extends beyond features 1347,1513
ERROR: transcript MLRB06241 with bounds 1,1225 extends beyond features 459,1225
ERROR: transcript MLRB06271 with bounds 1,16773 extends beyond features 6755,16773
ERROR: transcript MLRB06305 with bounds 5221,20387 extends beyond features 5221,20343
ERROR: transcript MLRB06314 with bounds 888,1503 extends beyond features 888,1448
ERROR: transcript MLRB0633121 with bounds 312706,333140 extends beyond features 312706,331681
ERROR: transcript MLRB06371 with bounds 1,1397 extends beyond features 36,1397
ERROR: transcript MLRB06441 with bounds 1,2304 extends beyond features 307,2304
ERROR: transcript MLRB0649208 with bounds 578313,584256 extends beyond features 578313,582843
ERROR: transcript MLRB06511 with bounds 1,20390 extends beyond features 14,20390
ERROR: transcript MLRB065389 with bounds 176992,181500 extends beyond features 176992,179564
ERROR: transcript MLRB06571 with bounds 1,904 extends beyond features 152,904
ERROR: transcript MLRB0657169 with bounds 461115,495736 extends beyond features 461115,495491
ERROR: transcript MLRB06701 with bounds 1,11542 extends beyond features 1195,11542
ERROR: transcript MLRB06711 with bounds 1,4129 extends beyond features 1084,4129
ERROR: transcript MLRB06741 with bounds 1,19182 extends beyond features 2138,19182
ERROR: transcript MLRB06751 with bounds 1,1625 extends beyond features 1438,1625
ERROR: transcript MLRB06811 with bounds 1,1220 extends beyond features 366,1220
ERROR: transcript MLRB06831 with bounds 1,22030 extends beyond features 767,22030
ERROR: transcript MLRB06991 with bounds 1,1157 extends beyond features 1,111
ERROR: transcript MLRB0702224 with bounds 559792,582877 extends beyond features 559792,582747
ERROR: transcript MLRB070915 with bounds 36131,40338 extends beyond features 36131,39830
ERROR: transcript MLRB07241 with bounds 1,17228 extends beyond features 5295,17228
ERROR: transcript MLRB07301 with bounds 1,13496 extends beyond features 1392,13496
ERROR: transcript MLRB0732227 with bounds 587809,602015 extends beyond features 587809,598398
ERROR: transcript MLRB07331 with bounds 1,9346 extends beyond features 3395,9346
ERROR: transcript MLRB07501 with bounds 1,13846 extends beyond features 509,13846
ERROR: transcript MLRB075268 with bounds 178326,182121 extends beyond features 178326,180630
ERROR: transcript MLRB07579 with bounds 16184,22853 extends beyond features 16184,22728
ERROR: transcript MLRB07701 with bounds 1,7847 extends beyond features 399,7847
ERROR: transcript MLRB07706 with bounds 8955,16379 extends beyond features 8955,16239
ERROR: transcript MLRB077523 with bounds 79717,89584 extends beyond features 79717,88714
ERROR: transcript MLRB077689 with bounds 236876,237710 extends beyond features 237534,237710
ERROR: transcript MLRB07861 with bounds 1,45774 extends beyond features 1449,45188
ERROR: transcript MLRB078854 with bounds 150432,173388 extends beyond features 150432,170900
ERROR: transcript MLRB07901 with bounds 1,1053 extends beyond features 569,1016
ERROR: transcript MLRB08041 with bounds 1,13443 extends beyond features 3727,13443
ERROR: transcript MLRB08091 with bounds 1,4824 extends beyond features 209,4824
ERROR: transcript MLRB082641 with bounds 109200,111448 extends beyond features 109200,109635
ERROR: transcript MLRB08401 with bounds 1,4977 extends beyond features 626,4977
ERROR: transcript MLRB08441 with bounds 1,261 extends beyond features 69,261
ERROR: transcript MLRB086116 with bounds 28538,31965 extends beyond features 28538,29697
ERROR: transcript MLRB08661 with bounds 1,43254 extends beyond features 158,43254
ERROR: transcript MLRB088013 with bounds 28132,32940 extends beyond features 28132,28280
ERROR: transcript MLRB08861 with bounds 1,5227 extends beyond features 1919,5227
ERROR: transcript MLRB08881 with bounds 1,3047 extends beyond features 2163,3047
ERROR: transcript MLRB08921 with bounds 1,48001 extends beyond features 1568,48001
ERROR: transcript MLRB08941 with bounds 1,8300 extends beyond features 23,8300
ERROR: transcript MLRB09001 with bounds 1,8804 extends beyond features 622,8804
ERROR: transcript MLRB090838 with bounds 58134,186957 extends beyond features 58134,186815
ERROR: transcript MLRB091240 with bounds 87323,87541 extends beyond features 87323,87485
ERROR: transcript MLRB09152 with bounds 80,2032 extends beyond features 80,1670
ERROR: transcript MLRB09171 with bounds 1,10178 extends beyond features 2216,10178
ERROR: transcript MLRB091718 with bounds 19321,19889 extends beyond features 19321,19557
ERROR: transcript MLRB09201 with bounds 1,40795 extends beyond features 115,40795
ERROR: transcript MLRB09251 with bounds 1,8799 extends beyond features 322,8799
ERROR: transcript MLRB092679 with bounds 197212,209958 extends beyond features 197212,206094
ERROR: transcript MLRB09311 with bounds 1,927 extends beyond features 802,927
ERROR: transcript MLRB09401 with bounds 1,2892 extends beyond features 543,2892
ERROR: transcript MLRB094228 with bounds 78186,83885 extends beyond features 78186,82296
ERROR: transcript MLRB09431 with bounds 1,49558 extends beyond features 486,49558
ERROR: transcript MLRB09521 with bounds 1,1102 extends beyond features 364,1102
ERROR: transcript MLRB09631 with bounds 1,5981 extends beyond features 1885,5981
ERROR: transcript MLRB096625 with bounds 41834,46963 extends beyond features 41834,44209
ERROR: transcript MLRB09751 with bounds 1,5281 extends beyond features 2313,5281
ERROR: transcript MLRB09861 with bounds 1,22486 extends beyond features 581,22486
ERROR: transcript MLRB09921 with bounds 1,443 extends beyond features 30,443
ERROR: transcript MLRB099819 with bounds 30989,78511 extends beyond features 30989,78438
ERROR: transcript MLRB101125 with bounds 97763,100330 extends beyond features 97763,100198
ERROR: transcript MLRB10335 with bounds 8358,22896 extends beyond features 8358,20880
ERROR: transcript MLRB10431 with bounds 1,12285 extends beyond features 1809,12285
ERROR: transcript MLRB10515 with bounds 5847,47630 extends beyond features 5847,46449
ERROR: transcript MLRB10661 with bounds 1,28995 extends beyond features 306,28995
ERROR: transcript MLRB10751 with bounds 589,1096 extends beyond features 589,839
ERROR: transcript MLRB10832 with bounds 253,19473 extends beyond features 253,19336
ERROR: transcript MLRB110312 with bounds 14106,58497 extends beyond features 14106,57263
ERROR: transcript MLRB1117112 with bounds 265203,281527 extends beyond features 265203,280823
ERROR: transcript MLRB11277 with bounds 10608,14672 extends beyond features 10608,12074
ERROR: transcript MLRB113496 with bounds 266455,272165 extends beyond features 266455,272137
ERROR: transcript MLRB113540 with bounds 43548,48660 extends beyond features 43548,48400
ERROR: transcript MLRB11403 with bounds 275,8991 extends beyond features 275,8922
ERROR: transcript MLRB11533 with bounds 329,25218 extends beyond features 329,24631
ERROR: transcript MLRB11611 with bounds 1,1286 extends beyond features 161,1286
ERROR: transcript MLRB11631 with bounds 1,2422 extends beyond features 1070,2422
ERROR: transcript MLRB11671 with bounds 1,2574 extends beyond features 1600,2408
ERROR: transcript MLRB116870 with bounds 183983,186713 extends beyond features 183983,186616
ERROR: transcript MLRB11865 with bounds 2330,2902 extends beyond features 2330,2606
ERROR: transcript MLRB12041 with bounds 1,5487 extends beyond features 194,5487
ERROR: transcript MLRB12051 with bounds 1,5001 extends beyond features 144,5001
ERROR: transcript MLRB12071 with bounds 1,9779 extends beyond features 220,9779
ERROR: transcript MLRB1207188 with bounds 461178,461473 extends beyond features 461186,461473
ERROR: transcript MLRB12131 with bounds 1,383 extends beyond features 29,383
ERROR: transcript MLRB121822 with bounds 31997,57775 extends beyond features 31997,56694
ERROR: transcript MLRB12231 with bounds 1,10510 extends beyond features 290,10510
ERROR: transcript MLRB12321 with bounds 1,11918 extends beyond features 2473,11918
ERROR: transcript MLRB12341 with bounds 1,12783 extends beyond features 435,12783
ERROR: transcript MLRB12421 with bounds 1,4464 extends beyond features 2250,4464
ERROR: transcript MLRB1242137 with bounds 387850,389855 extends beyond features 389408,389855
ERROR: transcript MLRB12511 with bounds 1,11120 extends beyond features 2065,11120
ERROR: transcript MLRB125119 with bounds 33761,40560 extends beyond features 33761,38740
ERROR: transcript MLRB125222 with bounds 75888,89713 extends beyond features 75888,86894
ERROR: transcript MLRB12545 with bounds 7456,9717 extends beyond features 7456,9365
ERROR: transcript MLRB12556 with bounds 2562,16976 extends beyond features 2562,16884
ERROR: transcript MLRB12621 with bounds 1,42740 extends beyond features 7047,42740
ERROR: transcript MLRB12631 with bounds 1,22497 extends beyond features 2239,22497
ERROR: transcript MLRB12744 with bounds 5734,11454 extends beyond features 5734,7140
ERROR: transcript MLRB12781 with bounds 1,5666 extends beyond features 2121,5666
ERROR: transcript MLRB12782 with bounds 1,12259 extends beyond features 48,12259
ERROR: transcript MLRB12821 with bounds 1,19680 extends beyond features 623,19680
ERROR: transcript MLRB128218 with bounds 48261,49668 extends beyond features 48261,49349
ERROR: transcript MLRB128297 with bounds 279608,287270 extends beyond features 279608,286787
ERROR: transcript MLRB13101 with bounds 1,760 extends beyond features 91,760
ERROR: transcript MLRB13131 with bounds 1,1418 extends beyond features 242,1418
ERROR: transcript MLRB131772 with bounds 189793,225549 extends beyond features 189793,225527
ERROR: transcript MLRB13338 with bounds 18085,32924 extends beyond features 18085,32675
ERROR: transcript MLRB135657 with bounds 140466,141459 extends beyond features 141235,141459
ERROR: transcript MLRB13614 with bounds 891,7689 extends beyond features 891,7575
ERROR: transcript MLRB137167 with bounds 185677,195083 extends beyond features 185677,194761
ERROR: transcript MLRB13737 with bounds 2394,10884 extends beyond features 2394,8994
ERROR: transcript MLRB13771 with bounds 1,6460 extends beyond features 30,6460
ERROR: transcript MLRB13781 with bounds 1,9192 extends beyond features 36,8806
ERROR: transcript MLRB13831 with bounds 1,37367 extends beyond features 210,37367
ERROR: transcript MLRB138371 with bounds 190465,225619 extends beyond features 190465,225021
ERROR: transcript MLRB13891 with bounds 1,47888 extends beyond features 585,47888
ERROR: transcript MLRB138920 with bounds 51733,52612 extends beyond features 51733,52111
ERROR: transcript MLRB139152 with bounds 109110,111159 extends beyond features 109110,110904
ERROR: transcript MLRB13971 with bounds 1,14577 extends beyond features 5000,14577
ERROR: transcript MLRB14002 with bounds 1244,6749 extends beyond features 1244,6244
ERROR: transcript MLRB14075 with bounds 8176,14917 extends beyond features 8176,14336
ERROR: transcript MLRB14131 with bounds 1,2363 extends beyond features 198,2363
ERROR: transcript MLRB142211 with bounds 12992,16762 extends beyond features 12992,16733
ERROR: transcript MLRB14521 with bounds 1,15939 extends beyond features 288,15939
ERROR: transcript MLRB145215 with bounds 23734,28478 extends beyond features 23734,25087
ERROR: transcript MLRB14569 with bounds 15979,25025 extends beyond features 15979,22021
ERROR: transcript MLRB14661 with bounds 910,1179 extends beyond features 910,1132
ERROR: transcript MLRB14771 with bounds 1,26636 extends beyond features 180,26636
ERROR: transcript MLRB14821 with bounds 2264,3961 extends beyond features 2264,2512
ERROR: transcript MLRB148742 with bounds 101977,108388 extends beyond features 101977,107339
ERROR: transcript MLRB14891 with bounds 1,1084 extends beyond features 82,1084
ERROR: transcript MLRB14921 with bounds 1,6060 extends beyond features 4311,6060
ERROR: transcript MLRB15131 with bounds 1,2224 extends beyond features 603,2224
ERROR: transcript MLRB15184 with bounds 1473,2260 extends beyond features 1473,1616
ERROR: transcript MLRB151937 with bounds 103055,143762 extends beyond features 103055,143726
ERROR: transcript MLRB152520 with bounds 60085,60443 extends beyond features 60085,60243
ERROR: transcript MLRB15361 with bounds 1,238 extends beyond features 20,238
ERROR: transcript MLRB15432 with bounds 399,5289 extends beyond features 399,1610
ERROR: transcript MLRB15451 with bounds 1,7653 extends beyond features 1392,7653
ERROR: transcript MLRB15551 with bounds 1,1021 extends beyond features 28,1021
ERROR: transcript MLRB15611 with bounds 1,1541 extends beyond features 197,1541
ERROR: transcript MLRB15911 with bounds 1,6916 extends beyond features 1395,6916
ERROR: transcript MLRB159111 with bounds 8018,68633 extends beyond features 8018,68153
ERROR: transcript MLRB15931 with bounds 1,1071 extends beyond features 592,1071
ERROR: transcript MLRB16093 with bounds 229,1026 extends beyond features 229,936
ERROR: transcript MLRB16211 with bounds 1,8019 extends beyond features 835,8019
ERROR: transcript MLRB162130 with bounds 87758,88410 extends beyond features 87758,88109
ERROR: transcript MLRB16331 with bounds 1,1130 extends beyond features 235,1130
ERROR: transcript MLRB16371 with bounds 1,1392 extends beyond features 125,1392
ERROR: transcript MLRB16381 with bounds 1,1675 extends beyond features 1048,1675
ERROR: transcript MLRB16441 with bounds 1,3998 extends beyond features 3771,3998
ERROR: transcript MLRB16521 with bounds 1,709 extends beyond features 18,709
ERROR: transcript MLRB165649 with bounds 90286,101956 extends beyond features 90286,101677
ERROR: transcript MLRB16591 with bounds 1,16152 extends beyond features 205,16152
ERROR: transcript MLRB16641 with bounds 1,25101 extends beyond features 41,25101
ERROR: transcript MLRB166913 with bounds 25242,30012 extends beyond features 25242,29881
ERROR: transcript MLRB1670206 with bounds 575679,580106 extends beyond features 575679,577879
ERROR: transcript MLRB16931 with bounds 1,3399 extends beyond features 2197,3399
ERROR: transcript MLRB169312 with bounds 23434,26113 extends beyond features 23434,25167
ERROR: transcript MLRB17021 with bounds 1,2051 extends beyond features 334,2051
ERROR: transcript MLRB17251 with bounds 1,1068 extends beyond features 1,947
ERROR: transcript MLRB17401 with bounds 1,10158 extends beyond features 1278,10158
ERROR: transcript MLRB17408 with bounds 11703,92878 extends beyond features 11703,92378
ERROR: transcript MLRB17422 with bounds 554,752 extends beyond features 554,733
ERROR: transcript MLRB17431 with bounds 107,1007 extends beyond features 107,945
ERROR: transcript MLRB17481 with bounds 1,20279 extends beyond features 205,20279
ERROR: transcript MLRB17494 with bounds 111,1008 extends beyond features 111,365
ERROR: transcript MLRB17501 with bounds 1,65274 extends beyond features 1278,65274
ERROR: transcript MLRB17581 with bounds 1,943 extends beyond features 151,943
ERROR: transcript MLRB17591 with bounds 1,9806 extends beyond features 996,9806
ERROR: transcript MLRB17592 with bounds 1,14837 extends beyond features 989,14837
ERROR: transcript MLRB17611 with bounds 1,1134 extends beyond features 882,1134
ERROR: transcript MLRB17901 with bounds 1,9521 extends beyond features 377,9521
ERROR: transcript MLRB179412 with bounds 27030,28737 extends beyond features 27030,27173
ERROR: transcript MLRB18101 with bounds 1,8112 extends beyond features 1045,8112
ERROR: transcript MLRB18112 with bounds 602,18050 extends beyond features 602,16644
ERROR: transcript MLRB18141 with bounds 1,25623 extends beyond features 12775,25623
ERROR: transcript MLRB18171 with bounds 1,8472 extends beyond features 90,8472
ERROR: transcript MLRB18271 with bounds 917,1171 extends beyond features 917,1115
ERROR: transcript MLRB18351 with bounds 1,5311 extends beyond features 4696,5311
ERROR: transcript MLRB183562 with bounds 158888,182238 extends beyond features 158888,181846
ERROR: transcript MLRB18361 with bounds 1211,1822 extends beyond features 1211,1323
ERROR: transcript MLRB18471 with bounds 1,7876 extends beyond features 453,7876
ERROR: transcript MLRB18551 with bounds 1,27391 extends beyond features 619,27391
ERROR: transcript MLRB18591 with bounds 1,2223 extends beyond features 340,2223
ERROR: transcript MLRB187316 with bounds 35267,41363 extends beyond features 35267,40688
ERROR: transcript MLRB18751 with bounds 1,1217 extends beyond features 803,1217
ERROR: transcript MLRB18815 with bounds 2740,3172 extends beyond features 2740,2853
ERROR: transcript MLRB18861 with bounds 890,1838 extends beyond features 890,1225
ERROR: transcript MLRB18884 with bounds 1519,1993 extends beyond features 1519,1977
ERROR: transcript MLRB18951 with bounds 1,27151 extends beyond features 239,27151
ERROR: transcript MLRB18991 with bounds 1,10087 extends beyond features 4643,10087
ERROR: transcript MLRB19021 with bounds 8,3373 extends beyond features 8,3319
ERROR: transcript MLRB19061 with bounds 1,11964 extends beyond features 64,11964
ERROR: transcript MLRB19194 with bounds 2879,35139 extends beyond features 2879,35105
ERROR: transcript MLRB19391 with bounds 1,2632 extends beyond features 87,2632
ERROR: transcript MLRB194115 with bounds 26829,29982 extends beyond features 26829,26944
ERROR: transcript MLRB19671 with bounds 1,1113 extends beyond features 577,1113
ERROR: transcript MLRB196722 with bounds 43055,52158 extends beyond features 43055,50678
ERROR: transcript MLRB19709 with bounds 29892,52663 extends beyond features 29892,51476
ERROR: transcript MLRB19761 with bounds 1,6191 extends beyond features 2005,6191
ERROR: transcript MLRB197613 with bounds 19364,29902 extends beyond features 19364,26199
ERROR: transcript MLRB19807 with bounds 33441,52434 extends beyond features 33441,52165
ERROR: transcript MLRB19901 with bounds 1,3855 extends beyond features 3515,3855
ERROR: transcript MLRB19951 with bounds 887,2320 extends beyond features 887,1475
ERROR: transcript MLRB20021 with bounds 1,11134 extends beyond features 1622,11134
ERROR: transcript MLRB201629 with bounds 81409,118276 extends beyond features 81409,115162
ERROR: transcript MLRB20207 with bounds 3282,4004 extends beyond features 3282,3417
ERROR: transcript MLRB20371 with bounds 1,5124 extends beyond features 102,5124
ERROR: transcript MLRB203946 with bounds 59932,77539 extends beyond features 59932,75495
ERROR: transcript MLRB20741 with bounds 1,3128 extends beyond features 1291,3128
ERROR: transcript MLRB20823 with bounds 10,3041 extends beyond features 10,2473
ERROR: transcript MLRB208355 with bounds 120216,176963 extends beyond features 120216,175741
ERROR: transcript MLRB20851 with bounds 1,13320 extends beyond features 5525,13320
ERROR: transcript MLRB20893 with bounds 3003,80387 extends beyond features 3003,80171
ERROR: transcript MLRB209162 with bounds 194476,199518 extends beyond features 194476,198921
ERROR: transcript MLRB21001 with bounds 1,28822 extends beyond features 2585,28822
ERROR: transcript MLRB210216 with bounds 44814,60057 extends beyond features 44814,58732
ERROR: transcript MLRB21091 with bounds 1,22478 extends beyond features 1284,22478
ERROR: transcript MLRB21121 with bounds 1,7955 extends beyond features 2026,7955
ERROR: transcript MLRB21181 with bounds 444,1012 extends beyond features 444,888
ERROR: transcript MLRB21251 with bounds 1,2229 extends beyond features 912,2229
ERROR: transcript MLRB21301 with bounds 1,1276 extends beyond features 19,1276
ERROR: transcript MLRB21311 with bounds 1,5930 extends beyond features 364,5930
ERROR: transcript MLRB21421 with bounds 1,1345 extends beyond features 38,1345
ERROR: transcript MLRB215816 with bounds 13954,33946 extends beyond features 13954,33342
ERROR: transcript MLRB216531 with bounds 82047,83901 extends beyond features 82047,83883
ERROR: transcript MLRB21661 with bounds 1,1276 extends beyond features 986,1276
ERROR: transcript MLRB217262 with bounds 166128,167658 extends beyond features 166128,167643
ERROR: transcript MLRB218624 with bounds 55969,136733 extends beyond features 55969,134871
ERROR: transcript MLRB21931 with bounds 1,22525 extends beyond features 1117,22525
ERROR: transcript MLRB220769 with bounds 163863,166480 extends beyond features 163863,164531
ERROR: transcript MLRB2235124 with bounds 422480,444391 extends beyond features 422480,444019
ERROR: transcript MLRB22423 with bounds 169,1007 extends beyond features 169,658
ERROR: transcript MLRB22431 with bounds 789,1425 extends beyond features 789,951
ERROR: transcript MLRB22601 with bounds 1,37687 extends beyond features 2654,37687
ERROR: transcript MLRB22901 with bounds 1,4487 extends beyond features 3378,4487
ERROR: transcript MLRB22912 with bounds 502,20445 extends beyond features 502,17230
ERROR: transcript MLRB22932 with bounds 127,1047 extends beyond features 127,1008
ERROR: transcript MLRB22971 with bounds 1,50555 extends beyond features 600,50555
ERROR: transcript MLRB23181 with bounds 1,7399 extends beyond features 693,7399
ERROR: transcript MLRB233170 with bounds 122187,149390 extends beyond features 122187,147808
ERROR: transcript MLRB23361 with bounds 1,975 extends beyond features 167,975
ERROR: transcript MLRB23381 with bounds 803,4305 extends beyond features 803,3932
ERROR: transcript MLRB23552 with bounds 4294,4918 extends beyond features 4294,4794
ERROR: transcript MLRB23611 with bounds 1,2247 extends beyond features 1406,2247
ERROR: transcript MLRB23651 with bounds 1,5825 extends beyond features 104,5825
ERROR: transcript MLRB23735 with bounds 1264,2644 extends beyond features 1264,1772
ERROR: transcript MLRB23761 with bounds 1,2797 extends beyond features 354,2797
ERROR: transcript MLRB23791 with bounds 1,5039 extends beyond features 3811,5039
ERROR: transcript MLRB23893 with bounds 3449,6276 extends beyond features 3449,5752
ERROR: transcript MLRB24013 with bounds 2698,7462 extends beyond features 2698,7038
ERROR: transcript MLRB24034 with bounds 613,2280 extends beyond features 613,1918
ERROR: transcript MLRB24171 with bounds 1,1002 extends beyond features 113,1002
ERROR: transcript MLRB24671 with bounds 1,20529 extends beyond features 1680,20529
ERROR: transcript MLRB24903 with bounds 198,3502 extends beyond features 198,1779
ERROR: transcript MLRB24991 with bounds 1,10519 extends beyond features 4241,10519
ERROR: transcript MLRB25077 with bounds 26551,30484 extends beyond features 26551,28346
ERROR: transcript MLRB25291 with bounds 1,5760 extends beyond features 968,5454
ERROR: transcript MLRB25321 with bounds 1,1017 extends beyond features 254,1017
ERROR: transcript MLRB25351 with bounds 1,9441 extends beyond features 53,9441
ERROR: transcript MLRB25452 with bounds 294,1235 extends beyond features 294,1148
ERROR: transcript MLRB25631 with bounds 8,1246 extends beyond features 8,219
ERROR: transcript MLRB25761 with bounds 1,10705 extends beyond features 2192,10705
ERROR: transcript MLRB25921 with bounds 1,1147 extends beyond features 13,1147
ERROR: transcript MLRB25961 with bounds 1,8288 extends beyond features 3011,8288
ERROR: transcript MLRB26041 with bounds 2587,3002 extends beyond features 2587,2743
ERROR: transcript MLRB26073 with bounds 1657,6460 extends beyond features 1657,3940
ERROR: transcript MLRB26323 with bounds 490,1030 extends beyond features 490,967
ERROR: transcript MLRB263577 with bounds 173852,185953 extends beyond features 173852,184684
ERROR: transcript MLRB26404 with bounds 2092,2982 extends beyond features 2092,2486
ERROR: transcript MLRB26791 with bounds 1,12218 extends beyond features 217,12218
ERROR: transcript MLRB26871 with bounds 200,1212 extends beyond features 200,973
ERROR: transcript MLRB269316 with bounds 44309,51704 extends beyond features 44309,51693
ERROR: transcript MLRB27361 with bounds 1,15992 extends beyond features 4882,15992
ERROR: transcript MLRB27461 with bounds 1,7570 extends beyond features 139,7570
ERROR: transcript MLRB274642 with bounds 117027,118645 extends beyond features 117027,118412
ERROR: transcript MLRB27621 with bounds 1,8308 extends beyond features 3629,8308
ERROR: transcript MLRB276810 with bounds 9650,21971 extends beyond features 9650,18845
ERROR: transcript MLRB27691 with bounds 1,27260 extends beyond features 2226,27260
ERROR: transcript MLRB27881 with bounds 1,910 extends beyond features 776,910
ERROR: transcript MLRB27891 with bounds 1,36764 extends beyond features 658,36764
ERROR: transcript MLRB27961 with bounds 1,24417 extends beyond features 238,24417
ERROR: transcript MLRB28013 with bounds 2733,6014 extends beyond features 2733,5832
ERROR: transcript MLRB28141 with bounds 1,28586 extends beyond features 235,28586
ERROR: transcript MLRB28181 with bounds 1,1482 extends beyond features 1250,1482
ERROR: transcript MLRB28201 with bounds 1,70582 extends beyond features 25,70582
ERROR: transcript MLRB28313 with bounds 1311,1930 extends beyond features 1311,1804
ERROR: transcript MLRB28381 with bounds 1,253 extends beyond features 96,253
ERROR: transcript MLRB286120 with bounds 61188,103032 extends beyond features 61188,101627
ERROR: transcript MLRB28691 with bounds 1,645 extends beyond features 37,645
ERROR: transcript MLRB28871 with bounds 1,2225 extends beyond features 270,2225
ERROR: transcript MLRB29295 with bounds 409,1485 extends beyond features 409,1452
ERROR: transcript MLRB29296 with bounds 790,1485 extends beyond features 790,1406
ERROR: transcript MLRB294619 with bounds 16706,23530 extends beyond features 16706,22847
ERROR: transcript MLRB29531 with bounds 1,3289 extends beyond features 504,3289
ERROR: transcript MLRB29585 with bounds 402,1039 extends beyond features 402,1014
ERROR: transcript MLRB29675 with bounds 2470,16799 extends beyond features 2470,14406
ERROR: transcript MLRB29851 with bounds 1144,1939 extends beyond features 1144,1463
ERROR: transcript MLRB29872 with bounds 116,1010 extends beyond features 116,613
ERROR: transcript MLRB29971 with bounds 1,21130 extends beyond features 3739,21130
ERROR: transcript MLRB29983 with bounds 160,1355 extends beyond features 160,1236
ERROR: transcript MLRB30131 with bounds 1,1133 extends beyond features 212,1133
ERROR: transcript MLRB30363 with bounds 88,1134 extends beyond features 88,1022
ERROR: transcript MLRB30411 with bounds 2546,6573 extends beyond features 2546,5097
ERROR: transcript MLRB30455 with bounds 7819,13876 extends beyond features 7819,12429
ERROR: transcript MLRB30485 with bounds 712,1274 extends beyond features 712,1126
ERROR: transcript MLRB30652 with bounds 420,1297 extends beyond features 420,1168
ERROR: transcript MLRB30691 with bounds 1,22697 extends beyond features 1180,22697
ERROR: transcript MLRB30891 with bounds 1338,5605 extends beyond features 1338,2932
ERROR: transcript MLRB31161 with bounds 1,59949 extends beyond features 5328,59949
ERROR: transcript MLRB31171 with bounds 1,3253 extends beyond features 2640,3253
ERROR: transcript MLRB31361 with bounds 1,901 extends beyond features 66,901
ERROR: transcript MLRB31401 with bounds 1,40877 extends beyond features 1973,40877
ERROR: transcript MLRB31503 with bounds 15,1535 extends beyond features 15,1504
ERROR: transcript MLRB31651 with bounds 289,1050 extends beyond features 289,927
ERROR: transcript MLRB31731 with bounds 1,1406 extends beyond features 1136,1307
ERROR: transcript MLRB31971 with bounds 9,1017 extends beyond features 9,970
ERROR: transcript MLRB32011 with bounds 1,3361 extends beyond features 1071,3361
ERROR: transcript MLRB32075 with bounds 3339,9169 extends beyond features 3339,8198
ERROR: transcript MLRB32252 with bounds 229,1144 extends beyond features 229,970
ERROR: transcript MLRB32571 with bounds 847,2632 extends beyond features 847,2240
ERROR: transcript MLRB32621 with bounds 1,7825 extends beyond features 56,7701
ERROR: transcript MLRB3274135 with bounds 299181,310534 extends beyond features 299181,310110
ERROR: transcript MLRB32781 with bounds 1,1312 extends beyond features 184,1312
ERROR: transcript MLRB32811 with bounds 1,1831 extends beyond features 874,1831
ERROR: transcript MLRB32931 with bounds 1,1296 extends beyond features 15,1296
ERROR: transcript MLRB32951 with bounds 1,17402 extends beyond features 1172,17402
ERROR: transcript MLRB329567 with bounds 139393,182389 extends beyond features 139393,181356
ERROR: transcript MLRB32971 with bounds 1,2771 extends beyond features 358,2575
ERROR: transcript MLRB33001 with bounds 1,1223 extends beyond features 21,1223
ERROR: transcript MLRB33301 with bounds 1349,2859 extends beyond features 1349,2278
ERROR: transcript MLRB33371 with bounds 1,18716 extends beyond features 3007,18716
ERROR: transcript MLRB33391 with bounds 1,1527 extends beyond features 1293,1527
ERROR: transcript MLRB33421 with bounds 1,24089 extends beyond features 1662,24089
ERROR: transcript MLRB33461 with bounds 1,270 extends beyond features 19,270
ERROR: transcript MLRB33641 with bounds 1,254 extends beyond features 13,254
ERROR: transcript MLRB33761 with bounds 294,1414 extends beyond features 294,1380
ERROR: transcript MLRB33821 with bounds 1,1402 extends beyond features 12,1402
ERROR: transcript MLRB338220 with bounds 52464,135125 extends beyond features 52464,132872
ERROR: transcript MLRB33921 with bounds 1,2591 extends beyond features 1371,2591
ERROR: transcript MLRB34053 with bounds 191,1212 extends beyond features 191,1183
ERROR: transcript MLRB34221 with bounds 1,869 extends beyond features 61,869
ERROR: transcript MLRB34382 with bounds 70,1036 extends beyond features 70,1004
ERROR: transcript MLRB34631 with bounds 1,49930 extends beyond features 1360,49930
ERROR: transcript MLRB346339 with bounds 109136,140560 extends beyond features 109136,136851
ERROR: transcript MLRB348766 with bounds 157799,162025 extends beyond features 157799,159355
ERROR: transcript MLRB35681 with bounds 1,1044 extends beyond features 665,1044
ERROR: transcript MLRB35741 with bounds 1,1053 extends beyond features 372,1053
ERROR: transcript MLRB35821 with bounds 1,683 extends beyond features 43,683
ERROR: transcript MLRB35822 with bounds 1,1736 extends beyond features 60,1736
ERROR: transcript MLRB35881 with bounds 1,3813 extends beyond features 3586,3813
ERROR: transcript MLRB35911 with bounds 1,6308 extends beyond features 4681,6308
ERROR: transcript MLRB35916 with bounds 4717,119488 extends beyond features 4717,119449
ERROR: transcript MLRB35996 with bounds 4957,5519 extends beyond features 4957,5161
ERROR: transcript MLRB36001 with bounds 1,955 extends beyond features 757,955
ERROR: transcript MLRB36011 with bounds 351,1018 extends beyond features 351,910
ERROR: transcript MLRB36101 with bounds 1,2399 extends beyond features 679,2399
ERROR: transcript MLRB36481 with bounds 220,2221 extends beyond features 220,390
ERROR: transcript MLRB36732 with bounds 225,1120 extends beyond features 225,658
ERROR: transcript MLRB36991 with bounds 1,938 extends beyond features 15,938
ERROR: transcript MLRB37303 with bounds 136,5483 extends beyond features 136,5216
ERROR: transcript MLRB37363 with bounds 256,16966 extends beyond features 256,16246
ERROR: transcript MLRB37552 with bounds 164,1028 extends beyond features 164,709
ERROR: transcript MLRB37612 with bounds 4268,4803 extends beyond features 4268,4765
ERROR: transcript MLRB37881 with bounds 1,943 extends beyond features 374,943
ERROR: transcript MLRB38075 with bounds 408,1149 extends beyond features 408,1108
ERROR: transcript MLRB38076 with bounds 951,1149 extends beyond features 951,1095
ERROR: transcript MLRB38101 with bounds 1,924 extends beyond features 32,924
ERROR: transcript MLRB38142 with bounds 397,6088 extends beyond features 397,4416
ERROR: transcript MLRB38281 with bounds 1,805 extends beyond features 119,805
ERROR: transcript MLRB38476 with bounds 856,2883 extends beyond features 856,2663
ERROR: transcript MLRB38491 with bounds 1,1379 extends beyond features 449,1379
ERROR: transcript MLRB38512 with bounds 111,1171 extends beyond features 111,395
ERROR: transcript MLRB38751 with bounds 102,1155 extends beyond features 102,1094
ERROR: transcript MLRB38831 with bounds 1,1420 extends beyond features 217,1096
ERROR: transcript MLRB38851 with bounds 1,1954 extends beyond features 12,1954
ERROR: transcript MLRB39021 with bounds 280,1073 extends beyond features 280,1047
ERROR: transcript MLRB39112 with bounds 367,1607 extends beyond features 367,1543
ERROR: transcript MLRB39231 with bounds 1,1733 extends beyond features 1626,1733
ERROR: transcript MLRB39352 with bounds 377,1062 extends beyond features 377,1021
ERROR: transcript MLRB39403 with bounds 18,1206 extends beyond features 18,1056
ERROR: transcript MLRB39541 with bounds 1,2458 extends beyond features 557,2076
ERROR: transcript MLRB39831 with bounds 1,6373 extends beyond features 4362,6373
ERROR: transcript MLRB40233 with bounds 9,1724 extends beyond features 9,1111
ERROR: transcript MLRB40291 with bounds 1,3403 extends beyond features 2290,3403
ERROR: transcript MLRB402960 with bounds 167211,181082 extends beyond features 167211,179436
ERROR: transcript MLRB40374 with bounds 1271,2703 extends beyond features 1271,2263
ERROR: transcript MLRB40561 with bounds 1,1300 extends beyond features 1007,1300
ERROR: transcript MLRB409434 with bounds 59092,61880 extends beyond features 59092,61199
ERROR: transcript MLRB40981 with bounds 1,27998 extends beyond features 609,27998
ERROR: transcript MLRB411525 with bounds 47405,54359 extends beyond features 47405,53939
ERROR: transcript MLRB41306 with bounds 6354,18968 extends beyond features 6354,18224
ERROR: transcript MLRB41307 with bounds 13294,18968 extends beyond features 13294,18204
ERROR: transcript MLRB41501 with bounds 1,1355 extends beyond features 1248,1355
ERROR: transcript MLRB41531 with bounds 1,1066 extends beyond features 35,1066
ERROR: transcript MLRB41621 with bounds 1,1043 extends beyond features 90,1043
ERROR: transcript MLRB41841 with bounds 146,2415 extends beyond features 146,698
ERROR: transcript MLRB42001 with bounds 1,379 extends beyond features 121,379
ERROR: transcript MLRB42041 with bounds 1,1980 extends beyond features 947,1980
ERROR: transcript MLRB42304 with bounds 1714,1877 extends beyond features 1714,1833
ERROR: transcript MLRB42321 with bounds 472,1337 extends beyond features 472,1297
ERROR: transcript MLRB42331 with bounds 1,62001 extends beyond features 452,62001
ERROR: transcript MLRB42582 with bounds 1694,2938 extends beyond features 1694,2798
ERROR: transcript MLRB42621 with bounds 1,760 extends beyond features 515,760
ERROR: transcript MLRB42622 with bounds 1,1011 extends beyond features 82,784
ERROR: transcript MLRB42745 with bounds 1137,5371 extends beyond features 1137,4889
ERROR: transcript MLRB42831 with bounds 1,391 extends beyond features 52,391
ERROR: transcript MLRB42832 with bounds 1,595 extends beyond features 19,595
ERROR: transcript MLRB43131 with bounds 1,2557 extends beyond features 11,2557
ERROR: transcript MLRB43431 with bounds 1,1419 extends beyond features 1281,1419
ERROR: transcript MLRB43551 with bounds 1,662 extends beyond features 44,662
ERROR: transcript MLRB43581 with bounds 1,40467 extends beyond features 1954,40467
ERROR: transcript MLRB4358136 with bounds 419109,447403 extends beyond features 419109,446889
ERROR: transcript MLRB43771 with bounds 754,1205 extends beyond features 754,1133
ERROR: transcript MLRB43781 with bounds 1,998 extends beyond features 226,998
ERROR: transcript MLRB43901 with bounds 1,1142 extends beyond features 823,1142
ERROR: transcript MLRB44001 with bounds 1,976 extends beyond features 845,976
ERROR: transcript MLRB44021 with bounds 1,649 extends beyond features 131,649
ERROR: transcript MLRB44022 with bounds 146,1159 extends beyond features 146,649
ERROR: transcript MLRB44071 with bounds 1,2229 extends beyond features 166,2229
ERROR: transcript MLRB44091 with bounds 1,575 extends beyond features 18,575
ERROR: transcript MLRB44192 with bounds 368,1196 extends beyond features 368,1179
ERROR: transcript MLRB44201 with bounds 84,1092 extends beyond features 84,1067
ERROR: transcript MLRB44471 with bounds 1,1017 extends beyond features 83,1017
ERROR: transcript MLRB44511 with bounds 1,852 extends beyond features 482,852
ERROR: transcript MLRB44531 with bounds 1,2938 extends beyond features 141,2938
ERROR: transcript MLRB44581 with bounds 1,963 extends beyond features 557,963
ERROR: transcript MLRB44601 with bounds 1,1010 extends beyond features 103,1010
ERROR: transcript MLRB44675 with bounds 550,1615 extends beyond features 550,1577
ERROR: transcript MLRB44811 with bounds 1,1003 extends beyond features 63,927
ERROR: transcript MLRB44861 with bounds 1,399 extends beyond features 211,399
ERROR: transcript MLRB44901 with bounds 1,6107 extends beyond features 5950,6107
ERROR: transcript MLRB45001 with bounds 1,9803 extends beyond features 1679,9803
ERROR: transcript MLRB451380 with bounds 184849,199849 extends beyond features 184849,198685
ERROR: transcript MLRB45221 with bounds 1,1305 extends beyond features 124,1305
ERROR: transcript MLRB45311 with bounds 1,12805 extends beyond features 673,12805
ERROR: transcript MLRB45491 with bounds 1,1087 extends beyond features 348,1087
ERROR: transcript MLRB45521 with bounds 1,19428 extends beyond features 675,19428
ERROR: transcript MLRB45561 with bounds 1,6714 extends beyond features 343,6714
ERROR: transcript MLRB45741 with bounds 1,1129 extends beyond features 846,1129
ERROR: transcript MLRB45781 with bounds 1,1315 extends beyond features 1145,1315
ERROR: transcript MLRB45821 with bounds 35,2260 extends beyond features 35,2234
ERROR: transcript MLRB45832 with bounds 1,15112 extends beyond features 1490,15112
ERROR: transcript MLRB45991 with bounds 1,2397 extends beyond features 716,2378
ERROR: transcript MLRB46011 with bounds 1,783 extends beyond features 16,783
ERROR: transcript MLRB46171 with bounds 751,4635 extends beyond features 751,3670
ERROR: transcript MLRB46181 with bounds 1,3468 extends beyond features 145,3468
ERROR: transcript MLRB46252 with bounds 706,1048 extends beyond features 706,860
ERROR: transcript MLRB46311 with bounds 1,910 extends beyond features 125,910
ERROR: transcript MLRB46313 with bounds 99,1839 extends beyond features 99,793
ERROR: transcript MLRB463913 with bounds 41208,93074 extends beyond features 41208,92608
ERROR: transcript MLRB46531 with bounds 15,1069 extends beyond features 15,1012
ERROR: transcript MLRB46793 with bounds 621,1505 extends beyond features 621,1419
ERROR: transcript MLRB47131 with bounds 1,958 extends beyond features 451,958
ERROR: transcript MLRB47191 with bounds 1,1328 extends beyond features 216,1328
ERROR: transcript MLRB47221 with bounds 1,1424 extends beyond features 96,1424
ERROR: transcript MLRB47561 with bounds 1,1781 extends beyond features 571,1781
ERROR: transcript MLRB47741 with bounds 1,35243 extends beyond features 191,35243
ERROR: transcript MLRB47811 with bounds 1,1200 extends beyond features 67,1200
ERROR: transcript MLRB47831 with bounds 1108,1251 extends beyond features 1108,1241
ERROR: transcript MLRB47892 with bounds 206,1102 extends beyond features 206,928
ERROR: transcript MLRB47981 with bounds 1,2465 extends beyond features 15,861
ERROR: transcript MLRB47992 with bounds 966,1992 extends beyond features 966,1124
ERROR: transcript MLRB48112 with bounds 848,1424 extends beyond features 848,1396
ERROR: transcript MLRB48291 with bounds 1,6519 extends beyond features 101,6519
ERROR: transcript MLRB48371 with bounds 1,594 extends beyond features 123,594
ERROR: transcript MLRB48592 with bounds 8,1477 extends beyond features 8,1363
ERROR: transcript MLRB48643 with bounds 2567,13383 extends beyond features 2567,13312
ERROR: transcript MLRB48702 with bounds 188,1040 extends beyond features 188,934
ERROR: transcript MLRB48901 with bounds 1,4607 extends beyond features 2206,4607
ERROR: transcript MLRB48971 with bounds 1,1983 extends beyond features 1021,1957
ERROR: transcript MLRB49201 with bounds 1,2104 extends beyond features 790,2104
ERROR: transcript MLRB49236 with bounds 1022,2815 extends beyond features 1022,2684
ERROR: transcript MLRB49261 with bounds 1,1832 extends beyond features 1493,1832
ERROR: transcript MLRB49324 with bounds 558,4783 extends beyond features 558,4385
ERROR: transcript MLRB49401 with bounds 994,2388 extends beyond features 994,1093
ERROR: transcript MLRB49501 with bounds 1,1594 extends beyond features 1507,1594
ERROR: transcript MLRB49541 with bounds 201,3381 extends beyond features 201,3233
ERROR: transcript MLRB49691 with bounds 1216,2939 extends beyond features 1216,1686
ERROR: transcript MLRB49851 with bounds 1,1787 extends beyond features 51,1787
ERROR: transcript MLRB50101 with bounds 1,585 extends beyond features 67,585
ERROR: transcript MLRB50181 with bounds 1,3380 extends beyond features 987,3380
ERROR: transcript MLRB50411 with bounds 1120,3400 extends beyond features 1120,1848
ERROR: transcript MLRB50471 with bounds 1,975 extends beyond features 198,975
ERROR: transcript MLRB505123 with bounds 48593,65563 extends beyond features 48593,65266
ERROR: transcript MLRB50791 with bounds 1,1255 extends beyond features 259,1255
ERROR: transcript MLRB50891 with bounds 756,1230 extends beyond features 756,896
ERROR: 613 features extend past subfeature boundaries, continuing anyway
# 613 errors detected in /home/wrf/genomes/mnemiopsis_leidyi/MLRB2.2.gff
# Reading GFF file /home/wrf/genomes/mnemiopsis_leidyi/trn15-30hpf.gff3
ERROR: identical bounds of subfeature exon within ML0081|comp7290_c0_seq2+, 157551 157581 , check GFF
ERROR: identical bounds of subfeature CDS within ML0081|comp7290_c0_seq2+, 157551 157581 , check GFF
ERROR: identical bounds of subfeature exon within ML0081|comp7290_c0_seq2+, 175805 175815 , check GFF
ERROR: identical bounds of subfeature CDS within ML0081|comp7290_c0_seq2+, 175805 175815 , check GFF
ERROR: identical bounds of subfeature exon within ML0081|comp7290_c0_seq2+, 176663 176674 , check GFF
ERROR: identical bounds of subfeature CDS within ML0081|comp7290_c0_seq2+, 176663 176674 , check GFF
ERROR: identical bounds of subfeature exon within ML0081|comp7290_c0_seq2+, 177872 178372 , check GFF
ERROR: identical bounds of subfeature CDS within ML0081|comp7290_c0_seq2+, 177872 178372 , check GFF
ERROR: identical bounds of subfeature exon within ML0081|comp7290_c0_seq2+, 178537 178776 , check GFF
ERROR: identical bounds of subfeature CDS within ML0081|comp7290_c0_seq2+, 178537 178776 , check GFF
ERROR: identical bounds of subfeature exon within ML0081|comp7290_c0_seq2+, 183387 183583 , check GFF
ERROR: identical bounds of subfeature CDS within ML0081|comp7290_c0_seq2+, 183387 183583 , check GFF
ERROR: already using transcript ML0156|comp2529176_c0_seq1+
ERROR: already using transcript ML0508|comp4033_c0_seq1-
ERROR: identical bounds of subfeature exon within ML0513|comp19995_c0_seq2-, 144217 144595 , check GFF
ERROR: identical bounds of subfeature CDS within ML0513|comp19995_c0_seq2-, 144217 144595 , check GFF
ERROR: identical bounds of subfeature exon within ML0513|comp19995_c0_seq2-, 13161 13291 , check GFF
ERROR: identical bounds of subfeature CDS within ML0513|comp19995_c0_seq2-, 13161 13291 , check GFF
ERROR: identical bounds of subfeature exon within ML0513|comp19995_c0_seq2-, 12469 12739 , check GFF
ERROR: identical bounds of subfeature CDS within ML0513|comp19995_c0_seq2-, 12469 12739 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 59732 61843 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 59732 61843 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 62308 62452 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 62308 62452 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 62680 62858 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 62680 62858 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 63515 63633 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 63515 63633 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 63922 63986 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 63922 63986 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 64418 64531 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 64418 64531 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 65055 65175 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 65055 65175 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 68733 68743 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 68733 68743 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 75166 75178 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 75166 75178 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 77392 77402 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 77392 77402 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 82331 82351 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 82331 82351 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 85113 85125 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 85113 85125 , check GFF
ERROR: identical bounds of subfeature exon within ML0590|comp13747_c0_seq1+, 85593 85621 , check GFF
ERROR: identical bounds of subfeature CDS within ML0590|comp13747_c0_seq1+, 85593 85621 , check GFF
ERROR: identical bounds of subfeature exon within ML0901|comp16774_c0_seq2-, 235502 236324 , check GFF
ERROR: identical bounds of subfeature CDS within ML0901|comp16774_c0_seq2-, 235502 236324 , check GFF
ERROR: identical bounds of subfeature exon within ML0901|comp16774_c0_seq2-, 234569 234763 , check GFF
ERROR: identical bounds of subfeature CDS within ML0901|comp16774_c0_seq2-, 234569 234763 , check GFF
ERROR: identical bounds of subfeature exon within ML0901|comp16774_c0_seq2-, 230520 230536 , check GFF
ERROR: identical bounds of subfeature CDS within ML0901|comp16774_c0_seq2-, 230520 230536 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 176233 176243 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 176233 176243 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 176840 176851 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 176840 176851 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 178901 178922 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 178901 178922 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 185784 185798 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 185784 185798 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 187834 187854 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 187834 187854 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 192249 192265 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 192249 192265 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 196143 196164 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 196143 196164 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 199805 199833 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 199805 199833 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 203307 203768 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 203307 203768 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 204961 205027 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 204961 205027 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 205529 205633 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 205529 205633 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 205729 205858 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 205729 205858 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 206702 206862 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 206702 206862 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 213529 213673 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 213529 213673 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 215030 215162 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 215030 215162 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 215305 215523 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 215305 215523 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 215673 215826 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 215673 215826 , check GFF
ERROR: identical bounds of subfeature exon within ML2100|comp15769_c0_seq1+, 216434 216563 , check GFF
ERROR: identical bounds of subfeature CDS within ML2100|comp15769_c0_seq1+, 216434 216563 , check GFF
ERROR: identical bounds of subfeature exon within ML2176|comp11867_c0_seq1+, 13767 13924 , check GFF
ERROR: identical bounds of subfeature CDS within ML2176|comp11867_c0_seq1+, 13767 13924 , check GFF
ERROR: identical bounds of subfeature exon within ML2176|comp11867_c0_seq1+, 14122 14252 , check GFF
ERROR: identical bounds of subfeature CDS within ML2176|comp11867_c0_seq1+, 14122 14252 , check GFF
# File contained:
#exon	114249
#CDS	114249
#gene	35353
#mRNA	35353
#contig	1766
# Collected exons for 35344 transcripts
WARNING: encountered 9 duplicated entries, ignorning them and continuing anyway
# 99 errors detected in /home/wrf/genomes/mnemiopsis_leidyi/trn15-30hpf.gff3
# Reading GFF file /home/wrf/genomes/mnemiopsis_leidyi/MlScaffold09_stringtie.gff3
# File contained:
#exon	119308
#CDS	119308
#gene	30052
#mRNA	30052
#contig	2697
# Collected exons for 30052 transcripts
# 0 errors detected in /home/wrf/genomes/mnemiopsis_leidyi/MlScaffold09_stringtie.gff3
# Reading GFF file /home/wrf/genomes/mnemiopsis_leidyi/GFAT01.1.renamed.pinfish.gtf
ERROR: already using transcript Mlei_GFAT01_c355364_g2_i2
ERROR: already using transcript Mlei_GFAT01_c369760_g2_i4
ERROR: already using transcript Mlei_GFAT01_c361417_g10_i2
ERROR: already using transcript Mlei_GFAT01_c368859_g1_i2
ERROR: already using transcript Mlei_GFAT01_c368859_g1_i1
ERROR: already using transcript Mlei_GFAT01_c326420_g2_i2
ERROR: already using transcript Mlei_GFAT01_c369546_g2_i1
ERROR: already using transcript Mlei_GFAT01_c370497_g1_i1
ERROR: already using transcript Mlei_GFAT01_c373536_g1_i3
ERROR: already using transcript Mlei_GFAT01_c373536_g1_i5
ERROR: already using transcript Mlei_GFAT01_c373536_g1_i6
ERROR: already using transcript Mlei_GFAT01_c373767_g1_i1
ERROR: already using transcript Mlei_GFAT01_c373767_g1_i4
ERROR: already using transcript Mlei_GFAT01_c373767_g1_i6
ERROR: already using transcript Mlei_GFAT01_c373767_g1_i9
ERROR: already using transcript Mlei_GFAT01_c373767_g1_i2
ERROR: already using transcript Mlei_GFAT01_c368349_g4_i2
ERROR: already using transcript Mlei_GFAT01_c372242_g1_i3
ERROR: already using transcript Mlei_GFAT01_c369125_g4_i1
ERROR: already using transcript Mlei_GFAT01_c359602_g2_i10
ERROR: already using transcript Mlei_GFAT01_c358708_g1_i2
ERROR: already using transcript Mlei_GFAT01_c365221_g3_i1
ERROR: already using transcript Mlei_GFAT01_c357985_g2_i2
ERROR: already using transcript Mlei_GFAT01_c374125_g2_i2
ERROR: already using transcript Mlei_GFAT01_c374125_g2_i1
ERROR: already using transcript Mlei_GFAT01_c365423_g3_i3
ERROR: already using transcript Mlei_GFAT01_c365423_g3_i2
ERROR: already using transcript Mlei_GFAT01_c363943_g6_i3
ERROR: already using transcript Mlei_GFAT01_c363943_g6_i2
ERROR: 30 errors printed, will not print further
# File contained:
#exon	347586
#mRNA	131139
# Collected exons for 122218 transcripts
WARNING: encountered 7190 duplicated entries, ignorning them and continuing anyway
ERROR: gene Mlei_GFAT01_c296844_g2_i1 has transcripts on different scaffolds ML0001;ML0309
ERROR: gene Mlei_GFAT01_c374426_g1_i3 has transcripts on different scaffolds ML0001;ML0966;ML2414
ERROR: gene Mlei_GFAT01_c374426_g1_i2 has transcripts on different scaffolds ML0001;ML0323;ML0966
ERROR: gene Mlei_GFAT01_c374426_g1_i1 has transcripts on different scaffolds ML0001;ML0966;ML2414
ERROR: gene Mlei_GFAT01_c374426_g1_i4 has transcripts on different scaffolds ML0001;ML0966;ML2414
ERROR: gene Mlei_GFAT01_c367614_g3_i2 has transcripts on different scaffolds ML0001;ML2338
ERROR: gene Mlei_GFAT01_c365200_g3_i1 has transcripts on different scaffolds ML0001;ML0776
ERROR: gene Mlei_GFAT01_c338029_g3_i1 has transcripts on different scaffolds ML0001;ML1489
ERROR: gene Mlei_GFAT01_c367926_g1_i2 has transcripts on different scaffolds ML0001;ML0615
ERROR: gene Mlei_GFAT01_c336452_g2_i1 has transcripts on different scaffolds ML0001;ML0569
WARNING: encountered 6710 genes split onto multiple scaffolds, continuing anyway
# 15631 errors detected in /home/wrf/genomes/mnemiopsis_leidyi/GFAT01.1.renamed.pinfish.gtf
# Reading annotation table from /home/wrf/git/genome-reannotations/jbrowse-tracks/mnemiopsis/Mlei_2.2_annotation_sheet.tab
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c358047_g2_i2 in ML0001.g4.i1
FORWARD flag found: forcing forward strand + for ML0001.22.1 in ML0001.g20.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c371442_g1_i1 in ML0003.g9.i1
ERROR: same exon (52387, 53056) on ML0010 used for multiple genes: ML00109a ML00108a
ERROR: same exon (43166, 44483) on ML0028 used for multiple genes: ML00288a ML00287a
ERROR: same exon (46173, 47154) on ML0028 used for multiple genes: ML002810a ML00289a
REVERSE flag found: forcing reverse strand - for ML0030.572.1 in ML0030.g5.i1
REVERSE flag found: forcing reverse strand - for ML0030.579.1 in ML0030.g9.i1
REVERSE flag found: forcing reverse strand - for ML0032.642.1 in ML0032.g23.i1
REVERSE flag found: forcing reverse strand - for ML0032.646.1 in ML0032.g29.i1
FORWARD flag found: forcing forward strand + for ML0032.668.1 in ML0032.g39.i1
REVERSE flag found: forcing reverse strand - for ML0032.680.1 in ML0032.g43.i1
REVERSE flag found: forcing reverse strand - for ML0032.682.1 in ML0032.g48.i1
FORWARD flag found: forcing forward strand + for ML0032.683.1 in ML0032.g49.i1
REVERSE flag found: forcing reverse strand - for ML0032.719.1 in ML0032.g69.i1
FORWARD flag found: forcing forward strand + for ML0035.770.1 in ML0035.g9.i1
ERROR: same exon (50171, 50739) on ML0036 used for multiple genes: ML00364a ML00363a
REVERSE flag found: forcing reverse strand - for ML0044.881.1 in ML0044.g12.i1
REVERSE flag found: forcing reverse strand - for ML0044.899.1 in ML0044.g25.i1
REVERSE flag found: forcing reverse strand - for ML0044.905.1 in ML0044.g29.i1
FORWARD flag found: forcing forward strand + for ML0046.967.1 in ML0046.g3.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c334211_g1_i2 in ML0046.g10.i1
ERROR: same exon (373929, 374641) on ML0049 used for multiple genes: ML004934a ML004933a
REVERSE flag found: forcing reverse strand - for ML0051.1127.1 in ML0051.g11.i1
FORWARD flag found: forcing forward strand + for ML0051.1134.1 in ML0051.g19.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c320144_g1_i1 in ML0053.g44.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c368262_g1_i1 in ML0053.g59.i1
ERROR: same exon (167382, 169216) on ML0061 used for multiple genes: ML006123a ML006121a
ERROR: same exon (4929, 6152) on ML0071 used for multiple genes: ML00712a ML00711a
FORWARD flag found: forcing forward strand + for ML0073.1548.1 in ML0073.g4.i1
REVERSE flag found: forcing reverse strand - for ML0083.1788.1 in ML0083.g26.i1
ERROR: same exon (68864, 69547) on ML0091 used for multiple genes: ML009114a ML009113a
FORWARD flag found: forcing forward strand + for ML0091.1925.1 in ML0091.g33.i1
FORWARD flag found: forcing forward strand + for ML0091.1927.1 in ML0091.g34.i1
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c371079_g1_i1 in ML0096.g12.i1
ERROR: same exon (120588, 123069) on ML0109 used for multiple genes: ML010910a ML01099a
ERROR: same exon (209548, 209825) on ML0120 used for multiple genes: ML012037b ML012022a
ERROR: same exon (46980, 50609) on ML0125 used for multiple genes: ML012510a ML01257a
ERROR: same exon (94807, 97053) on ML0125 used for multiple genes: ML012520a ML012518a
ERROR: same exon (89473, 90187) on ML0144 used for multiple genes: ML01446a ML01445a
REVERSE flag found: forcing reverse strand - for ML0148.2804.1 in ML0148.g6.i1
ERROR: same exon (17551, 18061) on ML0150 used for multiple genes: ML01504a ML01503a
ERROR: same exon (12720, 13560) on ML0153 used for multiple genes: ML01535a ML01534a
FORWARD flag found: forcing forward strand + for ML0157.2978.1 in ML0157.g9.i1
FORWARD flag found: forcing forward strand + for ML0157.2988.1 in ML0157.g13.i1
FORWARD flag found: forcing forward strand + for ML0157.2990.1 in ML0157.g14.i1
REVERSE flag found: forcing reverse strand - for ML0163.3086.1 in ML0163.g2.i1
REVERSE flag found: forcing reverse strand - for ML0163.3088.1 in ML0163.g3.i1
REVERSE flag found: forcing reverse strand - for ML0163.3101.1 in ML0163.g15.i1
FORWARD flag found: forcing forward strand + for ML0163.3106.1 in ML0163.g16.i1
ERROR: same exon (25711, 26689) on ML0172 used for multiple genes: ML01724a ML01723a
ERROR: same exon (300876, 302838) on ML0174 used for multiple genes: ML017428a ML017427a
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c354901_g1_i1 in ML0180.g35.i1
ERROR: same exon (17573, 18256) on ML0182 used for multiple genes: ML01825a ML01824a
REVERSE flag found: forcing reverse strand - for ML0200.3755.1 in ML0200.g6.i1
REVERSE flag found: forcing reverse strand - for ML0200.3756.1 in ML0200.g7.i1
REVERSE flag found: forcing reverse strand - for ML0200.3775.1 in ML0200.g15.i1
FORWARD flag found: forcing forward strand + for ML0200.3790.1 in ML0200.g27.i1
FORWARD flag found: forcing forward strand + for ML0200.3796.1 in ML0200.g32.i1
FORWARD flag found: forcing forward strand + for ML0200.3802.1 in ML0200.g34.i1
FORWARD flag found: forcing forward strand + for ML0200.3811.1 in ML0200.g39.i1
FORWARD flag found: forcing forward strand + for ML0200.3817.1 in ML0200.g43.i1
REVERSE flag found: forcing reverse strand - for ML0200.3840.1 in ML0200.g58.i1
FORWARD flag found: forcing forward strand + for ML0200.3849.1 in ML0200.g65.i1
FORWARD flag found: forcing forward strand + for ML0200.3850.1 in ML0200.g66.i1
FORWARD flag found: forcing forward strand + for ML0200.3851.1 in ML0200.g67.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c328635_g1_i1 in ML0211.g26.i1
FORWARD flag found: forcing forward strand + for ML0211.4011.1 in ML0211.g43.i1
ERROR: same exon (80587, 81676) on ML0224 used for multiple genes: ML02249a ML02248a
ERROR: same exon (141345, 143651) on ML0224 used for multiple genes: ML022416a ML022415a
ERROR: same exon (52779, 53550) on ML0231 used for multiple genes: ML02318a ML02317a
FORWARD flag found: forcing forward strand + for ML0266.4607.1 in ML0266.g2.i1
FORWARD flag found: forcing forward strand + for ML0273.4641.1 in ML0273.g4.i1
FORWARD flag found: forcing forward strand + for ML0273.4642.1 in ML0273.g5.i1
FORWARD flag found: forcing forward strand + for ML0273.4643.1 in ML0273.g6.i1
FORWARD flag found: forcing forward strand + for ML0273.4644.1 in ML0273.g7.i1
FORWARD flag found: forcing forward strand + for ML0273.4645.1 in ML0273.g8.i1
REVERSE flag found: forcing reverse strand - for ML0280.4733.1 in ML0280.g4.i1
REVERSE flag found: forcing reverse strand - for ML0280.4740.1 in ML0280.g7.i1
ERROR: same exon (45077, 45784) on ML0302 used for multiple genes: ML03028a ML03027a
REVERSE flag found: forcing reverse strand - for ML0302.4953.1 in ML0302.g21.i1
ERROR: same exon (177609, 178789) on ML0302 used for multiple genes: ML030222a ML030220a
ERROR: same exon (179210, 181213) on ML0302 used for multiple genes: ML030223a ML030222a
ERROR: same exon (43569, 44925) on ML0303 used for multiple genes: ML03039a ML03034a
FORWARD flag found: forcing forward strand + for ML0312.5146.1 in ML0312.g4.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c373238_g1_i1 in ML0325.g9.i1
REVERSE flag found: forcing reverse strand - for ML0329.5420.1 in ML0329.g11.i1
REVERSE flag found: forcing reverse strand - for ML0329.5436.1 in ML0329.g20.i1
ERROR: same exon (17030, 19323) on ML0331 used for multiple genes: ML03315a ML03313a
FORWARD flag found: forcing forward strand + for ML0332.5472.1 in ML0332.g15.i1
FORWARD flag found: forcing forward strand + for ML0332.5473.1 in ML0332.g16.i1
REVERSE flag found: forcing reverse strand - for ML0332.5480.1 in ML0332.g25.i1
REVERSE flag found: forcing reverse strand - for ML0332.5494.1 in ML0332.g36.i1
REVERSE flag found: forcing reverse strand - for ML0332.5503.1 in ML0332.g42.i1
ERROR: same exon (193063, 195334) on ML0336 used for multiple genes: ML033624a ML033623a
ERROR: same exon (41142, 42495) on ML0339 used for multiple genes: ML03394a ML03393a
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c354408_g2_i2 in ML0346.g11.i1
REVERSE flag found: forcing reverse strand - for ML0346.5728.1 in ML0346.g26.i1
REVERSE flag found: forcing reverse strand - for ML0346.5747.1 in ML0346.g37.i1
REVERSE flag found: forcing reverse strand - for ML0346.5782.1 in ML0346.g61.i1
REVERSE flag found: forcing reverse strand - for ML0352.5854.1 in ML0352.g3.i1
ERROR: same exon (18785, 21078) on ML0359 used for multiple genes: ML03592a ML03591a
FORWARD flag found: forcing forward strand + for ML0361.5953.1 in ML0361.g1.i1
FORWARD flag found: forcing forward strand + for ML0361.5959.1 in ML0361.g5.i1
ERROR: same exon (111636, 111740) on ML0365 used for multiple genes: ML036515a ML0365.5998.1 ML0365.5998.1
ERROR: same exon (111636, 111740) on ML0365 used for multiple genes: ML036515a ML0365.5998.1 ML0365.5998.1
ERROR: same exon (84674, 86578) on ML0378 used for multiple genes: ML03786a ML03785a
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c360857_g1_i2 in ML0380.g35.i1
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c361933_g1_i1 in ML0380.g39.i1
FORWARD flag found: forcing forward strand + for ML0387.6166.1 in ML0387.g3.i1
FORWARD flag found: forcing forward strand + for ML0397.6249.1 in ML0397.g2.i1
FORWARD flag found: forcing forward strand + for ML0397.6250.1 in ML0397.g3.i1
REVERSE flag found: forcing reverse strand - for ML0397.6260.1 in ML0397.g12.i1
REVERSE flag found: forcing reverse strand - for ML0397.6279.1 in ML0397.g23.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c366750_g1_i1 in ML0398.g9.i1
REVERSE flag found: forcing reverse strand - for ML040024a_long_exon_spatacsin in ML0400.g22.i1
ERROR: same exon (184641, 186066) on ML0405 used for multiple genes: ML040515a ML040514a
ERROR: same exon (40290, 41283) on ML0407 used for multiple genes: ML04076a ML04075a
ERROR: same exon (30513, 31906) on ML0414 used for multiple genes: ML04144a ML04142a
ERROR: same exon (39290, 40839) on ML0421 used for multiple genes: ML04215a ML04214a
ERROR: same exon (108485, 109942) on ML0421 used for multiple genes: ML042115a ML042114a
REVERSE flag found: forcing reverse strand - for ML0427.6617.1 in ML0427.g17.i1
REVERSE flag found: forcing reverse strand - for ML0433.6681.1 in ML0433.g6.i1
ERROR: same exon (119361, 122383) on ML0435 used for multiple genes: ML043512a ML043510a
REVERSE flag found: forcing reverse strand - for ML0438.6740.1 in ML0438.g1.i1
ERROR: same exon (48844, 49361) on ML0441 used for multiple genes: ML04419a ML04418a
FORWARD flag found: forcing forward strand + for ML0441.6798.1 in ML0441.g33.i1
ERROR: same exon (21673, 25261) on ML0446 used for multiple genes: ML04466a ML04464a
ERROR: same exon (38791, 40645) on ML0449 used for multiple genes: ML04497a ML04496a
REVERSE flag found: forcing reverse strand - for ML0452.6866.1 in ML0452.g2.i1
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c371527_g3_i2 in ML0463.g22.i1
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c363924_g1_i1 in ML0476.g6.i1
REVERSE flag found: forcing reverse strand - for ML0479.7281.1 in ML0479.g40.i1
REVERSE flag found: forcing reverse strand - for ML0482.7331.1 in ML0482.g3.i1
REVERSE flag found: forcing reverse strand - for ML0490.7396.1 in ML0490.g10.i1
ERROR: same exon (176814, 177523) on ML0496 used for multiple genes: ML049620a ML049619a
REVERSE flag found: forcing reverse strand - for ML0496.7495.1 in ML0496.g29.i1
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c365886_g6_i1 in ML0502.g2.i1
ERROR: same exon (4981, 6468) on ML0504 used for multiple genes: ML05042a ML05041a
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c370691_g2_i1 in ML0508.g12.i1
FORWARD flag found: forcing forward strand + for ML0508.7648.1 in ML0508.g13.i1
REVERSE flag found: forcing reverse strand - for ML0508.7685.1 in ML0508.g35.i1
REVERSE flag found: forcing reverse strand - for ML0508.7686.1 in ML0508.g36.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c354853_g2_i1 in ML0509.g14.i1
ERROR: same exon (280603, 281412) on ML0513 used for multiple genes: ML051336a ML051335a
FORWARD flag found: forcing forward strand + for ML0517.7810.1 in ML0517.g3.i1
ERROR: same exon (124744, 125969) on ML0517 used for multiple genes: ML051721a ML051720a
FORWARD flag found: forcing forward strand + for ML0523.7891.1 in ML0523.g7.i1
REVERSE flag found: forcing reverse strand - for ML0536.8016.1 in ML0536.g11.i1
REVERSE flag found: forcing reverse strand - for ML0536.8018.1 in ML0536.g15.i1
ERROR: same exon (33623, 34585) on ML0549 used for multiple genes: ML05496a ML05495a
FORWARD flag found: forcing forward strand + for ML0569.8354.1 in ML0569.g43.i1
REVERSE flag found: forcing reverse strand - for ML0590.8559.1 in ML0590.g12.i1
ERROR: same exon (103763, 104900) on ML0598 used for multiple genes: ML059814a ML059813a
ERROR: same exon (78442, 81086) on ML0617 used for multiple genes: ML061714a ML061713a
REVERSE flag found: forcing reverse strand - for ML0622.8805.1 in ML0622.g12.i1
ERROR: same exon (281270, 283409) on ML0633 used for multiple genes: ML063336a ML063335a
ERROR: same exon (86895, 87881) on ML0639 used for multiple genes: ML063913a ML063912a
ERROR: same exon (174776, 175585) on ML0653 used for multiple genes: ML065324a ML065323a
ERROR: same exon (176130, 177168) on ML0653 used for multiple genes: ML065325a ML065324a
ERROR: same exon (44805, 45313) on ML0657 used for multiple genes: ML065712a ML065711a
ERROR: same exon (236121, 236590) on ML0657 used for multiple genes: ML065738a ML065737a
FORWARD flag found: forcing forward strand + for ML0665.9263.1 in ML0665.g1.i1
FORWARD flag found: forcing forward strand + for ML0665.9272.1 in ML0665.g9.i1
FORWARD flag found: forcing forward strand + for ML0665.9274.1 in ML0665.g10.i1
FORWARD flag found: forcing forward strand + for ML0665.9275.1 in ML0665.g12.i1
ERROR: same exon (197224, 199773) on ML0671 used for multiple genes: ML067122a ML067120a
FORWARD flag found: forcing forward strand + for ML0681|comp6961_c0_seq1 in ML0681.g4.i1
FORWARD flag found: forcing forward strand + for ML0681.9420.1 in ML0681.g6.i1
REVERSE flag found: forcing reverse strand - for ML0681.9447.1 in ML0681.g25.i1
REVERSE flag found: forcing reverse strand - for ML0683.9507.1 in ML0683.g22.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c367464_g3_i2 in ML0683.g23.i1
ERROR: same exon (4486, 4815) on ML0690 used for multiple genes: ML06902a ML06901a
ERROR: same exon (108427, 110592) on ML0691 used for multiple genes: ML069116a ML069114a
ERROR: same exon (200359, 201684) on ML0697 used for multiple genes: ML069719a ML069718a
FORWARD flag found: forcing forward strand + for ML0702.9670.1 in ML0702.g17.i1
FORWARD flag found: forcing forward strand + for ML0702.9695.1 in ML0702.g40.i1
ERROR: same exon (15714, 17331) on ML0724 used for multiple genes: ML07243a ML07242a
ERROR: same exon (76087, 77277) on ML0739 used for multiple genes: ML07398a ML07397a
ERROR: same exon (228096, 228295) on ML0742 used for multiple genes: ML074222a ML074221a
REVERSE flag found: forcing reverse strand - for ML0742.10361.1 in ML0742.g31.i1
FORWARD flag found: forcing forward strand + for ML0742.10364.1 in ML0742.g34.i1
FORWARD flag found: forcing forward strand + for ML0742.10365.1 in ML0742.g35.i1
FORWARD flag found: forcing forward strand + for ML0742.10385.1 in ML0742.g53.i1
FORWARD flag found: forcing forward strand + for ML0742.10387.1 in ML0742.g54.i1
ERROR: same exon (14909, 16074) on ML0748 used for multiple genes: ML07484a ML07483a
FORWARD flag found: forcing forward strand + for ML0755.10535.1 in ML0755.g3.i1
FORWARD flag found: forcing forward strand + for ML0755.10537.1 in ML0755.g4.i1
REVERSE flag found: forcing reverse strand - for ML076026a_comp55979_comp395959 in ML0760.g27.i1
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c358802_g1_i1 in ML0763.g6.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c355788_g1_i1 in ML0763.g7.i1
ERROR: same exon (97125, 97932) on ML0763 used for multiple genes: ML076313a ML076312a
ERROR: same exon (105623, 106520) on ML0769 used for multiple genes: ML076914a ML076913a
FORWARD flag found: forcing forward strand + for ML0780.10812.1 in ML0780.g3.i1
FORWARD flag found: forcing forward strand + for ML0797.10982.1 in ML0797.g14.i1
REVERSE flag found: forcing reverse strand - for ML0797.10983.1 in ML0797.g15.i1
ERROR: same exon (28918, 29835) on ML0806 used for multiple genes: ML08064a ML08063a
ERROR: same exon (196261, 197727) on ML0820 used for multiple genes: ML082014a ML082013a
ERROR: same exon (15421, 16280) on ML0823 used for multiple genes: ML08234a ML08233a
REVERSE flag found: forcing reverse strand - for ML0842.11469.1 in ML0842.g12.i1
ERROR: same exon (102304, 103338) on ML0848 used for multiple genes: ML084811a ML084810a
ERROR: same exon (180911, 181964) on ML0850 used for multiple genes: ML085025a ML085024a
REVERSE flag found: forcing reverse strand - for ML0855.11637.1 in ML0855.g3.i1
FORWARD flag found: forcing forward strand + for ML0855.11640.1 in ML0855.g4.i1
REVERSE flag found: forcing reverse strand - for ML0855.11638.1 in ML0855.g5.i1
REVERSE flag found: forcing reverse strand - for ML0857.11653.1 in ML0857.g11.i1
REVERSE flag found: forcing reverse strand - for ML0857.11658.1 in ML0857.g12.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c370581_g1_i1 in ML0857.g25.i1
ERROR: same exon (73726, 75996) on ML0864 used for multiple genes: ML086411a ML08649a
ERROR: same exon (58667, 59139) on ML0882 used for multiple genes: ML08829a ML08828a
ERROR: same exon (21531, 22651) on ML0897 used for multiple genes: ML08972a ML08971a
ERROR: same exon (622, 1523) on ML0900 used for multiple genes: ML09002a ML09001a
ERROR: same exon (127710, 128596) on ML0926 used for multiple genes: ML092617a ML092616a
ERROR: same exon (16263, 17250) on ML0943 used for multiple genes: ML09433a ML09432a
REVERSE flag found: forcing reverse strand - for ML0943.12531.1 in ML0943.g15.i1
ERROR: same exon (22439, 23825) on ML0966 used for multiple genes: ML09664a ML09663a
ERROR: same exon (24624, 25975) on ML0973 used for multiple genes: ML09734a ML09733a
ERROR: same exon (51197, 52488) on ML0992 used for multiple genes: ML09928a ML09927a
FORWARD flag found: forcing forward strand + for ML1022.13094.1 in ML1022.g18.i1
FORWARD flag found: forcing forward strand + for ML1022.13096.1 in ML1022.g20.i1
FORWARD flag found: forcing forward strand + for ML1022.13099.1 in ML1022.g24.i1
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c364201_g4_i2 in ML1022.g52.i1
REVERSE flag found: forcing reverse strand - for ML1022.13132.1 in ML1022.g57.i1
ERROR: same exon (195167, 197600) on ML1043 used for multiple genes: ML104328a ML104326a
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c371586_g5_i1 in ML1054.g13.i1
ERROR: same exon (235513, 236309) on ML1054 used for multiple genes: ML105426a ML105425a
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c373325_g1_i1 in ML1080.g4.i1
REVERSE flag found: forcing reverse strand - for ML10807a_end_part_fixed in ML1080.g9.i1
FORWARD flag found: forcing forward strand + for ML1080.13537.1 in ML1080.g11.i1
FORWARD flag found: forcing forward strand + for ML1080.13538.1 in ML1080.g12.i1
REVERSE flag found: forcing reverse strand - for ML1117.13689.1 in ML1117.g4.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c350781_g1_i1 in ML1117.g9.i1
REVERSE flag found: forcing reverse strand - for ML1117.13703.1 in ML1117.g16.i1
REVERSE flag found: forcing reverse strand - for ML1117.13704.1 in ML1117.g17.i1
ERROR: same exon (56447, 57766) on ML1146 used for multiple genes: ML11467a ML11466a
ERROR: same exon (58829, 61559) on ML1146 used for multiple genes: ML11469a ML11467a
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c347896_g1_i2 in ML1154.g1.i1
REVERSE flag found: forcing reverse strand - for ML1154.13888.1 in ML1154.g8.i1
ERROR: same exon (43665, 44787) on ML1154 used for multiple genes: ML11548a ML11547a
ERROR: same exon (7530, 8291) on ML1156 used for multiple genes: ML11563a ML11562a
ERROR: same exon (60893, 61533) on ML1182 used for multiple genes: ML11825a ML11824a
ERROR: same exon (11184, 13681) on ML1191 used for multiple genes: ML11914a ML11912a
ERROR: same exon (1674, 4571) on ML1203 used for multiple genes: ML12032a ML12031a
FORWARD flag found: forcing forward strand + for ML12365a_comp11100_c0_seq1 in ML1236.g5.i1
REVERSE flag found: forcing reverse strand - for ML1284.14664.1 in ML1284.g5.i1
REVERSE flag found: forcing reverse strand - for ML1284.14667.1 in ML1284.g6.i1
FORWARD flag found: forcing forward strand + for ML1284.14700.1 in ML1284.g38.i1
REVERSE flag found: forcing reverse strand - for ML1286.14717.1 in ML1286.g4.i1
ERROR: same exon (69601, 70690) on ML1293 used for multiple genes: ML129312a ML129311a
FORWARD flag found: forcing forward strand + for ML1293.14763.1 in ML1293.g15.i1
FORWARD flag found: forcing forward strand + for ML1305.14844.1 in ML1305.g5.i1
FORWARD flag found: forcing forward strand + for ML1311.14897.1 in ML1311.g5.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c357450_g1_i1 in ML1311.g35.i1
ERROR: same exon (41734, 43442) on ML1316 used for multiple genes: ML13166a ML13165a
ERROR: same exon (59251, 60001) on ML1360 used for multiple genes: ML136012a ML136011a
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c355932_g1_i1 in ML1360.g21.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c353400_g1_i1 in ML1360.g26.i1
ERROR: same exon (14728, 15202) on ML1397 used for multiple genes: ML13973a ML13972a
ERROR: same exon (113203, 114980) on ML1430 used for multiple genes: ML143020a ML143019a
ERROR: same exon (94568, 95890) on ML1439 used for multiple genes: ML143910a ML14399a
REVERSE flag found: forcing reverse strand - for ML1442.15827.1 in ML1442.g18.i1
ERROR: same exon (11219, 12440) on ML1465 used for multiple genes: ML14653a ML14652a
ERROR: same exon (60315, 63326) on ML1465 used for multiple genes: ML14658a ML14657a
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c336910_g1_i1 in ML1477.g11.i1
FORWARD flag found: forcing forward strand + for ML1477.16036.1 in ML1477.g13.i1
FORWARD flag found: forcing forward strand + for ML1489.16152.1 in ML1489.g19.i1
ERROR: same exon (369240, 370071) on ML1489 used for multiple genes: ML148947a ML148946a
FORWARD flag found: forcing forward strand + for ML1500.16324.1 in ML1500.g1.i1
ERROR: same exon (299583, 299729) on ML1541 used for multiple genes: Mlei_GFAT01_c365621_g2_i7 ML154127a ML154127a
ERROR: same exon (518342, 519757) on ML1541 used for multiple genes: ML154149a ML154148a
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c261493_g1_i1 in ML1545.g5.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c335718_g2_i1 in ML1545.g6.i1
FORWARD flag found: forcing forward strand + for ML1603.16770.1 in ML1603.g17.i1
FORWARD flag found: forcing forward strand + for ML1603.16773.1 in ML1603.g19.i1
REVERSE flag found: forcing reverse strand - for ML1613|comp15021_c0_seq1 in ML1613.g34.i1
FORWARD flag found: forcing forward strand + for ML1613.16843.1 in ML1613.g37.i1
REVERSE flag found: forcing reverse strand - for ML1613.16846.1 in ML1613.g39.i1
ERROR: same exon (101713, 102937) on ML1658 used for multiple genes: ML165816a ML165815a
FORWARD flag found: forcing forward strand + for ML1670.17028.1 in ML1670.g27.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c366454_g2_i2 in ML1670.g33.i1
FORWARD flag found: forcing forward strand + for ML1670.17044.1 in ML1670.g39.i1
ERROR: same exon (511136, 511286) on ML1670 used for multiple genes: ML167051a Mlei_GFAT01_c373979_g1_i1
ERROR: same exon (511136, 511286) on ML1670 used for multiple genes: ML167051a Mlei_GFAT01_c373979_g1_i1
ERROR: same exon (17696, 20719) on ML1692 used for multiple genes: ML16924a ML16922a
REVERSE flag found: forcing reverse strand - for ML1705.17161.1 in ML1705.g2.i1
ERROR: same exon (120953, 122198) on ML1737 used for multiple genes: ML173715a ML173714a
REVERSE flag found: forcing reverse strand - for ML1740.17260.1 in ML1740.g2.i1
ERROR: same exon (179116, 180032) on ML1747 used for multiple genes: ML174720a ML174719a
FORWARD flag found: forcing forward strand + for ML1747.17354.1 in ML1747.g60.i1
ERROR: same exon (37098, 37959) on ML1759 used for multiple genes: ML17598a ML17597a
ERROR: same exon (7279, 9025) on ML1810 used for multiple genes: ML18104a ML18103a
ERROR: same exon (9547, 10321) on ML1810 used for multiple genes: ML18105a ML18104a
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c345167_g1_i1 in ML1912.g2.i1
ERROR: same exon (195355, 198034) on ML1932 used for multiple genes: ML193214a ML193213a
FORWARD flag found: forcing forward strand + for ML1991.18320.1 in ML1991.g2.i1
FORWARD flag found: forcing forward strand + for ML1991.18327.1 in ML1991.g10.i1
FORWARD flag found: forcing forward strand + for ML2002.18403.1 in ML2002.g5.i1
REVERSE flag found: forcing reverse strand - for ML2002.18413.1 in ML2002.g13.i1
REVERSE flag found: forcing reverse strand - for ML2002.18415.1 in ML2002.g14.i1
FORWARD flag found: forcing forward strand + for ML20264a_GFAT01_c357077_g1_i2_c340685_g1_i1 in ML2026.g5.i1
ERROR: same exon (28962, 30056) on ML2056 used for multiple genes: ML20564a ML20563a
ERROR: same exon (6826, 8333) on ML2068 used for multiple genes: ML20683a ML20682a
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c341435_g1_i1 in ML2100.g4.i1
FORWARD flag found: forcing forward strand + for ML2100.18990.1 in ML2100.g14.i1
REVERSE flag found: forcing reverse strand - for ML2100.19011.1 in ML2100.g32.i1
ERROR: same exon (23511, 28418) on ML2140 used for multiple genes: ML21408a ML21405a
REVERSE flag found: forcing reverse strand - for ML2154|comp128032_c0_seq1 in ML2154.g19.i1
FORWARD flag found: forcing forward strand + for ML2163.19226.1 in ML2163.g14.i1
REVERSE flag found: forcing reverse strand - for ML2163.19254.1 in ML2163.g44.i1
REVERSE flag found: forcing reverse strand - for ML2163.19264.1 in ML2163.g51.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c370202_g2_i5 in ML2163.g57.i1
REVERSE flag found: forcing reverse strand - for ML2163.19280.1 in ML2163.g58.i1
ERROR: same exon (13228, 13668) on ML2169 used for multiple genes: ML21694a ML21693a
ERROR: same exon (7266, 7962) on ML2216 used for multiple genes: ML22163a ML22162a
ERROR: same exon (20042, 23808) on ML2216 used for multiple genes: ML22165a ML22163a
REVERSE flag found: forcing reverse strand - for ML2345.20154.1 in ML2345.g5.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c350843_g1_i1 in ML2345.g68.i1
ERROR: same exon (217169, 218806) on ML2383 used for multiple genes: ML238322a ML238321a
ERROR: same exon (148209, 149359) on ML2395 used for multiple genes: ML239518a ML239517a
ERROR: same exon (38463, 40908) on ML2414 used for multiple genes: ML24145a ML24144a
ERROR: same exon (51430, 52979) on ML2574 used for multiple genes: ML25745a ML25744a
ERROR: same exon (32640, 33114) on ML2576 used for multiple genes: ML25763a ML25762a
REVERSE flag found: forcing reverse strand - for ML2576.20593.1 in ML2576.g7.i1
FORWARD flag found: forcing forward strand + for Mlei_GFAT01_c372566_g5_i1 in ML2576.g26.i1
REVERSE flag found: forcing reverse strand - for ML2576.20643.1 in ML2576.g33.i1
ERROR: same exon (198058, 198489) on ML2582 used for multiple genes: ML258229a ML258228a
ERROR: same exon (349085, 350451) on ML2582 used for multiple genes: ML258248a ML258247a
ERROR: same exon (355041, 356248) on ML2582 used for multiple genes: ML258251a ML258250a
ERROR: same exon (53343, 54205) on ML2617 used for multiple genes: ML261713a ML261712a
REVERSE flag found: forcing reverse strand - for ML2679.20964.1 in ML2679.g8.i1
ERROR: same exon (204511, 206027) on ML2715 used for multiple genes: ML271532a ML271531a
ERROR: same exon (161863, 162808) on ML2744 used for multiple genes: ML274428a ML274427a
ERROR: same exon (141835, 142481) on ML2769 used for multiple genes: ML276921a ML276920a
ERROR: same exon (199718, 202131) on ML2769 used for multiple genes: ML276932a ML276931a
ERROR: same exon (17020, 18188) on ML2795 used for multiple genes: ML27953a ML27952a
ERROR: same exon (122208, 123347) on ML2798 used for multiple genes: ML279813a ML279812a
ERROR: same exon (45002, 47128) on ML2825 used for multiple genes: ML28258a ML28257a
ERROR: same exon (83346, 85653) on ML2825 used for multiple genes: ML282514a ML282513a
ERROR: same exon (81498, 82430) on ML2897 used for multiple genes: ML289712a ML289711a
ERROR: same exon (73790, 74573) on ML2931 used for multiple genes: ML29319a ML29318a
ERROR: same exon (68046, 69365) on ML3055 used for multiple genes: ML305514a ML305513a
ERROR: same exon (221525, 223225) on ML3055 used for multiple genes: ML305532a ML305531a
REVERSE flag found: forcing reverse strand - for ML3061.21965.1 in ML3061.g13.i1
REVERSE flag found: forcing reverse strand - for ML3061.21967.1 in ML3061.g15.i1
REVERSE flag found: forcing reverse strand - for ML3061.21969.1 in ML3061.g17.i1
REVERSE flag found: forcing reverse strand - for ML3061.21970.1 in ML3061.g18.i1
FORWARD flag found: forcing forward strand + for ML3061.21971.1 in ML3061.g19.i1
FORWARD flag found: forcing forward strand + for ML3061.21982.1 in ML3061.g21.i1
ERROR: same exon (89666, 89770) on ML3274 used for multiple genes: ML327421a_GFAT01_c374541_g2_i1 ML327413a ML327413a
REVERSE flag found: forcing reverse strand - for ML3274.22421.1 in ML3274.g43.i1
ERROR: same exon (53831, 55017) on ML3463 used for multiple genes: ML34638a ML34637a
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c369001_g2_i1 in ML3475.g2.i1
ERROR: same exon (24630, 24723) on ML3475 used for multiple genes: ML34753a ML34751a_c374561_g1_i1
ERROR: same exon (24630, 24723) on ML3475 used for multiple genes: ML34753a ML34751a_c374561_g1_i1
FORWARD flag found: forcing forward strand + for ML3517.22870.1 in ML3517.g14.i1
ERROR: same exon (130517, 131860) on ML3530 used for multiple genes: ML353012a ML353011a
ERROR: same exon (242572, 242716) on ML3588 used for multiple genes: Mlei_GFAT01_c358036_g5_i7 ML358824a ML358824a
REVERSE flag found: forcing reverse strand - for ML3593.23073.1 in ML3593.g2.i1
ERROR: same exon (9117, 9854) on ML3763 used for multiple genes: ML37634a ML37633a
ERROR: same exon (100688, 101974) on ML3881 used for multiple genes: ML388111a ML388110a
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c372552_g3_i1 in ML3983.g9.i1
FORWARD flag found: forcing forward strand + for ML3983.23523.1 in ML3983.g38.i1
FORWARD flag found: forcing forward strand + for ML4029.23560.1 in ML4029.g12.i1
REVERSE flag found: forcing reverse strand - for Mlei_GFAT01_c368473_g2_i1 in ML4029.g15.i1
ERROR: same exon (99430, 99637) on ML4186 used for multiple genes: ML4186.23735.1 ML418611a ML418611a
ERROR: same exon (99430, 99637) on ML4186 used for multiple genes: ML4186.23735.1 ML418611a ML418611a
ERROR: same exon (82786, 85244) on ML4343 used for multiple genes: ML434313a ML434311a
FORWARD flag found: forcing forward strand + for ML4358.23906.1 in ML4358.g19.i1
REVERSE flag found: forcing reverse strand - for ML4358.23904.1 in ML4358.g20.i1
ERROR: same exon (217020, 217989) on ML4358 used for multiple genes: ML435826a ML435825a
ERROR: same exon (5068, 6036) on ML4366 used for multiple genes: ML43662a ML43661a
REVERSE flag found: forcing reverse strand - for ML4583.24152.1 in ML4583.g3.i1
REVERSE flag found: forcing reverse strand - for ML4583.24154.1 in ML4583.g4.i1
FORWARD flag found: forcing forward strand + for ML4583.24162.1 in ML4583.g11.i1
REVERSE flag found: forcing reverse strand - for ML4608.24229.1 in ML4608.g3.i1
REVERSE flag found: forcing reverse strand - for ML4608.24232.1 in ML4608.g6.i1
FORWARD flag found: forcing forward strand + for ML4608.24246.1 in ML4608.g10.i1
FORWARD flag found: forcing forward strand + for ML4635.24292.1 in ML4635.g18.i1
ERROR: same exon (15403, 17145) on ML4682 used for multiple genes: ML46822a ML46821a
FORWARD flag found: forcing forward strand + for ML4965.24598.1 in ML4965.g10.i1
# Counted 185 manual annotations, and 184 sequences
WARNING: Curated seq  ML08929a-outer-Ntm  not found in annotation instructions (-i)
# Read 17082 annotations from /home/wrf/git/genome-reannotations/jbrowse-tracks/mnemiopsis/Mlei_2.2_annotation_sheet.tab
# 17082 total annotations, of these:
#    344 genes were only given a description
#    532 genes were new
#    19 genes were given additional isoforms
#    2039 genes were flagged for removal
#       of these, 1641 were replaced with another gene
#       398 were simply removed
#    3 genes were flagged as pseudogenes, and were ignored
# 8682 genes are + strand
# 7999 genes are - strand
# Counted 149 exons used by 2 or more genes
# User given reasons for correction included:
#split	636
#fixed UTR	533
#fused	493
#missing gene	254
#truncated 5pr	204
#missing exons	142
#REVERSE	124
#FORWARD	107
#truncated 3pr	80
#incomplete	66
#wrong start	33
#wrong end	32
#junk	18
#wrong strand	17
#add isoforms	15
#missing isoform	11
#misassembly	11
#missing MXE	10
#missing exon	8
#corrected number	7
#incorrect	5
#missing isoforms	3
#duplicate gene	3
#unsupported	3
#PSEUDOGENE	3
#extra exon	2
#unannotated	2
#add isoform	2
#cassette exon	1
#missing variant	1
#wrong splice site	1
#split extra exon	1
#trunctated 3pr	1
#no support	1
#allele	1
# Transcripts derived from files:
#ML2.2.gff3	14508
#MlScaffold09_stringtie.gff3	1020
#GFAT01.1.renamed.pinfish.gtf	987
#manual	182
#trn15-30hpf.gff3	90
#MLRB2.2.gff	54
# Wrote 16841 transcripts for 16681 genes
# Writing gene correspondance table to ML2_to_2.3.21_names.tab
# Processed completed in 0.2 minutes
```

### cell type notes ###

```
# should be smooth muscle
#grep ML305521a *
#41559_2018_575_MOESM7_ESM_mnemiopsis_clusters.C53.csv:ML305521a;6002;191;3.1822725758;1.8436262856;Myosin_head/Myosin_head/Myosin_tail_1/; Myosin-10; Zipper, isoform F; Myosin-1

# should be muscle form
#grep ML022011a *
#41559_2018_575_MOESM7_ESM_mnemiopsis_clusters.C45.csv:ML022011a;3723;921;24.7381144239;4.6389108094;Myosin_head/Myosin_tail_1/; Myosin-7; Myosin heavy chain, isoform P; Myosin-2
#41559_2018_575_MOESM7_ESM_mnemiopsis_clusters.C46.csv:ML022011a;3723;154;4.1364491002;3.8461622389;Myosin_head/Myosin_tail_1/; Myosin-7; Myosin heavy chain, isoform P; Myosin-2
#41559_2018_575_MOESM7_ESM_mnemiopsis_clusters.C47.csv:ML022011a;3723;1932;51.893634166;4.062104737;Myosin_head/Myosin_tail_1/; Myosin-7; Myosin heavy chain, isoform P; Myosin-2

# should be non muscle, but is in the same cells as the muscle form
#grep ML02275a *
#41559_2018_575_MOESM7_ESM_mnemiopsis_clusters.C26.csv:ML02275a;4359;345;7.9146593255;1.0897299968;Myosin_head/Myosin_head/Myosin_tail_1/; MYH7 protein; Myosin heavy chain, isoform S; Myosin-1
#41559_2018_575_MOESM7_ESM_mnemiopsis_clusters.C3.csv:ML02275a;4359;171;3.9229181005;1.343221859;Myosin_head/Myosin_head/Myosin_tail_1/; MYH7 protein; Myosin heavy chain, isoform S; Myosin-1
#41559_2018_575_MOESM7_ESM_mnemiopsis_clusters.C43.csv:ML02275a;4359;1341;30.7639366827;3.2251313209;Myosin_head/Myosin_head/Myosin_tail_1/; MYH7 protein; Myosin heavy chain, isoform S; Myosin-1
#41559_2018_575_MOESM7_ESM_mnemiopsis_clusters.C45.csv:ML02275a;4359;69;1.5829318651;1.3186947611;Myosin_head/Myosin_head/Myosin_tail_1/; MYH7 protein; Myosin heavy chain, isoform S; Myosin-1
#41559_2018_575_MOESM7_ESM_mnemiopsis_clusters.C46.csv:ML02275a;4359;23;0.527643955;1.104296193;Myosin_head/Myosin_head/Myosin_tail_1/; MYH7 protein; Myosin heavy chain, isoform S; Myosin-1
#41559_2018_575_MOESM7_ESM_mnemiopsis_clusters.C47.csv:ML02275a;4359;408;9.3599449415;1.6737967089;Myosin_head/Myosin_head/Myosin_tail_1/; MYH7 protein; Myosin heavy chain, isoform S; Myosin-1
#41559_2018_575_MOESM7_ESM_mnemiopsis_clusters.C4.csv:ML02275a;4359;234;5.3682037164;1.5614809958;Myosin_head/Myosin_head/Myosin_tail_1/; MYH7 protein; Myosin heavy chain, isoform S; Myosin-1
```

