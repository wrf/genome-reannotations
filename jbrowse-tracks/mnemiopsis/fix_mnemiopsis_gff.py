#!/usr/bin/env python

'''
fix_mnemiopsis_gff.py ML2.2.gff3 > ML2.2_no_comment.gff
'''

import sys

for line in open(sys.argv[1],'r'):
	line = line.strip()
	if line and line[0] != "#":
		lsplits = line.split("\t")
		feature = lsplits[2]
		if feature=="contig":
			continue
		attributes = lsplits[8]
		attrd = dict([(field.strip().split("=")) for field in attributes.split(";") if field])
		if feature=="gene": # changed to ID and Name
			geneid = attrd.get("ID")[:-1]
			newattrs = "ID={0};Name={0}".format(geneid)
			lsplits[8] = newattrs
		elif feature=="mRNA": # change to ID and Parent
			transcriptid = attrd.get("ID")
			geneid = attrd.get("Parent")[:-1]
			newattrs = "ID={0};Parent={1};Name={0}".format(transcriptid,geneid)
			lsplits[8] = newattrs
		print >> sys.stdout, "\t".join(lsplits)
