# Renilla processing steps
From [Jiang et al 2019 A hybrid de novo assembly of the sea pansy genome](https://doi.org/10.1093/gigascience/giz026)

### Process scaffolds for display
Prepare scaffold index for the [v1 scaffolds](http://rmue.reefgenomics.org/download/)

`~/samtools-1.8/samtools faidx final_genome.fasta`

Link and create the reference sequence:

```
ln -s /mnt/data/genomes/renilla_muelleri/final_genome.fasta final_genome.fasta
ln -s /mnt/data/genomes/renilla_muelleri/final_genome.fasta.fai final_genome.fasta.fai
/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta final_genome.fasta --out ./
```

### AUGUSTUS gene predictions
Probably used [reformatgff.py](https://bitbucket.org/wrf/sequences/src/master/reformatgff.py) to remove comments and do some reformatting, though the precise commands were lost

```
ln -s /mnt/data/genomes/renilla_muelleri/anno_1_nocomments_cds.gff anno_1_nocomments_cds.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff anno_1_nocomments_cds.gff --trackType CanvasFeatures --trackLabel AUGUSTUS
```

### Mapping short reads
Map short reads ([SRR7879228](https://www.ncbi.nlm.nih.gov/sra/SRX4717871[accn]), two libraries, 125bp paired) for DNA coverage wiggle plot

```
hisat2 -q -x final_genome.fasta -1 REN_B_USD16082383_HJWVLALXX_L7_1_paired.fq.gz,REN_B_USD16082383_HJWVLALXX_L8_1_paired.fq.gz -2 REN_B_USD16082383_HJWVLALXX_L7_2_paired.fq.gz,REN_B_USD16082383_HJWVLALXX_L8_2_paired.fq.gz -S renilla_L7_L8_vs_final.sam -p 6 --no-spliced-alignment 2> renilla_L7_L8_vs_final.log

~/samtools-1.8/samtools view -Su renilla_L7_L8_vs_final.sam | ~/samtools-1.8/samtools sort - -o renilla_L7_L8_vs_final.sorted.bam

~/samtools-1.8/samtools view -b renilla_L7_L8_vs_final.sorted.bam | ~/bedtools2/bin/bedtools genomecov -ibam stdin -bga > renilla_L7_L8_vs_final.sorted.bam.cov

sizecutter.py -f final_genome.fasta | sed s/" "/"\t"/ > final_genome.sizes

bedGraphToBigWig renilla_L7_L8_vs_final.sorted.bam.cov final_genome.sizes renilla_L7_L8_vs_final_bowtie2.bw
```

### Mapping nanopore reads with [graphmap](https://github.com/isovic/graphmap)
Map reads (should be [SRR7879226](https://www.ncbi.nlm.nih.gov/sra/SRX4717873[accn])) with graphmap by [Sovic 2016](https://doi.org/10.1038/ncomms11307)

`~/git/graphmap/bin/Linux-x64/graphmap align -r final_genome.fasta -d clean_renilla_subread.fasta -o clean_renilla_subread_vs_final.sam`

Convert to .bam and create index:

```
~/samtools-1.8/samtools view -Su clean_renilla_subread_vs_final.sam | ~/samtools-1.8/samtools sort - -o clean_renilla_subread_vs_final.sorted.bam
~/samtools-1.8/samtools index clean_renilla_subread_vs_final.sorted.bam
```

Prepare for jbrowse, and add the information to `tracks.conf`:

```
ln -s /mnt/data/genomes/renilla_muelleri/clean_renilla_subread_vs_final.sorted.bam clean_renilla_subread_vs_final.sorted.bam
ln -s /mnt/data/genomes/renilla_muelleri/clean_renilla_subread_vs_final.sorted.bam.bai clean_renilla_subread_vs_final.sorted.bam.bai
```

### Adding PFAM domains and BLAST vs human proteins
Normal annotation of domains with PFAM:

`hmmscan --cpu 4 --domtblout anno_1.aa.pfam.tab ~/db/Pfam-A.hmm anno_1.aa > /dev/null`

`~/git/genomeGTFtools/pfam2gff.py -g anno_1_nocomments_cds.gff -e 1e-3 -x -i anno_1.aa.pfam.tab > anno_1_pfam.gff`

Using the same [model database with cnidarians](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/nematostella/) as for *Nematostella*. The `--add-description` option is used, which displays the SwissProt description of the matched protein in blue underneath the ID. The `--add-accession` option is used to include the SwissProt accession, so the mouse click will open the protein.

`~/diamond-latest/diamond blastx -q anno_1.codingseq -d ~/db/model_organism_uniprot.w_cnido.fasta > renilla_vs_models_blastx_e3.tab`

`~/git/genomeGTFtools/blast2genomegff.py -p blastx -g anno_1_nocomments_cds.gff -b renilla_vs_models_blastx_e3.tab -d ~/db/model_organism_uniprot.w_cnido.fasta -S --add-description --add-accession -x > renilla_vs_models_blastx_e3.gff`

```
ln -s /mnt/data/genomes/renilla_muelleri/anno_1_pfam.gff
ln -s /mnt/data/genomes/renilla_muelleri/renilla_vs_models_blastx_e3.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff renilla_vs_models_blastx_e3.gff --trackType CanvasFeatures --trackLabel blast-v-human
/var/www/html/jbrowse/bin/flatfile-to-json.pl --out ./ --gff anno_1_pfam.gff --trackType CanvasFeatures --trackLabel PFAM-domains
```

In the `trackList.json` file, the mouse-over for the blast hits is changed to display the target.

```
         "onClick" : {
            "action" : "newWindow",
            "label" : "function() { return this.feature.get('score') + ' : ' + this.feature.get('target') ; }",
            "title" : "{name} {type}",
            "url" : "https://www.uniprot.org/uniprot/{accession}"
         },
```

### Bioluminescence loci ###

```
~/git/genomeGTFtools/extract_coordinates.py -c -s jcf7180000022627 -b 35000 -e 45000 -g anno_1_nocomments_cds.gff > renilla_s22627_35k_rluc_locus_short_annot.tab
~/git/genomeGTFtools/extract_coordinates.py -c -s jcf7180000022724 -b 25500 -e 35500 -g anno_1_nocomments_cds.gff > renilla_s22724_25k_rluc_locus_short_annot.tab
~/git/genomeGTFtools/extract_coordinates.py -c -s jcf7180000028822 -b 36000 -e 46000 -g anno_1_nocomments_cds.gff > renilla_s28822_36k_rluc_locus_short_annot.tab

~/git/genomeGTFtools/extract_coordinates.py -s NW_021163015.1 -b 3902000 -e 4102000 -g GCF_004324835.1_DenGig_1.0_genomic.gff > dgig_4002k_rluc_locus_short_annot.tab
~/git/genomeGTFtools/extract_coordinates.py -s NW_021163020.1 -b 1094000 -e 1104000 -g GCF_004324835.1_DenGig_1.0_genomic.gff > dgig_1100k_rluc_locus_short_annot.tab

```

### Microsynteny with other cnidarians ###
Using the script [microsynteny.py](https://github.com/wrf/genomeGTFtools/blob/master/microsynteny.py). There appear to occasionally be problems due to tandem duplications creating false positives.

| Db species | Alignments | HSPs | Queries aligned | Blocks | Genes in blocks | Longest | Mb Span | 
|-----------------|------------|------|-----------------|--------|-----------------|---------|------| 
| Dendronephthya gigantea | 175618 | 177296 | 18688 | 1439 | 6438 | 33 | 52.303 |
| Acropora tenuis | 126891 | 128283 | 16062 | 329 | 1138 | 9 | 8.426 |
| Porites lutea | 126008 | 127097 | 16612 | 368 | 1246 | 8 | 8.267 |
| Nematostella vectensis | 88504 | 89419 | 14424 | 227 | 759 | 8 | 5.582 |
| Nemopilema nomurai | 107091 | 107779 | 14482 | 128 | 417 | 5 | 2.183 |
| Hydra vulgaris | 97957 | 98475 | 12950 | 34 | 108 | 4 | 0.791 |

Checking first against [Nematostella v2](https://ndownloader.figshare.com/files/1215191), by [Putnam 2007](https://doi.org/10.1126/science.1139158), due to its widespread use.

`~/diamond-latest/diamond makedb --in nveGenes.vienna130208.protein.fasta --db nveGenes.vienna130208.protein.fasta`

`~/diamond-latest/diamond blastp -q anno_1.aa -d ~/genomes/nematostella_vectensis/nveGenes.vienna130208.protein.fasta -o renilla_augustus_vs_nve2.tab`

`~/git/genomeGTFtools/microsynteny.py -q anno_1_nocomments_cds.gff -d ~/genomes/nematostella_vectensis/nveGenes.vienna130208.nemVec1.gff -b renilla_augustus_vs_nve2.tab -G > renilla_augustus_vs_nve2.microsynteny.gff`

```
# Found 569 possible split genes
# Found 227 total putative synteny blocks for 759 genes
# Average block is 3.34, longest block was 8 genes
# Total block span was 5582266 bases
3 169
4 44
5 10
6 3
8 1
```

Shown above, very little microsynteny is detected.

Next, using the data from the octocoral [Dendronephthya gigantea](http://www.jellyfishgenome.com/index.php/%27%27Dendronephthya_gigantea%27%27_genome), by [Jeon 2019](https://doi.org/10.1093/gbe/evz043). The assembly is a bit larger (286Mb vs 172Mb) but broadly better than *Renilla*, with 1321 scaffolds and N50 of 1.445Mb.

`~/diamond-latest/diamond makedb --in GCF_004324835.1_DenGig_1.0_protein.faa --db GCF_004324835.1_DenGig_1.0_protein.faa`

The format of the GFF is inconvenient for downstream analyses, so the subset of features for proteins are extracted:

`./filter_dengig_gff_cds.py GCF_004324835.1_DenGig_1.0_genomic.gff > GCF_004324835.1_DenGig_1.0_genomic.cds_only.gff`	

`~/git/genomeGTFtools/microsynteny.py -q anno_1_nocomments_cds.gff -d ~/genomes/dendronephthya_gigantea/GCF_004324835.1_DenGig_1.0_genomic.cds_only.gff -b renilla_augustus_vs_dengig1.tab -c -G > renilla_augustus_vs_dengig1.microsynteny.gff`

Substantial microsynteny is detected, in one case (`jcf7180000021602`) it spans the entire 300kb scaffold.

```
# Found 835 possible split genes
# Found 1439 total putative synteny blocks for 6438 genes
# Average block is 4.47, longest block was 33 genes
# Total block span was 52303665 bases
3 643
4 349
5 162
6 108
7 66
8 27
9 18
10 19
11 26
12 6
13 2
14 2
15 4
16 2
20 3
28 1
33 1
```

Next, examining [Acropora tenuis](http://aten.reefgenomics.org/download/), by the [ReFuGe2020 project](https://doi.org/10.3389/fmars.2015.00068). The 486Mb assembly is relatively good, with 614 scaffolds and N50 of 2.836Mb. 

`~/diamond-latest/diamond makedb --in aten_0.11.maker_post_001.proteins.fasta --db aten_0.11.maker_post_001.proteins.fasta`

`~/diamond-latest/diamond blastp -q anno_1.aa -d ~/genomes/acropora_tenuis/aten_0.11.maker_post_001.proteins.fasta -o renilla_augustus_vs_aten01.tab`

`~/git/genomeGTFtools/microsynteny.py -q anno_1_nocomments_cds.gff -d ~/genomes/acropora_tenuis/aten_0.11.maker_post_001.genes.gff -b renilla_augustus_vs_aten01.tab -G > renilla_augustus_vs_aten01.microsynteny.gff`

Next, the coral [Porites lutea](http://plut.reefgenomics.org/download/), by the [ReFuGe2020 project](https://doi.org/10.3389/fmars.2015.00068). The 552Mb assembly is acceptable, with 2975 scaffolds and N50 of 660kb.

`~/diamond-latest/diamond makedb --in plut2v1.1.proteins.fasta --db plut2v1.1.proteins.fasta`

`~/diamond-latest/diamond blastp -q anno_1.aa -d ~/genomes/porites_lutea/plut2v1.1.proteins.fasta -o renilla_augustus_vs_Plutv2.tab`

`~/git/genomeGTFtools/microsynteny.py -q anno_1_nocomments_cds.gff -d ~/genomes/porites_lutea/plut2v1.1.genes.gff3.gz -b renilla_augustus_vs_Plutv2.tab -G > renilla_augustus_vs_Plutv2.microsynteny.gff`

Next, the scyphomedusa [Nemopilema nomurai](https://www.ncbi.nlm.nih.gov/Traces/wgs/PEDN01), by [Kim 2019](https://doi.org/10.1186/s12915-019-0643-7). Compared to some other recent jellyfish projects, this one has a small genome (210Mb) and relatively contiguous, 1463 scaffolds with 849kb N50.

`~/diamond-latest/diamond makedb --in NNO.final_gene_set.pep.fa --db NNO.final_gene_set.pep.fa`

`~/diamond-latest/diamond blastp -q anno_1.aa -d ~/genomes/nemopilema_nomurai/NNO.final_gene_set.pep.fa -o renilla_augustus_vs_NNOv1.tab`

`~/git/genomeGTFtools/microsynteny.py -q anno_1_nocomments_cds.gff -d ~/genomes/nemopilema_nomurai/NNO.final_gene_set.gff3.gz -b renilla_augustus_vs_NNOv1.tab -G > renilla_augustus_vs_NNOv1.microsynteny.gff`

Against [Hydra vulgaris Dovetail reannotation](https://arusha.nhgri.nih.gov/hydra/download/?dl=asl):

`~/diamond-latest/diamond makedb --in hydra_augustus_shortname_prots.fasta --db hydra_augustus_shortname_prots.fasta`

`~/diamond-latest/diamond blastp -q anno_1.aa -d ~/genomes/hydra_magnipapillata/hydra_augustus_shortname_prots.fasta -o renilla_augustus_vs_HvDT.tab`

`~/git/genomeGTFtools/microsynteny.py -q anno_1_nocomments_cds.gff -d ~/genomes/hydra_magnipapillata/hydra.augustus.gff3 -b renilla_augustus_vs_HvDT.tab -G > renilla_augustus_vs_HvDT.microsynteny.gff`

