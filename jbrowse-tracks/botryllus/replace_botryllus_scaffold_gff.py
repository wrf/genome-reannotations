#!/usr/bin/env python
#
# replace_botryllus_scaffold_gff.py

"""
~/git/genome-reannotations/jbrowse-tracks/botryllus/replace_botryllus_scaffold_gff.py botznik-chr-ctg.gff botznik-chr-all.clean.gff > augustus_ctg_reassigned.gff
"""

import sys
from Bio import SeqIO

def get_contig_start_index(contig_starts, feature_start, n):
	for i,startpos in enumerate( contig_starts ):
		if i < n:
			continue
		if feature_start >= startpos:
			pass
	#		sys.stderr.write("{}  {}>{}\n".format(i, feature_start, startpos))
		else:
			return i
	return i

if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	
	linecounter = 0
	chrun_contig_counter = 0

	contig_gff = sys.argv[1]
	gene_gff = sys.argv[2]

	contig_order = []
	contig_starts = []
	contig_ends = []

	sys.stderr.write( "# Reading {}\n".format(contig_gff) )
	#chr2	Botznik	contig	1	11095	.	.	.	ID=botctg000016;Name=botctg000016;Note=Length 11095
	#chr2	Botznik	contig	11096	42540	.	.	.	ID=botctg000033;Name=botctg000033;Note=Length 31445
	for line in open(contig_gff, 'r'):
		line = line.strip()
		if line and line[0]!="#":
			linecounter += 1
			lsplits = line.split("\t")
			scaffold = lsplits[0]
			if scaffold != "chrUn":
				continue
			chrun_contig_counter += 1

			feature_start = int(lsplits[3])
			feature_end = int(lsplits[4])
			feature_length = feature_end - feature_start + 1
			attributes = lsplits[8]
			attrd = dict([(field.strip().split("=",1)) for field in attributes.split(";") if field])
			contig_id = attrd.get("ID")
			contig_length = int(attrd.get("Note").split(" ")[-1])
			if feature_length != contig_length:
				sys.stderr.write("WARNING: {} contig length {} not equal to {}\n".format( contig_id, feature_length, contig_length ) )
		#	position_to_contig[feature_start] = contig_id
			contig_starts.append(feature_start)
			contig_ends.append(feature_end)
			contig_order.append(contig_id)
	sys.stderr.write( "# {} lines for {} contigs\n".format(linecounter, chrun_contig_counter) )




	linecounter2 = 0
	chrun_gene_counter = 0
	start_n = 0

	sys.stderr.write( "# Reading {}\n".format(gene_gff) )
	for line in open(gene_gff,'r'):
		line = line.strip()
		if line and line[0]!="#":
			linecounter2 += 1
			lsplits = line.split("\t")
			scaffold = lsplits[0]
			if scaffold == "chrUn":
				chrun_gene_counter += 1
			else:
				print( line, file=sys.stdout )
				continue

			feature_start = int(lsplits[3])
			feature_end = int(lsplits[4])
			feature_length = feature_end - feature_start + 1
			attributes = lsplits[8]
			feature_type = lsplits[2]

			ctg_index = get_contig_start_index(contig_starts, feature_start, start_n)

			start_n = ctg_index

			contig_id = contig_order[ctg_index]
			ctg_start_pos = contig_starts[ctg_index]
			ctg_last_pos = contig_ends[ctg_index]

			feat_adj_start = feature_start - ctg_start_pos + 1
			feat_adj_end = feature_end - ctg_start_pos + 1

			sys.stderr.write("{}  {}  {}  {}  {}  {}  {}\n".format(ctg_index, contig_id, feature_type, ctg_start_pos, ctg_last_pos, feat_adj_start, feat_adj_end))


			lsplits[0] = contig_id
			lsplits[3] = str(  )
			lsplits[4] = str(  )
			outline = "{}\n".format( "\t".join(lsplits) )
			sys.stdout.write(outline)

	sys.stderr.write( "# {} gff lines of {} unassigned genes \n".format(linecounter2, chrun_gene_counter) )

contig_gff_ex = """
chrUn	Botznik	contig	1	2793	.	.	.	ID=botctg014748;Name=botctg014748;Note=Length 2793
chrUn	Botznik	contig	2794	3121	.	.	.	ID=botctg014749;Name=botctg014749;Note=Length 328
chrUn	Botznik	contig	3122	3464	.	.	.	ID=botctg014750;Name=botctg014750;Note=Length 343
chrUn	Botznik	contig	3465	5243	.	.	.	ID=botctg014751;Name=botctg014751;Note=Length 1779
chrUn	Botznik	contig	5244	8457	.	.	.	ID=botctg014752;Name=botctg014752;Note=Length 3214
chrUn	Botznik	contig	8458	10700	.	.	.	ID=botctg014753;Name=botctg014753;Note=Length 2243
chrUn	Botznik	contig	10701	12340	.	.	.	ID=botctg014754;Name=botctg014754;Note=Length 1640
chrUn	Botznik	contig	12341	12781	.	.	.	ID=botctg014755;Name=botctg014755;Note=Length 441
chrUn	Botznik	contig	12782	13441	.	.	.	ID=botctg014756;Name=botctg014756;Note=Length 660
chrUn	Botznik	contig	13442	14436	.	.	.	ID=botctg014757;Name=botctg014757;Note=Length 995
chrUn	Botznik	contig	14437	19046	.	.	.	ID=botctg014758;Name=botctg014758;Note=Length 4610
chrUn	Botznik	contig	19047	20134	.	.	.	ID=botctg014759;Name=botctg014759;Note=Length 1088
chrUn	Botznik	contig	20135	21221	.	.	.	ID=botctg014760;Name=botctg014760;Note=Length 1087
chrUn	Botznik	contig	21222	24391	.	.	.	ID=botctg014761;Name=botctg014761;Note=Length 3170
chrUn	Botznik	contig	24392	29048	.	.	.	ID=botctg014762;Name=botctg014762;Note=Length 4657
chrUn	Botznik	contig	29049	30108	.	.	.	ID=botctg014763;Name=botctg014763;Note=Length 1060
chrUn	Botznik	contig	30109	30776	.	.	.	ID=botctg014764;Name=botctg014764;Note=Length 668
chrUn	Botznik	contig	30777	31996	.	.	.	ID=botctg014765;Name=botctg014765;Note=Length 1220
chrUn	Botznik	contig	31997	32900	.	.	.	ID=botctg014766;Name=botctg014766;Note=Length 904
chrUn	Botznik	contig	32901	33656	.	.	.	ID=botctg014767;Name=botctg014767;Note=Length 756
chrUn	Botznik	contig	33657	36108	.	.	.	ID=botctg014768;Name=botctg014768;Note=Length 2452
chrUn	Botznik	contig	36109	37249	.	.	.	ID=botctg014769;Name=botctg014769;Note=Length 1141
chrUn	Botznik	contig	37250	37848	.	.	.	ID=botctg014770;Name=botctg014770;Note=Length 599
chrUn	Botznik	contig	37849	38353	.	.	.	ID=botctg014771;Name=botctg014771;Note=Length 505
chrUn	Botznik	contig	38354	38997	.	.	.	ID=botctg014772;Name=botctg014772;Note=Length 644
chrUn	Botznik	contig	38998	39536	.	.	.	ID=botctg014773;Name=botctg014773;Note=Length 539
chrUn	Botznik	contig	39537	39745	.	.	.	ID=botctg014774;Name=botctg014774;Note=Length 209
chrUn	Botznik	contig	39746	40095	.	.	.	ID=botctg014775;Name=botctg014775;Note=Length 350
chrUn	Botznik	contig	40096	40357	.	.	.	ID=botctg014776;Name=botctg014776;Note=Length 262
chrUn	Botznik	contig	40358	47052	.	.	.	ID=botctg014777;Name=botctg014777;Note=Length 6695
chrUn	Botznik	contig	47053	49676	.	.	.	ID=botctg014778;Name=botctg014778;Note=Length 2624
chrUn	Botznik	contig	49677	50347	.	.	.	ID=botctg014779;Name=botctg014779;Note=Length 671
"""

augustus_gff_ex = """
chrUn	AUGUSTUS	gene	36326	36817	1	+	.	ID=g1;Name=g1;Note=g1|introns:0|%transcript_support:0
chrUn	AUGUSTUS	transcript	36326	36817	.	+	.	g1.t1
chrUn	AUGUSTUS	start_codon	36326	36328	.	+	0	transcript_id "g1.t1"; gene_id "g1";
chrUn	AUGUSTUS	CDS	36326	36817	.	+	0	transcript_id "g1.t1"; gene_id "g1";
chrUn	AUGUSTUS	stop_codon	36815	36817	.	+	0	transcript_id "g1.t1"; gene_id "g1";
chrUn	AUGUSTUS	gene	37461	37682	1	-	.	ID=g2;Name=g2;Note=g2|introns:0|%transcript_support:0
chrUn	AUGUSTUS	transcript	37461	37682	.	-	.	g2.t1
chrUn	AUGUSTUS	stop_codon	37461	37463	.	-	0	transcript_id "g2.t1"; gene_id "g2";
chrUn	AUGUSTUS	CDS	37461	37682	.	-	0	transcript_id "g2.t1"; gene_id "g2";
chrUn	AUGUSTUS	start_codon	37680	37682	.	-	0	transcript_id "g2.t1"; gene_id "g2";
chrUn	AUGUSTUS	gene	37872	38315	1	-	.	ID=g3;Name=g3;Note=g3|introns:0|%transcript_support:0
chrUn	AUGUSTUS	transcript	37872	38315	.	-	.	g3.t1
chrUn	AUGUSTUS	stop_codon	37872	37874	.	-	0	transcript_id "g3.t1"; gene_id "g3";
chrUn	AUGUSTUS	CDS	37872	38315	.	-	0	transcript_id "g3.t1"; gene_id "g3";
chrUn	AUGUSTUS	start_codon	38313	38315	.	-	0	transcript_id "g3.t1"; gene_id "g3";
chrUn	AUGUSTUS	gene	38701	38958	1	-	.	ID=g4;Name=g4;Note=g4|introns:0|%transcript_support:0
chrUn	AUGUSTUS	transcript	38701	38958	.	-	.	g4.t1
chrUn	AUGUSTUS	stop_codon	38701	38703	.	-	0	transcript_id "g4.t1"; gene_id "g4";
chrUn	AUGUSTUS	CDS	38701	38958	.	-	0	transcript_id "g4.t1"; gene_id "g4";
chrUn	AUGUSTUS	start_codon	38956	38958	.	-	0	transcript_id "g4.t1"; gene_id "g4";
chrUn	AUGUSTUS	gene	49678	50251	1	-	.	ID=g5;Name=g5;Note=g5|introns:0|%transcript_support:0
chrUn	AUGUSTUS	transcript	49678	50251	.	-	.	g5.t1
chrUn	AUGUSTUS	CDS	49678	50251	.	-	0	transcript_id "g5.t1"; gene_id "g5";
chrUn	AUGUSTUS	start_codon	50249	50251	.	-	0	transcript_id "g5.t1"; gene_id "g5";
"""




