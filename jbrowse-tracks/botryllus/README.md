# Botryllus schlosseri genome tracks #
Using `botznik v1 assembly`, by [Voskoboynik 2013](https://doi.org/10.7554/eLife.00569). I don't remember where these were downloaded, since they are not on [Ensembl](https://www.ensembl.org/info/about/species.html).

The [reformatted assembly](https://bitbucket.org/wrf/genome-reannotations/downloads/botryllus_chr_w_ctgs.fa.gz) and [transcript GFF](https://bitbucket.org/wrf/genome-reannotations/downloads/botznik-transcripts.minimap.gff.gz) are in the [downloads folder](https://bitbucket.org/wrf/genome-reannotations/downloads/).

## reorganizing the assembly ##
The assembly is in a weird format, with 13 partial chromosomes, and all remaining contigs put into a single unassigned `chrUn` contig, which is substantially larger than all other chromosomes combined - 406Mb vs 173Mb.

```
 sizecutter.py -f botznik-chr.fa
chr1	10112029
chr2	8858196
chr3	2087537
chr4	11122902
chr5	30896539
chr6	10480803
chr7	15370816
chr8	2612869
chr9	21778431
chr10	22705474
chr11	2805763
chr12	18286204
chr13	16420441
chrUn	406853556
```

The 106347 unassigned contigs are pulled out, to make a file of their own:

```
grep chrUn botznik-chr-ctg.gff > unassigned_chr.gff 
cat unassigned_chr.gff | cut -f 2 -d "=" | cut -f 1 -d ";" > unassigned_chr.names
getAinB.py unassigned_chr.names botznik-ctg.fa.gz > unassigned_ctg.fa
getAinB.py chr.names botznik-chr.fa.gz > chr.fa
cat chr.fa unassigned_ctg.fa > botryllus_chr_w_ctgs.fa
```

## getting transcripts ##
The contigs were all mashed together, so instead of trying to parse them out, I use [minimap](https://github.com/lh3/minimap2) to put the transcripts back onto the contigs.

```
~/minimap2-2.23_x64-linux/minimap2 -a -x splice --secondary=no botryllus_chr_w_ctgs.fa botznik-transcripts.fa | ~/samtools-1.14/samtools sort - -o botznik-transcripts.bam
~/samtools-1.14/samtools flagstat botznik-transcripts.bam
72113 + 0 in total (QC-passed reads + QC-failed reads)
72113 + 0 primary
0 + 0 secondary
0 + 0 supplementary
0 + 0 duplicates
0 + 0 primary duplicates
70918 + 0 mapped (98.34% : N/A)
70918 + 0 primary mapped (98.34% : N/A)
```

Again, using pinfish can remake the gff.

```
~/git/pinfish/spliced_bam2gff/spliced_bam2gff -M -s botznik-transcripts.bam > botznik-transcripts.minimap.gtf
~/git/genomeGTFtools/misc/stringtie_gtf_to_gff3.py botznik-transcripts.minimap.gtf > botznik-transcripts.minimap.gff
```

This differs somewhat from the original, possibly some repeat or junk transcripts did not map.

```
gtfstats.py -i botznik-chr-all.clean.gff
CDS	180747
exon	180747
gene	72113
mRNA	72638
start_codon	59592
stop_codon	62984
Total exon length 51052410 , average 282.45
Counted 176120 non-redundant groups (when merging)
Total non-redundant exon length 50345108

gtfstats.py -i botznik-transcripts.minimap.gff
exon	173189
transcript	70918
Total exon length 50175967 , average 289.72
Counted 170563 non-redundant groups (when merging)
Total non-redundant exon length 49176598
```



