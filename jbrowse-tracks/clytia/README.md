# Clytia hemisphaerica #
Genome of the hydrozoan *Clytia hemisphaerica*, by [Leclere 2019](https://doi.org/10.1038/s41559-019-0833-2).

## prepare scaffolds ##
Scaffolds appear to be version 1 with [7644 scaffolds](http://marimba.obs-vlfr.fr/downloads/). Version 2 is live on their browser, but currently there are no annotation files (as of Apr 2020).

`~/samtools-1.9/samtools faidx clytia_assembly_dl_issue48.fa`

Make gap track:

`~/git/genomeGTFtools/repeat2gtf.py -l clytia_assembly_dl_issue48.fa > clytia_assembly_dl_issue48.n_gaps.gff`

```
# Counted 102282 repeats of 73975364 total bases, average 723.25
# Longest repeat was 39576 bases on sc0001792
```

## blast against models ##

`~/diamond-latest/diamond blastx -q transcripts.fa -d ~/db/model_organism_uniprot.w_cnido.fasta -o transcripts.vs_models.tab`

`~/git/genomeGTFtools/blast2genomegff.py -b transcripts.vs_models.tab -g merged_transcript_models.gff3 -S --add-description --add-accession -d ~/db/model_organism_uniprot.w_cnido.fasta > transcripts.vs_models.gff`

## map ESTs from NCBI ##
EST set contains 85991 sequences. Of these, 87838 (98%) mapped with [minimap2](https://github.com/lh3/minimap2).

`~/minimap2-2.17_x64-linux/minimap2 -a -x splice -t 4 --secondary=no clytia_assembly_dl_issue48.fa Clytia_hemisphaerica_NCBI_ESTs.fasta | ~/samtools-1.9/samtools sort - -o Clytia_hemisphaerica_NCBI_ESTs.bam`

`~/samtools-1.9/samtools index Clytia_hemisphaerica_NCBI_ESTs.bam`

