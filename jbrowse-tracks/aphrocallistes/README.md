# Aphrocallistes vastus #
Genome of the glass sponge *Aphrocallistes vastus*, by [Francis et al 2023](https://royalsocietypublishing.org/doi/10.1098/rsos.230423) using [v1.29](https://github.com/PalMuc/Aphrocallistes_vastus_genome):

![screenshot of Aphrocallistes browser](https://bitbucket.org/wrf/genome-reannotations/raw/e199e2096df9a2b580cd533e12c6bcd1689ffb5e/jbrowse-tracks/aphrocallistes/avas_jbrowse_screenshot.png)


Prepare [scaffolds](https://github.com/PalMuc/Aphrocallistes_vastus_genome/releases/tag/v1.29), i.e. in the jbrowse folder, link and make the scaffold track:

```
ln -s /mnt/data/genomes/aphrocallistes_vastus_PORI/v1.29/Avas.v1.29_scaffolds.fasta
ln -s /mnt/data/genomes/aphrocallistes_vastus_PORI/v1.29/Avas.v1.29_scaffolds.fasta.fai 
/var/www/html/jbrowse/bin/prepare-refseqs.pl --indexed_fasta Avas.v1.29_scaffolds.fasta --out ./
```

Make gap track:

```
~/git/genomeGTFtools/repeat2gtf.py -l Avas.v1.29_scaffolds.fasta > Avas.v1.29_scaffolds.n_gaps.gff
# Parsing repeats of N and n from Avas.v1.29_scaffolds.fasta
# Counted 186 sequences  Thu Feb  9 14:31:31 2023
# Counted 110 repeats of 28153 total bases, average 255.94
# Longest repeat was 2023 bases on Aphrocallistes_vastus_HiC-scaffold_008
```

Link and make the gap track:

```
ln -s ~/genomes/aphrocallistes_vastus_PORI/v1.29/Avas.v1.29_scaffolds.n_gaps.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff Avas.v1.29_scaffolds.n_gaps.gff --trackType CanvasFeatures --trackLabel N-gaps --out ./
```

Prepare the manual annotation GFF:

```
ln -s /mnt/data/genomes/aphrocallistes_vastus_PORI/Avas.current_v.annotations.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff Avas.current_v.annotations.gff --trackType CanvasFeatures --trackLabel manual-v1.29 --out ./
/var/www/html/jbrowse/bin/generate-names.pl --tracks manual-v1.29 --out ./
```

Blast against model organisms, using [blast2genomegff.py](https://github.com/wrf/genomeGTFtools/blob/master/blast2genomegff.py):

```
~/git/genomeGTFtools/blast2genomegff.py -b v1.29/Avas.v1.29_annotations.blast_vs_models.tab -d ~/db/model_organism_uniprot.w_cnido.fasta -g v1.29/Avas.v1.29_annotations.fr.gff -S --add-description --add-accession > v1.29/Avas.v1.29_annotations.blast_vs_models.gff
ln -s /mnt/data/genomes/aphrocallistes_vastus_PORI/v1.29/Avas.v1.29_annotations.blast_vs_models.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff Avas.v1.29_annotations.blast_vs_models.gff --trackType CanvasFeatures --trackLabel blast-vs-models --out ./
```

Blast against [*Oopsacas minuta*](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/oopsacas/):

```
~/git/genomeGTFtools/blast2genomegff.py -b Avas.v1.29_annotations.prot.vs_oopsacas_gb.tab -d ~/genomes/oopsacas_minuta_PORI/JAKMXF01.1.gbff.prot.fa -g Avas.v1.29_annotations.fr.gff -p blastp --percent > Avas.v1.29_annotations.prot.vs_oopsacas_gb.gff
ln -s /mnt/data/genomes/aphrocallistes_vastus_PORI/v1.29/Avas.v1.29_annotations.prot.vs_oopsacas_gb.gff
/var/www/html/jbrowse/bin/flatfile-to-json.pl --gff Avas.v1.29_annotations.prot.vs_oopsacas_gb.gff --trackType CanvasFeatures --trackLabel blast-vs-oopsacas --out ./
```

Even though the data are not in the best shape, just for the sake of thoroughness, mapping of the [Moroz 454 RNAseq datasets](https://www.ncbi.nlm.nih.gov/sra/SRX011009[accn]), with [minimap2](https://github.com/lh3/minimap2):

`cat SRR027149.fastq SRR027150.fastq > SRR027150.w_49.fastq`

`~/minimap2-2.23_x64-linux/minimap2 -a -x splice --secondary=no v1.29/Avas.v1.29_scaffolds.fasta SRR027150.w_49.fastq | ~/samtools-1.14/samtools sort - -o v1.29/Avas.v1.29.SRR027150.minimap2.bam`

Add bigwigs:

```
ln -s ~/genomes/aphrocallistes_vastus_PORI/v1.29/COV_ONT-DNA.bw
ln -s ~/genomes/aphrocallistes_vastus_PORI/v1.29/COV_ONT-RNA.bw
ln -s ~/genomes/aphrocallistes_vastus_PORI/v1.29/COV_PE-DNA.bw
ln -s ~/genomes/aphrocallistes_vastus_PORI/v1.29/COV_PE-RNA.bw
ln -s ~/genomes/aphrocallistes_vastus_PORI/v1.29/COV_PE-RNA_stranded.fwd.bw
ln -s ~/genomes/aphrocallistes_vastus_PORI/v1.29/COV_PE-RNA_stranded.rev.bw
```



