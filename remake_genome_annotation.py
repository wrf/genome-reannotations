#!/usr/bin/env python
#
# remake_genome_annotation.py  created 2020-03-26

'''
remake_genome_annotation.py  last modified 2023-08-16

remake_genome_annotation.py -g main.gff -a alternate.gff -m manual.nucls.fasta -i instructions.tab

    tabular fields in instructions include
  scaffold  gene  remove  alternate  manual  reason  gene  notes
      scaffold   - scaffold ID of the annotation
      gene       - gene ID to add or remove, from existing or best set
                   each line must have either a single ID or nothing
                   can be left blank if adding a new gene
      remove     - y/n flag to remove the original features
      alternate  - comma separated IDs of alternate annotation programs
      manual     - sequence ID of manual annotations
      reason     - reason to change or remove, e.g.:
                       missing gene, missing MXE, missing variant, missing exons,
                       fused, split, wrong strand, truncated 5pr, truncated 3pr
                   can also include a few specific instructions to force strand:
                       FORWARD, REVERSE
                   or to disable any further gene predictions:
                       PSEUDOGENE
      gene       - gene name (if known), appended as Description tag in GFF
      checked    - y/n whether the annotation has been manually checked
                   this is entirely for the browser, as the contents of this flag
                   do not affect the processing of the code
      notes      - any other information, for user_note tag in GFF
                   this also does not affect the code

    only two fields are required, scaffold and either alternate or manual
    the first 6 columns affect how the code operates,
    the other 3 are only to change what information ends up in the GFF

    for example:

~/git/genome-reannotations/remake_genome_annotation.py -m ml_manually_fixed_txs.direct.gtf -i ~/git/genome-reannotations/jbrowse-tracks/mnemiopsis/Mlei_2.2_annotation_sheet.tab -g ~/genomes/mnemiopsis_leidyi/ML2.2.gff3 -a ~/genomes/mnemiopsis_leidyi/MLRB2.2.gff ~/genomes/mnemiopsis_leidyi/trn15-30hpf.gff3 ~/genomes/mnemiopsis_leidyi/MlScaffold09_stringtie.gff3 ~/genomes/mnemiopsis_leidyi/GFAT01.1.renamed.pinfish.gtf -s ML2.3.21 -f ML2_to_2.3.21_names.tab > ML2.3.21_annotations.gff 2> ML2.3.21_annotations.log
'''

import sys
import os
import argparse
import time
import gzip
import subprocess
from collections import defaultdict
from Bio import SeqIO

example_annotation_table = """#scaffold	gene	remove	alternate	manual	reason	gene	checked	notes
ML0011	ML001110a	y	ML0011.177.1		fused	Glutamate dehydrogenase GLUD-g2		
ML0011		0	MLRB001130		fused	Glutamate dehydrogenase GLUD-g1		
ML0011	ML001111a	y			split			fragment of GLUD-g1
ML0011		0	ML0011.187.1		fused	Similar to nucleolin		
"""


def get_manual_annotations(manualfile):
	'''from the file name, read nucleotides as fasta and return as BioPython seq dict'''
	if manualfile.rsplit('.',1)[-1]=="gz": # autodetect gzip format
		opentype = gzip.open
		sys.stderr.write("# Reading nucleotide annotations from {} as gzipped\n".format(manualfile) )
	else: # otherwise assume normal open for fasta format
		opentype = open
		sys.stderr.write("# Reading nucleotide annotations from {}\n".format(manualfile) )
	seqdict = SeqIO.to_dict(SeqIO.parse(opentype(manualfile,'rt'),"fasta"))
	sys.stderr.write("# Counted {} manual correction sequences from {}\n".format(len(seqdict), manualfile) )
	return seqdict


################################################################################


def read_gff_to_dict(gff_file, do_quiet_mode, ignore_errors=False):
	'''
    read GFF file, return two dicts,
    one where keys are transcript IDs and values are all lines associated with the transcript
    the other where keys are gene IDs and values are a redundant list of transcript IDs,
      usually each will be a single entry linking to the same name
    '''

	if gff_file.rsplit('.',1)[-1]=="gz": # autodetect gzip format
		opentype = gzip.open
		sys.stderr.write("# Reading GFF file {} as gzipped\n".format(gff_file) )
	else: # otherwise assume normal open for fasta format
		opentype = open
		sys.stderr.write("# Reading GFF file {}\n".format(gff_file) )

	# main dictionary to store all data
	transcript_to_lines = defaultdict(list) # key is transcript ID, value is list of strings
	gene_to_transcripts = defaultdict(list) # key is gene ID, what will be in the annotation, value is list of transcript IDs
	subfeature_lines = defaultdict(list) # key is transcript ID, value is list of strings, of feature start end

	# strictly, gene features are ignored
	# mRNA or transcripts for each gene are collected by gene,
	# and the gene feature is recreated later from the bounds
	toplevel = ["mRNA", "transcript", "rRNA"]
	sublevel = ["exon", "CDS", "five_prime_UTR", "three_prime_UTR"]

	# for genes_to_transcripts
	# programs where each gene AND each transcript get their own key
	# meaning a gene will have multiple transcripts in the list in the value
	# and each transcript will have only itself in the list
	programs_w_parent = ["AUGUSTUS", "gth", "StringTie"]

	# tracker to check if any exons or features ever extend beyond tx bounds, as they never should
	transcript_bound_dict = {} # key is transcript ID, value is bounds
	exon_bounds_dict = defaultdict(list) # key is transcript ID, value is list of all bounds, upper and lower, in no order

	genes_to_scaffolds = defaultdict(list) # key is gene ID, value is list of scaffold, which should never have more than 1

	feature_counter = defaultdict(int) # key is feature, value is count
	seen_transcripts = defaultdict(int) # track genes already read
	error_count = 0

	for line in opentype(gff_file,'rt'):
		if line.strip() and line[0]!="#":
			# reset type with each line, then determine GTF or GFF
			is_GME_type = False # gene mRNA exon, meaning ignore genes
			is_TE_type = False # transcript exon, such as from pinfish or stringtie

			lsplits = line.strip().split("\t") # remove terminal line break
			scaffold = lsplits[0]
			program = lsplits[1]
			feature = lsplits[2]
			feature_start = int(lsplits[3])
			feature_end = int(lsplits[4])
			strand = lsplits[6]
			attributes = lsplits[8]

			# check for normal format
			if len(lsplits) != 9:
				raise IOError("ERROR: expecting 9 columns, check line\n{}\n".format( line ))

			# check for correct position order
			# start should always be less than or equal to end
			if feature_end < feature_start: # this should never happen
				raise ValueError("ERROR: feature end {1} is before feature start {0}, stopping program, check GFF\n{2}".format( feature_start, feature_end,  line ) )

			# begin parsing attributes
			if attributes.find("ID")>-1 or attributes.find("Parent")>-1: # meaning GFF3
				is_GME_type = True
				attrd = dict([(field.strip().split("=")) for field in attributes.split(";") if field.count("=")])
			elif attributes.find("gene_id")>-1 or attributes.find("transcript_id")>-1:
				# some programs give GTF
				is_TE_type = True
				attrd = dict([(field.strip().split(" ",1)) for field in attributes.split(";") if field])
			feature_counter[feature] += 1

			# for transcript type features, "gene" features are ignored
			if feature in toplevel:
				if is_GME_type:
					transcript_id = attrd.get("ID")
					if program in programs_w_parent: # both of these should have GME type and have mutliple isoforms per gene
						gene_id = attrd.get("Parent", None)
						if gene_id is None: # try to assign Parent for top level, otherwise take tx ID
							gene_id = transcript_id
					else: # other or unspecified programs
						gene_id = transcript_id
				if is_TE_type:
					if program=="StringTie":
						transcript_id = attrd.get("transcript_id").replace('"','')
						gene_id = attrd.get("gene_id").replace('"','')
					else:
						transcript_id = attrd.get("transcript_id").replace('"','')
						gene_id = transcript_id

				# track scaffolds per gene
				# as sometimes due to mapping problems, genes can have tx on different scaffolds
				if scaffold not in genes_to_scaffolds.get(gene_id,[]):
					genes_to_scaffolds[gene_id].append(scaffold)

				# count that each transcript occurs only once
				# mapped ESTs, Trinity tx, may map more than once
				# only the first appearance in the file is counted
				seen_transcripts[transcript_id] += 1
				if seen_transcripts.get(transcript_id,0) > 1:
					error_count += 1
					if error_count < 30: # only print the first 100 warnings
						if not do_quiet_mode:
							sys.stderr.write("ERROR: already using transcript {}\n".format(transcript_id) )
					elif error_count == 30:
						if not do_quiet_mode:
							sys.stderr.write("ERROR: 30 errors printed, will not print further\n".format(transcript_id) )
					if ignore_errors:
						continue

				# otherwise add feature data to the dicts
				else:
					transcript_bounds = [feature_start,feature_end]
					transcript_bound_dict[transcript_id] = transcript_bounds
					transcript_to_lines[transcript_id].append(line)

				# add each transcript to the respective list in each gene
				gene_to_transcripts[gene_id].append(transcript_id)

				# if using BRAKER2 or gth,
				# add additional "gene" for the transcript itself
				if program in programs_w_parent:
					gene_to_transcripts[transcript_id].append(transcript_id)

			# after that, then read through subfeatures
			elif feature in sublevel:
				if is_GME_type:
					transcript_id = attrd.get("Parent")
				elif is_TE_type:
					transcript_id = attrd.get("transcript_id").replace('"','')

				# if parent feature has already been added
				if seen_transcripts.get(transcript_id)==1:
					# check if subfeature extends beyond parent feature bounds, which it never should
					if feature_start < transcript_bound_dict.get(transcript_id)[0] or feature_end > transcript_bound_dict.get(transcript_id)[1]:
						error_count += 1
						if ignore_errors:
							print("ERROR: {} feature {},{} extends beyond tx {} boundaries, continuing anyway".format( feature, feature_start,feature_end, transcript_id), file=sys.stderr)
						else:
							raise ValueError("ERROR: {} feature {},{} extends beyond tx {} boundaries, stopping program, check GFF".format( feature, feature_start,feature_end, transcript_id) )
					# otherwise add the boundaries to the dictionary for that tx
					exon_bounds_dict[transcript_id].extend([feature_start,feature_end])

					# track partial line strings, in case duplicate exons occur in the same tx
					line_part = "{} {} {}".format(feature, feature_start, feature_end)
					if line_part not in subfeature_lines.get(transcript_id,[]):
						subfeature_lines[transcript_id].append(line_part)
					else: # do not add duplicate lines
						error_count += 1
						sys.stderr.write("ERROR: identical bounds of subfeature {} within {}, {} {} , check GFF\n".format( feature, transcript_id, feature_start, feature_end ) )
						continue

					# keep track of lines for each tx, so add it
					if line not in transcript_to_lines.get(transcript_id,[]):
						transcript_to_lines[transcript_id].append(line)
					else: # do not add duplicate lines
						error_count += 1
						sys.stderr.write("ERROR: identical lines of subfeature {} of {} , check GFF\n".format( feature, transcript_id ) )

				# for any duplicate transcripts, likely from double mapping
				elif seen_transcripts.get(transcript_id) > 1:
					if ignore_errors: # skip this problem as well
						pass
				else: # meaning 0 so no higher level feature has been encountered yet
					sys.stderr.write("ERROR: {} sublevel feature {} does not have recognized higher level \n".format(transcript_id, feature) )
				
	# print list of features
	sys.stderr.write("# File contained:\n")
	for feature, count in sorted(feature_counter.items(), key=lambda x: x[1], reverse=True):
		sys.stderr.write("#{}\t{}\n".format( feature, count ) )
	sys.stderr.write("# Collected exons for {} transcripts\n".format( len(transcript_to_lines) ) )

	# check for duplicate transcripts, and raise error if not skipping
	duplicate_counts = [x for x in seen_transcripts.values() if x > 1]
	if duplicate_counts:
		if ignore_errors:
			sys.stderr.write("WARNING: encountered {} duplicated entries, ignorning them and continuing anyway\n".format( len(duplicate_counts) ) )
		else:
			raise KeyError("ERROR: encountered duplicated entries, stopping program, check GFF")

	# check for genes split onto 2 or more scaffolds
	# this should never happen
	split_scaffold_genes = 0
	for gene_id, scaf_list in genes_to_scaffolds.items():
		if len(scaf_list) > 1:
			split_scaffold_genes += 1
			error_count += 1
			if not do_quiet_mode:
				sys.stderr.write("ERROR: gene {} has transcripts on different scaffolds {}\n".format( gene_id, ";".join(scaf_list) ) )
	if split_scaffold_genes:
		if ignore_errors:
			sys.stderr.write("WARNING: encountered {} genes split onto multiple scaffolds, continuing anyway\n".format( split_scaffold_genes ) )
		else:
			raise KeyError("ERROR: encountered genes split onto multiple scaffolds, stopping program, check GFF")

	# check for transcripts that are missing exons
	# that is, where the transcript bounds extend past the bounds of the first and last features
	out_of_bounds_tx = 0
	no_exons_for_tx = 0
	for transcript_id, transcript_bounds in transcript_bound_dict.items():
		# if the transcript has no matching exons, a non-real value -1 is returned in a list
		# so that min and max do not make a TypeError
		exon_min_bound = min(exon_bounds_dict.get(transcript_id,[-1]))
		exon_max_bound = max(exon_bounds_dict.get(transcript_id,[-1]))
		if exon_min_bound == -1 and exon_max_bound == -1:
			no_exons_for_tx += 1
			if not do_quiet_mode:
				sys.stderr.write("ERROR: no exons reported for transcript {}\n".format( transcript_id ) )
			continue
		if transcript_bounds[0] < exon_min_bound or transcript_bounds[1] > exon_max_bound:
			out_of_bounds_tx += 1
			error_count += 1
			if not do_quiet_mode:
				sys.stderr.write("ERROR: transcript {} with bounds {},{} extends beyond features {},{}\n".format( transcript_id, transcript_bounds[0], transcript_bounds[1], exon_min_bound, exon_max_bound) )
	if out_of_bounds_tx:
		if ignore_errors:
			sys.stderr.write("ERROR: {} features extend past subfeature boundaries, continuing anyway\n".format( out_of_bounds_tx ) )
		else:
			raise ValueError("ERROR: {} features extend past subfeature boundaries, stopping program, check GFF".format( out_of_bounds_tx ) )
	if no_exons_for_tx:
		#TODO check if this causes problems and should be changed to raise
		sys.stderr.write("ERROR: {} transcript features contain no exons, continuing anyway\n".format( no_exons_for_tx ) )

	# report total damage
	sys.stderr.write("# {} errors detected in {}\n".format( error_count , gff_file ) )

	# return dictionary
	return transcript_to_lines, gene_to_transcripts


################################################################################


def DoL_empty(DoL):
	"""
	checks if a dictionary of lists (DoL) is empty.
	For example
	 { key: [], key2: []} is empty
	 { key: [1,2], key2: []} is not

	Returns True if empty, false if there is something in the DoL
	"""
	for key in DoL:
		if len(DoL[key]) > 0:
			return False
	return True


################################################################################


def read_annotation_instructions(instructionfile, main_annotations, alt_annotations, manual_annotations, genes_to_tx_dict, tx_to_file_dict, digits_to_show, gene_label, clean_text, replace_text, source_genome, use_subfeature_id, do_quiet_mode=False, is_dummy_mode=False, column_count=9):
	'''read instruction file, and print the modified GFF'''

	# declare some variables
	do_remove = False # whether to remove the original annotation, or just add to it
	do_add_gene = False # whether adding a new isoform or gene

	# set up counters
	instruction_counter = 0 # count number of instruction lines
	novelty_counter = 0 # count number of new or unannotated genes
	pseudogene_counter = 0 # count pseudogene flag

	# set up tracking dicts for summary stats
	# counter for each ID, all should be called once
	remove_counter = defaultdict(int)
	replacement_counter = defaultdict(int)
	removing_only_counter = defaultdict(int)
	add_isoform_counter = defaultdict(int)
	alt_id_counter = defaultdict(int)
	manual_id_counter = defaultdict(int)
	gene_name_counter = defaultdict(int)
	file_usage_counter = defaultdict(int)

	# count whatever reasons are given by the user
	fix_reason_counter = defaultdict(int)

	# trackers for genes to be printed
	scaffold = ""
	gene_counter = 0 # this is reset to zero for each chromosome
	isoform_counter = 0

	# check strand of genes
	gene_strand_counter = defaultdict(int)

	# tracker for exons used by two or more genes, which should not happen
	# typically this occurs from overlapping UTRs from genes predicted using non-stranded RNAseq
	# otherwise the same exon should by definition never be used by two genes
	# trans splicing leader exons should be excluded
	used_exon_tracker = defaultdict(list) # key is exon boundaries, value is list of genes using it, is reset for each scaffold
	double_exon_count = 0

	# track fate of each gene, to print later
	# string of 2 tab separated fields is appended for each line in the annotation instructions
	# new genes are given the tag "new"
	# and genes without any replacement are given the tag "removed"
	# old_gene  new_gene
	fate_list = []
	gene_fate_new = "NA"

	# track genes that have already been printed once
	printed_txs = {} # key is transcript ID, value is True
	total_genes = 0 # count of written gene tags

	toplevel = ["mRNA", "transcript", "rRNA"]
	sublevel = ["exon", "CDS", "five_prime_UTR", "three_prime_UTR"]

	# print gff3 header line, expected by some programs
	if not is_dummy_mode:
		print( "##gff-version 3", file=sys.stdout )

	sys.stderr.write("# Reading annotation table from {}\n".format(instructionfile) )
	for line in open(instructionfile,'rt'):
		# parse the file
		# each line is intended to be a gene
		# could removing an existing gene, replacing it with another gene
		# simply removing it because it is an error, or should be fused with another gene
		# or adding isoforms or gene information
		# thus, all isoforms on each line will end up as a single gene feature
		if line.strip() and line[0]!="#": # ignore empty lines and comments
			lsplits = line.split("\t")
			if lsplits[1]=="gene_id": # means header line, ignore
				continue

			instruction_counter += 1
			if len(lsplits) != column_count:
				sys.stderr.write("WARNING: Row {} has {} columns, should have {}, continuing anyway\n{}".format(instruction_counter, len(lsplits), column_count, line) )

			# set up variables for each column
			annot_scaffold = lsplits[0]
			gene_id = lsplits[1]
			remove_flag = lsplits[2].strip()
			alternate_ids = lsplits[3]
			manual_ids = lsplits[4]
			fix_reason = lsplits[5]
			gene_description = lsplits[6].strip()
			is_checked = lsplits[7].strip()
			user_notes = lsplits[8].strip()

			if not annot_scaffold.strip():
				sys.stderr.write("ERROR: Row {} missing scaffold, for {}\n{}".format(instruction_counter, gene_id, line) )
			if annot_scaffold != scaffold:
			# we have either just started, or transitioned to a new chromosome
			# so reset the gene counter for that scaffold
				scaffold = annot_scaffold
				used_exon_tracker = defaultdict(list) # reset exon tracker
				gene_counter = 0

			# sort out strand if needs to be fixed
			top_level_strand = None
			# if anything is given, then count frequency
			is_pseudogene = False
			if fix_reason:
				# separately count instructions for FORWARD and REVERSE
				if fix_reason.find("FORWARD")>-1:
					fix_reason_counter["FORWARD"] += 1
					top_level_strand = "+"
					fix_reason_noFR = fix_reason.replace("FORWARD","").strip()
					if fix_reason_noFR:
						fix_reason_counter[fix_reason_noFR] += 1
						fix_reason = fix_reason_noFR
				elif fix_reason.find("REVERSE")>-1:
					fix_reason_counter["REVERSE"] += 1
					top_level_strand = "-"
					fix_reason_noFR = fix_reason.replace("REVERSE","").strip()
					if fix_reason_noFR:
						fix_reason_counter[fix_reason_noFR] += 1
						fix_reason = fix_reason_noFR
				else:
					fix_reason_counter[fix_reason] += 1
				if fix_reason.find("PSEUDOGENE")>-1: # search for pseudogene tag
					is_pseudogene = True
					pseudogene_counter += 1
					continue

			# keys can be: main, alternate, manual
			source_keys = ["main", "alternate", "manual"]
			isoforms_in_this_gene = { key:[] for key in source_keys}
			do_remove = False
			do_add_gene = False
			for key in source_keys:
				# establish if removing or adding
				if key=="main":
					if remove_flag=="y" or remove_flag=="Y" or remove_flag=="1":
						do_remove = True
						continue
					else:
						tx_list = genes_to_tx_dict.get(gene_id,[])
						isoforms_in_this_gene[key].extend(tx_list)
				elif key=="alternate" and alternate_ids:
					do_add_gene = True
					for alt_id in alternate_ids.split(","):
						alt_id = alt_id.strip() # remove any spaces
						alt_id_counter[alt_id] += 1
						tx_list = genes_to_tx_dict.get(alt_id,[])
						if len(tx_list) == 0: # this should never be 0
							if not is_dummy_mode: # in dummy mode, genes_to_tx_dict is probably empty, so just ignore this error
								sys.stderr.write("ERROR: no isoforms found for alternate gene ID  {} , check annotation spreadsheet -i at line {}\n".format( alt_id, instruction_counter) )
						isoforms_in_this_gene[key].extend(tx_list)
				elif key=="manual" and manual_ids:
					do_add_gene = True
					for manual_id in manual_ids.split(","):
						manual_id = manual_id.strip() # remove any spaces
						manual_id_counter[manual_id] += 1
						tx_list = genes_to_tx_dict.get(manual_id,[])
						isoforms_in_this_gene[key].extend(tx_list)
				# if there are no tx from that key category, then remove the key from the next steps
				if len(isoforms_in_this_gene[key]) == 0:
					isoforms_in_this_gene.pop(key)

			# do all counting for diagnostic purproses here
			# if removing
			if do_remove:
				remove_counter[gene_id] += 1
				if gene_id:
					gene_fate_old = gene_id
				else: # meaning no main isoform given, but remove, this should not happen
					sys.stderr.write("WARNING: Row {} is flagged to remove, but has no gene ID, this is probably an error\n".format(instruction_counter) )
				if do_add_gene: # meaning replace gene with correct annotation
					replacement_counter[gene_id] += 1
				else: # meaning just removing a gene
					gene_fate_new = "removed"
					if fix_reason_noFR: # should list the reason if available
						gene_fate_new = fix_reason_noFR
					removing_only_counter[gene_id] += 1
			else: # meaning not removing
				if gene_id: # and not adding a new gene, modifying existing one
					if do_add_gene: # meaning add isoforms only
						add_isoform_counter[gene_id] += 1
					elif gene_description: # meaning just annotating a gene
						gene_name_counter[gene_id] += 1
					else:
						# this would be a line with only a gene and nothing else, so just print that gene normally
						pass
					gene_fate_old = gene_id
					# gene_fate_new will be added later for the isoform
				elif do_add_gene: # not removing, gaining a new unannotated gene
					novelty_counter += 1
					gene_fate_old = "new"
					fix_reason = "new"

			# in dummy mode, stop here for each row, meaning only count
			if is_dummy_mode:
				continue
			# if there are any isoforms to print, begin the process
			if not DoL_empty(isoforms_in_this_gene):
				# meaning there are some transcripts here

				# format scaffold name, would normally be like scaffold1, s001, chr1, c01, etc
				output_scaffold = annot_scaffold
				if clean_text: # if clean text is given, then replace
					output_scaffold = output_scaffold.replace(clean_text, replace_text)

				# get gene number, will display as g1, or g001 if -n is used
				# so will look like chr1.g1 or chr1.g0001
				gene_counter += 1
				gene_number = str(gene_counter).zfill(digits_to_show)
				this_gene = "{}.g{}".format(output_scaffold, gene_number)

				# add optional prefix label
				# so will look like Prefix.chr1.g1
				if gene_label: # prefix the gene label, if given
					this_gene = "{}.{}".format(gene_label, this_gene)

				# reset some things for each gene
				isoform_counter = 0
				print_buffer = ""
				gene_coords = [-1, -1]
				this_scaffold = ""
				used_exons = defaultdict(list) # key is exon boundaries, value is list of genes using it

				# start looping through transcripts
				# dictionary structure of  isoforms_in_this_gene  :
				# {'alternate': ['ML4965.24598.1'], 'main': []}
				for key, isoform_list in isoforms_in_this_gene.items(): # main, alternate, manual
					collect_isoforms = {} # key is transcript ID, value is list of annotation strings
					if key=="main":
						for gene_id in isoform_list:
							collect_isoforms[gene_id] = main_annotations[gene_id]
					elif key=="alternate":
						for gene_id in isoform_list:
							# check alt and main, for cases where only one isoform of the main is called
							try:
								collect_isoforms[gene_id] = alt_annotations[gene_id]
							except KeyError: # allow pulling sub-isoforms from main after
								sys.stderr.write("INFORMATION: cannot find alternate isoform {}, trying in main instead\n".format(gene_id) )
								collect_isoforms[gene_id] = main_annotations[gene_id]
					elif key=="manual":
						for gene_id in isoform_list:
							collect_isoforms[gene_id] = manual_annotations[gene_id]
					else: # should not happen, so flag and continue
						sys.stderr.write("WARNING: unknown key {} , continuing anyway\n".format(key) )

					# begin generating formatted isoforms of the gene
					# dictionary format of  collect_isoforms  :
					# {'ML50514a': 
					# ['ML5051\tEVM2.2\tmRNA\t43791\t44303\t.\t+\t.\tID=ML50514a;Parent=ML50514a;Name=ML50514a\n',
					# 'ML5051\tEVM2.2\texon\t43791\t44303\t.\t+\t.\tParent=ML50514a\n', 
					# 'ML5051\tEVM2.2\tCDS\t43791\t44303\t.\t+\t.\tParent=ML50514a\n']}
					for isoform, tx_lines in collect_isoforms.items():
						isoform_counter += 1 # starting from 0, iterate first
						this_isoform = "{}.i{}".format(this_gene,isoform_counter)
						exon_counter = 0
						for gffline in tx_lines:
							if line.strip():
								gffsplit = gffline.split("\t")
								if len(gffsplit) != 9: # should have been caught earlier, but check again
									raise IOError("ERROR: incorrect line format for {}\n{}\n".format(isoform, gffline))
								gff_scaffold = gffsplit[0]
								if gff_scaffold != annot_scaffold:
									sys.stderr.write("WARNING: scaffolds {0} {1} do not match for {2} , using scaffold from the GFF {0}\n".format(gff_scaffold, annot_scaffold, isoform) )
								if source_genome: # if a source version is given, replace it here
									gffsplit[1] = source_genome
								featuretype = gffsplit[2]
								featurestart = int(gffsplit[3])
								featureend = int(gffsplit[4])
								if gene_coords[0]==-1 or featurestart < gene_coords[0]:
									gene_coords[0] = featurestart
								if gene_coords[1]==-1 or featureend > gene_coords[1]:
									gene_coords[1] = featureend

								# determine if there is a strand
								featurestrand = gffsplit[6]
								if top_level_strand is None: # meaning was not manually specified by user
									if featurestrand not in ["+", "-"]: # is neither forward or reverse
										# report to user and do nothing else
										if not do_quiet_mode:
											sys.stderr.write("WARNING: no strand {} given for {} in {} , keeping with no strand\n".format( featurestrand, isoform, this_isoform ) )
								else: # always set to top level if flag is given
									gffsplit[6] = top_level_strand
									featurestrand = top_level_strand

								# process transcripts
								if featuretype in toplevel: # mRNA or transcript
									if featuretype!="rRNA": # assume rRNA are declared correctly from another program
									# force transcript feature type, as not all transcripts are mRNA, this should be set later
										gffsplit[2] = "transcript"

									if isoform in printed_txs:
										raise KeyError("ERROR: duplicate isoform {} , check annotation spreadsheet -i at line {}".format(isoform, instruction_counter) )
									else:
										printed_txs[isoform] = True
									attributes = "ID={0};Parent={1};source_ID={2}".format( this_isoform, this_gene, isoform )
									file_usage_counter[ tx_to_file_dict.get(isoform, "MISSING") ] += 1
									# note here if the strand if forced by user command with the isoforms affected
									# messages are not given for individual exon features
									if top_level_strand=="+":
										if not do_quiet_mode:
											sys.stderr.write("FORWARD flag found: forcing forward strand + for {} in {}\n".format( isoform, this_isoform ) )
									if top_level_strand=="-":
										if not do_quiet_mode:
											sys.stderr.write("REVERSE flag found: forcing reverse strand - for {} in {}\n".format( isoform, this_isoform ) )
								# process exons CDS et cetera
								elif featuretype in sublevel:
									exon_counter += 1

									if use_subfeature_id: # make ID like: chr01.g001.i1.exon from parent chr01.g001.i1
										attributes = "ID={0}.{1};Parent={0}".format( this_isoform, featuretype.lower() )
									else:
										attributes = "Parent={0}".format(this_isoform)

									exon_bounds = (featurestart,featureend)
									# check if exon is already used in another gene, which should never happen
									if exon_bounds in used_exon_tracker:
										double_exon_count += 1
										if not do_quiet_mode:
											sys.stderr.write("ERROR: same exon {} on {} used for multiple genes: {} {}\n".format(exon_bounds, gff_scaffold, isoform, " ".join(used_exon_tracker.get(exon_bounds)) ) )
									else:
										used_exons[exon_bounds].append(isoform)
								else: # things like [ "start_codon" , "stop_codon" , "intron" ]
									if not do_quiet_mode:
										sys.stderr.write("WARNING: unaccepted feature type {} for {} , keeping anyway\n".format( featuretype, isoform ) )
									attributes = "Parent={0}".format(this_isoform)
								gffsplit[8] = attributes
								# collect all lines for transcripts and exons into a string, to print later
								print_buffer += "{}\n".format( "\t".join(gffsplit) )
								
				# after collecting all isoforms, check that any were actually collected
				if isoform_counter==0:
					sys.stderr.write("WARNING: zero isoforms collected for {} on spreadsheet line {}\n".format( gene_id, instruction_counter ) )

				# create and print the gene feature
				total_genes += 1
				featuretype = "gene"
				# build up feature attributes from columns in the annotation sheet
				gene_attributes = "ID={0};Name={0}".format(this_gene)
				if gene_description:
					gene_attributes += ";description={}".format(gene_description)
				if is_checked:
					gene_attributes += ";is_checked={}".format(is_checked)
				if user_notes:
					gene_attributes += ";user_note={}".format(user_notes)
				if is_pseudogene: # this appears to be the standard syntax
					gene_attributes += ";pseudo=true"
					featuretype = "pseudogene"
				# make list of fields, then join and write
				gene_fields = [gff_scaffold, source_genome, featuretype, str(gene_coords[0]), str(gene_coords[1]), ".", featurestrand, ".", gene_attributes]
				gene_outline = "{}\n".format("\t".join(gene_fields) )
				sys.stdout.write(gene_outline)
				# print associated transcripts
				sys.stdout.write(print_buffer)

				# sort out any remaining operations

				# keep track of gene strand
				# featurestrand is given by the last feature of that gene
				# or declared with FORWARD or REVERSE
				# this should never be .
				gene_strand_counter[featurestrand] += 1

				# transfer exons to used_exon_tracker for this scaffold
				used_exon_tracker.update(used_exons)
				# get gene name for the correspondance table
				gene_fate_new = this_gene
			# make gene fate string
			gene_fate_str = "{}\t{}\t{}".format(gene_fate_old, gene_fate_new, fix_reason)
			fate_list.append(gene_fate_str)

	if not is_dummy_mode: # in dummy mode, just report summary counts
		# checks for equal usage of manual seqs and IDs
		sys.stderr.write("# Counted {} manual annotations, and {} sequences\n".format( len(manual_annotations), len(manual_id_counter) ) )
		for gene_id in manual_annotations.keys():
			if gene_id not in manual_id_counter:
				sys.stderr.write("WARNING: Curated seq  {}  not found in annotation instructions (-i)\n".format(gene_id) )
		for gene_id in manual_id_counter.keys():
			# must check both dicts, 
			# as manual_annotations is tx IDs with lines, and genes_to_tx_dict is gene IDs with tx
			# thus adding gene IDs in the annotation should not flag, 
			# even though there is no direct tx in manual_annotations
			if gene_id not in manual_annotations and gene_id not in genes_to_tx_dict:
				sys.stderr.write("ERROR: No manual annotations found (-m) for ID  {}\n".format(gene_id) )

		# check for alternate IDs that were not found, as above
		for gene_id in alt_id_counter.keys():
			if gene_id not in alt_annotations and gene_id not in genes_to_tx_dict:
				sys.stderr.write("ERROR: No alternate annotations found (-a) for ID  {}\n".format(gene_id) )

		# checks for single usage of each annotation
		for gene_id, use_count in alt_id_counter.items():
			if use_count > 1:
				sys.stderr.write("WARNING: Seq ID  {}  was used {} times, should be only once\n".format(gene_id, use_count) )
		for gene_id, use_count in manual_id_counter.items():
			if use_count > 1:
				sys.stderr.write("WARNING: Seq ID  {}  was used {} times, should be only once\n".format(gene_id, use_count) )

	# report basic counts
	sys.stderr.write("# Read {} annotations from {}\n".format(instruction_counter, instructionfile) )
	sys.stderr.write("# {} total annotations, of these:\n".format(instruction_counter) )
	sys.stderr.write("#    {} genes were only given a description\n".format( len(gene_name_counter) ) )
	sys.stderr.write("#    {} genes were new\n".format(novelty_counter) )
	sys.stderr.write("#    {} genes were given additional isoforms\n".format( len(add_isoform_counter) ) )
	sys.stderr.write("#    {} genes were flagged for removal\n".format( len(remove_counter) ) )
	sys.stderr.write("#       of these, {} were replaced with another gene\n".format( len(replacement_counter) ) )
	sys.stderr.write("#       {} were simply removed\n".format( len(removing_only_counter) ) )
	if pseudogene_counter:
		sys.stderr.write("#    {} genes were flagged as pseudogenes, and were ignored\n".format( pseudogene_counter ) )

	# report gene strand frequency
	for strand in sorted(gene_strand_counter.keys()):
		sys.stderr.write("# {} genes are {} strand\n".format( gene_strand_counter[strand], strand ) )

	# report double exon usage
	sys.stderr.write("# Counted {} exons used by 2 or more genes\n".format( double_exon_count ) )

	# list reasons for fixing
	sys.stderr.write("# User given reasons for correction included:\n")
	for fix_reason, count in sorted(fix_reason_counter.items(), key=lambda x: x[1], reverse=True):
		sys.stderr.write("#{}\t{}\n".format( fix_reason, count ) )

	# list sources of the transcripts from which main and alternate files
	sys.stderr.write("# Transcripts derived from files:\n")
	for used_file, count in sorted(file_usage_counter.items(), key=lambda x: x[1], reverse=True):
		sys.stderr.write("#{}\t{}\n".format( os.path.basename(used_file), count ) )

	# write final count of genes
	sys.stderr.write("# Wrote {} transcripts for {} genes\n".format( len(printed_txs), total_genes) )

	return fate_list

	# end read_annotation_instructions()

################################################################################

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--instructions', help="annotation instructions as tabular file")
	parser.add_argument('-a','--alternate-annotations', metavar="x.gff", nargs="*", help="all alternate annotations, space separated list of GFFs")
	parser.add_argument('-g','--gff', help="main annotation file, as GFF" )
	parser.add_argument('-m','--manual', help="manually annotated sequences, as fasta, can be .gz")
	parser.add_argument('-f','--correspondance-file', help="optional file name to print old names matched with new names")
	parser.add_argument('-l','--gene-label', help="prefix name for ID tag")
	parser.add_argument('-b','--subfeature-id', action="store_true", help="give ID to all subfeatures, instead of only Parent")
	parser.add_argument('-n','--number-digits', type=int, default=1, help="optional number of digits to show on gene IDs, so with -n 3 , .g9.i1 would show as g009.i1")
	parser.add_argument('--clean-scaffold-text', help="text to remove for scaffold name in gene ID", default="")
	parser.add_argument('--replace-scaffold-text', help="replacement for scaffold name in gene ID", default="")
	parser.add_argument('-s','--source-version', default="remake", help="name of source genome or version, for second column in GFF [remake]")
	parser.add_argument('-q','--quiet', action="store_true", help="do not print minor errors")
	parser.add_argument('-p','--pretend', action="store_true", help="do not print GFF, only overview stats")
	args = parser.parse_args(argv)

	# to measure progress
	starttime = time.time()

	genes_to_tx = {} # key is gene ID, value is list of tx IDs
	tx_to_file = {} # key is tx, value is filename, to count which tx are used from each file
	manual_annot_dict = {}
	if args.manual and os.path.exists(args.manual):
		sys.stderr.write("# Getting manual annotations from -m\n")
		manual_annot_dict, manual_g_to_tx = read_gff_to_dict(args.manual, args.quiet, False)
		genes_to_tx.update(manual_g_to_tx)
		tx_to_file.update( dict( [ (k, "manual") for k in manual_annot_dict.keys() ] ) )

	main_annotations = {}
	if args.gff:
		if os.path.exists(args.gff):
			sys.stderr.write("# Getting main gene annotation from -g\n")
			main_annotations, main_g_to_tx = read_gff_to_dict(args.gff, args.quiet, False)
			genes_to_tx.update(main_g_to_tx)
			tx_to_file.update( dict( [ (k, args.gff) for k in main_annotations.keys() ] ) )
		else:
			sys.stderr.write("ERROR: cannot find file {}, check -g\n".format( args.gff ) )
	else:
		sys.stderr.write("# INFORMATION: no GFF -g given, forcing summary mode -p\n" )
		args.pretend = True # if no GFF is given, force pretend mode anyway

	alt_annotations = {}
	alt_annot_file_list = args.alternate_annotations
	if alt_annot_file_list is None:
			sys.stderr.write("WARNING: no alternate annotation sets provided -a, continuing anyway\n" )
	else:
		sys.stderr.write("# Getting alternate annotation from -a\n")
		for annot_file in alt_annot_file_list:
			if annot_file and os.path.exists(annot_file):
				alt_annot_lines, alt_g_to_tx = read_gff_to_dict(annot_file, args.quiet, True)
				alt_annotations.update( alt_annot_lines )
				genes_to_tx.update( alt_g_to_tx )
				tx_to_file.update( dict( [ (k, annot_file) for k in alt_annot_lines.keys() ] ) )
			else:
				sys.stderr.write("ERROR: cannot find file {}, check -a\n".format( annot_file ) )

	# main part, parsing through the spreadsheet of annotation fixes
	if args.instructions:
		if os.path.exists(args.instructions):
			correspondance_list = read_annotation_instructions(args.instructions, main_annotations, alt_annotations, manual_annot_dict, genes_to_tx, tx_to_file, args.number_digits, args.gene_label, args.clean_scaffold_text, args.replace_scaffold_text, args.source_version, args.subfeature_id, args.quiet, args.pretend )
		else:
			sys.stderr.write("ERROR: cannot find file {}, check -i\n".format( args.instructions ) )
	else:
		sys.stderr.write("# INFORMATION: no annotation instruction table -i given, will only check GFFs\n" )

	if args.correspondance_file:
		sys.stderr.write("# Writing gene correspondance table to {}\n".format( args.correspondance_file ) )
		with open(args.correspondance_file,'w') as cf:
			cf.write( "\n".join(correspondance_list) )

	sys.stderr.write("# Processed completed in {:.1f} minutes\n".format((time.time() - starttime) / 60) )

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
