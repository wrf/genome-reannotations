# genome size and exon-intron-genic comparison

file1 = "~/git/genome-reannotations/working_size_comparison.tab"

dat1 = read.table(file1, header=TRUE, sep="\t", row.names=1)

total = rev(dat1[["Total"]])
Exon = rev(dat1[["Exon"]])
genic = rev(dat1[["Genic"]])
Intronic_gaps = rev(dat1[["Intron.gaps"]])
Gene_gaps = rev(dat1[["Intergenic.gaps"]])

Intron = genic - Exon - Intronic_gaps
Intergenic = total - genic - Gene_gaps

calcstats = cbind(Exon, Intron, Intronic_gaps, Intergenic, Gene_gaps)

labels = rev(gsub(" ","\n", rownames(dat1)))

statsandlabs = data.frame(calcstats, row.names=labels)

num_species = length(rownames(dat1))
groupstarts = match(c(1:9),dat1[,"Group"])

colvec = c("#00cc22", "#DA6077", "#997788", "#64b8f0", "#778899")

legendlabs = c("Exon","Intron","Intronic gaps","Intergenic","Intergenic gaps")



modelspecies = c("Drosophila melanogaster", "Caenorhabditis elegans", "Danio rerio", "Gallus gallus", "Canis lupus", "Mus musculus", "Homo sapiens")
bestgenomes = match(modelspecies,rownames(dat1))


# # # # # # #
# Intron Intergenic Plot for all species
# # # # # # #

pdf(file="~/git/genome-reannotations/large_intron_intergenic_plot.pdf", width=7, height=7)
xm = 2100
par(mar=c(4.5,4.5,2,2))
intronswg = dat1[["Genic"]] - dat1[["Exon"]]
intergenicwg = dat1[["Total"]] - dat1[["Genic"]]
# 16 circle for single cell euks
# 1 open circle for non-bilats
# 15 square for lophotrochozoans - capitella to lottia
# 15 square for ecdysozoans - limulus to beetles
# 17 triangle for deuterostomes - urchin to lamprey
# 22 open square black for verts - shark to birds
# 22 open square green for reptiles - turtles etc
# 6 open triangle for birds
# 5 diamond for mammals
cladelabs = c("Protists", "Non-bilaterians", "Lophotrochozoans", "Ecdysozoans", "Deuterostomes", "Vertebrates", "Non-avian reptiles", "Birds", "Mammals")
cladereps = unname(table(dat1[,"Group"]))
cladesymbols = c(19,21,15,15,17,22,22,6,5)
cladecols = c("#00cd60", "#0808ba", "#2cbabf", "#d8a722", "#c276ff", "#453600", "#599400", "#6da7d5", "#c8005d")
plot(intronswg, intergenicwg, xlim=c(0,xm), ylim=c(0,xm), xlab="Introns (Mb)", ylab="Intergenic (Mb)", frame.plot=FALSE, pch=rep(cladesymbols, cladereps), cex=1.55, col=rep(cladecols, cladereps), lwd=2.8, cex.axis=1.2, cex.lab=1.2)
rect(-10,-10,500,500,lty=3)
abline(0,1, lty=2) # 1:1 reference line
twoletternames = sapply(strsplit(row.names(dat1),' '),function(w){paste(substr(w,1,1),collapse='')})
threeletternames = gsub("([\\w])\\w+\\s([\\w][\\w]\\d?).*","\\1\\2",row.names(dat1),perl=TRUE)
smallcut = 480
bigonlynames = threeletternames[dat1[["Total"]]>smallcut]
bigonlyinge = intergenicwg[dat1[["Total"]]>smallcut]
bigonlyintr = intronswg[dat1[["Total"]]>smallcut]
text(bigonlyintr+xm/100, bigonlyinge, bigonlynames, pos=4)
legend(1280,1000,legend=cladelabs,pch=cladesymbols, col=cladecols, pt.lwd=2, cex=1.2)
dev.off()


# # # # # # #
# Log version Intron Intergenic Plot
# # # # # # #
pdf(file="~/git/genome-reannotations/large_intron_intergenic_log_plot.pdf", width=7, height=7)
par(mar=c(4.5,4.5,2,2))
plot(log10(intronswg),log10(intergenicwg), xlim=c(0.5,4), ylim=c(0.5,4), xlab="Introns (log Mb)", ylab="Intergenic (log Mb)", frame.plot=FALSE, pch=16, cex=3, col=c("#77777766"), lwd=2.8, cex.axis=1.4, cex.lab=1.4)
logiilm = lm(log10(intergenicwg) ~ log10(intronswg))
abline(0,1, lwd=4, col="#a02323aa") # 1:1 reference line
#abline(logiilm, lty=4, lwd=2)
text(2.5,3.75, bquote( "R"^2 == .(rs), list(rs=round(c(summary(logiilm)$r.squared),digits=4))), col="black", cex=2)

logiilm.best = lm(log10(intergenicwg[bestgenomes]) ~ log10(intronswg[bestgenomes]))
#abline(logiilm.best, lty=2, lwd=2, col="#a02323")
text(4,2.5, bquote( "R"^2 == .(rs), list(rs=round(c(summary(logiilm.best)$r.squared),digits=4))), col="#a02323", cex=2, pos=2)
ismodel = rep(0,length(intronswg))
ismodel[bestgenomes] <- 1
logiilm.multi = lm(log10(intergenicwg) ~ log10(intronswg) + ismodel)
xnamespacing = c(0.05,0.05,0.05,0.05,0.05,0.1,0.05)
ynamespacing = c(0,0,0,-0.1,-0.1,-0.05,0)

#weirdprotistindex = c(1,2,8,15,17,32)
#standoutspecies = c("Symbiodinium kawagutii","Symbiodinium minutum","Salpingoeca rosetta","Hoilungia hongkongensis","Exaiptasia pallida","Apis mellifera")
#weirdprotistindex = match(standoutspecies, rownames(dat1))

#text(log10(intronswg[weirdprotistindex])+0.04, log10(intergenicwg[weirdprotistindex]), threeletternames[weirdprotistindex], pos=4)
#text(log10(intronswg[bestgenomes])+xnamespacing, log10(intergenicwg[bestgenomes])+ynamespacing, threeletternames[bestgenomes], pos=4)
points(log10(intronswg[bestgenomes]), log10(intergenicwg[bestgenomes]), pch=23, cex=4, bg="#eed432cd")
text(4,0.5, paste("n =",length(intronswg)), cex=2, pos=2)
dev.off()




# # # # # # #
# Total size against all features
# # # # # # #

pdf(file="~/git/genome-reannotations/large_total_size_vs_feature_plot.pdf", width=8, height=7)
par(mar=c(4.4,4.5,2,2))
plot(dat1[["Total"]],dat1[["Exon"]],xlim=c(0,3550),ylim=c(0,2500),pch=16, col="#10ab2244", xlab="Total size (Mb)", ylab="Feature amount (Mb)", frame.plot=FALSE, cex=3, cex.axis=1.4, cex.lab=1.4)
ecor = cor(dat1[["Total"]],dat1[["Exon"]])
ecor2 = ecor^2
et = lm(dat1[["Exon"]] ~ dat1[["Total"]])
abline(et, lty=2, lwd=4, col="#146d20aa")
icor = cor(dat1[["Total"]],intronswg)
icor2 = icor^2
it = lm(intronswg ~ dat1[["Total"]])
abline(it, lty=2, lwd=4, col="#ac4556aa")
gcor = cor(dat1[["Total"]],intergenicwg)
gcor2 = gcor^2
gt = lm(intergenicwg ~ dat1[["Total"]])
abline(gt, lty=2, lwd=4, col="#1d5d85aa")
points(dat1[["Total"]], intronswg, pch=16, col="#ee607766", cex=3, lwd=2.4)
points(dat1[["Total"]], intergenicwg, pch=16, col="#66d0ff66", cex=3, lwd=2.4)
legend(100,2500,legend=c("Exon", "Intron", "Intergenic"), pch=16, col=c("#10ab22","#ee6077","#88d0ff"), cex=2)
#text(3250,500, bquote( "R"^2 == .(rs), list(rs=round(c(summary(et)$r.squared),digits=4))), col="#146d20", cex=1.2)
#text(3000,300, bquote( y == .(coef(et)[1]) + .(coef(et)[2]) * x ), col="#146d20", cex=1.2 )
#text(3250,1000, bquote( "R"^2 == .(rs), list(rs=round(c(summary(it)$r.squared),digits=4))), col="#ac4556", cex=1.2)
#text(3000,800, bquote( y == .(coef(it)[1]) + .(coef(it)[2]) * x ), col="#ac4556", cex=1.2 )
#text(3250,2500, bquote( "R"^2 == .(rs), list(rs=round(c(summary(gt)$r.squared),digits=4))), col="#1d5d85", cex=1.2)
#text(3000,2300, bquote( y == .(coef(gt)[1]) + .(coef(gt)[2]) * x ), col="#1d5d85", cex=1.2 )
text(500,1250,paste("n =",length(dat1[["Total"]])), cex=2)
dev.off()

pdf(file="~/git/genome-reannotations/large_log_size_vs_log_feature.pdf", width=8, height=7)
par(mar=c(4.4,4.5,2,2))
plot(log10(dat1[["Total"]]),log10(dat1[["Exon"]]),xlim=c(1,5),ylim=c(0.5,5),pch=16, col="#10ab2244", xlab="Total size (log Mb)", ylab="Feature amount (log Mb)", frame.plot=FALSE, cex=3, cex.axis=1.4, cex.lab=1.4)
logit = lm(log10(intronswg) ~ log10(dat1[["Total"]]))
abline(logit, lty=1, lwd=3, col="#ac455688")
loggt = lm(log10(intergenicwg) ~ log10(dat1[["Total"]]))
abline(loggt, lty=1, lwd=3, col="#1d5d8588")
points(log10(dat1[["Total"]]), log10(intronswg), pch=16, col="#ee607766", cex=3, lwd=2.4)
points(log10(dat1[["Total"]]), log10(intergenicwg), pch=16, col="#66d0ff66", cex=3, lwd=2.4)
dev.off()


# # # # # # #
# Exonic fraction
# # # # # # #

pdf(file="~/git/genome-reannotations/large_total_size_vs_exonic_plot.pdf", width=8, height=7)
par(mar=c(4.4,4.5,2,1.5))
exonfraction = dat1[["Exon"]]/dat1[["Total"]]
plot(dat1[["Total"]],exonfraction, type='p', xlim=c(0,3800), ylim=c(0,1), frame.plot=FALSE, axes=FALSE, xlab="Total genome size (Mb)", ylab="Exonic fraction", pch=16, cex=3, col=c("#10ab2244"), cex.axis=1.3, cex.lab=1.3)
axis(1, at=seq(0,3500,500), labels=seq(0,3500,500) )
axis(2, at=seq(0,1,0.1), labels=seq(0,1,0.1) )
sortedgsizes = sort(dat1[["Total"]], index.return=TRUE)
genomevals = sortedgsizes$x
exhypermodel = lm(exonfraction[sortedgsizes$ix] ~ I(1/sortedgsizes$x))
efraction.model = predict(exhypermodel,list(genomevals))
lines(genomevals, efraction.model, col="#146d20aa", lty=1, lwd=3)
points(dat1[["Total"]][bestgenomes],exonfraction[bestgenomes], pch=23, cex=4, bg="#eed432cd")
xexonoffsets = c(30,30,0,-100,0,-10,-10)
yexonoffsets = c(-0.01,0,0.03,0.03,0.05,0.04,0.04)
text(1000,0.48, bquote( R^2 == .(rs), list(rs=round(c(summary(exhypermodel)$r.squared),digits=4))), col="#146d20", cex=2)
#text(2500,0.30, bquote( hat(y)[i] == .(coef(exhypermodel)[1]) + .(coef(exhypermodel)[2]) / x[i] ), col="#a02323", cex=3 )
text(2500,0.48, "y = 1/x", col="#146d20", cex=3 )
text(500,0.91,paste("n =",length(dat1[["Exon"]])), cex=2)
dev.off()


pdf(file="~/git/genome-reannotations/large_total_size_vs_exonic_amount.pdf", width=8, height=7)
par(mar=c(4.4,4.5,2,1.5))
exonfraction = dat1[["Exon"]]/dat1[["Total"]]
plot(dat1[["Total"]],dat1[["Exon"]], type='p', xlim=c(0,3800), ylim=c(0,150), frame.plot=FALSE, axes=FALSE, xlab="Total genome size (Mb)", ylab="Total exons (Mb)", pch=16, cex=3, col=c("#10ab2244"), cex.axis=1.3, cex.lab=1.3)
axis(1, at=seq(0,3500,500), labels=seq(0,3500,500), cex.axis=1.3 )
axis(2, cex.axis=1.3)
sortedgsizes = sort(dat1[["Total"]], index.return=TRUE)
genomevals = sortedgsizes$x
exhypermodel = lm(exonfraction[sortedgsizes$ix] ~ I(1/sortedgsizes$x))
efraction.model = predict(exhypermodel,list(genomevals))
abline(et, lty=1, lwd=3, col="#146d20aa")
points(dat1[["Total"]][bestgenomes],dat1[["Exon"]][bestgenomes], pch=23, cex=4, bg="#eed432cd")
text(1000,120, bquote( "R"^2 == .(rs), list(rs=round(c(summary(et)$r.squared),digits=4))), col="#146d20", cex=2)
#text(2500,0.48, "y = 1/x", col="#146d20", cex=3 )
text(500,0.91,paste("n =",length(dat1[["Exon"]])), cex=2)
dev.off()


# # # # # # #
# Total size against average exon length and intron length
# # # # # # #
humanlist = c("Homo sapiens")
humanindex = match(humanlist,rownames(dat1))
introncolorid = rep(1,dim(dat1)[1])
introncolorid[humanindex] = 2
exoncolor = c("#10ab2244","#2133eeaa")
introncolor = c("#ee607766","#2133eeaa")

pdf(file="~/git/genome-reannotations/large_total_size_vs_intron_length_hsap_hl.pdf", width=8, height=7)
par(mar=c(4.4,4.5,2,2))
plot(dat1[["Total"]],dat1[["Avg.exon.length"]],xlim=c(0,3550),ylim=c(0,10000),pch=16, col=exoncolor[introncolorid], xlab="Total size (Mb)", ylab="Average length (bp)", frame.plot=FALSE, cex=3, cex.axis=1.3, cex.lab=1.4)
points(dat1[["Total"]], dat1[["Avg.intron.length"]], pch=16, col=introncolor[introncolorid], cex=3, lwd=2.4)
avgecor = cor(dat1[["Total"]],dat1[["Avg.exon.length"]])
avgecor2 = avgecor^2
avget = lm(dat1[["Avg.exon.length"]] ~ dat1[["Total"]])
abline(avget, lty=2, lwd=3, col="#146d20")
text(3000,1900, bquote( "R"^2 == .(rs), list(rs=round(c(summary(avget)$r.squared),digits=4))), col="#146d20", cex=2)
#text(2800,1100, bquote( y == .(coef(avget)[1]) + .(coef(avget)[2]) * x ), col="#146d20", cex=1.7 )
avgicor = cor(dat1[["Total"]],dat1[["Avg.intron.length"]])
avgicor2 = avgicor^2
avgit = lm(dat1[["Avg.intron.length"]] ~ dat1[["Total"]])
abline(avgit, lty=2, lwd=3, col="#ac4556")
text(3000,9300, bquote( "R"^2 == .(rs), list(rs=round(c(summary(avgit)$r.squared),digits=4))), col="#ac4556", cex=2)
#text(2800,8700, bquote( y == .(coef(avgit)[1]) + .(coef(avgit)[2]) * x ), col="#ac4556", cex=1.2 )
legend(100,10000,legend=c("Exon", "Intron"), pch=16, col=c("#10ab22","#ee6077"), cex=1.8, pt.cex=2 )
text(500,6000,paste("n =",length(dat1[["Total"]])), cex=2)
dev.off()

pdf(file="~/git/genome-reannotations/large_total_size_vs_intron_length_hsap_hl_no_labels.pdf", width=8, height=7)
par(mar=c(4.4,4.5,2,2))
plot(dat1[["Total"]],dat1[["Avg.exon.length"]],xlim=c(0,3550),ylim=c(0,8500),pch=16, col=exoncolor[introncolorid], xlab="Total genome size (Mb)", ylab="Average length (bp)", frame.plot=FALSE, cex=3, cex.axis=1.6, cex.lab=1.6)
points(dat1[["Total"]], dat1[["Avg.intron.length"]], pch=16, col=introncolor[introncolorid], cex=3, lwd=2.4)
text(3000,1300, "Average exon", col="#146d20", cex=2.3)
text(1800,7800, "Average\nintron", col="#ac4556", cex=2.3)
#text(500,8000,paste("n =",length(dat1[["Total"]])), cex=2)
dev.off()

pdf(file="~/git/genome-reannotations/large_log_size_vs_log_intron_length_hsap_hl.pdf", width=8, height=7)
par(mar=c(4.4,4.5,2,2))
plot(log10(dat1[["Total"]]),log10(dat1[["Avg.exon.length"]]),xlim=c(1,5),ylim=c(2,5),pch=16, col=exoncolor[introncolorid], xlab="Total size (Mb)", ylab="Average length (bp)", frame.plot=FALSE, cex=3, cex.axis=1.3, cex.lab=1.4)
avglogit = lm( log10(dat1[["Avg.intron.length"]]) ~ log10(dat1[["Total"]]) )
abline(avglogit, lty=1, lwd=3, col="#ac455688")
points(log10(dat1[["Total"]]), log10(dat1[["Avg.intron.length"]]), pch=16, col=introncolor[introncolorid], cex=3, lwd=2.4)
dev.off()

# # # # # # #
# Genic fraction
# # # # # # #

pdf(file="~/git/genome-reannotations/large_total_size_vs_genic_plot.pdf", width=8, height=7)
par(mar=c(4.4,4.5,2,1.5))
# gaps are already included since genic is measured by gff intervals
genicfraction = dat1[["Genic"]]/dat1[["Total"]]
plot(dat1[["Total"]],genicfraction, type='p', xlim=c(0,3700), ylim=c(0,1), frame.plot=FALSE, axes=FALSE, xlab="Total genome size (Mb)", ylab="Genic fraction", pch=16, cex=3, col="#c8842366", cex.axis=1.4, cex.lab=1.4)
axis(1, at=seq(0,3500,500), labels=seq(0,3500,500), cex.axis=1.4 )
axis(2, at=seq(0,1,0.1), labels=seq(0,1,0.1), cex.axis=1.4 )
abline(0.5,0, lty=2, lwd=4, col="#b618d299")
points(dat1[["Total"]][bestgenomes],genicfraction[bestgenomes], pch=23, cex=4, bg="#eed432cd")
sortedgsizes = sort(dat1[["Total"]], index.return=TRUE)
genomevals = sortedgsizes$x
besttotals = dat1[["Total"]][bestgenomes]
modelhyperlm = lm(genicfraction[bestgenomes] ~ I(1/besttotals) )
mhlm.predict = predict(modelhyperlm, data.frame(besttotals=genomevals) )
lines(genomevals, mhlm.predict, col="#a0232399", lty=1, lwd=3)
text(2000,0.90, bquote( "model hyp R"^2 == .(rs), list(rs=round(c(summary(modelhyperlm)$r.squared),digits=4))), col="#a02323", cex=2)
#text(750,0.92, pos=4, bquote( hat(y)[i] == .(coef(modelhyperlm)[1]) + .(coef(modelhyperlm)[2]) / x[i] ), col="#a02323", cex=1.2 )
text(3000,0.05,paste("n =",length(dat1[["Genic"]])), cex=2)
dev.off()



