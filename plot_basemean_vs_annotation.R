# examine relationship between manually annotated genes and expression level
# ie. do high or low expression levels increase likelihood of annotation errors with existing programs
# created by WRF 2023-04-04



deg_data_file = "~/git/Aphrocallistes_vastus_genome/differential_gene_expression/Data/avas_v1_29_annotations_prot_fr_blastp_k10.annotation_table_clusters.tsv.gz"
deg_data = read.table(deg_data_file, header=TRUE, sep="\t", quote='"')
head(deg_data)

gene_names = read.table("~/git/Aphrocallistes_vastus_genome/manual_annotation/alternate_tracks_for_final/Avas.v1.29_names.t.tab", sep="\t")
head(gene_names)
names_to_deg = match(gene_names$V2, gsub(".i1","",deg_data$SeqName) )
gene_is_new = gene_names$V3=="new"
gene_is_fused = gene_names$V3=="fused"
gene_is_split = gene_names$V3=="split"

gene_colors = rep("#00000011",length(deg_data$SeqName))
gene_colors[names_to_deg[gene_is_new]] = "#00bb00"
gene_colors[names_to_deg[gene_is_fused]] = "#bb0000"
gene_colors[names_to_deg[gene_is_split]] = "#0000bb"

plot(deg_data$Length, deg_data$baseMean, xlim=c(0,2000), ylim=c(0,3000), 
     pch=16, col=gene_colors)
ranked_basemean = sort(deg_data$baseMean, decreasing=TRUE, index.return=TRUE)
plot(ranked_basemean$x, type='h', ylim=c(0,500),
     col=gene_colors[ranked_basemean$ix] )

