# Genome reannotation for comparative analysis

Repo contains the data for paper: Francis, W.R. and G. Woerheide (2017) [Similar ratios of introns to intergenic sequence across animal genomes.](https://academic.oup.com/gbe/article-lookup/doi/10.1093/gbe/evx103) Genome Biology and Evolution 9 (6): 1582-1598.

The project originally was about what fraction of the genome is composed of genes. To do this, many genomes needed substantial reannotation in order to make meaningful comparisons, which is still true today.

Information about how I make, use, and view the new annotations in [JBrowse](https://jbrowse.org/) can be found in the [jbrowse folder](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/)

Raw data and scripts for the 2017 analysis can be found in the [paper_data folder](https://bitbucket.org/wrf/genome-reannotations/src/master/paper_data/)

Re-annotations, GFF files, proteins, and AUGUSTUS training parameters can be found in the [Downloads section](https://bitbucket.org/wrf/genome-reannotations/downloads)

Many more species have been added since 2017, so the original set is only presented for historical interest. If you have any interest in comparing or expanding on this work, just use the [current version](https://bitbucket.org/wrf/genome-reannotations/src/master/working_size_comparison.tab). Also see comparisons across phyla and including bacteria [on my github repo](https://github.com/wrf/genome-size).

![metazoan_reannotation_barplot.png](https://bitbucket.org/repo/bkdK8e/images/3585534770-metazoan_reannotation_barplot.png)

Some unpublished additional [analyses about intron length are here](https://github.com/wrf/misc-analyses/tree/master/intron_evolution).

All values of gene and exon data come from a [GFF/GTF](https://www.ensembl.org/info/website/upload/gff.html) file and a fasta file of the scaffolds/contigs. The values are summarized with the Python script `gtfstats.py` (see [here for latest version](https://bitbucket.org/wrf/sequences/src/master/gtfstats.py)). For cases where exons or genes are not explicitly defined (such as when CDS is given but not exon features), other options like `-c` or `-g` must be used. The file `gtfstats_command_list.txt` contains a list of commands used in the dataset from the paper.

## Regeneration of figures for current version ##
As new genome data become available, a current version of the table and figures can be remade. Updated versions of all paper figures can be regenerated with the R script.

`Rscript working_genome_size_evolution_all_figures_pdf_version.R`

This script assumes the path of this folder is `~/git/genome-reannotations/`, which should be changed in the script accordingly.

![large_total_size_vs_feature_plot.png](https://raw.githubusercontent.com/wrf/misc-analyses/master/figures_for_repo/large_total_size_vs_feature_plot.png)
![large_total_size_vs_intron_length.png](https://raw.githubusercontent.com/wrf/misc-analyses/master/figures_for_repo/large_total_size_vs_intron_length.png)
![large_intron_intergenic_log_plot.png](https://raw.githubusercontent.com/wrf/misc-analyses/master/figures_for_repo/large_intron_intergenic_log_plot.png)


## Some general notes on genome annotation ##
Annotation programs and methods have different sensitivities. RNAseq based methods are generally reliable, but can still be limited in low coverage regions/genes. De novo predictions will have trouble with more complex exon structures, and may 'revive' pseudogenes or inactive transposons. Therefore, manual annotation may be necessary for easily 1 in 4 genes.

![fig3_misassembly_schematic_v2.png](https://bitbucket.org/wrf/genome-reannotations/raw/071e0f246c57ba1ef95a73d1cfebad244f3afcc9/paper_figures/fig3_misassembly_schematic_v2.png)

In my experience, certain gene types tend to misassemble or be misannotated. 

* Very close genes on the same strand (possibly overlapping UTRs) end up being fused (see example below).
* Very long genes, especially those with repeated domains, e.g. dynein heavy chains, often end up fragmented in annotations.
* Tandem duplicates of genes can be frequently misannotated. Many times these are single exon, so reads often will not map correctly, or appear as though they bridge neighboring genes.

![alternate_start_vs_cascade_scheme_v1.png](https://raw.githubusercontent.com/wrf/misc-analyses/master/figures_for_repo/alternate_start_vs_cascade_scheme_v1.png)
![run-on_tx_scheme_v1.png](https://raw.githubusercontent.com/wrf/misc-analyses/master/figures_for_repo/run-on_tx_scheme_v1.png)

Thus, there are some good practices when reannotating a genome, or for something like a genome annotation jamboree.

* Start with a base set of genes, typically whatever program gives the best results, probably based on some kind of RNAseq gene predictions. Ideally, at least half are correct.
* Indicate which genes of that set should be kept, and what should be exchanged with an alternate program
* If manual annotations (like defining the nucleotides, splice sites, etc) are needed, keep those in a GFF file
* Keep the nucleotide transcripts, as those are needed to reannotate a new version of the assembly, and also for any submission to GenBank

## checking for completeness of long genes ##

`prealigner.py -s long_genes_for_annot_check.human.fasta -b blastp -R --RD -e 1e-20 ephydatia_muelleri/augustus_sysnames_prots.fasta`

## AUGUSTUS Training Parameters ##
* [Ciona intestinalis](https://bitbucket.org/wrf/genome-reannotations/downloads/Ciona_augustus_parameters.tar.gz): v2005 assembly trained with CAP3 assembled ESTs (1.1M)
* [Branchiostoma floridae](https://bitbucket.org/wrf/genome-reannotations/downloads/Brafl1_parameters.tar.gz): assembly v2 trained with CAP3 assembled ESTs (334k)
* [Lottia gigantea](https://bitbucket.org/wrf/genome-reannotations/downloads/lotgi1_parameters.tar.gz): Lotgi1 trained with CAP3 assembled ESTs (252k)
* [Helobdella robusta](https://bitbucket.org/wrf/genome-reannotations/downloads/helro_parameters.tar.gz): Helro1 trained with CAP3 assembled ESTs (101k)
* [Capitella teleta](https://bitbucket.org/wrf/genome-reannotations/downloads/Capitella_augustus_parameters.tar.gz): Capca1 trained with CAP3 assembled ESTs (138k)
* [Mnemiopsis leidyi](https://bitbucket.org/wrf/genome-reannotations/downloads/mleidyi_parameters.tar.gz): ML0.9 scaffolds trained with Trinity transcripts, processing [steps are here](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/mnemiopsis/)
* [Tethya wilhelma](https://bitbucket.org/wrf/genome-reannotations/downloads/twilhelma_parameters.tar.gz): trained with strand-specific Trinity transcripts
* [Monosiga brevicollis](https://bitbucket.org/wrf/genome-reannotations/downloads/mbrevicollis_parameters.tar): Monbr1 scaffolds trained with JGI EST set (29k ESTs)

To use training parameters to find specific genes with a protein profile, copy the parameters.tar file into the species folder in the AUGUSTUS directory and unzip it:

```
cp Brafl1_parameters.tar.gz ~/augustus-3.0.3/config/species/
tar -zxpf Brafl1_parameters.tar.gz
```

Generate protein profiles from a multiple sequence alignment with the script, note that any gaps remove that position from the profile:

`~/augustus-3.0.3/scripts/msa2prfl.pl piezo.aln > piezo.prfl`

When using AUGUSTUS, set the species accordingly:

`~/augustus-3.0.3/bin/augustus --species=Branchiostoma_floridae --proteinprofile=piezo.prfl --predictionStart=10000 --predictionEnd=130000 --protein=on --cds=on --strand=reverse --gff3=on --genemodel=exactlyone bf311.fa > bf311_piezo.gff`

## Re-annotating with AUGUSTUS for all contigs ##
Run AUGUSTUS with training parameters:

`~/augustus-3.0.3/bin/augustus --species=Ciona_intestinalis --strand=both --genemodel=atleastone --codingseq=on --protein=on --cds=on --sample=100 --keep_viterbi=true --alternatives-from-sampling=true --minexonintronprob=0.2 --minmeanexonintronprob=0.5 --maxtracks=2 --gff3=on --exonnames=on ../ciona050324.unmasked.fasta > ciona_augustus_maxtrack2.gff`

Extract proteins with [extract_augustus_features.py](https://bitbucket.org/wrf/sequences/src/master/extract_augustus_features.py):

`extract_augustus_features.py -p ciona_augustus_maxtrack2.prots.fasta -c ciona_augustus_maxtrack2.nucl.fasta ciona_augustus_maxtrack2.gff`


## Species and public data sources for the 2017 version:
* Octopus bimaculoides (using Obimaculoides-280.fa, 19 PE-SRA libs) [PRJNA285380](http://www.ncbi.nlm.nih.gov/Traces/study/?acc=SRP058882)
* Petromyzon marinus (using Petromyzon-marinus-7.0, 16 SE-SRA libs) [PRJNA50489](http://www.ncbi.nlm.nih.gov/bioproject/PRJNA50489), now outdated due to version by [Smith2018](http://dx.doi.org/10.1038/s41588-017-0036-1)
* Branchiostoma floridae (using Branchiostoma-floridae-v2.0, 1 PE-SRA lib) [SRR923751](http://www.ncbi.nlm.nih.gov/sra/SRR923751), now outdated due to the new [v3 assembly and annotation](https://bitbucket.org/wrf/genome-reannotations/downloads/amphioxus_7u5tJ.cm_numbers.gff.gz) by [Simakov 2020](http://dx.doi.org/10.1038/s41559-020-1156-z)
* Hydra magnipapillata (using [Hm105-Dovetail-Assembly-1.0.fasta](https://arusha.nhgri.nih.gov/hydra/download/?dl=asl), 2 PE-SRA libs) [SRR922615](https://www.ncbi.nlm.nih.gov/sra/SRX315376) and [SRR1024340](https://www.ncbi.nlm.nih.gov/sra/SRX329137)
* Trichoplax adherens (using [Triad1](http://genome.jgi.doe.gov/Triad1/Triad1.home.html) scaffolds, reannotated with AUGUSTUS, [processing steps are here](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/trichoplax/) ), reannotation was used for comparison with the new species of placozoan *Hoilungia hongkongnesis*, please cite paper by [Eitel 2018](http://dx.plos.org/10.1371/journal.pbio.2005359)
* Monosiga brevicollis (using [Monbr1](http://genome.jgi.doe.gov/Monbr1/Monbr1.home.html), AUGUSTUS)

## Transcriptome summary for the 2017 version:
* Octopus bimaculoides: [156493 StringTie transcripts](https://bitbucket.org/wrf/genome-reannotations/downloads/obi-rnaseq-stringtie.gtf.gz) vs 33638 genes
* Petromyzon marinus: [67617 StringTie transcripts](https://bitbucket.org/wrf/genome-reannotations/downloads/pmar_16lib_rna_stringtie.gtf.gz) vs 34895 transcripts
* Branchiostoma floridae: [68092 StringTie transcripts](https://bitbucket.org/wrf/genome-reannotations/downloads/bflo-rnaseq_stringtie.gtf.gz) vs 50817 genes
* Hydra magnipapillata: [52366 StringTie transcripts](https://bitbucket.org/wrf/genome-reannotations/downloads/hydra_stringtie.gtf.gz) vs 22980 genes
* Trichoplax adherens: [12633 new genes](https://bitbucket.org/wrf/genome-reannotations/downloads/Trichoplax_scaffolds_JGI_AUGUSTUS_maxtrack2.gff.gz) vs [11520 genes](https://bitbucket.org/wrf/genome-reannotations/src/master/jbrowse-tracks/trichoplax/Triad1_best_genes_w_parent.gff.gz)
* Monosiga brevicollis: [10864 new genes](https://bitbucket.org/wrf/genome-reannotations/downloads/Monbr1_augustus_v1.nocomment.gff.gz) vs 9196 genes


## Significance of cassette exons ##
Cassette exons show a preference for keeping phase (that is, multiples of 3 basepairs), but this is not the case for retained introns, based on data from [v1 of the *Tethya wilhelma* genome](https://bitbucket.org/molpalmuc/tethya_wilhelma-genome/src). This was also the argument of [Grau-Bove 2018](https://doi.org/10.1186/s13059-018-1499-9).

```
> twi_cas_exons = c(6904, 5179, 5331)
> binom.test(x= twi_cas_exons[1], n= sum(twi_cas_exons), p=0.33)

	Exact binomial test
data:  twi_cas_exons[1] and sum(twi_cas_exons)
number of successes = 6904, number of trials = 17414, p-value < 2.2e-16
alternative hypothesis: true probability of success is not equal to 0.33
95 percent confidence interval:
 0.3891864 0.4037734
sample estimates:
probability of success 
             0.3964626 

> twi_ret_intron = c(1202, 1204, 1159)
> binom.test(x= twi_ret_intron[1], n= sum(twi_ret_intron), p=0.33)

	Exact binomial test
data:  twi_ret_intron[1] and sum(twi_ret_intron)
number of successes = 1202, number of trials = 3565, p-value = 0.3638
alternative hypothesis: true probability of success is not equal to 0.33
95 percent confidence interval:
 0.3216472 0.3529541
sample estimates:
probability of success 
             0.3371669 

> twi_cas_expected = rep( sum(twi_cas_exons)/3, 3)
> twi_chisq = sum( (twi_cas_exons - twi_cas_expected) ^ 2 / twi_cas_expected )
> twi_chisq
[1] 314.2907
> qchisq(p=0.99, df=3)
[1] 11.34487
> pchisq(q=twi_chisq, df=3, lower.tail = FALSE)
[1] 8.028342e-68

```

